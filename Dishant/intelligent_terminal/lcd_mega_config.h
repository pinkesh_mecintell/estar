/*
 * lcd_mega_config.h
 *
 * Created: 6/15/2012 6:39:14 PM
 *  Author: Pinkesh
 */ 


#ifndef LCD_MEGA_CONFIG_H_
#define LCD_MEGA_CONFIG_H_

#include <avr/io.h>

/**
* port pin configuration definition
*/
#define lcd_mega_configPORT PORTC
#define lcd_mega_configPIN PINC
#define lcd_mega_configDDR DDRC

/**
* RS,RW,EN pin configuration
* \nFor example if lcd_mega_configRS = 1 then RS pin of LCD
*	is connected with PORTPIN-1 pin of MicroController.
*/
#define lcd_mega_configRS 0
#define lcd_mega_configRW 1
#define lcd_mega_configEN 2

/**
* defines which 4-pins of MicroController are used for data-pins of LCD.
* i.e upper or lower pins of MicroController are used with lcd pins.
* \n value = 0 then Lower pins are used
* \n value = 1 then Upper pins are used
* \n 
*\n		PIN of LCD		value=0		value=1
*\n		connected with
*\n			DB4			PIN-0			PIN-4
*\n			DB5			PIN-1			PIN-5
*\n			DB6			PIN-2			PIN-6
*\n			DB7			PIN-3			PIN-7	
*/
#define lcd_mega_configDATANibble 1



/************************************************************************//**
*  Function Inclusion Definition										
*  \n value = 0 then that function is not included in build process		
* 			if we don't need particular function then we can omit		
* 			it's function implementation by setting include_value = 0	
* 			and we can reduce the HEX file size                         
*  \n value = 1 then that function is included								
************************************************************************/
#define lcd_mega_includeString 1
#define lcd_mega_includeStringF 1
#define lcd_mega_includeShowvalue 1
/***********************************************************************/
#endif /* LCD_MEGA_CONFIG_H_ */