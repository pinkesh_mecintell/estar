/*
 * external_display.h
 *
 * Created: 16/12/13 6:56:14 PM
 *  Author: Pinkesh
 */ 


#ifndef EXTERNAL_DISPLAY_H_
#define EXTERNAL_DISPLAY_H_

#include <avr/io.h>
#include "utilities.h"


/********************************/
/**** Configuration *************/
/********************************/

#define EXTERNAL_DISPLAY_PORT	PORTF
#define EXTERNAL_DISPLAY_DDR	DDRF

#define EXTERNAL_DISPLAY_DATA_Pin		1
#define EXTERNAL_DISPLAY_CLOCK_Pin		2
#define EXTERNAL_DISPLAY_LATCH_Pin		0

#define EXTERNAL_DISPLAY_CLOCK_Delay	30	//< delay in microsecond
#define EXTERNAL_DISPLAY_LATCH_DELAY	30	//< delay in microsecond

/*********************************/


/***** Function Declaration ******/
/**
* Initializes Shift Register interfacing pins
* Data, Clock, Latch(total 3 pin)
*/
void external_display_Initialize();

/**
* Main High level function to write a single byte to
* Output shift register.
* The byte is serially transfered to shift register
* but it is not latched i.e. The byte is not available on
* output line Q0 to Q7 of the shift register IC.
* To set output pin as per new written byte, LatchPulse()
* function should be called.
*/
void external_display_WriteByte(unsigned char data);

/**
* Sends pulse on LATCH PIN of shift register
* so that new written byte is latched and
* its output appeared on output pins.
*/
void external_display_LatchPulse();





#endif /* EXTERNAL_DISPLAY_H_ */