/*
 * external_display.c
 *
 * Created: 16/12/13 6:56:33 PM
 *  Author: Pinkesh
 */ 

#include <util/delay.h>
#include "external_display.h"
#include "utilities.h"


/**** Static Function Declaration *****/
static void external_display_ClockPulse();
/**************************************/

/***** Function Definition *****/
void external_display_Initialize()
{
	//< data_pin=0
	SET(EXTERNAL_DISPLAY_DDR,EXTERNAL_DISPLAY_DATA_Pin);
	CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_DATA_Pin);
	
	//< clock_pin=0
	SET(EXTERNAL_DISPLAY_DDR,EXTERNAL_DISPLAY_CLOCK_Pin);
	CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_CLOCK_Pin);
	
	//< latch_pin=0
	SET(EXTERNAL_DISPLAY_DDR,EXTERNAL_DISPLAY_LATCH_Pin);
	CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_LATCH_Pin);
}

static void external_display_ClockPulse()
{
	SET(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_CLOCK_Pin);
	_delay_us(EXTERNAL_DISPLAY_CLOCK_Delay);
	CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_CLOCK_Pin);
	_delay_us(EXTERNAL_DISPLAY_CLOCK_Delay);
}

void external_display_LatchPulse()
{
	CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_LATCH_Pin);
	_delay_us(EXTERNAL_DISPLAY_LATCH_DELAY);
	SET(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_LATCH_Pin);
	_delay_us(EXTERNAL_DISPLAY_LATCH_DELAY);
}

void external_display_WriteByte(unsigned char data)
{
	unsigned char i;
	
	//< sends MSB bit first
	for(i=0;i<8;i++)
	{
		//< Output the data on Data line
		//< according to the Value of MSB
		if(data & 0b10000000)
		{
			//< MSB is 1 so output high
			SET(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_DATA_Pin);
		}
		else
		{
			//< MSB is 0 so output low
			CLR(EXTERNAL_DISPLAY_PORT,EXTERNAL_DISPLAY_DATA_Pin);
		}
		
		//< Pulse the Clock line
		external_display_ClockPulse();
		//< bring next bit at MSB position
		data=data<<1;
	}
}