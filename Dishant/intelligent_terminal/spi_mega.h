/*
 * spi_mega.h
 *
 * Created: 6/10/2012 11:11:21 AM
 *  Author: Pinkesh
 */ 


#ifndef SPI_MEGA_H_
#define SPI_MEGA_H_

#include <avr/io.h>
#include "spi_mega_config.h"

/**
* configuration of bits CPHA and CPOL as per spi_mega_configDataMode
*/
#define spi_mega_DataMode (spi_mega_configDataMode<<CPHA)

/**
* configuration of bits SPI2X, SPR1 and SPR0 as per spi_mega_config_SCKFrequency
*/
#if ( spi_mega_configSCKFrequency==0 || spi_mega_configSCKFrequency==2 || spi_mega_configSCKFrequency==4 )
#define spi_mega_SPI2X 1
#endif
#if ( spi_mega_configSCKFrequency==4 || spi_mega_configSCKFrequency==5 || spi_mega_configSCKFrequency==6 )
#define spi_mega_SPR1 1
#endif
#if ( spi_mega_configSCKFrequency==2 || spi_mega_configSCKFrequency==3 || spi_mega_configSCKFrequency==6 )
#define spi_mega_SPR0 1
#endif

#ifndef spi_mega_SPI2X
#define spi_mega_SPI2X 0
#endif
#ifndef spi_mega_SPR1
#define spi_mega_SPR1 0
#endif
#ifndef spi_mega_SPR0
#define spi_mega_SPR0 0
#endif

/************************************************************************/
/* Function Declaration                                                 */
/************************************************************************/

/**
* Initialize SPI module as per SPI_MEGA_CONFIG.h file
*/
void spi_mega_Initialize(void);

/**
* transmit one byte specified in argument
* \param spi_byte data(one byte) which should be transmitted
* \return         received byte in this packet(byte) communication
*/
uint8_t	spi_mega_TransmittData(uint8_t spi_byte);

#endif /* SPI_MEGA_H_ */