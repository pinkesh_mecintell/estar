/*
 * pendrive_dump.h
 *
 * Created: 19/01/14 12:44:39 PM
 *  Author: Pinkesh
 */ 


#ifndef PENDRIVE_DUMP_H_
#define PENDRIVE_DUMP_H_


//< 1=use pendrive function
//< 0=not use
#define USE_PENDRIVE	1


//< all the following pendrive cmd except CMD_PENDRIVE_CREATE_FILE_START
//< requires response from pendrive interface
#define CMD_PENDRIVE_COM_START			0xF0
#define CMD_PENDRIVE_COM_STOP			0xF1
#define CMD_PENDRIVE_CREATE_FILE_START	0xF2
#define CMD_PENDRIVE_CREATE_FILE_STOP	0xF3
#define CMD_PENDRIVE_WRITE_DATA_IN_FILE	0xF4

//< pendrive cmd responses
#define RESPONSE_PENDRIVE_OK		0
#define RESPONSE_PENDRIVE_ERROR		1
#define RESPONSE_PENDRIVE_NONE		2




#if (USE_PENDRIVE==1)

//< global variables declaration
extern volatile unsigned char pendrive_response;

//< function declaration
//< dump all the records in pendrive
void setting_pendrive_dumping();

#endif


#endif /* PENDRIVE_DUMP_H_ */