/*
 * main.c
 *
 * Created: 03/09/13 2:42:58 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "utilities.h"
#include "lcd_mega.h"
#include "ps2_kb.h"
#include "spi_mega.h"
#include "SD_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "ADS123x.h"
#include "buzzer.h"
#include "internal_display.h"
#include "function_keys.h"
#include "SD_addresses.h"
#include "printer.h"
#include "usart.h"
#include "password_strings.h"
#include "uart_vnc2.h"
#include "TM1624.h"
#include "pendrive_dump.h"

/*** Macro Definition ****/


/*************************/

/**** Global Variable ****/
//< sd card buffer
uint8_t SD_data_buf[SD_mega_BLOCK_LEN];
////////////////////

//////////////////
//< ps2 key board related
PS2_KEY ps2_key;
//////////////////////////

//////////////////////////
//< LCD related
char lcd_Line1_data[LCD_LEN+1];
char lcd_Line2_data[LCD_LEN+1];
unsigned char lcd_Line1_pointer;
unsigned char lcd_Line2_pointer;
unsigned char lcd_Line1_max;
unsigned char lcd_Line1_min;
unsigned char lcd_Line2_max;
unsigned char lcd_Line2_min;
unsigned char lcd_CursorLine;
unsigned char LCD_CODE[] = {'0','1','2','3','4','5','6','7','8','9',' ','-'};
///////////////////////////

///////////////////////////
//< RTC related
RTC_DATA RTC_data;
///////////////////////////

///////////////////////////
//< ADS1230 - ADC related
signed long int ADC_count=100;
signed long int ADC_buff[ADC_BUFF_LENGTH]={0};
unsigned char ADC_buff_count=0;
///////////////////////////

///////////////////////////
//< internal 7-segment display related
unsigned char SEG_CODE[] = {0x18,0xDE,0x34,0x94,0xD2,0x91,0x11,0xDC,0x10,0x90,0xFF,0xF7};
/////////////////////////////

///////////////////////////
//< external 7-segment display related
unsigned char EXT_SEG_CODE[] = {0xE7,0x21,0xCB,0x6B,0x2B,0x6E,0xEE,0x23,0xEF,0x6F,0x00,0x08};
/////////////////////////////

/////////////////////////////
//< weight related
signed long int weight=0;
unsigned char weight_str[6];//< 0=MSB,5=LSB
WEIGHT_CONSTANTS weight_const;
/////////////////////////////

//////////////////////
//< RST entry related
unsigned int RST_no;
//////////////////////

//////////////////////
//< Login System related
unsigned char login_type=LOGIN_NONE;
unsigned char operator_no=LOGIN_NONE;

//SD_mega_CommError commError;
unsigned char fun_ret;

extern volatile unsigned char printer_status;
/*************************/


/**** Function Declaration *****/
void init_main();
/*******************************/


/***** Main Function start *****/
int main()
{
	unsigned long int try;
	unsigned char str[10];
	unsigned char temp;
	
	_delay_ms(500);
	init_main();
	_delay_ms(200);
	
	
	//< TM1624 based display testing
/*	while(1)
	{
		TM1624_DisplayData();
		_delay_ms(1000);
	}
*/

	//< internal 7-segment display test - all segment ON

	internal_display_WriteByte(0);//< LSB
	internal_display_WriteByte(0);
	internal_display_WriteByte(0);
	internal_display_WriteByte(0);
	internal_display_WriteByte(0);
	internal_display_WriteByte(0);//< MSB
	internal_display_LatchPulse();
	while(1);
	
	

/***** MAIN CODE START ******/

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Hello"));
	_delay_ms(1000);
	lcd_mega_ClearDispaly();
	
	
	///////////////////////////////
	//< Displaying Startup Header//
	///////////////////////////////
	display_startup_header();


	//< ps2 keyboard related
	ps2_kb_start();
	_delay_ms(10);
	//< RTC related
	if(!ds1307_CheckRTC())
	{
		lcd_mega_ClearDispaly();
		lcd_mega_String("RTC not found");
		while(1);
	}
	
	//< RST NUMBER related
	RST_check();

	//< LCD related
	lcd_normal_display();
	

	while(1)
	{		
		//< displaying time/date
		if(ds1307_RefreshRTC_data())
		{
			lcd_mega_gotoxy2(0);
			//< date display
			lcd_mega_String(RTC_data.date);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.month);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.year);

			//< time display
			lcd_mega_gotoxy2(10);
			lcd_mega_String(RTC_data.hour);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.minute);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.second);
		}
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String("RTC Error");
			while(1);
		}		
		
		//< getting ADC_count from ADS123x
		ADC_getReading();
		
		//< get current weight from ADC
		ADC_getWeight();
		
		//< displaying weight on LCD
		lcd_mega_gotoxy1(8);
		lcd_mega_SendData(LCD_CODE[weight_str[0]]);//< MSB
		lcd_mega_SendData(LCD_CODE[weight_str[1]]);
		lcd_mega_SendData(LCD_CODE[weight_str[2]]);
		lcd_mega_SendData(LCD_CODE[weight_str[3]]);
		lcd_mega_SendData(LCD_CODE[weight_str[4]]);
		lcd_mega_SendData(LCD_CODE[weight_str[5]]);//< LSB
		lcd_mega_SendData(' ');
		lcd_mega_SendData('K');
		lcd_mega_SendData('G');
		

		//< displaying weight on internal 7-segment display
		internal_display_WriteByte(SEG_CODE[weight_str[5]]);//< LSB
		internal_display_WriteByte(SEG_CODE[weight_str[4]]);
		internal_display_WriteByte(SEG_CODE[weight_str[3]]);
		internal_display_WriteByte(SEG_CODE[weight_str[2]]);
		internal_display_WriteByte(SEG_CODE[weight_str[1]]);
		internal_display_WriteByte(SEG_CODE[weight_str[0]]);//< MSB
		internal_display_LatchPulse();

		
		
		//< displaying weight on external display
		external_display_WriteByte(SEG_CODE[weight_str[5]]);//< LSB
		external_display_WriteByte(SEG_CODE[weight_str[4]]);
		external_display_WriteByte(SEG_CODE[weight_str[3]]);
		external_display_WriteByte(SEG_CODE[weight_str[2]]);
		external_display_WriteByte(SEG_CODE[weight_str[1]]);
		external_display_WriteByte(SEG_CODE[weight_str[0]]);//< MSB
		external_display_LatchPulse();
		
		
		//< get key pressed
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			
			////////////////////////////////
			//< Special Key Processing START
			////////////////////////////////
			if(ps2_key.type==SPECIAL_KEY)
			{
				////////////////////////////////
				//< Function key processing START
				////////////////////////////////
				if(ps2_key.val==SPECIAL_KEY_F1)
				{
					function_F1();
					lcd_normal_display();
				}
				else if (ps2_key.val==SPECIAL_KEY_F2)
				{
					function_F2();
					lcd_normal_display();
				}
				else if (ps2_key.val==SPECIAL_KEY_F4)
				{
					function_F4();
					lcd_normal_display();
				}
				else if (ps2_key.val==SPECIAL_KEY_F5)
				{
					function_F5();
					lcd_normal_display();
				}
				else if (ps2_key.val==SPECIAL_KEY_F6)
				{
					function_F6();
					lcd_normal_display();
				}
				else if (ps2_key.val==SPECIAL_KEY_F7)
				{
					function_F7();
					lcd_normal_display();
				}
				////////////////////////////////
				//< ALT + X key processing START
				////////////////////////////////
				else if(ps2_key.val==SPECIAL_KEY_ALT)
				{
					try=0;
					while(!ps2_kb_available_key())
					{
						if((++try)>1000000)
						{
							break;
						}
					}
					if(ps2_kb_getkey(&ps2_key))
					{
						if(ps2_key.type==NORMAL_KEY)
						{
							switch(ps2_key.val)
							{
								//< ALT+S: For testing codes
								case 'S':
									lcd_normal_display();
									break;
								//< ALT+L: Engineer's Login
								case 'L':
									engg_login();
									lcd_normal_display();
									break;
								//< ALT+P: Engineer Change Password
								case 'P':
									engg_change_pass();
									lcd_normal_display();
									break;
								//< ALT+Z: AUTO ZERO-Hardware Calibration
								case 'Z':
									//lcd_mega_ClearDispaly();
									//lcd_mega_StringF(PSTR("-AUTO ZERO-"));
									//ADS123x_Calibrate();
									//_delay_ms(1000);
									setting_AutoZero();
									lcd_normal_display();
									break;
								//< ALT+T: time setting
								case 'T':
									setting_date_time();
									/*commError = SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
									if(commError.error_occure)
									{
										lcd_mega_ClearDispaly();
										lcd_mega_String("Error");
										lcd_mega_gotoxy2(0);
										if(commError.commandExecution)
										{
											lcd_mega_String("Command Execution");
										}
										else if(commError.dataRejected)
										{
											lcd_mega_String("Data Rejected");
										}
										else if(commError.timeout)
										{
											lcd_mega_String("Time Out");
										}
										_delay_ms(1000);
									}
									else
									{
										for(int j=0;j<512;j++)
										{
											usart0_WriteData(SD_data_buf[j]);
											usart0_WriteData(' ');
										}
									}
									*/
									lcd_normal_display();
									break;
								
								//< ALT+Q: to view ADC count
								case 'Q':
									setting_view_ADC_count();
									lcd_normal_display();
									break;
									
								//< ALT+R: to view RST number
								case 'R':
									view_rst_no();
									lcd_normal_display();
									break;
							}
						}
						else if(ps2_key.type==SPECIAL_KEY)
						{
							switch(ps2_key.val)
							{
								//< ALT+F1 : Weight Measurement Calibration
								case SPECIAL_KEY_F1:
									setting_weightCallibration();
									lcd_normal_display();
									break;
									
								//< ALT+F2
								case SPECIAL_KEY_F2:
									setting_custom_sleep();
//									setting_pendrive_dumping();
//									print_signature_line_type1();
// 									printer_status=STATUS_PRINTER_NONE;
// 									uart_vnc2_WriteData(CMD_PRINTER_STATUS);
// // 									temp = printer_get_status();
// // 									lcd_mega_ClearDispaly();
// // 									lcd_mega_Showvalue(temp);
// // 									_delay_ms(1000);
// 									_delay_ms(2000);
									lcd_normal_display();
									break;
									
								//< ALT+F6
								case SPECIAL_KEY_F6:
									setting_ALT_F6();
									lcd_normal_display();
									break;
									
								//< ALT+F7
								case SPECIAL_KEY_F7:
									setting_ALT_F7();
									lcd_normal_display();
									break;
									
								//< ALT+F8
								case SPECIAL_KEY_F8:
									setting_ALT_F8();
									lcd_normal_display();
									break;
									
								//<ALT+F4	
								case SPECIAL_KEY_F4:
									setting_ALT_F4();
									lcd_normal_display();
									break;
									
								//<ALT+F3
								case SPECIAL_KEY_F3:
									setting_ALT_F3();
									lcd_normal_display();
									break;
									
								//<ALT+F10
								case SPECIAL_KEY_F10:
									setting_ALT_F10();
									lcd_normal_display();
									break;
									
								//<ALT+F11
								case SPECIAL_KEY_F11:
									function_operator_logout();
									lcd_normal_display();
									break;
									
								//<ALT+F12
								case SPECIAL_KEY_F12:
									function_operator_login();
									lcd_normal_display();
									break;
							}
						}
					}
				}
				////////////////////////////////
				//< ALT + X key processing END
				////////////////////////////////
				
				//////////////////////////////////
				//< Function key Processing START
				//////////////////////////////////
				//< F1 - First Entry
				
				//////////////////////////////////
				//< Function key Processing END
				//////////////////////////////////
			}
			////////////////////////////////
			//< Special Key Processing END
			////////////////////////////////
		}

//		_delay_ms(100);
	}
	
	
	while(1);
	return 0;
}
/****** Main Function End *****/


/****** Function Defintion *****/
void init_main()
{
	//< lcd initialization
	SET(DDRC,3);//< lcd select pin
	LCD_ON;
	_delay_ms(10);	
	lcd_mega_Initialize();
	//< cursor off
//	lcd_mega_CursorOff();
	//< lcd buffer related
	lcd_Line1_data[LCD_LEN]='\0';
	lcd_Line2_data[LCD_LEN]='\0';

	//< ps2 keyboard initialize
	ps2_kb_Initialize();
	
	//< spi initialization
	spi_mega_Initialize();
	
	//< SD card initialization
	unsigned char i;
	SD_mega_CardInfo CardInfo;
	for (i=0; i<10; i++)
	{
		CardInfo = SD_mega_Initialize();
		if(CardInfo.success_init)
		{
			break;
		}
	}		
	if(CardInfo.success_init==0)
	{
		lcd_mega_StringF(PSTR("Memory Init Fail"));
		_delay_ms(2000);
		//while(1);
	}
	
	//< I2C bus initialization
	//< for RTC-DS1307
	//< pull up SCL/SDA pins
	SET(PORTD,0);//< SCL
	CLR(DDRD,0);
	SET(PORTD,1);//< SDA
	CLR(DDRD,1);
	//< i2c initialization
	I2C_InitMaster();
	//< RTC_data structure variable initializing
	RTC_data.second[2]='\0';
	RTC_data.minute[2]='\0';
	RTC_data.hour[2]='\0';
	RTC_data.date[2]='\0';
	RTC_data.month[2]='\0';
	RTC_data.year[2]='\0';
	
	//< printer related
	printer_init();
	
	//< ADS123x initialization
	ADS123x_Initialize();
	//< hardware callibration of ADC
	ADS123x_Calibrate();
	//< retrive WEIGHT_CONSTANTS
	WEIGHT_CONSTANTS_Read();
	
	//< buzzer initialization
	buzzer_init();

	//< shift register initialization for internal 7-segment display
//	internal_display_Initialize();
	//< TM1624 based internal display initialization
	TM1624_Initialize();
	
	//< external display initialization
	external_display_Initialize();
	
	//< computer data backup - serial port - usart0 initialize
	usart0_Initialize();
	
	//< uart VNC2 connection init - UART1
	uart_vnc2_Init();
	
	//< global interrupt enable
	sei();
}
