/*
 * lcd_mega_utilities.h
 *
 * Created: 6/16/2012 2:19:14 AM
 *  Author: Pinkesh
 */ 


#ifndef LCD_MEGA_UTILITIES_H_
#define LCD_MEGA_UTILITIES_H_

#include <util/delay.h>
#include "lcd_mega_config.h"


/**
* macro for toggling EN pin with necessary delay
* delay - 35us for upto 16MHZ
* delay - 40us for upto 20MHZ
*/
#define TOGGLE_EN	SET(lcd_mega_configPORT,lcd_mega_configEN); \
					_delay_us(50); \
					CLR(lcd_mega_configPORT,lcd_mega_configEN);
					//_delay_us(40);
/**
* lcd display command for to go at 1st position of LINE-1 / LINE2
*/
#define lcd_mega_LINE1 0x80
#define lcd_mega_LINE2 0xC0

/**
* macro function to go at any position on lcd display
*/
#define lcd_mega_gotoxy1(x) lcd_mega_SendCmd(lcd_mega_LINE1+x)
#define lcd_mega_gotoxy2(x) lcd_mega_SendCmd(lcd_mega_LINE2+x)

/**
* macro function for clear the display
*/
#define lcd_mega_ClearDispaly() lcd_mega_SendCmd(0x01); \
								_delay_us(765); \
								_delay_us(765);

#define lcd_mega_ReturnHome() lcd_mega_SendCmd(0x02); \
								_delay_us(767);		

/**
* Macro function for shifting display left by one position
*/
#define lcd_mega_MoveDisplayLEFT()	lcd_mega_SendCmd(0x18)

/**
* Macro function for shifting display right by one position
*/
#define lcd_mega_MoveDisplayRIGHT()	lcd_mega_SendCmd(0x1C)

/**
* Macro function for shifting cursor left by one position
*/
#define lcd_mega_MoveCursorLEFT()	lcd_mega_SendCmd(0x10)

/**
* Macro function for shifting cursor right by one position
*/
#define lcd_mega_MoveCursorRIGHT()	lcd_mega_SendCmd(0x14)

/**
* Macro function to ON the cursor without blinking
*/
#define lcd_mega_CursorOn() lcd_mega_SendCmd(0x0E)

/**
* Macro function to ON the cursor with blinking
*/
#define lcd_mega_CursorWithBlinking() lcd_mega_SendCmd(0x0F)

/**
* Macro function to off cursor and blinking
*/
#define lcd_mega_CursorOff() lcd_mega_SendCmd(0x0C)
#endif /* LCD_MEGA_UTILITIES_H_ */