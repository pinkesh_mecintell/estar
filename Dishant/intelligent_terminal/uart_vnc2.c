/*
 * uart_vnc2.c
 *
 * Created: 03/12/13 2:22:01 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "lcd_mega.h"
#include "utilities.h"
#include "usart.h"
#include "buzzer.h"
#include "uart_vnc2.h"
#include "ps2_kb.h"
#include "printer.h"
#include "pendrive_dump.h"

//< extern variable

//< global Variables
//volatile char uart_vnc2_RxBuff[UART_VNC2_RECEIVE_BUFF_SIZE]; //< USART Receive Buffer

/////////////////< local variables ////////////////////////
//volatile static int8_t UQFront;
//volatile static int8_t UQEnd;

//< usb keyboard related
#if (KB_TYPE==KB_USB)
	//< LOCAL macros
	#define USB_KEY_TYPE	0
	#define USB_KEY_VAL		1
	//< function declaration (used by vnc2_uart receive interrupt)
	extern void ps2_kb_put_kbbuff(unsigned char c);
	volatile static unsigned char usb_kb_state=USB_KEY_TYPE;
	volatile static unsigned char usb_kb_key_type;
#endif

//< printer related
#if (PRINTER_BUS_TYPE==PRINTER_USB)
	volatile unsigned char printer_status;
#endif

volatile static unsigned char running=RUNNING_NONE;
///////////////////////////////////////////////////////////


/*** FUNTION IMPLEMENTATION ***/
void uart_vnc2_Init()
{
	//< Setup queue
//	UQFront=-1;
//	UQEnd=-1;

	//< Set Baud rate - 115200 bps
	//< F_CPU = 14.7456 MHz
	UBRR1L=7;

	//< Set Frame Format
	//< No Parity-1 StopBit-char size 8-bit
	UCSR1C=(1<<UCSZ11)|(1<<UCSZ10);

	//< Enable The recevier and transmitter
	//< Enable Interrupt - RXCIE - Receive complete
	UCSR1B=(1<<TXEN1)|(1<<RXEN1)|(1<<RXCIE1);
}

//< The USART Recieve ISR
ISR(USART1_RX_vect)
{
	//< Read the data
	char data=UDR1;
	
	////////////// USB Key Board Processing ////////////
#if (KB_TYPE==KB_USB)
	if(data==USB_KEY_BOARD_START_CHAR && running==RUNNING_NONE)
	{
		//< usb key board communication start
		running = RUNNING_USB_KEY_BOARD;
		usb_kb_state = USB_KEY_TYPE;
		return;
	}
	//< usb key board processing
	if(running==RUNNING_USB_KEY_BOARD)
	{
		if(usb_kb_state==USB_KEY_TYPE)
		{
			//< save usb key type temporary
			usb_kb_key_type = data;
			usb_kb_state = USB_KEY_VAL;
		}
		else
		{
			//< save usb key press into buffer
			ps2_kb_put_kbbuff(usb_kb_key_type);
			ps2_kb_put_kbbuff(data);
			//< buzzer related
			BUZZER_ON;
			BUZZER_TIMER_ON;
			running = RUNNING_NONE;
		}
		return;
	}
#endif
	////////////////////////////////////////////////////
	
	//////// USB printer related processing ////////////
#if (PRINTER_BUS_TYPE==PRINTER_USB)
	if(data==PRINTER_START_CHAR && running==RUNNING_NONE)
	{
		//< usb printer communication start
		running = RUNNING_PRINTER;
		printer_status=STATUS_PRINTER_NONE;
		return;
	}
	//< printer processing
	if(running==RUNNING_PRINTER)
	{
		printer_status = data;
		running = RUNNING_NONE;
		return;
	}
#endif
	////////////////////////////////////////////////////
	
	
	/////// Pendrive related processing ////////////////
#if (USE_PENDRIVE==1)
	if(data==PENDRIVE_START_CHAR && running==RUNNING_NONE)
	{
		//< pendrive communication start
		running = RUNNING_PENDRIVE;
		pendrive_response=RESPONSE_PENDRIVE_NONE;
		return;
	}
	//< pendrive processing
	if(running==RUNNING_PENDRIVE)
	{
		pendrive_response = data;
		running = RUNNING_NONE;
		return;
	}
#endif	
	////////////////////////////////////////////////////


/*	
	//< Now add it to queue
	if(((UQEnd==UART_VNC2_RECEIVE_BUFF_SIZE-1) && UQFront==0) || ((UQEnd+1)==UQFront))
	{
		//< Queue Full
		UQFront++;
		if(UQFront==UART_VNC2_RECEIVE_BUFF_SIZE)
		{
			UQFront=0;
		}
	}
	
	if(UQEnd==UART_VNC2_RECEIVE_BUFF_SIZE-1)
	{
		UQEnd=0;
	}
	else
	{
		UQEnd++;
	}
	uart_vnc2_RxBuff[UQEnd]=data;

	if(UQFront==-1)
	{
		UQFront=0;
	}
*/
}

void uart_vnc2_WriteData(char data)
{
	//< Wait For Transmitter to become ready
	while(!(UCSR1A & (1<<UDRE1)));

	//Now write
	UDR1=data;
}

void uart_vnc2_WriteString(char *str)
{
	while(*str != '\0')
	{
		uart_vnc2_WriteData(*str);
		str++;
	}
}

void uart_vnc2_WriteStringF(const char *str)
{
	while(pgm_read_byte(&(*str)))
	{
		uart_vnc2_WriteData(pgm_read_byte(&(*str++)));
	}
}

/*
uint8_t uart_vnc2_DataAvailable()
{
	if(UQFront==-1)
	{
		return 0;
	}
	
	if(UQFront<UQEnd)
	{
		return(UQEnd-UQFront+1);
	}
	else if(UQFront>UQEnd)
	{
		return (UART_VNC2_RECEIVE_BUFF_SIZE-UQFront+UQEnd+1);
	}
	else
	{
		return 1;
	}
}

char uart_vnc2_ReadData()
{
	char data;
	
	//< Check if q is empty
	if(UQFront==-1)
	{
		return 0;
	}
	
	data=uart_vnc2_RxBuff[UQFront];
	
	if(UQFront==UQEnd)
	{
		//< If single data is left
		//< So queue is empty
		UQFront=UQEnd=-1;
	}
	else
	{
		UQFront++;

		if(UQFront==UART_VNC2_RECEIVE_BUFF_SIZE)
		{
			UQFront=0;
		}
	}

	return data;
}

void uart_vnc2_ReadBuffer(void *buff,uint16_t len)
{
	uint16_t i;
	for(i=0;i<len;i++)
	{
		((char*)buff)[i]=uart_vnc2_ReadData();
	}
}

void uart_vnc2_FlushBuffer()
{
	while(uart_vnc2_DataAvailable()>0)
	{
		uart_vnc2_ReadData();
	}
}

*/