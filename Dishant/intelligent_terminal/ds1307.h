/*
 * ds1307.h
 *
 * Created: 11/08/13 8:28:45 PM
 *  Author: Pinkesh
 */ 


#ifndef DS1307_H_
#define DS1307_H_

#define DS1307_CHIP_ADDRESS 0xD0

typedef struct  
{
	unsigned char second[3];
	unsigned char minute[3];
	unsigned char hour[3];
	unsigned char date[3];
	unsigned char month[3];
	unsigned char year[3];
}RTC_DATA;

unsigned char ds1307_ReadRegister(unsigned char address,unsigned char *data);
unsigned char ds1307_WriteRegister(unsigned char address,unsigned char data);
unsigned char ds1307_RefreshRTC_data();
unsigned char ds1307_CheckRTC();

#endif /* DS1307_H_ */