/*
 * SD_addresses.h
 *
 * Created: 05/10/13 12:28:16 PM
 *  Author: Pinkesh
 */ 

#ifndef SD_ADDRESSES_H_
#define SD_ADDRESSES_H_

#define ADD_RST_NO					3100000 //< address
#define RST_NO_CURRENT_ADD_OFFSET	0	//< (reset value = 0)
#define RST_NO_ADD_MAGIC_BYTE		511	//< (reset value = 0xAA)
#define RST_NO						0	//< (4)(MSB ... LSB) (reset value = 1)
#define RST_NO_WRITE_TIMES			4	//< (2)(MSB ... LSB) (reset value = 0)
#define ADD_RST_NO_COPY_OFFSET		200

/*
//< comment has (length(in byte))/(default value)/(all values) format
#define RST_ENTRY_TYPE	0	//< (1)/(non-zero)/(0/nonzero) 0=Occupied, non-zero=not occupied
#define RST_VEHICLE_NO	1	//< (12)/()/(printable characters)
#define RST_SUPPLIER	13	//< (12)/()/(printable characters)
#define RST_MATERIAL	25	//< (12)/()/(printable characters)
#define RST_ADDRESS		37	//< (12)/()/(printable characters)
#define RST_SOURCE		49	//< (12)/()/(printable characters)
#define RST_CHARGE1_IS_PRESENT		61	//< (1)/(nonzero)/(0/nonzero) 0=present,nonzero=not present
#define RST_CHARGE1					62	//< (5)/()/(xxxxx)
#define RST_CHARGE2_IS_PRESENT		67	//< (1)/(nonzero)/(0/nonzero)
#define RST_CHARGE2					68	//< (5)/()/(xxxxx)
#define RST_TOTALCHARGE_IS_PRESENT	73	//< (1)/(nonzero)/(0/nonzero)
#define RST_TOTALCHARGE				74	//< (5)/()/(xxxxx)
#define RST_FIRSTENTRYTYPE			79	//< (1)/()/('G','T')
#define RST_GROSSWEIGHT_IS_PRESENT	80	//< (1)/(nonzero)/(0/nonzero)
#define RST_GROSSWEIGHT				81	//< (6)/()/(xxxxxx) LSB first-MSB last
#define RST_TAREWEIGHT_IS_PRESENT	87	//< (1)/(nonzero)/(0/nonzero)
#define RST_TAREWEIGHT				88	//< (6)/()/(xxxxxx)
#define RST_NETWEIGHT_IS_PRESENT	94	//< (1)/(nonzero)/(0/nonzero)
#define RST_NETWEIGHT				95	//< (6)/()/(xxxxxx)
#define RST_GROSS_DATE				101	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_GROSS_TIME				107	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_TARE_DATE				111	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_TARE_TIME				117	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_BOTHENTRY_COMPLETED		121 //< (1)/(non-zero)/(0/nonzero)
/////////////////////////////////////
*/

#define CODE_ENTRY_START	3000005//< ADD
#define CODE1_ENTRY_DATA	0 //<(25)
#define CODE1_ENTRY_TARE	25//<(4)(MSB...LSB)
#define CODE2_ENTRY_DATA	29//<(25)
#define CODE2_ENTRY_CHARGES	54//<(4)
#define CODE2_ENTRY_MOB_NO	58//<(12)
#define CODE3_ENTRY_DATA	70//<(25)
#define CODE3_ENTRY_CHARGES	95//<(4)
#define CODE3_ENTRY_MOB_NO	99//<(12)
#define CODE4_ENTRY_DATA	111//<(25)
#define CODE4_ENTRY_CHARGES	136//<(4)
#define CODE4_ENTRY_MOB_NO	140//<(12)
///////////////////////////////////////

//in each 512 block we store Data Digits(3byte) and there storage add (1 byte)
#define CODE_ENTRY_TABLE_START	3000000 //< BLOCK ADD
#define CODE1_ENTRY_TABLE		0		//< BLOCK ADD offset
#define CODE2_ENTRY_TABLE		1		//< BLOCK ADD offset
#define CODE3_ENTRY_TABLE		2		//< BLOCK ADD offset
#define CODE4_ENTRY_TABLE		3		//< BLOCK ADD offset
#define CODE_ENTRY_POINTER		509		//< (1)/(0)/(0-32) points to location of next empty code data storage space 
#define CODE_TABLE_POINTER		510		//< (1)/(0)/(0-(4*32)+32) unsigned char
///////////////////////////////////////

//RST entry save data locations
//< comment has (length(in byte))/(default value)/(all values) format
#define RST_ENTRY_IS_OCCUPIED		0	//< (1)/(FF)/('Y'/FF) Y=Occupied, non-'Y'=not occupied
#define RST_ENTRY1_IS_PRESENT		1	//< (1)/('N')/(Y/N)
#define RST_ENTRY1              	2	//< (25)/()/(printable characters)
#define RST_ENTRY2_IS_PRESENT		27	//< (1)/('N')/(Y/N)
#define RST_ENTRY2 					28	//< (25)/()/(printable characters)
#define RST_ENTRY3_IS_PRESENT		53	//< (1)/('N')/(Y/N)
#define RST_ENTRY3					54	//< (25)/()/(printable characters)
#define RST_ENTRY4_IS_PRESENT		79	//< (1)/('N')/(Y/N)
#define RST_ENTRY4					80	//< (25)/()/(printable characters)
#define RST_ENTRY5_IS_PRESENT		105	//< (1)/('N')/(Y/N)
#define RST_ENTRY5					106	//< (25)/()/(printable characters)
#define RST_ENTRY6_IS_PRESENT		131	//< (1)/('N')/(Y/N)
#define RST_ENTRY6					132	//< (25)/()/(printable characters)
#define RST_ENTRY7_IS_PRESENT		157	//< (1)/('N')/(Y/N)
#define RST_ENTRY7					158	//< (25)/()/(printable characters)
#define RST_ENTRY8_IS_PRESENT		183	//< (1)/('N')/(Y/N)
#define RST_ENTRY8					184	//< (25)/()/(printable characters)
#define RST_ENTRY9_IS_PRESENT		209	//< (1)/('N')/(Y/N)
#define RST_ENTRY9					210	//< (25)/()/(printable characters)
#define RST_ENTRY10_IS_PRESENT		235	//< (1)/('N')/(Y/N)
#define RST_ENTRY10					236	//< (25)/()/(printable characters)
#define RST_CHARGE1_IS_PRESENT		261	//< (1)/('N')/(Y/N)
#define RST_CHARGE1					262	//< (4)/()/(xxxxx)
#define RST_CHARGE2_IS_PRESENT		266	//< (1)/('N')/(Y/N)
#define RST_CHARGE2					267	//< (4)/()/(xxxxx)
#define RST_MOBILE_NO_IS_PRESENT	271  //< (1)/('N')/(Y/N)
#define RST_MOBILE_NO				272  //< (12)/()/(Printable numbers)
#define RST_TOTALCHARGE_IS_PRESENT	284	//< (1)/('N')/(Y/N)
#define RST_TOTAL_CHARGE			285	//< (4)/()/(xxxxx)
#define RST_FIRST_ENTRY_TYPE		289	//< (1)/('G')/('G','T')
#define RST_FIRSTWEIGHT_IS_PRESENT	290	//< (1)/('N')/(Y/N)
#define RST_FIRSTWEIGHT				291	//< (4)/()/(xxxxxx) LSB first-MSB last
#define RST_SECONDWEIGHT_IS_PRESENT	295	//< (1)/('N')/(Y/N)
#define RST_SECONDWEIGHT			296	//< (4)/()/(xxxxxx)
#define RST_THIRDWEIGHT_IS_PRESENT	300	//< (1)/('N')/(Y/N)
#define RST_THIRDWEIGHT			    301	//< (4)/()/(xxxxxx)
#define RST_NETWEIGHT_IS_PRESENT	305	//< (1)/(N)/(Y/N)
#define RST_NETWEIGHT				306	//< (4)/()/(xxxxxx)
#define RST_FIRST_DATE				310	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_FIRST_TIME				316	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_SECOND_DATE				320	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_SECOND_TIME				326	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_THIRD_DATE				330	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_THIRD_TIME				336	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_FIRST_OPRATOR			340 //< (16)/()/(printable characters)
#define RST_SECOND_OPRATOR			356 //< (16)/()/(printable characters)
#define RST_THIRD_OPRATOR			372 //< (16)/()/(printable characters)
#define RST_BOTH_ENTRY_COMPLETED	388 //< (1)/('N')/(Y/N)
#define RST_ENTRY_OFFSET			26 //Entry store offset
/////////////////////////////////////

#endif /* SD_ADDRESSES_H_ */