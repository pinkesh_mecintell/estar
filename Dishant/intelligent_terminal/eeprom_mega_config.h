/*
 * eeprom_mega_config.h
 *
 * Created: 6/12/2012 7:16:55 PM
 *  Author: Pinkesh
 */ 


#ifndef EEPROM_MEGA_CONFIG_H_
#define EEPROM_MEGA_CONFIG_H_

#include <avr/io.h>
#include "utilities.h"



#define EEPROM_MEGA_InterruptEnable (SET(EECR,EERIE))
#define EEPROM_MEGA_InterruptDisable (CLR(EECR,EERIE))



/************************************************************************/
/* Function Inclusion Definition										*/
/* value = 0 then that function is not included in build process		*/
/*			if we don't need particular function then we can omit		*/
/*			it's function implementation by setting include_value = 0	*/
/*			and we can reduce the HEX file size                         */
/* value = 1 then that function is included								*/
/************************************************************************/
#define eeprom_mega_includeReadByte 1
#define eeprom_mega_includeWriteByte 1
#define eeprom_mega_includeReadData 0
#define eeprom_mega_includeWriteData 0
#define eeprom_mega_includeReadWriteData 1
/************************************************************************/

#endif /* EEPROM_MEGA_CONFIG_H_ */