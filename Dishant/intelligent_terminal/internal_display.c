/*
 * shift_register.c
 *
 * Created: 04/10/13 7:05:03 PM
 *  Author: Pinkesh
 */ 

#include <util/delay.h>
#include "internal_display.h"
#include "utilities.h"


/**** Static Function Declaration *****/
static void internal_display_ClockPulse();
/**************************************/

/***** Function Definition *****/
void internal_display_Initialize()
{
	//< data_pin=0
	SET(INTERNAL_DISPLAY_DDR,INTERNAL_DISPLAY_DATA_Pin);
	CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_DATA_Pin);
	
	//< clock_pin=0
	SET(INTERNAL_DISPLAY_DDR,INTERNAL_DISPLAY_CLOCK_Pin);
	CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_CLOCK_Pin);
	
	//< latch_pin=0
	SET(INTERNAL_DISPLAY_DDR,INTERNAL_DISPLAY_LATCH_Pin);
	CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_LATCH_Pin);
}

static void internal_display_ClockPulse()
{
	SET(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_CLOCK_Pin);
	_delay_us(INTERNAL_DISPLAY_CLOCK_Delay);
	CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_CLOCK_Pin);
	_delay_us(INTERNAL_DISPLAY_CLOCK_Delay);
}

void internal_display_LatchPulse()
{
	SET(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_LATCH_Pin);
	_delay_us(INTERNAL_DISPLAY_LATCH_DELAY);
	CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_LATCH_Pin);
	_delay_us(INTERNAL_DISPLAY_LATCH_DELAY);
}

void internal_display_WriteByte(unsigned char data)
{
	unsigned char i;
	
	//< sends MSB bit first
	for(i=0;i<8;i++)
	{
		//< Output the data on Data line
		//< according to the Value of MSB
		if(data & 0b10000000)
		{
			//< MSB is 1 so output high
			SET(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_DATA_Pin);
		}
		else
		{
			//< MSB is 0 so output low
			CLR(INTERNAL_DISPLAY_PORT,INTERNAL_DISPLAY_DATA_Pin);
		}
		
		//< Pulse the Clock line
		internal_display_ClockPulse();
		//< bring next bit at MSB position
		data=data<<1;
	}
}