/*
 * i2c.h
 *
 * Created: 11/10/2012 5:10:46 PM
 *  Author: Pinkesh
 */ 


#ifndef I2C_H_
#define I2C_H_

#include <avr/io.h>

#define I2C_READ_LAST_DATA 1
#define I2C_READ_NOT_LAST_DATA 0

#define I2C_STATUS		(TWSR & 0xF8)
#define I2C_MT_SLA_ACK		0x18
#define I2C_MT_DATA_ACK		0x28
#define I2C_MR_SLA_ACK		0x40
#define I2C_MR_DATA_ACK		0x50
#define I2C_MR_DATA_NACK	0x58

/**
 define value for TWBR register
 \n this value determine I2C clock speed
*/
#define I2C_configTWBR 70

/**
 define value for TWPS0 and TWPS1 bits of TWSR register
 \n TWPS0 and TWPS1 bits determine I2C clock speed
 \n value = 0 then (TWPS1,TWPS0) = (0,0)
 \n value = 1 then (TWPS1,TWPS0) = (0,1)
 \n value = 2 then (TWPS1,TWPS0) = (1,0)
 \n value = 3 then (TWPS1,TWPS0) = (1,1)
*/
#define I2C_configTWPS_bits 0



/**
* Initialize I2C module in master mode
*/
void I2C_InitMaster(void);

/**
* Send start condition
*/
void I2C_SendStart(void);

/**
* Send repeated start condition
*/
void I2C_SendRepeatedStart(void);

/**
* Send the slave address, data direction bit and wait for the ACK signal
*/
void I2C_SendAddress(unsigned char data);

/**
* Send the 8-bit data and wait for the ACK
*/
void I2C_SendData(unsigned char data);

/**
* Send the STOP condition
*/
void I2C_SendStop(void);

/**
* read data from the slave device
* \n \param		status	as per this argument value
*						ACK or NACK signal transmitted
*						after receiving byte
* \n \return			received data byte
*/
unsigned char I2C_ReadData(unsigned char status);

#endif /* I2C_H_ */