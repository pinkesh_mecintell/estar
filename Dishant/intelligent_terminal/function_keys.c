/*
 * function_keys.c
 *
 * Created: 05/10/13 12:16:52 PM
 *  Author: Pinkesh
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdbool.h>
#include "lcd_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "ps2_kb.h"
#include "buzzer.h"
#include "utilities.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "printer.h"
#include "SD_addresses.h"
#include "SD_mega.h"
#include "setting_controls.h"
#include "function_keys.h"
#include "usart.h"
#include "password_strings.h"
#include "ADS123x.h"


/***** Extern Variables *****/
//////////////////
//< sd card buffer
extern uint8_t SD_data_buf[];
extern unsigned char weight_str[];//< 0=MSB,5=LSB
extern unsigned char LCD_CODE[];
//////////////////////////

//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////

/////////////////////////////
//< weight related
extern signed long int weight;
/////////////////////////////

///////////////////////////
//< RTC related
extern RTC_DATA RTC_data;
///////////////////////////
extern unsigned char fun_ret;
///////////////////////////
extern unsigned char login_type;
extern unsigned char operator_no;
/****************************/
//to get data from wrapper use as a flag
unsigned char from_where = '1';  // will be 4 if from F4 or F6


/***** local function ****/
//static void remove_space_from_string(unsigned char *str);
/*************************/


void RST_check()
{
	unsigned char cur_rst_add;
	unsigned long int cur_rst_no;
	unsigned int write_cycles;
	unsigned long int cur_rst_no_copy;
	unsigned int write_cycles_copy;

	//< check MAGIC_BYTE
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	if(SD_data_buf[RST_NO_ADD_MAGIC_BYTE]!=0xAA)
	{
		//< if magic byte is not already set then
		//< set all the fields with reset value
		//< RST_NO_CURRENT_OFFSET
		SD_data_buf[RST_NO_CURRENT_ADD_OFFSET] = 1;
		//< Magic byte
		SD_data_buf[RST_NO_ADD_MAGIC_BYTE]=0xAA;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO);

		//< write RST_NO=0 and WRITE_TIMES=0 at current location
		//< RST_NO
		SD_data_buf[RST_NO] = 0;//< MSB
		SD_data_buf[RST_NO+1] = 0;
		SD_data_buf[RST_NO+2] = 0;
		SD_data_buf[RST_NO+3] = 1;//< LSB
		//< RST_NO_WRITE_TIMES
		SD_data_buf[RST_NO_WRITE_TIMES] = 0;//< MSB
		SD_data_buf[RST_NO_WRITE_TIMES+1] = 0;//< LSB
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+1);
		//< also write at copy location
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+1);

		return;
	}

	cur_rst_add = SD_data_buf[RST_NO_CURRENT_ADD_OFFSET];

	//< read current RST_NO loaction
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	//< retrive rst_no
	cur_rst_no = ((unsigned long int)SD_data_buf[RST_NO] << 24) + \
					((unsigned long int)SD_data_buf[RST_NO+1] << 16) + \
					((unsigned long int)SD_data_buf[RST_NO+2] << 8) + \
					((unsigned long int)SD_data_buf[RST_NO+3]);
	//< retrive write cycles
	write_cycles = ((unsigned int)SD_data_buf[RST_NO_WRITE_TIMES] << 8) + SD_data_buf[RST_NO_WRITE_TIMES+1];

	//< read copy location
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
	//< retrive rst_no
	cur_rst_no_copy = ((unsigned long int)SD_data_buf[RST_NO] << 24) + \
					((unsigned long int)SD_data_buf[RST_NO+1] << 16) + \
					((unsigned long int)SD_data_buf[RST_NO+2] << 8) + \
					((unsigned long int)SD_data_buf[RST_NO+3]);
	//< retrive write cycles
	write_cycles_copy = ((unsigned int)SD_data_buf[RST_NO_WRITE_TIMES] << 8) + SD_data_buf[RST_NO_WRITE_TIMES+1];

	//< check current location and copy location
	//< has same values or not
	if(cur_rst_no!=cur_rst_no_copy || write_cycles!=write_cycles_copy)
	{
		//< write copy value in rst_no_current_add location
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
		cur_rst_no = cur_rst_no_copy;
		write_cycles = write_cycles_copy;
	}


	if(write_cycles>50000)
	{
		//< increment RST_NO location
		cur_rst_add+=1;
		SD_data_buf[RST_NO_CURRENT_ADD_OFFSET] = cur_rst_add;
		SD_data_buf[RST_NO_ADD_MAGIC_BYTE]=0xAA;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO);

		//< write RST_NO=cur_rst_no and WRITE_TIMES=0 at current location
		SD_data_buf[RST_NO]=(unsigned char)(cur_rst_no>>24);//< MSB
		SD_data_buf[RST_NO+1]=(unsigned char)(cur_rst_no>>16);
		SD_data_buf[RST_NO+2]=(unsigned char)(cur_rst_no>>8);
		SD_data_buf[RST_NO+3]=(unsigned char)(cur_rst_no);//< LSB
		SD_data_buf[RST_NO_WRITE_TIMES]=0;//< MSB
		SD_data_buf[RST_NO_WRITE_TIMES+1]=0;//< LSB
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
		//< also write at copy location
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
	}
}

unsigned long int RST_read()
{
	unsigned char cur_rst_add;
	unsigned long int cur_rst_no;

	//< read current rst_no address
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	cur_rst_add = SD_data_buf[RST_NO_CURRENT_ADD_OFFSET];

	//< read current rst_no
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	//< retrive rst_no
	cur_rst_no = ((unsigned long int)SD_data_buf[RST_NO] << 24) + \
					((unsigned long int)SD_data_buf[RST_NO+1] << 16) + \
					((unsigned long int)SD_data_buf[RST_NO+2] << 8) + \
					((unsigned long int)SD_data_buf[RST_NO+3]);
	return cur_rst_no;
}

void RST_write(unsigned long int _rst_no)
{
	unsigned char cur_rst_add;
	unsigned int write_cycle;

	//< read current rst_no address
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	cur_rst_add = SD_data_buf[RST_NO_CURRENT_ADD_OFFSET];

	//< read current rst_no and write cycle
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	//< retrieve write cycles
	write_cycle = ((unsigned int)SD_data_buf[RST_NO_WRITE_TIMES] << 8) + SD_data_buf[RST_NO_WRITE_TIMES+1];

	//< write new rst_no and increment write cycle
	write_cycle+=1;
	SD_data_buf[RST_NO]=(unsigned char)(_rst_no>>24);//< MSB
	SD_data_buf[RST_NO+1]=(unsigned char)(_rst_no>>16);
	SD_data_buf[RST_NO+2]=(unsigned char)(_rst_no>>8);
	SD_data_buf[RST_NO+3]=(unsigned char)(_rst_no);//< LSB
	SD_data_buf[RST_NO_WRITE_TIMES]=(unsigned char)(write_cycle>>8);//< MSB
	SD_data_buf[RST_NO_WRITE_TIMES+1]=(unsigned char)(write_cycle);//< LSB
	SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	//< also write at copy location
	SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
}

void function_F1()
{
	unsigned long int rst_no,tare;
	unsigned int j;
	unsigned char call_pointer=1,i;

	//get current RST no
	rst_no=RST_read();
	//clear SD_data_buf for further use
	for (j=0;j<512;j++)
	{
		SD_data_buf[j]=' ';
	}
	//Show RST no on display
	ltoa(rst_no,lcd_Line2_data,10);
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("At RST No"));
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	_delay_ms(1000);
	//First get input for all entries enabled
	i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	while(call_pointer<=14)
	{
		//call appropriate section of F1 function
		fun_ret = function_get_data(call_pointer);
		if (fun_ret==RETURN_CAUSE_ESC)
		{
			if (call_pointer==1)
			//Return if first entry
			{
				return;
			}
			else if (call_pointer==11)
			{
				//back from charges go to last enabled entry
				call_pointer = i;
			}
			else
			{
				//go back to previous function
				call_pointer--;
			}
		}
		else
		{
			//return successful go to next section
			call_pointer++;
		}
		//If all enabled entries done go to next portion
		if (call_pointer==(i+1))
		{
			call_pointer = 11; //Go for charges block next
		}
	}
	//P/S/C
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Prnt-Save-Cancel"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C):"),0,0);
	if(get_character_from_user_P(&i,PSTR("PSC"),'S',9,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='C')
	{
		return;
	}
	else if (i=='S' || i=='P')
	{
		//save date here
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_FIRST_DATE] = RTC_data.date[0];
		SD_data_buf[RST_FIRST_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_FIRST_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_FIRST_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_FIRST_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_FIRST_DATE+5] = RTC_data.year[1];

		//< Time
		SD_data_buf[RST_FIRST_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_FIRST_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_FIRST_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_FIRST_TIME+3] = RTC_data.minute[1];

		//< Save Charges Detail
		SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';   //Set nett charges
		SD_data_buf[RST_TOTAL_CHARGE] = SD_data_buf[RST_CHARGE1];
		SD_data_buf[RST_TOTAL_CHARGE+1] = SD_data_buf[RST_CHARGE1+1];
		SD_data_buf[RST_TOTAL_CHARGE+2] = SD_data_buf[RST_CHARGE1+2];
		SD_data_buf[RST_TOTAL_CHARGE+3] = SD_data_buf[RST_CHARGE1+3];
		//Set second charges to 0
		tare = 0;
		SD_data_buf[RST_CHARGE2_IS_PRESENT] = 'N';
		SD_data_buf[RST_CHARGE2] = (unsigned char)(tare >> 24);//< MSB
		SD_data_buf[RST_CHARGE2+1] = (unsigned char)(tare >> 16);
		SD_data_buf[RST_CHARGE2+2] = (unsigned char)(tare >> 8);
		SD_data_buf[RST_CHARGE2+3] = (unsigned char)(tare);		//< LSB

		//Save Weight Details
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 'Y';
		SD_data_buf[RST_NETWEIGHT] = SD_data_buf[RST_FIRSTWEIGHT];
		SD_data_buf[RST_NETWEIGHT+1] = SD_data_buf[RST_FIRSTWEIGHT+1];
		SD_data_buf[RST_NETWEIGHT+2] = SD_data_buf[RST_FIRSTWEIGHT+2];
		SD_data_buf[RST_NETWEIGHT+3] = SD_data_buf[RST_FIRSTWEIGHT+3];

		//< only first entry complete
		SD_data_buf[RST_BOTH_ENTRY_COMPLETED] = 'N';

		//< operator name save
		if(login_type==LOGIN_OPRT)
		{
			for (i=0;i<16;i++)
			{
				SD_data_buf[RST_FIRST_OPRATOR+i]=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*operator_no)+OPERATOR_NAME+i);
			}
		}

		//< this entry is now occupied
		SD_data_buf[RST_ENTRY_IS_OCCUPIED] = 'Y';

		//< save entry in sd card
		SD_mega_WriteSingleBlock(SD_data_buf,rst_no);

		if (i=='P')
		{
			F1_print_sleep_type1(rst_no);
		}
	}

	RST_write(rst_no+1);    //---------------Don't include during testing
}

unsigned char function_get_data(unsigned char fun_num)
//Used internally for F1 entries
{
	unsigned char i,local_data_buff[512],code_ret,user_ret,enter_ret;
	signed long int charges;

	if (fun_num == 20)
	//F5 Entry1 input Function
	{
		//Check if code system for Entry 1 is enabled or not
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY1_ENABLE)=='Y')
		{
		RE_ENTER_CODE0_ENTRY:
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);
			//Set string in line 2 if user have input data
			if (SD_data_buf[RST_ENTRY1_IS_PRESENT]=='Y')
			{
				for (i=6;i<31;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY1+i-6];
				}
			}
			//get Entry input from user
			user_ret = get_string_from_user(lcd_Line2_data,6,6,31,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//search if the code exists
				code_ret=get_code_location(lcd_Line2_data,CODE1_ENTRY_TABLE);
				if (code_ret==NO_MATCH_FOUND)
				{
					//If wrong code than prompt user about it
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Not Found...!"));
					_delay_ms(1000);
					//Prompt user to press Enter/ESC
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Enter to Proceed"));
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Esc to ReEnter"));

					//take user Input
					enter_ret = wait_for_enter();
					if (enter_ret==RETURN_CAUSE_ENTER)
					{
						//Enter pressed save user input data in buffer
						SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';  //Entry Edited raise flag
						for (i=0;i<25;i++)
						{
							SD_data_buf[RST_ENTRY1+i]=lcd_Line2_data[i];
						}
						SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]='N';
					}
					else if (enter_ret==RETURN_CAUSE_ESC)
					{
						//Esc pressed go back for user input
						goto RE_ENTER_CODE0_ENTRY;
					}
				}
				else
				{
					//Found input code location
					//read code data values from found location in local buffer
					SD_mega_ReadSingleBlock(local_data_buff,CODE_ENTRY_START+code_ret);
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code = "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					//save the entry while printing on LCD
					SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';  //Entry Edited raise flag
					for (i=0;i<25;i++)
					{
						lcd_mega_SendData(local_data_buff[CODE1_ENTRY_DATA+i]);
						SD_data_buf[RST_ENTRY1+i]=local_data_buff[CODE1_ENTRY_DATA+i];
					}
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);
					//Show Tare weight saved with this code
					lcd_mega_ClearDispaly();
					lcd_mega_StringF(PSTR("Tare weight :"));
					lcd_mega_gotoxy2(0);
					//get tare weight
					charges = ((unsigned long int)local_data_buff[CODE1_ENTRY_TARE] << 24)+\
					((unsigned long int)local_data_buff[CODE1_ENTRY_TARE+1] << 16)+\
					((unsigned long int)local_data_buff[CODE1_ENTRY_TARE+2] << 8)+\
					((unsigned long int)local_data_buff[CODE1_ENTRY_TARE+3]);
					clear_lcd_data(LINE2);
					ltoa(charges,lcd_Line2_data,10);
					lcd_mega_String(lcd_Line2_data);  //Show Tare weight
					SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT] = 'Y';
					SD_data_buf[RST_SECONDWEIGHT] = local_data_buff[CODE1_ENTRY_TARE];
					SD_data_buf[RST_SECONDWEIGHT+1] = local_data_buff[CODE1_ENTRY_TARE+1];
					SD_data_buf[RST_SECONDWEIGHT+2] = local_data_buff[CODE1_ENTRY_TARE+2];
					SD_data_buf[RST_SECONDWEIGHT+3] = local_data_buff[CODE1_ENTRY_TARE+3];
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);
				}
			}
			else  //return cause enter without change
			{
				//Entry 1 stays un-Edited
				if (SD_data_buf[RST_ENTRY1_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY1_IS_PRESENT]='N';
					SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]='N';
				}
			}
		}
		else
		{
			//Code Entry is disabled get direct input
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			if (SD_data_buf[RST_ENTRY1_IS_PRESENT]=='Y')
			{
				for (i=0;i<25;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY1+i];
				}
			}
			//get User input
			user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				//Entry 1 stays un-Edited
				SD_data_buf[RST_ENTRY1_IS_PRESENT]='N';
				SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]='N';
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//Save Entry
				SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';
				SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]='N';
				for(i=0;i<25;i++)
				{
					SD_data_buf[RST_ENTRY1+i]=lcd_Line2_data[i];
				}
			}
		}
	}

	else if (fun_num == 1)
	//Entry1 input Function
	{
		//Check if code system for Entry 1 is enabled or not
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY1_ENABLE)=='Y')
		{
		RE_ENTER_CODE1_ENTRY:
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			if (from_where=='4')
			//from function F4/F6 only show data
			{
				if (SD_data_buf[RST_ENTRY1_IS_PRESENT]=='Y')
				{
					for (i=0;i<25;i++)
					{
						lcd_Line2_data[i]=SD_data_buf[RST_ENTRY1+i];
					}
				}
				lcd_mega_ClearDispaly();
				lcd_mega_String(lcd_Line1_data);
				lcd_mega_gotoxy2(0);
				lcd_mega_String(lcd_Line2_data);
				return wait_for_enter();
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);
			//Set string in line 2 if user have input data
			if (SD_data_buf[RST_ENTRY1_IS_PRESENT]=='Y')
			{
				for (i=6;i<31;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY1+i-6];
				}
			}
			//get Entry input from user
			user_ret = get_string_from_user(lcd_Line2_data,6,6,31,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//search if the code exists
				code_ret=get_code_location(lcd_Line2_data,CODE1_ENTRY_TABLE);
				if (code_ret==NO_MATCH_FOUND)
				{
					//If wrong code than prompt user about it
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Not Found...!"));
					_delay_ms(1000);
					//Prompt user to press Enter/ESC
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Enter to Proceed"));
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Esc to ReEnter"));

					//take user Input
					enter_ret = wait_for_enter();
					if (enter_ret==RETURN_CAUSE_ENTER)
					{
						//Enter pressed save user input data in buffer
						SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';  //Entry Edited raise flag
						for (i=0;i<25;i++)
						{
							SD_data_buf[RST_ENTRY1+i]=lcd_Line2_data[i];
						}
					}
					else if (enter_ret==RETURN_CAUSE_ESC)
					{
						//Esc pressed go back for user input
						goto RE_ENTER_CODE1_ENTRY;
					}
				}
				else
				{
					//Found input code location
					//read code data values from found location in local buffer
					SD_mega_ReadSingleBlock(local_data_buff,CODE_ENTRY_START+code_ret);
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code = "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					//save the entry while printing on LCD
					SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';  //Entry Edited raise flag
					for (i=0;i<25;i++)
					{
						lcd_mega_SendData(local_data_buff[CODE1_ENTRY_DATA+i]);
						SD_data_buf[RST_ENTRY1+i]=local_data_buff[CODE1_ENTRY_DATA+i];
					}
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);
				}
			}
			else  //return cause enter without change
			{
				//Entry 1 stays un-Edited
				if (SD_data_buf[RST_ENTRY1_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY1_IS_PRESENT]='N';
				}
			}
		}
		else
		{
			//Code Entry is disabled get direct input
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			if (SD_data_buf[RST_ENTRY1_IS_PRESENT]=='Y')
			{
				for (i=0;i<25;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY1+i];
				}
			}
			//get User input
			user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				//Entry 1 stays un-Edited
				SD_data_buf[RST_ENTRY1_IS_PRESENT]='N';
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//Save Entry
				SD_data_buf[RST_ENTRY1_IS_PRESENT]='Y';
				for(i=0;i<25;i++)
				{
					SD_data_buf[RST_ENTRY1+i]=lcd_Line2_data[i];
				}
			}
		}
	}

	else if (fun_num == 2)
	//Entry2 input Function
	{
		//Check if code system for Entry 1 is enabled or not
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY2_ENABLE)=='Y')
		{
		RE_ENTER_CODE2_ENTRY:
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
			}
			if (from_where=='4')
			//from function F4/F6 only show data
			{
				if (SD_data_buf[RST_ENTRY2_IS_PRESENT]=='Y')
				{
					for (i=0;i<25;i++)
					{
						lcd_Line2_data[i]=SD_data_buf[RST_ENTRY2+i];
					}
				}
				lcd_mega_ClearDispaly();
				lcd_mega_String(lcd_Line1_data);
				lcd_mega_gotoxy2(0);
				lcd_mega_String(lcd_Line2_data);
				return wait_for_enter();
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);
			//Set string in line 2 if user have input data
			if (SD_data_buf[RST_ENTRY2_IS_PRESENT]=='Y')
			{
				for (i=6;i<31;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY2+i-6];
				}
			}
			//get Entry input from user
			user_ret = get_string_from_user(lcd_Line2_data,6,6,31,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//search if the code exists
				code_ret=get_code_location(lcd_Line2_data,CODE2_ENTRY_TABLE);
				if (code_ret==NO_MATCH_FOUND)
				{
					//If wrong code than prompt user about it
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Not Found...!"));
					_delay_ms(1000);
					//Prompt user to press Enter/ESC
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Enter to Proceed"));
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Esc to ReEnter"));

					//take user Input
					enter_ret = wait_for_enter();
					if (enter_ret==RETURN_CAUSE_ENTER)
					{
						//Enter pressed save user input data in buffer
						SD_data_buf[RST_ENTRY2_IS_PRESENT]='Y';  //Entry Edited raise flag
						for (i=0;i<25;i++)
						{
							SD_data_buf[RST_ENTRY2+i]=lcd_Line2_data[i];
						}
					}
					else if (enter_ret==RETURN_CAUSE_ESC)
					{
						goto RE_ENTER_CODE2_ENTRY;
					}
				}
				else
				{
					//Found input code location
					//read code data values from found location in local buffer
					SD_mega_ReadSingleBlock(local_data_buff,CODE_ENTRY_START+code_ret);
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code = "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					//save the entry while printing on LCD
					SD_data_buf[RST_ENTRY2_IS_PRESENT]='Y';  //Entry Edited raise flag
					for (i=0;i<25;i++)
					{
						lcd_mega_SendData(local_data_buff[CODE2_ENTRY_DATA+i]);
						SD_data_buf[RST_ENTRY2+i]=local_data_buff[CODE2_ENTRY_DATA+i];
					}
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);

					//if charges associated with this entry show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='2')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Charges"));
						lcd_mega_gotoxy2(0);
						charges = ((unsigned long int)local_data_buff[CODE2_ENTRY_CHARGES] << 24)+\
								((unsigned long int)local_data_buff[CODE2_ENTRY_CHARGES+1] << 16)+\
								((unsigned long int)local_data_buff[CODE2_ENTRY_CHARGES+2] << 8)+\
								((unsigned long int)local_data_buff[CODE2_ENTRY_CHARGES+3]);
						ltoa(charges,lcd_Line2_data,10);
						lcd_mega_String(lcd_Line2_data);
						//save charges to current data buffer
						SD_data_buf[RST_CHARGE1_IS_PRESENT]='Y';
						SD_data_buf[RST_CHARGE1]=local_data_buff[CODE2_ENTRY_CHARGES];
						SD_data_buf[RST_CHARGE1+1]=local_data_buff[CODE2_ENTRY_CHARGES+1];
						SD_data_buf[RST_CHARGE1+2]=local_data_buff[CODE2_ENTRY_CHARGES+2];
						SD_data_buf[RST_CHARGE1+3]=local_data_buff[CODE2_ENTRY_CHARGES+3];
						//Currently set total charges = charge 1
						SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';
						SD_data_buf[RST_TOTAL_CHARGE]=local_data_buff[CODE2_ENTRY_CHARGES];
						SD_data_buf[RST_TOTAL_CHARGE+1]=local_data_buff[CODE2_ENTRY_CHARGES+1];
						SD_data_buf[RST_TOTAL_CHARGE+2]=local_data_buff[CODE2_ENTRY_CHARGES+2];
						SD_data_buf[RST_TOTAL_CHARGE+3]=local_data_buff[CODE2_ENTRY_CHARGES+3];
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}

					//if Mobile no. associated with this entry than show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='2')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Mobile No."));
						lcd_mega_gotoxy2(0);
						//Save mobile no. Entry
						SD_data_buf[RST_MOBILE_NO_IS_PRESENT]='Y';
						for (i=0;i<12;i++)
						{
							SD_data_buf[RST_MOBILE_NO+i]=local_data_buff[CODE2_ENTRY_MOB_NO+i];
							lcd_mega_SendData(local_data_buff[CODE2_ENTRY_MOB_NO+i]);
						}
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}
				}
			}
			else  //return cause enter without change
			{
				//Entry 2 stays un-Edited
				if (SD_data_buf[RST_ENTRY2_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY2_IS_PRESENT]='N';
				}
			}
		}
		else
		{
			//Code Entry is disabled get direct input
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
			}
			if (SD_data_buf[RST_ENTRY2_IS_PRESENT]=='Y')
			{
				for (i=0;i<25;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY2+i];
				}
			}
			//get User input
			user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				//Entry 2 stays un-Edited
				SD_data_buf[RST_ENTRY2_IS_PRESENT]='N';
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//Save Entry
				SD_data_buf[RST_ENTRY2_IS_PRESENT]='Y';
				for(i=0;i<25;i++)
				{
					SD_data_buf[RST_ENTRY2+i]=lcd_Line2_data[i];
				}
			}
		}
	}

	else if (fun_num == 3)
	//Entry3 input Function
	{
		//Check if code system for Entry 3 is enabled or not
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY3_ENABLE)=='Y')
		{
		RE_ENTER_CODE3_ENTRY:
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
			}
			if (from_where=='4')
			//from function F4/F6 only show data
			{
				if (SD_data_buf[RST_ENTRY3_IS_PRESENT]=='Y')
				{
					for (i=0;i<25;i++)
					{
						lcd_Line2_data[i]=SD_data_buf[RST_ENTRY3+i];
					}
				}
				lcd_mega_ClearDispaly();
				lcd_mega_String(lcd_Line1_data);
				lcd_mega_gotoxy2(0);
				lcd_mega_String(lcd_Line2_data);
				return wait_for_enter();
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);
			//Set string in line 2 if user have input data
			if (SD_data_buf[RST_ENTRY3_IS_PRESENT]=='Y')
			{
				for (i=6;i<31;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY3+i-6];
				}
			}
			//get Entry input from user
			user_ret = get_string_from_user(lcd_Line2_data,6,6,31,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//search if the code exists
				code_ret=get_code_location(lcd_Line2_data,CODE3_ENTRY_TABLE);
				if (code_ret==NO_MATCH_FOUND)
				{
					//If wrong code than prompt user about it
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Not Found...!"));
					_delay_ms(1000);
					//Prompt user to press Enter/ESC
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Enter to Proceed"));
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Esc to ReEnter"));

					//take user Input
					enter_ret = wait_for_enter();
					if (enter_ret==RETURN_CAUSE_ENTER)
					{
						//Enter pressed save user input data in buffer
						SD_data_buf[RST_ENTRY3_IS_PRESENT]='Y';  //Entry Edited raise flag
						for (i=0;i<25;i++)
						{
							SD_data_buf[RST_ENTRY3+i]=lcd_Line2_data[i];
						}
					}
					else if (enter_ret==RETURN_CAUSE_ESC)
					{
						goto RE_ENTER_CODE3_ENTRY;
					}
				}
				else
				{
					//Found input code location
					//read code data values from found location in local buffer
					SD_mega_ReadSingleBlock(local_data_buff,CODE_ENTRY_START+code_ret);
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code = "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					//save the entry while printing on LCD
					SD_data_buf[RST_ENTRY3_IS_PRESENT]='Y';  //Entry Edited raise flag
					for (i=0;i<25;i++)
					{
						lcd_mega_SendData(local_data_buff[CODE3_ENTRY_DATA+i]);
						SD_data_buf[RST_ENTRY3+i]=local_data_buff[CODE3_ENTRY_DATA+i];
					}
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);

					//if charges associated with this entry show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='3')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Charges"));
						lcd_mega_gotoxy2(0);
						charges = ((unsigned long int)local_data_buff[CODE3_ENTRY_CHARGES] << 24)+\
						((unsigned long int)local_data_buff[CODE3_ENTRY_CHARGES+1] << 16)+\
						((unsigned long int)local_data_buff[CODE3_ENTRY_CHARGES+2] << 8)+\
						((unsigned long int)local_data_buff[CODE3_ENTRY_CHARGES+3]);
						ltoa(charges,lcd_Line2_data,10);
						lcd_mega_String(lcd_Line2_data);
						//save charges to current data buffer
						SD_data_buf[RST_CHARGE1_IS_PRESENT]='Y';
						SD_data_buf[RST_CHARGE1]=local_data_buff[CODE3_ENTRY_CHARGES];
						SD_data_buf[RST_CHARGE1+1]=local_data_buff[CODE3_ENTRY_CHARGES+1];
						SD_data_buf[RST_CHARGE1+2]=local_data_buff[CODE3_ENTRY_CHARGES+2];
						SD_data_buf[RST_CHARGE1+3]=local_data_buff[CODE3_ENTRY_CHARGES+3];
						//Currently set total charges = charge 1
						SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';
						SD_data_buf[RST_TOTAL_CHARGE]=local_data_buff[CODE3_ENTRY_CHARGES];
						SD_data_buf[RST_TOTAL_CHARGE+1]=local_data_buff[CODE3_ENTRY_CHARGES+1];
						SD_data_buf[RST_TOTAL_CHARGE+2]=local_data_buff[CODE3_ENTRY_CHARGES+2];
						SD_data_buf[RST_TOTAL_CHARGE+3]=local_data_buff[CODE3_ENTRY_CHARGES+3];
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}

					//if Mobile no. associated with this entry than show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='3')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Mobile No."));
						lcd_mega_gotoxy2(0);
						//Save mobile no. Entry
						SD_data_buf[RST_MOBILE_NO_IS_PRESENT]='Y';
						for (i=0;i<12;i++)
						{
							SD_data_buf[RST_MOBILE_NO+i]=local_data_buff[CODE3_ENTRY_MOB_NO+i];
							lcd_mega_SendData(local_data_buff[CODE3_ENTRY_MOB_NO+i]);
						}
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}
				}
			}
			else  //return cause enter without change
			{
				//Entry 3 stays un-Edited
				if (SD_data_buf[RST_ENTRY3_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY3_IS_PRESENT]='N';
				}
			}
		}
		else
		{
			//Code Entry is disabled get direct input
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
			}
			if (SD_data_buf[RST_ENTRY3_IS_PRESENT]=='Y')
			{
				for (i=0;i<25;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY3+i];
				}
			}
			//get User input
			user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				//Entry 3 stays un-Edited
				if (SD_data_buf[RST_ENTRY3_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY3_IS_PRESENT]='N';
				}
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//Save Entry
				SD_data_buf[RST_ENTRY3_IS_PRESENT]='Y';
				for(i=0;i<25;i++)
				{
					SD_data_buf[RST_ENTRY3+i]=lcd_Line2_data[i];
				}
			}
		}
	}

	else if (fun_num == 4)
	//Entry4 input Function
	{
		//Check if code system for Entry 4 is enabled or not
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY4_ENABLE)=='Y')
		{
		RE_ENTER_CODE4_ENTRY:
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
			}
			if (from_where=='4')
			//from function F4/F6 only show data
			{
				if (SD_data_buf[RST_ENTRY4_IS_PRESENT]=='Y')
				{
					for (i=0;i<25;i++)
					{
						lcd_Line2_data[i]=SD_data_buf[RST_ENTRY4+i];
					}
				}
				lcd_mega_ClearDispaly();
				lcd_mega_String(lcd_Line1_data);
				lcd_mega_gotoxy2(0);
				lcd_mega_String(lcd_Line2_data);
				return wait_for_enter();
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);
			//Set string in line 2 if user have input data
			if (SD_data_buf[RST_ENTRY4_IS_PRESENT]=='Y')
			{
				for (i=6;i<31;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY4+i-6];
				}
			}
			//get Entry input from user
			user_ret = get_string_from_user(lcd_Line2_data,6,6,31,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//search if the code exists
				code_ret=get_code_location(lcd_Line2_data,CODE4_ENTRY_TABLE);
				if (code_ret==NO_MATCH_FOUND)
				{
					//If wrong code than prompt user about it
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Not Found...!"));
					_delay_ms(1000);
					//Prompt user to press Enter/ESC
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Enter to Proceed"));
					lcd_mega_gotoxy2(0);
					lcd_mega_StringF(PSTR("Esc to ReEnter"));

					//take user Input
					enter_ret = wait_for_enter();
					if (enter_ret==RETURN_CAUSE_ENTER)
					{
						//Enter pressed save user input data in buffer
						SD_data_buf[RST_ENTRY4_IS_PRESENT]='Y';  //Entry Edited raise flag
						for (i=0;i<25;i++)
						{
							SD_data_buf[RST_ENTRY4+i]=lcd_Line2_data[i];
						}
					}
					else if (enter_ret==RETURN_CAUSE_ESC)
					{
						goto RE_ENTER_CODE4_ENTRY;
					}
				}
				else
				{
					//Found input code location
					//read code data values from found location in local buffer
					SD_mega_ReadSingleBlock(local_data_buff,CODE_ENTRY_START+code_ret);
					lcd_mega_ClearDispaly();
					lcd_mega_gotoxy1(0);
					lcd_mega_StringF(PSTR("Code = "));
					lcd_mega_SendData(lcd_Line2_data[0]);
					lcd_mega_SendData(lcd_Line2_data[1]);
					lcd_mega_SendData(lcd_Line2_data[2]);
					lcd_mega_gotoxy2(0);
					//save the entry while printing on LCD
					SD_data_buf[RST_ENTRY4_IS_PRESENT]='Y';  //Entry Edited raise flag
					for (i=0;i<25;i++)
					{
						lcd_mega_SendData(local_data_buff[CODE4_ENTRY_DATA+i]);
						SD_data_buf[RST_ENTRY4+i]=local_data_buff[CODE4_ENTRY_DATA+i];
					}
					//wait for user to press Enter
					while (wait_for_enter() != RETURN_CAUSE_ENTER);

					//if charges associated with this entry show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='4')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Charges"));
						lcd_mega_gotoxy2(0);
						charges = ((unsigned long int)local_data_buff[CODE4_ENTRY_CHARGES] << 24)+\
						((unsigned long int)local_data_buff[CODE4_ENTRY_CHARGES+1] << 16)+\
						((unsigned long int)local_data_buff[CODE4_ENTRY_CHARGES+2] << 8)+\
						((unsigned long int)local_data_buff[CODE4_ENTRY_CHARGES+3]);
						ltoa(charges,lcd_Line2_data,10);
						lcd_mega_String(lcd_Line2_data);
						//save charges to current data buffer
						SD_data_buf[RST_CHARGE1_IS_PRESENT]='Y';
						SD_data_buf[RST_CHARGE1]=local_data_buff[CODE4_ENTRY_CHARGES];
						SD_data_buf[RST_CHARGE1+1]=local_data_buff[CODE4_ENTRY_CHARGES+1];
						SD_data_buf[RST_CHARGE1+2]=local_data_buff[CODE4_ENTRY_CHARGES+2];
						SD_data_buf[RST_CHARGE1+3]=local_data_buff[CODE4_ENTRY_CHARGES+3];
						//Currently set total charges = charge 1
						SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';
						SD_data_buf[RST_TOTAL_CHARGE]=local_data_buff[CODE4_ENTRY_CHARGES];
						SD_data_buf[RST_TOTAL_CHARGE+1]=local_data_buff[CODE4_ENTRY_CHARGES+1];
						SD_data_buf[RST_TOTAL_CHARGE+2]=local_data_buff[CODE4_ENTRY_CHARGES+2];
						SD_data_buf[RST_TOTAL_CHARGE+3]=local_data_buff[CODE4_ENTRY_CHARGES+3];
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}

					//if Mobile no. associated with this entry than show it
					if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='4')
					{
						lcd_mega_ClearDispaly();
						lcd_mega_gotoxy1(0);
						lcd_mega_StringF(PSTR("Mobile No."));
						lcd_mega_gotoxy2(0);
						//Save mobile no. Entry
						SD_data_buf[RST_MOBILE_NO_IS_PRESENT]='Y';
						for (i=0;i<12;i++)
						{
							SD_data_buf[RST_MOBILE_NO+i]=local_data_buff[CODE4_ENTRY_MOB_NO+i];
							lcd_mega_SendData(local_data_buff[CODE4_ENTRY_MOB_NO+i]);
						}
						//wait for user to press Enter
						while (wait_for_enter() != RETURN_CAUSE_ENTER);
					}
				}
			}
			else  //return cause enter without change
			{
				//Entry 4 stays un-Edited
				if (SD_data_buf[RST_ENTRY4_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY4_IS_PRESENT]='N';
				}
			}
		}
		else
		{
			//Code Entry is disabled get direct input
			clear_lcd_data(LINE_BOTH);
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
			}
			if (SD_data_buf[RST_ENTRY4_IS_PRESENT]=='Y')
			{
				for (i=0;i<25;i++)
				{
					lcd_Line2_data[i]=SD_data_buf[RST_ENTRY4+i];
				}
			}
			//get User input
			user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (user_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				//Entry 4 stays un-Edited
				if (SD_data_buf[RST_ENTRY4_IS_PRESENT] != 'Y')
				{
					SD_data_buf[RST_ENTRY4_IS_PRESENT]='N';
				}
			}
			else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				//Save Entry
				SD_data_buf[RST_ENTRY4_IS_PRESENT]='Y';
				for(i=0;i<25;i++)
				{
					SD_data_buf[RST_ENTRY4+i]=lcd_Line2_data[i];
				}
			}
		}
	}

	else if (fun_num == 5)
	//Entry5 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i);
		}
		if (SD_data_buf[RST_ENTRY5_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY5+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 5 stays un-Edited
			if (SD_data_buf[RST_ENTRY5_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY5_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY5_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY5+i]=lcd_Line2_data[i];
			}
		}
	}

	else if (fun_num == 6)
	//Entry6 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY6+i);
		}
		if (SD_data_buf[RST_ENTRY6_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY6+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 6 stays un-Edited
			if (SD_data_buf[RST_ENTRY6_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY6_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY6_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY6+i]=lcd_Line2_data[i];
			}
		}
	}

	else if (fun_num == 7)
	//Entry7 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY7+i);
		}
		if (SD_data_buf[RST_ENTRY7_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY7+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 7 stays un-Edited
			if (SD_data_buf[RST_ENTRY7_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY7_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY7_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY7+i]=lcd_Line2_data[i];
			}
		}
	}

	else if (fun_num == 8)
	//Entry8 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY8+i);
		}
		if (SD_data_buf[RST_ENTRY8_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY8+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 8 stays un-Edited
			if (SD_data_buf[RST_ENTRY8_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY8_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY8_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY8+i]=lcd_Line2_data[i];
			}
		}
	}

	else if (fun_num == 9)
	//Entry9 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY9+i);
		}
		if (SD_data_buf[RST_ENTRY9_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY9+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 9 stays un-Edited
			if (SD_data_buf[RST_ENTRY9_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY9_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY9_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY9+i]=lcd_Line2_data[i];
			}
		}
	}

	else if (fun_num == 10)
	//Entry10 input Function
	{
		clear_lcd_data(LINE_BOTH);
		for (i=0;i<12;i++)
		{
			lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY10+i);
		}
		if (SD_data_buf[RST_ENTRY10_IS_PRESENT]=='Y')
		{
			for (i=0;i<25;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[RST_ENTRY10+i];
			}
		}
		if (from_where=='4')
		//from function F4/F6 only show data
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String(lcd_Line1_data);
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			return wait_for_enter();
		}
		//get User input
		user_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
		if (user_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			//Entry 10 stays un-Edited
			if (SD_data_buf[RST_ENTRY10_IS_PRESENT] != 'Y')
			{
				SD_data_buf[RST_ENTRY10_IS_PRESENT]='N';
			}
		}
		else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//Save Entry
			SD_data_buf[RST_ENTRY10_IS_PRESENT]='Y';
			for(i=0;i<25;i++)
			{
				SD_data_buf[RST_ENTRY10+i]=lcd_Line2_data[i];
			}
		}
	}

	///////////////////////////////
	else if (fun_num == 11)
	//Charges
	{
		if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) != '0')
		{
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_gotoxy1(0);
			lcd_mega_StringF(PSTR("Charges (RS):"));
			set_string_in_lcd_dataF(LINE1,PSTR("Charges (RS):"),0,0);
			if (SD_data_buf[RST_CHARGE1_IS_PRESENT]=='Y')
			//if charge1 value is associated with an entry already
			{
				charges = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
				((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
				((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
				((unsigned long int)SD_data_buf[RST_CHARGE1+3]);
				ltoa(charges,lcd_Line2_data,10);
				if (from_where=='4')
				//from function F4/F6 only show data
				{
					lcd_mega_ClearDispaly();
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					return wait_for_enter();
				}
				if (eeprom_mega_ReadByte(ADD_CODE_CHARGES_EDIT)=='Y')
				//Charges are editable by user
				{
					user_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_ALL_DATA);
					if (user_ret == RETURN_CAUSE_ESC)
					{
						return RETURN_CAUSE_ESC;
					}
					else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
					//save edited charges
					{
						charges = atol(lcd_Line2_data);
						SD_data_buf[RST_CHARGE1] = (unsigned char)(charges >> 24);
						SD_data_buf[RST_CHARGE1+1] = (unsigned char)(charges >> 16);
						SD_data_buf[RST_CHARGE1+2] = (unsigned char)(charges >> 8);
						SD_data_buf[RST_CHARGE1+3] = (unsigned char)(charges);
						SD_data_buf[RST_CHARGE1_IS_PRESENT] = 'Y';
					}
				}
				else
				//Charges Uneditable
				{
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					//wait for user to press Enter
					while (1)
					{
						enter_ret=wait_for_enter();
						if (enter_ret==RETURN_CAUSE_ENTER)
						{
							return RETURN_CAUSE_ENTER;
						}
						else if (enter_ret==RETURN_CAUSE_ESC)
						{
							return RETURN_CAUSE_ESC;
						}
					}
				}
			}
			else
			//charges not yet associated
			{
				clear_lcd_data(LINE_BOTH);
				set_string_in_lcd_dataF(LINE1,PSTR("Charges (RS):"),0,0);
				if (from_where=='4')
				//from function F4/F6 only show data
				{
					lcd_mega_ClearDispaly();
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					return wait_for_enter();
				}
				user_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
				if (user_ret == RETURN_CAUSE_ESC)
				{
					return RETURN_CAUSE_ESC;
				}
				else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
				{
					SD_data_buf[RST_CHARGE1_IS_PRESENT]='Y';
					charges = atol(lcd_Line2_data);
					SD_data_buf[RST_CHARGE1] = (unsigned char)(charges >> 24);
					SD_data_buf[RST_CHARGE1+1] = (unsigned char)(charges >> 16);
					SD_data_buf[RST_CHARGE1+2] = (unsigned char)(charges >> 8);
					SD_data_buf[RST_CHARGE1+3] = (unsigned char)(charges);
				}
				else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
				{
					if (SD_data_buf[RST_CHARGE1_IS_PRESENT]!='Y')
					{
						SD_data_buf[RST_CHARGE1_IS_PRESENT]='N';
					}
				}
			}
		}
	}

	else if (fun_num == 12)
	//F1 Show weight
	{
		lcd_mega_ClearDispaly();
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Weight Press Ent"));

			//< getting ADC_count from ADS123x
			ADC_getReading();

			//< get current weight from ADC
			ADC_getWeight();

			//< displaying weight on LCD
			lcd_mega_gotoxy1(0);
			lcd_mega_SendData(LCD_CODE[weight_str[0]]);//< MSB
			lcd_mega_SendData(LCD_CODE[weight_str[1]]);
			lcd_mega_SendData(LCD_CODE[weight_str[2]]);
			lcd_mega_SendData(LCD_CODE[weight_str[3]]);
			lcd_mega_SendData(LCD_CODE[weight_str[4]]);
			lcd_mega_SendData(LCD_CODE[weight_str[5]]);//< LSB
			lcd_mega_SendData(' ');
			lcd_mega_SendData('K');
			lcd_mega_SendData('G');

			enter_ret = wait_for_enter();
			if (enter_ret==RETURN_CAUSE_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (enter_ret==RETURN_CAUSE_ENTER)
			{
				//save Current shown weight
				SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT]='Y';
				SD_data_buf[RST_FIRSTWEIGHT] = (unsigned char)(weight >> 24);
				SD_data_buf[RST_FIRSTWEIGHT+1] = (unsigned char)(weight >> 16);
				SD_data_buf[RST_FIRSTWEIGHT+2] = (unsigned char)(weight >> 8);
				SD_data_buf[RST_FIRSTWEIGHT+3] = (unsigned char)(weight);
			}
	}

	else if (fun_num == 13)
	//G / T input
	{
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Gross/Tare (G/T)"),0,0);
		if(get_character_from_user_P(&i,PSTR("GT"),'G',0,LINE2)==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		//save Entry type
		SD_data_buf[RST_FIRST_ENTRY_TYPE] = i;
	}

	else if (fun_num == 15)
	//F2 Entry Charges 2
	{
		if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) == '2')
		{
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_gotoxy1(0);
			lcd_mega_StringF(PSTR("Charges 2 (RS):"));
			set_string_in_lcd_dataF(LINE1,PSTR("Charges 2 (RS):"),0,0);
			if (SD_data_buf[RST_CHARGE2_IS_PRESENT]=='Y')
			//if charge2 value is associated with an entry already
			{
				charges = ((unsigned long int)SD_data_buf[RST_CHARGE2] << 24)+\
				((unsigned long int)SD_data_buf[RST_CHARGE2+1] << 16)+\
				((unsigned long int)SD_data_buf[RST_CHARGE2+2] << 8)+\
				((unsigned long int)SD_data_buf[RST_CHARGE2+3]);
				ltoa(charges,lcd_Line2_data,10);
				if (from_where=='4')
				//from function F4/F6 only show data
				{
					lcd_mega_ClearDispaly();
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					return wait_for_enter();
				}
				if (eeprom_mega_ReadByte(ADD_CODE_CHARGES_EDIT)=='Y')
				//Charges are editable by user
				{
					user_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_ALL_DATA);
					if (user_ret == RETURN_CAUSE_ESC)
					{
						return RETURN_CAUSE_ESC;
					}
					else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
					//save edited charges
					{
						charges = atol(lcd_Line2_data);
						SD_data_buf[RST_CHARGE2] = (unsigned char)(charges >> 24);
						SD_data_buf[RST_CHARGE2+1] = (unsigned char)(charges >> 16);
						SD_data_buf[RST_CHARGE2+2] = (unsigned char)(charges >> 8);
						SD_data_buf[RST_CHARGE2+3] = (unsigned char)(charges);
					}
				}
				else
				//Charges Uneditable
				{
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					//wait for user to press Enter
					while (1)
					{
						enter_ret=wait_for_enter();
						if (enter_ret==RETURN_CAUSE_ENTER)
						{
							return RETURN_CAUSE_ENTER;
						}
						else if (enter_ret==RETURN_CAUSE_ESC)
						{
							return RETURN_CAUSE_ESC;
						}
					}
				}
			}
			else
			//charges not yet associated
			{
				clear_lcd_data(LINE_BOTH);
				set_string_in_lcd_dataF(LINE1,PSTR("Charges 2 (RS):"),0,0);
				if (from_where=='4')
				//from function F4/F6 only show data
				{
					lcd_mega_ClearDispaly();
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					return wait_for_enter();
				}
				user_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
				if (user_ret == RETURN_CAUSE_ESC)
				{
					return RETURN_CAUSE_ESC;
				}
				else if (user_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
				{
					SD_data_buf[RST_CHARGE2_IS_PRESENT]='Y';
					charges = atol(lcd_Line2_data);
					SD_data_buf[RST_CHARGE2] = (unsigned char)(charges >> 24);
					SD_data_buf[RST_CHARGE2+1] = (unsigned char)(charges >> 16);
					SD_data_buf[RST_CHARGE2+2] = (unsigned char)(charges >> 8);
					SD_data_buf[RST_CHARGE2+3] = (unsigned char)(charges);
				}
				else if (user_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
				{
					if (SD_data_buf[RST_CHARGE2_IS_PRESENT]!='Y')
					{
						SD_data_buf[RST_CHARGE2_IS_PRESENT]='N';
					}
				}
			}
		}
	}

	else if (fun_num == 16)
	//F2 Show weight
	{
		lcd_mega_ClearDispaly();
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Weight Press Ent"));

		//< getting ADC_count from ADS123x
		ADC_getReading();

		//< get current weight from ADC
		ADC_getWeight();

		//< displaying weight on LCD
		lcd_mega_gotoxy1(0);
		lcd_mega_SendData(LCD_CODE[weight_str[0]]);//< MSB
		lcd_mega_SendData(LCD_CODE[weight_str[1]]);
		lcd_mega_SendData(LCD_CODE[weight_str[2]]);
		lcd_mega_SendData(LCD_CODE[weight_str[3]]);
		lcd_mega_SendData(LCD_CODE[weight_str[4]]);
		lcd_mega_SendData(LCD_CODE[weight_str[5]]);//< LSB
		lcd_mega_SendData(' ');
		lcd_mega_SendData('K');
		lcd_mega_SendData('G');

		enter_ret = wait_for_enter();
		if (enter_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if (enter_ret==RETURN_CAUSE_ENTER)
		{
			//save Current shown weight
			SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]='Y';
			SD_data_buf[RST_SECONDWEIGHT] = (unsigned char)(weight >> 24);
			SD_data_buf[RST_SECONDWEIGHT+ 1] = (unsigned char)(weight >> 16);
			SD_data_buf[RST_SECONDWEIGHT+2] = (unsigned char)(weight >> 8);
			SD_data_buf[RST_SECONDWEIGHT+3] = (unsigned char)(weight);
		}
	}

	else if (fun_num == 17)
	//F5 Second weight
	{
		//get tare weight manually
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Tare Weight :"),0,0);
		if (SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT] == 'Y')
		//Code weight associated with Vehicle no. so show it
		{
			charges = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		//take user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return RETURN_CAUSE_ESC;
		}
		else if(fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			charges = atol(lcd_Line2_data);
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			charges = 0;
		}
		//< save tare weight
		SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT] = 'Y';
		SD_data_buf[RST_SECONDWEIGHT] = (unsigned char)(charges >> 24);//< MSB
		SD_data_buf[RST_SECONDWEIGHT+1] = (unsigned char)(charges >> 16);
		SD_data_buf[RST_SECONDWEIGHT+2] = (unsigned char)(charges >> 8);
		SD_data_buf[RST_SECONDWEIGHT+3] = (unsigned char)(charges);//< LSB
	}

	else if (fun_num == 18)
	//F4 or F6 Show First weight
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("First Weight :"));
		if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT]=='Y')
		//Only if First Weight is present
		{
			charges = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
					((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
					((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
					((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		else
		{
			lcd_Line2_data[0]='0';
			lcd_Line2_data[1]=NULL;
		}
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		return wait_for_enter();
	}

	else if (fun_num == 19)
	//F4 or F6 Show Second Weight
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("Second Weight :"));
		if (SD_data_buf[RST_SECONDWEIGHT_IS_PRESENT]=='Y')
		//Only if First Weight is present
		{
			charges = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		else
		{
			lcd_Line2_data[0]='0';
			lcd_Line2_data[1]=NULL;
		}
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		return wait_for_enter();
	}

	else if (fun_num == 21)
	//F4 or F6 Show Net Weight
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("Net Weight :"));
		if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT]=='Y')
		//Only if Net Weight is present
		{
			charges = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
			((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		return wait_for_enter();
	}

	else if (fun_num == 22)
	//F4 or F6 Show Total Charges
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("Total Charges :"));
		if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=='Y')
		//Only if Total charges are present
		{
			charges = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
			((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		return wait_for_enter();
	}

	return RETURN_CAUSE_ENTER;
}

void F1_print_sleep_type1(unsigned long int rst)
{
	unsigned char i,no_entries;
	signed long int charges;

	no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	PRINTER_REVERSE_PAGE;
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	print_header_type1();
	PRINTER_ENTER;
	//Print first block for all entries
	//Print RST no first
	printer_writeStringF(PSTR("RST NO      : "));
	//Print RST no
	clear_lcd_data(LINE2);
	itoa(rst,lcd_Line2_data,10);
	//Print RST no in bold
	PRINTER_ESC;
	PRINTER_SET_BOLD;
	PRINTER_ESC;
	PRINTER_SET_HEADER_FONT;
	for (i=0;i<25;i++)
	{
		printer_writeData(lcd_Line2_data[i]);
	}
	PRINTER_ESC;
	PRINTER_CANCEL_BOLD;
	PRINTER_ESC;
	PRINTER_CANCEL_HEADER_FONT;
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	printer_writeData(' ');printer_writeData(' ');
	if (no_entries >= 1)
	//If there is 1 or more entries
	{
		//print 1st entry
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print 1st entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY1+i]);
		}
		PRINTER_ENTER; //Go to next line
	}
	if (no_entries >= 2)
	{
		//print 2nd entry
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print second entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY2+i]);
		}
		printer_writeData(' ');
		if (no_entries==2)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 3)
	{
		//entry 3
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 3 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY3+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=4)
	{
		//entry 4
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 4 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY4+i]);
		}
		printer_writeData(' ');
		if (no_entries==4)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 5)
	{
		//entry 5
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 5 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY5+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=6)
	{
		//entry 6
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY6+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 6 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY6+i]);
		}
		printer_writeData(' ');
		if (no_entries==6)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 7)
	{
		//entry 7
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY7+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 7 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY7+i]);
		}
		PRINTER_ENTER;
	}

if (no_entries>=8)
	{
		//entry 8
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY8+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 8 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY8+i]);
		}
		printer_writeData(' ');
		if (no_entries==8)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries>=9)
	{
		//entry 9
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY9+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 9 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY9+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries==10)
	{
		//entry 10
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY10+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 10 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY10+i]);
		}
		PRINTER_ENTER;
	}
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	//print weight
	PRINTER_ENTER;
	if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='G')
	{
		printer_writeStringF(PSTR("GROSS Wt:"));
	}
	else if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='T')
	{
		printer_writeStringF(PSTR("TARE Wt :"));
	}
	printer_writeData(' ');
	if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
	{
		PRINTER_ESC;
		PRINTER_SET_BOLD;
	}
	PRINTER_ESC;
	PRINTER_SET_HEADER_FONT;
	printer_writeData(LCD_CODE[weight_str[0]]);//< MSB
	printer_writeData(LCD_CODE[weight_str[1]]);
	printer_writeData(LCD_CODE[weight_str[2]]);
	printer_writeData(LCD_CODE[weight_str[3]]);
	printer_writeData(LCD_CODE[weight_str[4]]);
	printer_writeData(LCD_CODE[weight_str[5]]);//< LSB
	if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
	{
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
	}
	PRINTER_ESC;
	PRINTER_CANCEL_HEADER_FONT;
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	printer_writeData(' ');
	printer_writeData('K');
	printer_writeData('G');
	//< Date
	//< 6-spaces
	for(i=0;i<6;i++)
	{
		printer_writeData(' ');
	}
	printer_writeStringF(PSTR("DATE:"));
	printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
	printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
	printer_writeData('/');
	printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
	printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
	printer_writeData('/');
	printer_writeData('2');//<YYYY
	printer_writeData('0');
	printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
	printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);

	//< Time
	//< 6-spaces
	for(i=0;i<6;i++)
	{
		printer_writeData(' ');
	}
	printer_writeStringF(PSTR("TIME:"));
	printer_writeData(SD_data_buf[RST_FIRST_TIME]);//<HH
	printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
	printer_writeData(':');
	printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);//<MM
	printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) != '0')
	{
		printer_writeStringF(PSTR("Charges(1): Rs "));
		charges = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+3]);
		clear_lcd_data(LINE2);
		ltoa(charges,lcd_Line2_data,10);
		for (i=0;i<6;i++)
		{
			printer_writeData(lcd_Line2_data[i]);
		}
	}
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	print_signature_line_type1();
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	//Print footer
	print_footer_type1();
	PRINTER_FORWARD_PAGE;
}

void F1_print_sleep_type2(unsigned long int rst)
{
	unsigned char i,no_entries;
	signed long int charges;

	no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	PRINTER_REVERSE_PAGE;
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	print_header_type1();  //-----------------------New Header function for type 2
	PRINTER_ENTER;
	//Print first block for all entries
	//Print RST no first
	printer_writeStringF(PSTR("RST NO      : "));
	//Print RST no
	clear_lcd_data(LINE2);
	itoa(rst,lcd_Line2_data,10);
	//Print RST no in bold
	PRINTER_ESC;
	PRINTER_SET_BOLD;
	for (i=0;i<25;i++)
	{
		printer_writeData(lcd_Line2_data[i]);
	}
	PRINTER_ESC;
	PRINTER_CANCEL_BOLD;
	//next Line
	PRINTER_ENTER;
	if (no_entries >= 1)
	//If there is 1 or more entries
	{
		//print 1st entry
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print 1st entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY1+i]);
		}
		PRINTER_ENTER; //Go to next line
	}
	if (no_entries >= 2)
	{
		//print 2nd entry
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print second entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY2+i]);
		}
		PRINTER_ENTER;
	}
	if(no_entries >= 3)
	{
		//entry 3
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 3 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY3+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=4)
	{
		//entry 4
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 4 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY4+i]);
		}
		PRINTER_ENTER;
	}
	if(no_entries >= 5)
	{
		//entry 5
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 5 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY5+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=6)
	{
		//entry 6
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY6+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 6 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY6+i]);
		}
		PRINTER_ENTER;
	}
	if(no_entries >= 7)
	{
		//entry 7
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY7+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 7 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY7+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=8)
	{
		//entry 8
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY8+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 8 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY8+i]);
		}
		PRINTER_ENTER;
	}
	if(no_entries>=9)
	{
		//entry 9
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY9+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 9 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY9+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries==10)
	{
		//entry 10
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY10+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 10 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY10+i]);
		}
		PRINTER_ENTER;
	}
	//Draw separator line
	for (i=0;i<40;i++)
	{
		printer_writeData('-');
	}
	//print weight
	PRINTER_ENTER;
	if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='G')
	{
		printer_writeStringF(PSTR("GROSS Wt:"));
	}
	else if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='T')
	{
		printer_writeStringF(PSTR("TARE Wt :"));
	}
	printer_writeData(' ');
	if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
	{
		PRINTER_ESC;
		PRINTER_SET_BOLD;
	}

	printer_writeData(LCD_CODE[weight_str[0]]);//< MSB
	printer_writeData(LCD_CODE[weight_str[1]]);
	printer_writeData(LCD_CODE[weight_str[2]]);
	printer_writeData(LCD_CODE[weight_str[3]]);
	printer_writeData(LCD_CODE[weight_str[4]]);
	printer_writeData(LCD_CODE[weight_str[5]]);//< LSB
	if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
	{
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
	}
	printer_writeData(' ');
	printer_writeData('K');
	printer_writeData('G');
	//< Date
	printer_writeData(' ');
	printer_writeStringF(PSTR("DATE:"));
	printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
	printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
	printer_writeData('/');
	printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
	printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
	printer_writeData('/');
	printer_writeData('2');//<YYYY
	printer_writeData('0');
	printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
	printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);

	//< Time
	printer_writeData(' ');
	printer_writeData(SD_data_buf[RST_FIRST_TIME]);//<HH
	printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
	printer_writeData(':');
	printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);//<MM
	printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<40;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) != '0')
	{
		printer_writeStringF(PSTR("Charges(1): Rs "));
		charges = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+3]);
		clear_lcd_data(LINE2);
		ltoa(charges,lcd_Line2_data,10);
		for (i=0;i<6;i++)
		{
			printer_writeData(lcd_Line2_data[i]);
		}
	}
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<40;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	print_signature_line_type1();
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<40;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	//Print footer
	print_footer_type1();
	PRINTER_FORWARD_PAGE;
}

void F2_print_sleep_type1(unsigned long int rst)
{
	unsigned char i,no_entries;
	signed long int charges,tare,gross;

	no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	print_header_type1();
	PRINTER_ENTER;
	//Print first block for all entries
	//Print RST no first
	printer_writeStringF(PSTR("RST NO      : "));
	//Print RST no
	clear_lcd_data(LINE2);
	itoa(rst,lcd_Line2_data,10);
	for (i=0;i<25;i++)
	{
		printer_writeData(lcd_Line2_data[i]);
	}
	printer_writeData(' ');printer_writeData(' ');
	if (no_entries >= 1)
	//If there is 1 or more entries
	{
		//print 1st entry
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print 1st entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY1+i]);
		}
		PRINTER_ENTER; //Go to next line
	}
	if (no_entries >= 2)
	//print 2nd entry
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print second entry data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY2+i]);
		}
		printer_writeData(' ');
		if (no_entries==2)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 3)
	//entry 3
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 3 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY3+i]);
		}
		PRINTER_ENTER;
	}
	if (no_entries>=4)
	//entry 4
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 4 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY4+i]);
		}
		printer_writeData(' ');
		if (no_entries==4)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 5)
	//entry 5
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 5 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY5+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries >= 6)
	//entry 6
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY6+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 6 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY6+i]);
		}
		printer_writeData(' ');
		if (no_entries==6)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries >= 7)
	//entry 7
	{

		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY7+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 7 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY7+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries>=8)
	//entry 8
	{
		//entry 8
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY8+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 8 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY8+i]);
		}
		printer_writeData(' ');
		if (no_entries==8)
		{
			PRINTER_ENTER;
		}
	}
	if(no_entries>=9)
	//entry 9
	{
		//entry 9
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY9+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 9 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY9+i]);
		}
		PRINTER_ENTER;
	}

	if (no_entries==10)
	//entry 10
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY10+i));
		}
		printer_writeStringF(PSTR(": "));
		//Print entry 10 data
		for (i=0;i<25;i++)
		{
			printer_writeData(SD_data_buf[RST_ENTRY10+i]);
		}
		PRINTER_ENTER;
	}

	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	//print weight
	PRINTER_ENTER;
	if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='G')
	//First Entry has gross weight start with it
	{
		printer_writeStringF(PSTR("GROSS Wt:"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		gross = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
		clear_lcd_data(LINE2);
		ltoa(gross,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		//< Date
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("DATE:"));
		printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
		printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
		printer_writeData('/');
		printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
		printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
		printer_writeData('/');
		printer_writeData('2');//<YYYY
		printer_writeData('0');
		printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
		printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);

		//< Time
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("TIME:"));
		printer_writeData(SD_data_buf[RST_FIRST_TIME]);//<HH
		printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
		printer_writeData(':');
		printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);//<MM
		printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
		PRINTER_ENTER;
		//Print tare weight in next line
		printer_writeStringF(PSTR("TARE Wt :"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		tare = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
		clear_lcd_data(LINE2);
		ltoa(tare,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		//< Date
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("DATE:"));
		printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
		printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
		printer_writeData('/');
		printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
		printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
		printer_writeData('/');
		printer_writeData('2');//<YYYY
		printer_writeData('0');
		printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
		printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);

		//< Time
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("TIME:"));
		printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
		printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
		printer_writeData(':');
		printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<MM
		printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
		PRINTER_ENTER;
		//Print net Weight
		printer_writeStringF(PSTR("NETT Wt :"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		tare = gross - tare;
		clear_lcd_data(LINE2);
		ltoa(tare,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		PRINTER_ENTER;
	}
	else if (SD_data_buf[RST_FIRST_ENTRY_TYPE]=='T')
	//Second Entry has Gross weight start with it
	{
		printer_writeStringF(PSTR("GROSS Wt:"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		gross = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
		clear_lcd_data(LINE2);
		ltoa(gross,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		//< Date
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("DATE:"));
		printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
		printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
		printer_writeData('/');
		printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
		printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
		printer_writeData('/');
		printer_writeData('2');//<YYYY
		printer_writeData('0');
		printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
		printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);

		//< Time
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("TIME:"));
		printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
		printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
		printer_writeData(':');
		printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<MM
		printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
		PRINTER_ENTER;
		//Print tare weight in next line
		printer_writeStringF(PSTR("TARE Wt :"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		tare = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
		clear_lcd_data(LINE2);
		ltoa(tare,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		//< Date
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("DATE:"));
		printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
		printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
		printer_writeData('/');
		printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
		printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
		printer_writeData('/');
		printer_writeData('2');//<YYYY
		printer_writeData('0');
		printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
		printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);

		//< Time
		//< 6-spaces
		for(i=0;i<6;i++)
		{
			printer_writeData(' ');
		}
		printer_writeStringF(PSTR("TIME:"));
		printer_writeData(SD_data_buf[RST_FIRST_TIME]);//<HH
		printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
		printer_writeData(':');
		printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);//<MM
		printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
		PRINTER_ENTER;

		//Print net Weight
		printer_writeStringF(PSTR("NETT Wt :"));
		printer_writeData(' ');
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_SET_BOLD;
		}
		PRINTER_ESC;
		PRINTER_SET_HEADER_FONT;
		tare = gross - tare;
		clear_lcd_data(LINE2);
		ltoa(tare,lcd_Line2_data,10);
		for (i=0;i<(6-strlen(lcd_Line2_data));i++)
		{
			printer_writeData(' ');
		}
		printer_writeString(lcd_Line2_data);
		if (eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL)=='B')
		{
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
		}
		PRINTER_ESC;
		PRINTER_CANCEL_HEADER_FONT;
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		printer_writeData(' ');
		printer_writeData('K');
		printer_writeData('G');
		PRINTER_ENTER;
	}

	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) != '0')
	{
		printer_writeStringF(PSTR("Charges(1): Rs "));
		charges = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_CHARGE1+3]);
		clear_lcd_data(LINE2);
		ltoa(charges,lcd_Line2_data,10);
		for (i=0;i<6;i++)
		{
			printer_writeData(lcd_Line2_data[i]);
		}
		printer_writeData(' ');printer_writeData(' ');
	}
	if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) == '1' || eeprom_mega_ReadByte(ADD_CHARGE_TYPE) == '2')
	{
		printer_writeStringF(PSTR("Charges(2): Rs "));
		charges = ((unsigned long int)SD_data_buf[RST_CHARGE2] << 24)+\
		((unsigned long int)SD_data_buf[RST_CHARGE2+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_CHARGE2+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_CHARGE2+3]);
		clear_lcd_data(LINE2);
		ltoa(charges,lcd_Line2_data,10);
		for (i=0;i<6;i++)
		{
			printer_writeData(lcd_Line2_data[i]);
		}
		printer_writeData(' ');printer_writeData(' ');
	}
	if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=='Y')
	{
		printer_writeStringF(PSTR("Total: Rs "));
		charges = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
		((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
		clear_lcd_data(LINE2);
		ltoa(charges,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
	}
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	print_signature_line_type1();
	PRINTER_ENTER;
	//Draw separator line
	for (i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	//Print footer
	print_footer_type1();
}

void function_operator_login()
//Operator login system
{
	unsigned char i,cmp_str[17],temp[17],l1,op_no=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+NO_OF_OPERATOR),flag=0;
	//Get user input
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Operator Name"),0,0);
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC || fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		for (i=0;i<16;i++)
		{
			temp[i]=lcd_Line2_data[i];
		}
		temp[16]=NULL;
	}
	//Check if user name exists
	for (l1=0;l1<op_no;l1++)
	{
		for (i=0;i<16;i++)
		{
			cmp_str[i]=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*l1)+OPERATOR_NAME+i);
		}
		cmp_str[16]=NULL;
		if(!strcmp(cmp_str, temp))
		{
			//username found Check password
			flag=1;
			clear_lcd_data(LINE_BOTH);
			set_string_in_lcd_dataF(LINE1,PSTR("Oprtor PassWord"),0,0);
			fun_ret=get_password_from_user(lcd_Line2_data,0,0,16,USE_NUMERIC|USE_CHARACTERS,STORE_ALL_DATA);
			if(fun_ret==RETURN_CAUSE_ESC || fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
			else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				for (i=0;i<16;i++)
				{
					temp[i]=lcd_Line2_data[i];
				}
			}
			//GET user password in cmp_str
			for (i=0;i<16;i++)
			{
				cmp_str[i]=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*l1)+OPERATOR_PASS+i);
			}
			if(!strcmp(cmp_str, temp))
			{
				//Authentication successful
				login_type = LOGIN_OPRT;
				operator_no = l1;
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Login Success"));
				_delay_ms(1000);
			}
			else
			{
				//Wrong Password
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Wrong Password"));
				_delay_ms(1000);
				return;
			}
		}
	}
	if (flag==0)
	{
		//Wrong Username
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Wrong Username"));
		_delay_ms(1000);
		return;
	}
}

void function_operator_logout()
{
	if (login_type==LOGIN_NONE)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("No One LoggIn"));
		_delay_ms(1000);
	}
	else
	{
		login_type = LOGIN_NONE;
		operator_no = LOGIN_NONE;
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Logout Success"));
		_delay_ms(1000);
	}
}

void engg_login()
{
	unsigned char cmp_str[17],i;

	if (login_type == LOGIN_ENGG)
	//Don't let double log
	{
		return;
	}
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password :"),0,0);
	fun_ret = get_password_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
	if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}
	for (i=0;i<16;i++)
	//Get current password from EEPROM
	{
		cmp_str[i] = eeprom_mega_ReadByte(ENGG_LOGIN_PASSWORD+i);
	}
	cmp_str[16] = NULL;
	if (!strcmp(cmp_str,lcd_Line2_data))
	//PassWord matches
	{
		login_type = LOGIN_ENGG;
		operator_no = LOGIN_NONE;
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Login Successful"));
		_delay_ms(1000);
	}
	else
	//Password Mismatch
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Wrong Password"));
		_delay_ms(1000);
	}
}

void engg_change_pass()
{
	unsigned char cmp_str[17],pass1[17],pass2[17],i;

	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Current Password"),0,0);
	fun_ret = get_password_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
	if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}
	for (i=0;i<16;i++)
	//Get current password from EEPROM
	{
		cmp_str[i] = eeprom_mega_ReadByte(ENGG_LOGIN_PASSWORD+i);
	}
	cmp_str[16] = NULL;
	if (!strcmp(cmp_str,lcd_Line2_data))
	//PassWord matches
	{
		//Enter New Password
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("New Password"),0,0);
		fun_ret = get_password_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
		if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		for (i=0;i<16;i++)
		{
			pass1[i] = lcd_Line2_data[i];
		}
		pass1[16] = NULL;

		//Verify password
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Verify Password"),0,0);
		fun_ret = get_password_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
		if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		for (i=0;i<16;i++)
		{
			pass2[i] = lcd_Line2_data[i];
		}
		pass2[16] = NULL;

		if (!strcmp(pass1,pass2))
		//Password Verified
		{
			for (i=0;i<16;i++)
			{
				eeprom_mega_WriteByte(ENGG_LOGIN_PASSWORD+i,pass1[i]);
			}
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Password Changed"));
			_delay_ms(1000);
		}
		else
		//Password verification failed
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Pass MissMatch"));
			_delay_ms(1000);
		}
	}
	else
	//Wrong password
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Wrong Password"));
		_delay_ms(1000);
	}
}

void function_F2()
{
	unsigned long int rst;
	unsigned char i,call_pointer=1,array_pointer=0,fun_F2[13];
	signed long int charges1,charges2;
	fun_F2[12]=NULL;

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter RST No."),0,0);
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,7,USE_NUMERIC,STORE_UPTO_POINTER);
	if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}
	//Checks for valid RST input
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		rst = atol(lcd_Line2_data);
		if (rst > MAX_RST_VALUE)
		{
			return;
		}
	}
	SD_mega_ReadSingleBlock(SD_data_buf,rst);
	if (SD_data_buf[RST_ENTRY_IS_OCCUPIED] != 'Y' || SD_data_buf[RST_BOTH_ENTRY_COMPLETED] == 'Y')
	{
		return;
	}
	i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	//Set array to call function
	while(call_pointer<i)
	{
		if (SD_data_buf[RST_ENTRY1_IS_PRESENT+(RST_ENTRY_OFFSET*(call_pointer-1))] == 'Y')
		//If Entry present don't call function
		{
			call_pointer++;
		}
		else
		{
			fun_F2[array_pointer]=call_pointer;
			call_pointer++;
			array_pointer++;
		}
	}
	if (eeprom_mega_ReadByte(ADD_CHARGE_TYPE) == '2')
	{
		fun_F2[array_pointer++]=15;  //Set Charges 2
	}
	fun_F2[array_pointer++]=16;  //Set Second Weight
	fun_F2[array_pointer]=NULL;
	i = strlen(fun_F2);
	array_pointer=0;
	while(array_pointer<i)
	{
		//call appropriate section of F2 function
		fun_ret = function_get_data(fun_F2[array_pointer]);
		if (fun_ret==RETURN_CAUSE_ESC)
		{
			if (array_pointer==0)
			{
				return;
			}
			array_pointer--;
		}
		else
		{
			array_pointer++;
		}
	}
	//P/S/C
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Prnt-Save-Cancel"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C):"),0,0);
	if(get_character_from_user_P(&i,PSTR("PSC"),'S',9,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='C')
	{
		return;
	}
	else if (i=='S' || i=='P')
	{
		//save date here
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_SECOND_DATE] = RTC_data.date[0];
		SD_data_buf[RST_SECOND_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_SECOND_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_SECOND_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_SECOND_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_SECOND_DATE+5] = RTC_data.year[1];

		//< Time
		SD_data_buf[RST_SECOND_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_SECOND_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_SECOND_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_SECOND_TIME+3] = RTC_data.minute[1];

		//< Save Charges Detail
		if (SD_data_buf[RST_CHARGE1_IS_PRESENT]!='Y' && SD_data_buf[RST_CHARGE2_IS_PRESENT]!='Y')
		//set total charge to none / 0
		{
			charges1=0;
			charges2=0;
		}
		else if (SD_data_buf[RST_CHARGE1_IS_PRESENT]!='Y')
		{
			charges1=0;

			charges2 = ((unsigned long int)SD_data_buf[RST_CHARGE2] << 24)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+3]);
		}
		else if (SD_data_buf[RST_CHARGE2_IS_PRESENT]!='Y')
		{
			charges1 = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+3]);

			charges2=0;
		}
		else
		{
			charges1 = ((unsigned long int)SD_data_buf[RST_CHARGE1] << 24)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_CHARGE1+3]);

			charges2 = ((unsigned long int)SD_data_buf[RST_CHARGE2] << 24)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+1] << 16)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+2] << 8)+\
			((unsigned long int)SD_data_buf[RST_CHARGE2+3]);
		}
		charges2 = charges1 + charges2;
		if (charges2 == 0)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='N';
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';
		}
		SD_data_buf[RST_TOTAL_CHARGE] = (unsigned char)(charges2 >> 24);
		SD_data_buf[RST_TOTAL_CHARGE+1] = (unsigned char)(charges2 >> 16);
		SD_data_buf[RST_TOTAL_CHARGE+2] = (unsigned char)(charges2 >> 8);
		SD_data_buf[RST_TOTAL_CHARGE+3] = (unsigned char)(charges2);

		//Save Weight Details
		charges1 = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);

		charges2 = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);

		if (charges2 >= charges1)
		{
			charges2 = charges2 - charges1;
		}
		else
		{
			charges2 = charges1 - charges2;
		}

		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 'Y';
		SD_data_buf[RST_NETWEIGHT] = (unsigned char)(charges2 >> 24);
		SD_data_buf[RST_NETWEIGHT+1] = (unsigned char)(charges2 >> 16);
		SD_data_buf[RST_NETWEIGHT+2] = (unsigned char)(charges2 >> 8);
		SD_data_buf[RST_NETWEIGHT+3] = (unsigned char)(charges2);

		//< both entry complete
		SD_data_buf[RST_BOTH_ENTRY_COMPLETED] = 'Y';

		//< operator name save
		if(login_type==LOGIN_OPRT)
		{
			for (i=0;i<16;i++)
			{
				SD_data_buf[RST_SECOND_OPRATOR+i]=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*operator_no)+OPERATOR_NAME+i);
			}
		}
		SD_data_buf[RST_ENTRY_IS_OCCUPIED] = 'Y';
		//< save entry in sd card
		SD_mega_WriteSingleBlock(SD_data_buf,rst);

		if (i=='P')
		{
			F2_print_sleep_type1(rst);
		}
	}
}

void function_F5()
{
	unsigned long int rst_no;
	signed long int tare,charges;
	unsigned int j;
	unsigned char i,call_pointer=2,array_pointer=0,fun_F5[14];

	//get current RST no
	rst_no=RST_read();
	//clear SD_data_buf for further use
	for (j=0;j<512;j++)
	{
		SD_data_buf[j]=' ';
	}
	//Show RST no on display
	ltoa(rst_no,lcd_Line2_data,10);
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("At RST No"));
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	_delay_ms(1000);
	//First get input for all entries enabled
	i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	//set calling array for wrapper
	fun_F5[array_pointer]=20; //Call F5 vehicle entry
	array_pointer++;
	for (call_pointer=2;call_pointer<=i;call_pointer++)
	{
		fun_F5[array_pointer++]=call_pointer;
	}
	fun_F5[array_pointer]=11; //Charges 1
	array_pointer++;
	fun_F5[array_pointer]=12; //Gross weight
	array_pointer++;
	fun_F5[array_pointer]=17; //Second weight
	array_pointer++;
	fun_F5[array_pointer]=NULL;
	i = strlen(fun_F5);
	array_pointer=0;
	while(array_pointer<i)
	{
		//call appropriate section of F5 function
		fun_ret = function_get_data(fun_F5[array_pointer]);
		if (fun_ret==RETURN_CAUSE_ESC)
		{
			if (array_pointer==0)
			{
				return;
			}
			array_pointer--;
		}
		else
		{
			array_pointer++;
		}
	}
	//Set First Entry as Gross
	SD_data_buf[RST_FIRST_ENTRY_TYPE]='G';

	//P/S/C
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Prnt-Save-Cancel"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C):"),0,0);
	if(get_character_from_user_P(&i,PSTR("PSC"),'S',9,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='C')
	{
		return;
	}
	else if (i=='S' || i=='P')
	{
		//save date here
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_FIRST_DATE] = RTC_data.date[0];
		SD_data_buf[RST_FIRST_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_FIRST_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_FIRST_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_FIRST_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_FIRST_DATE+5] = RTC_data.year[1];

		SD_data_buf[RST_SECOND_DATE] = RTC_data.date[0];
		SD_data_buf[RST_SECOND_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_SECOND_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_SECOND_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_SECOND_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_SECOND_DATE+5] = RTC_data.year[1];

		//< Time
		SD_data_buf[RST_FIRST_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_FIRST_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_FIRST_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_FIRST_TIME+3] = RTC_data.minute[1];

		SD_data_buf[RST_SECOND_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_SECOND_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_SECOND_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_SECOND_TIME+3] = RTC_data.minute[1];

		//< Save Charges Detail
		SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]='Y';   //Set nett charges
		SD_data_buf[RST_TOTAL_CHARGE] = SD_data_buf[RST_CHARGE1];
		SD_data_buf[RST_TOTAL_CHARGE+1] = SD_data_buf[RST_CHARGE1+1];
		SD_data_buf[RST_TOTAL_CHARGE+2] = SD_data_buf[RST_CHARGE1+2];
		SD_data_buf[RST_TOTAL_CHARGE+3] = SD_data_buf[RST_CHARGE1+3];
		//Set second charges to 0
		tare = 0;
		SD_data_buf[RST_CHARGE2_IS_PRESENT] = 'Y';
		SD_data_buf[RST_CHARGE2] = (unsigned char)(tare >> 24);//< MSB
		SD_data_buf[RST_CHARGE2+1] = (unsigned char)(tare >> 16);
		SD_data_buf[RST_CHARGE2+2] = (unsigned char)(tare >> 8);
		SD_data_buf[RST_CHARGE2+3] = (unsigned char)(tare);		//< LSB

		//Save Weight Details
		tare = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);

		charges = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
		((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);

		if (charges >= tare)
		{
			charges = charges - tare;
		}
		else
		{
			charges = tare - charges;
		}
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 'Y';
		SD_data_buf[RST_NETWEIGHT] = (unsigned char)(charges >> 24);
		SD_data_buf[RST_NETWEIGHT+1] = (unsigned char)(charges >> 16);
		SD_data_buf[RST_NETWEIGHT+2] = (unsigned char)(charges >> 8);
		SD_data_buf[RST_NETWEIGHT+3] = (unsigned char)(charges);

		//< both entry complete
		SD_data_buf[RST_BOTH_ENTRY_COMPLETED] = 'Y';

		//< operator name save
		if(login_type==LOGIN_OPRT)
		{
			//save both operator names
			for (i=0;i<16;i++)
			{
				SD_data_buf[RST_FIRST_OPRATOR+i]=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*operator_no)+OPERATOR_NAME+i);
				SD_data_buf[RST_SECOND_OPRATOR+i] = SD_data_buf[RST_FIRST_OPRATOR+i];
			}
		}
		SD_data_buf[RST_ENTRY_IS_OCCUPIED] = 'Y';
		//< save entry in sd card
		SD_mega_WriteSingleBlock(SD_data_buf,rst_no);

		if (i=='P')
		{
			F2_print_sleep_type1(rst_no);
		}
	}
	RST_write(rst_no + 1);
}

unsigned char does_date_match(unsigned long int from_date,unsigned long int to_date,unsigned int from_time,unsigned int to_time,unsigned long int rst)
{
	unsigned long int curr_date;
	unsigned int curr_time;

	SD_mega_ReadSingleBlock(SD_data_buf,rst);
	if (SD_data_buf[RST_ENTRY_IS_OCCUPIED] != 'Y')
	{
		return false;
	}
	//convert given rst block date to long for comparison
	curr_date = (SD_data_buf[RST_FIRST_DATE+4]-'0')*100000UL + \
				(SD_data_buf[RST_FIRST_DATE+5]-'0')*10000UL + \
				(SD_data_buf[RST_FIRST_DATE+2]-'0')*1000UL + \
				(SD_data_buf[RST_FIRST_DATE+3]-'0')*100UL + \
				(SD_data_buf[RST_FIRST_DATE+0]-'0')*10UL + \
				(SD_data_buf[RST_FIRST_DATE+1]-'0')*1UL;

	curr_time = (SD_data_buf[RST_FIRST_TIME+0]-'0')*1000U + \
				(SD_data_buf[RST_FIRST_TIME+1]-'0')*100U + \
				(SD_data_buf[RST_FIRST_TIME+2]-'0')*10U + \
				(SD_data_buf[RST_FIRST_TIME+3]-'0')*1U;

	if (curr_date>=from_date && curr_date<=to_date && curr_time>=from_time && curr_time<= to_time)
	//current date is in the date constraints
	{
		return true;
	}
	else
	//current date is out side the date constraints
	{
		return false;
	}
}

unsigned char does_it_match(unsigned long int rst,unsigned char choice,unsigned char* ip_str,unsigned char f_c_p)
{
	unsigned char i,cmp_str[26];
	SD_mega_ReadSingleBlock(SD_data_buf,rst);  //read this rst entry in temporary buffer

	if (choice == '1')
	//All input
	{
		if(SD_data_buf[RST_ENTRY1_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		//Pass only completed Entries
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		//Pass Only Partial Entry
		{
			return false;
		}
		return true;
	}

	if (choice == '2')
	//Entry 1 compare
	{
		if(SD_data_buf[RST_ENTRY1_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		//Pass only completed Entries
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		//Pass Only Partial Entry
		{
			return false;
		}
		//Any other f_c_p entry let pass through the loop
		for (i=0;i<25;i++)
		{
			cmp_str[i] = SD_data_buf[RST_ENTRY1+i];
		}
		cmp_str[25] = NULL;
		if (!strcmp(cmp_str,ip_str))
		//String is a match
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if (choice == '3')
	//Entry 2 compare
	{
		if(SD_data_buf[RST_ENTRY2_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		{
			return false;
		}
		for (i=0;i<25;i++)
		{
			cmp_str[i] = SD_data_buf[RST_ENTRY2+i];
		}
		cmp_str[25] = NULL;
		if (!strcmp(cmp_str,ip_str))
		//String is a match
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if (choice == '4')
	//Entry 3 compare
	{
		if(SD_data_buf[RST_ENTRY3_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		{
			return false;
		}
		for (i=0;i<25;i++)
		{
			cmp_str[i] = SD_data_buf[RST_ENTRY3+i];
		}
		cmp_str[25] = NULL;
		if (!strcmp(cmp_str,ip_str))
		//String is a match
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if (choice == '5')
	//Entry 4 compare
	{
		if(SD_data_buf[RST_ENTRY4_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		{
			return false;
		}
		for (i=0;i<25;i++)
		{
			cmp_str[i] = SD_data_buf[RST_ENTRY4+i];
		}
		cmp_str[25] = NULL;
		if (!strcmp(cmp_str,ip_str))
		//String is a match
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if (choice == '6')
	//Entry 5 compare
	{
		if(SD_data_buf[RST_ENTRY5_IS_PRESENT] != 'Y')
		//if entry don't exist return false immediately
		{
			return false;
		}
		if (f_c_p == 'C' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'Y')
		{
			return false;
		}
		else if (f_c_p == 'P' && SD_data_buf[RST_BOTH_ENTRY_COMPLETED] != 'N')
		{
			return false;
		}
		for (i=0;i<25;i++)
		{
			cmp_str[i] = SD_data_buf[RST_ENTRY5+i];
		}
		cmp_str[25] = NULL;
		if (!strcmp(cmp_str,ip_str))
		//String is a match
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

void print_footer_summery_report(unsigned long int records,unsigned long int weight,unsigned long int charges)
{
	unsigned char i,j;
	//Set Fonts
	PRINTER_SET_REPORT_FONT;
	//Separator line
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	printer_writeStringF(PSTR("Total   -       "));
	//print Total records
	ltoa(records,lcd_Line2_data,10);
	printer_writeString(lcd_Line2_data);
	j = strlen(lcd_Line2_data);
	printer_writeData(' ');
	for (i=0;i<(27-j);i++)
	{
		printer_writeData('-');
	}
	printer_writeData('>');
	printer_writeData(' ');

	//Print Total weight
	ltoa(weight,lcd_Line2_data,10);
	j = strlen(lcd_Line2_data);
	for (i=0;i<(6-j);i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(lcd_Line2_data);
	//Print separator
	printer_writeData(' ');
	printer_writeData('|');
	printer_writeData(' ');

	//print Total charges
	ltoa(charges,lcd_Line2_data,10);
	j = strlen(lcd_Line2_data);
	for (i=0;i<(6-j);i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(lcd_Line2_data);
	PRINTER_ENTER;
	//Separator line
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ESC;
	PRINTER_CANCLE_REPORT_FONT;
}

void print_footer_general_report(unsigned long int weight,unsigned long int charges)
{
	unsigned char i,j;
	//Set Fonts
	PRINTER_ENTER;
	PRINTER_SET_REPORT_FONT;
	//Separator line
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	printer_writeStringF(PSTR("Total   --------------------->"));
	//Print spaces
	for (i=0;i<92;i++)
	{
		printer_writeData(' ');
	}
	//Print Total weight
	ltoa(weight,lcd_Line2_data,10);
	j = strlen(lcd_Line2_data);
	for (i=0;i<(6-j);i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(lcd_Line2_data);
	//Print separator
	printer_writeData(' ');
	printer_writeData('|');
	printer_writeData(' ');

	//print Total charges
	ltoa(charges,lcd_Line2_data,10);
	j = strlen(lcd_Line2_data);
	for (i=0;i<(6-j);i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(lcd_Line2_data);
	PRINTER_ENTER;
	//Separator line
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ESC;
	PRINTER_CANCLE_REPORT_FONT;
}

void print_header_general_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice)
{
	unsigned char i,no_entries;
	PRINTER_ENTER;
	PRINTER_SET_REPORT_FONT;
	no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);

	for (i=0;i<40;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_DATA+i));
	}

	PRINTER_ENTER;
	if (rst_date == 'R')
	//Print from and To rst line
	{
		printer_writeStringF(PSTR("Report - from RST : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
	}
	else
	//Print from and To date Line
	{
		printer_writeStringF(PSTR("Report - from DATE : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
	}

	PRINTER_ENTER;
	//Last header line
	printer_writeData('F');
	printer_writeData('o');
	printer_writeData('r');
	printer_writeData(' ');
	if (choice == '1')
	//print all
	{
		printer_writeStringF(PSTR("All"));
	}
	else if (choice == '2')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
	}
	else if (choice == '3')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
	}
	else if (choice == '4')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
	}
	else if (choice == '5')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
	}
	else if (choice == '6')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
	}
	printer_writeStringF(PSTR(" Records"));
	PRINTER_ENTER;
	//Start Printing Table Header
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	printer_writeStringF(PSTR(" SNO |   RST  |    DATE    | "));
	if (no_entries >= 1)
	//Print first entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 2)
	//Print second entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 3)
	//Print third entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 4)
	//Print forth entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 5)
	//Print fifth entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}

	//Print weights now
	printer_writeStringF(PSTR("Gr. Kg | Tr. Kg | NET Kg | CHG Rs"));
	//Print separator line
	PRINTER_ENTER;
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
}

void print_header_time_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice)
{
	unsigned char i,no_entries;
	PRINTER_ENTER;
	PRINTER_SET_REPORT_FONT;
	no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);

	for (i=0;i<40;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_DATA+i));
	}

	PRINTER_ENTER;
	if (rst_date == 'R')
	//Print from and To rst line
	{
		printer_writeStringF(PSTR("Report - from RST : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
	}
	else
	//Print from and To date Line
	{
		printer_writeStringF(PSTR("Report - from DATE : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
	}

	PRINTER_ENTER;
	//Last header line
	printer_writeData('F');
	printer_writeData('o');
	printer_writeData('r');
	printer_writeData(' ');
	if (choice == '1')
	//print all
	{
		printer_writeStringF(PSTR("All"));
	}
	else if (choice == '2')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
	}
	else if (choice == '3')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
	}
	else if (choice == '4')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
	}
	else if (choice == '5')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
	}
	else if (choice == '6')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
	}
	printer_writeStringF(PSTR(" Records"));
	PRINTER_ENTER;
	//Start Printing Table Header
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	printer_writeStringF(PSTR(" SNO |   RST   | "));
	if (no_entries >= 1)
	//Print first entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 2)
	//Print second entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	if (no_entries >= 3)
	//Print third entry title
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
	}
	//Print weights now
	printer_writeStringF(PSTR("Gr. Kg |   Date & Time    | Tr. Kg |   Date & Time    | NET Kg | CHG Rs |"));
	//Print separator line
	PRINTER_ENTER;
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
}

void print_header_summery_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice)
{
	unsigned char i;
	PRINTER_ENTER;
	PRINTER_SET_REPORT_FONT;

	if (rst_date == 'R')
	//Print from and To rst line
	{
		printer_writeStringF(PSTR("Summary Report - from RST : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeString(lcd_Line2_data);
	}
	else
	//Print from and To date Line
	{
		printer_writeStringF(PSTR("Summary Report - from DATE : " ));
		ltoa(frm_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
		printer_writeStringF(PSTR(" To  "));
		ltoa(to_rst,lcd_Line2_data,10);
		printer_writeData(lcd_Line2_data[4]);
		printer_writeData(lcd_Line2_data[5]);
		printer_writeData('/');
		printer_writeData(lcd_Line2_data[2]);
		printer_writeData(lcd_Line2_data[3]);
		printer_writeData('/');
		printer_writeData('2');
		printer_writeData('0');
		printer_writeData(lcd_Line2_data[0]);
		printer_writeData(lcd_Line2_data[1]);
	}

	PRINTER_ENTER;
	//Last header line
	printer_writeData('F');
	printer_writeData('o');
	printer_writeData('r');
	printer_writeData(' ');
	if (choice == '1')
	//print all
	{
		printer_writeStringF(PSTR("All"));
	}
	else if (choice == '2')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
		}
	}
	else if (choice == '3')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
		}
	}
	else if (choice == '4')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
		}
	}
	else if (choice == '5')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
		}
	}
	else if (choice == '6')
	//print first entry
	{
		for (i=0;i<12;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i));
		}
	}
	printer_writeStringF(PSTR(" Records"));
	PRINTER_ENTER;
	//Print separator Line
	for (i=0;i<137;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	//Start printing Header line
	for (i=0;i<6;i++)
	{
		printer_writeData(' ');
	}
	printer_writeStringF(PSTR("Records |"));
	for (i = 0; i < 30; ++i)
	{
		printer_writeData(' ');
	}
	printer_writeStringF(PSTR("| NET Kg | CHG Rs |"));
	PRINTER_ENTER;
}

void function_F7()
//Print Report
{
	unsigned char i,report_type,full_smry,rst_date,choice,f_c_p,cmp_str[26],j,no_entries;
	unsigned long int from_rst, to_rst, to_date, from_date,l1,sr_no=1,save_var,sum_charges=0,sum_weight=0,no_records;
	unsigned int from_time,to_time;
	//For date query branch
	unsigned char label_length = 0;
	unsigned char D1=label_length,D2=label_length+1,C1=label_length+2,M1=label_length+3,M2=label_length+4,C2=label_length+5,Y3=label_length+8,Y4=label_length+9;
	unsigned char H1=label_length,H2=label_length+1;

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Gen/Time Report"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(G/T) : "),0,0);
	if(get_character_from_user_P(&report_type,PSTR("GT"),'G',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (report_type == 'G')
	//General report printing ask if full or summery
	{
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Full/smry Report"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(F/S) : "),0,0);
		if(get_character_from_user_P(&full_smry,PSTR("FS"),'F',8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
	}
	else
	//Time report printing always full
	{
		full_smry = 'F';
	}

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("RST/Date Report"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(R/D) : "),0,0);
	if(get_character_from_user_P(&rst_date,PSTR("RD"),'R',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (rst_date == 'R')
	//RST based query
	{
		//Get from_rst number first
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("RST No"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Start : "),0,0);
		fun_ret = get_string_from_user(lcd_Line2_data,8,8,14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		else
		{
			from_rst = atol(lcd_Line2_data);
		}
		//Get to_rst number next
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("RST No"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("End : "),0,0);
		fun_ret = get_string_from_user(lcd_Line2_data,6,6,12,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		else
		{
			to_rst = atol(lcd_Line2_data);
		}

		//Give 6 choices to user to compare from
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_Line1_data[0] = 'A';
		lcd_Line1_data[1] = 'l';
		lcd_Line1_data[2] = 'l';
		lcd_Line1_data[3] = '/';
		lcd_Line1_pointer = 4;
		i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
		if (i >= 1)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 2)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 3)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 4)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 5)
		{
			lcd_Line2_data[0] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5);
			lcd_Line2_data[1] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+1);
			lcd_Line2_data[2] = ' ';

			lcd_Line2_data[3] = '(';
			lcd_Line2_data[4] = '1';
			lcd_Line2_data[5] = '-';
			lcd_Line2_data[6] = '6';
			lcd_Line2_data[7] = ')';
			lcd_Line2_data[8] = ':';
			lcd_Line2_data[9] = ' ';
			if(get_character_from_user_P(&choice,PSTR("123456"),'1',10,LINE2)==RETURN_CAUSE_ESC)
			{
				return;
			}
		}
		else
		{
			lcd_Line2_data[0] = '(';
			lcd_Line2_data[1] = '1';
			lcd_Line2_data[2] = '-';
			lcd_Line2_data[3] = '6';
			lcd_Line2_data[4] = ')';
			lcd_Line2_data[5] = ':';
			lcd_Line2_data[6] = ' ';
			if(get_character_from_user_P(&choice,PSTR("123456"),'1',10,LINE2)==RETURN_CAUSE_ESC)
			{
				return;
			}
		}
		//get string from user to compare
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		if (choice == '2')
		//Entry 1 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '3')
		//Entry 2 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '4')
		//Entry 3 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '5')
		//Entry 4 Compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '6')
		//Entry 5 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		//Check for F / C / P
		set_string_in_lcd_dataF(LINE1,PSTR("Full/Comp/Pend"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("F/C/P : "),0,0);
		if(get_character_from_user_P(&f_c_p,PSTR("FCP"),'F',8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}

		if (report_type == 'G' && full_smry == 'F')
		//Print Full general type report
		{
			//Start printing
			clear_lcd_data(LINE_BOTH);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Printing Wait..."));
			no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
			//Print Report Header
			print_header_general_report(rst_date,from_rst,to_rst,choice);
			//Set font for records
			PRINTER_ESC;
			PRINTER_SET_REPORT_FONT;
			for (l1 = from_rst;l1 <= to_rst;l1++)
			//Loop through all rst queried
			{
				if(does_it_match(l1,choice,cmp_str,f_c_p))
				//Print this RST
				{
					//Print Right justified sr_no first
					ltoa(sr_no,lcd_Line1_data,10);
					j = strlen(lcd_Line1_data);
					for (i = 0; i < (4-j) ; i++)
					//For Right justified
					{
						printer_writeData(' ');
					}
					printer_writeString(lcd_Line1_data);
					//increment Sr_no
					sr_no++;
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');

					//print rst no
					ltoa(l1,lcd_Line1_data,10);
					j = strlen(lcd_Line1_data);
					for (i = 0; i < (7-j) ; i++)
					//For Right justified
					{
						printer_writeData(' ');
					}
					printer_writeString(lcd_Line1_data);
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//print Date
					printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
					printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
					printer_writeData('/');
					printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
					printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
					printer_writeData('/');
					printer_writeData('2');//<YYYY
					printer_writeData('0');
					printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
					printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//Start printing Entries
					if (no_entries >= 1)
					//Entry 1 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY1+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 2)
					//Entry 2 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY2+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 3)
					//Entry 3 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY3+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 4)
					//Entry 4 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY4+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 5)
					//Entry 5 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY5+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}

					//Print weight
					//Gross Weight print
					if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'G')
					//First weight is Gross print First weight
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
					}
					else
					//Second Weight is Gross Print it first
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
					}
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');
					//Tare Weight print
					if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'T')
					//First weight is Tare print First weight
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line1_data);
						}
					}
					else
					//Second Weight is Gross Print it
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
					}
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');
					//Net weight
					if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT] != 'Y')
					{
						printer_writeStringF(PSTR("------"));
						sum_weight = sum_weight + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
						ltoa(save_var,lcd_Line2_data,10);
						//Save Net weight sum in temp variable
						sum_weight = sum_weight + save_var;
						j = strlen(lcd_Line2_data);
						for (i = 0; i < (6-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line2_data);
					}
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//Print Charges
					if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] != 'Y')
					{
						printer_writeStringF(PSTR("------"));
						sum_charges = sum_charges + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
						ltoa(save_var,lcd_Line2_data,10);
						//Save Net weight sum in temp variable
						sum_charges = sum_charges + save_var;
						j = strlen(lcd_Line2_data);
						for (i = 0; i < (6-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line2_data);
					}
				}
			}
			//Print Footer
			print_footer_general_report(sum_weight,sum_charges);
		}
		else if (report_type == 'G' && full_smry == 'S')
		//Print General Summery report
		{
			no_records = 0;
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Printing Wait..."));
			no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
			//Print Report Header
			print_header_summery_report(rst_date,from_rst,to_rst,choice);
			//Set font for records
			PRINTER_ESC;
			PRINTER_SET_REPORT_FONT;
			for (l1 = from_rst;l1 <= to_rst;l1++)
			//Loop through all rst queried
			{
				if(does_it_match(l1,choice,cmp_str,f_c_p))
				{
					no_records++;
					//Net weight
					if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT] != 'Y')
					{
						sum_weight = sum_weight + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
						sum_weight = sum_weight + save_var;
					}

					//Total Charges
					if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] != 'Y')
					{
						sum_charges = sum_charges + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
						sum_charges = sum_charges + save_var;
					}
				}
			}
			print_footer_summery_report(no_records,sum_weight,sum_charges);
		}
		else if (report_type == 'T')
		//Print Time report
		{
			//Start printing
			clear_lcd_data(LINE_BOTH);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Printing Wait..."));
			no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
			//Print Report Header
			print_header_time_report(rst_date,from_rst,to_rst,choice);
			//Set font for records
			PRINTER_ESC;
			PRINTER_SET_REPORT_FONT;
			for (l1 = from_rst;l1 <= to_rst;l1++)
			//Loop through all rst queried
			{
				if(does_it_match(l1,choice,cmp_str,f_c_p))
				//Print this RST
				{
					//Print Right justified sr_no first
					ltoa(sr_no,lcd_Line1_data,10);
					j = strlen(lcd_Line1_data);
					for (i = 0; i < (4-j) ; i++)
					//For Right justified
					{
						printer_writeData(' ');
					}
					printer_writeString(lcd_Line1_data);
					//increment Sr_no
					sr_no++;
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//print rst no
					ltoa(l1,lcd_Line1_data,10);
					j = strlen(lcd_Line1_data);
					for (i = 0; i < (7-j) ; i++)
					//For Right justified
					{
						printer_writeData(' ');
					}
					printer_writeString(lcd_Line1_data);
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//Start printing Entries
					if (no_entries >= 1)
					//Entry 1 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY1+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 2)
					//Entry 2 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY2+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}
					if (no_entries >= 3)
					//Entry 3 print
					{
						for (i=0;i<12;i++)//print only 12 characters
						{
							printer_writeData(SD_data_buf[RST_ENTRY3+i]);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
					}

					//Print weight
					//Gross Weight print
					if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'G')
					//First weight is Gross print First weight
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
							printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_FIRST_TIME]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
							printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_FIRST_TIME]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
					}
					else
					//Second Weight is Gross Print it first
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
							printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
							printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
							printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
							printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
							printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
							printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
					}

					//Tare Weight print
					if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'T')
					//First weight is Tare print First weight
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
							printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_FIRST_TIME]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line1_data);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
							printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_FIRST_TIME]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
							printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
					}
					else
					//Second Weight is Gross Print it
					{
						if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
							printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
							printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
							printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
							//print Date
							printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
							printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
							printer_writeData('/');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
							printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
							printer_writeData('/');
							printer_writeData('2');//<YYYY
							printer_writeData('0');
							printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
							printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
							printer_writeData(' ');
							//Print Time
							printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
							printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
							printer_writeData(':');
							printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
							printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
					}

					//Net weight
					if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT] != 'Y')
					{
						printer_writeStringF(PSTR("------"));
						sum_weight = sum_weight + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
						ltoa(save_var,lcd_Line2_data,10);
						//Save Net weight sum in temp variable
						sum_weight = sum_weight + save_var;
						j = strlen(lcd_Line2_data);
						for (i = 0; i < (6-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line2_data);
					}
					//print field separator
					printer_writeData(' ');
					printer_writeData('|');
					printer_writeData(' ');

					//Print Charges
					if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] != 'Y')
					{
						printer_writeStringF(PSTR("------"));
						sum_charges = sum_charges + 0;
					}
					else
					{
						save_var = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
						((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
						ltoa(save_var,lcd_Line2_data,10);
						//Save Net weight sum in temp variable
						sum_charges = sum_charges + save_var;
						j = strlen(lcd_Line2_data);
						for (i = 0; i < (6-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line2_data);
					}
					printer_writeData(' ');
					printer_writeData('|');
					PRINTER_ENTER;
				}
			}
			//Print Footer
			print_footer_general_report(sum_weight,sum_charges);
		}
	}
	else
	//Date based query
	{
		//get starting date
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("Starting Date:"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("DD/MM/YYYY"));

		//< Set LCD pointers
		lcd_Line2_pointer = label_length;
		lcd_Line2_max = lcd_Line2_pointer+10;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;

		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		while(1)
		//< take user input
		{
			while(1)
			//< get key pressed
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			//< numeric key processing
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					if (lcd_Line2_pointer==C1 || lcd_Line2_pointer==C2)
					{
						lcd_Line2_data[lcd_Line2_pointer]='/';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
			}
			else if(ps2_key.type==SPECIAL_KEY)
			//< special key processing
			{
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				//< backspace checking
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						if (lcd_Line2_pointer==(C1+1) || lcd_Line2_pointer==(C2+1))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				//< enter key processing
				{
					if (lcd_Line2_pointer<lcd_Line2_max)
					{
						return;
					}
					//< store user input temporary
					from_date = (lcd_Line2_data[Y3]-'0')*100000UL + \
								(lcd_Line2_data[Y4]-'0')*10000UL + \
								(lcd_Line2_data[M1]-'0')*1000UL + \
								(lcd_Line2_data[M2]-'0')*100UL + \
								(lcd_Line2_data[D1]-'0')*10UL + \
								(lcd_Line2_data[D2]-'0')*1UL;
					break;
				}
			}
		}

		//Get End date
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("End Date:"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("DD/MM/YYYY"));
		//< Set LCD pointers
		lcd_Line2_pointer = label_length;
		lcd_Line2_max = lcd_Line2_pointer+10;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;

		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		while(1)
		//< take user input
		{
			while(1)
			//< get key pressed
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			//< numeric key processing
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					if (lcd_Line2_pointer==C1 || lcd_Line2_pointer==C2)
					{
						lcd_Line2_data[lcd_Line2_pointer]='/';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
			}
			else if(ps2_key.type==SPECIAL_KEY)
			//< special key processing
			{
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				//< backspace checking
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						if (lcd_Line2_pointer==(C1+1) || lcd_Line2_pointer==(C2+1))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				//< enter key processing
				{
					if (lcd_Line2_pointer<lcd_Line2_max)
					{
						return;
					}
					//< store user input temporary
					to_date = (lcd_Line2_data[Y3]-'0')*100000UL + \
								(lcd_Line2_data[Y4]-'0')*10000UL + \
								(lcd_Line2_data[M1]-'0')*1000UL + \
								(lcd_Line2_data[M2]-'0')*100UL + \
								(lcd_Line2_data[D1]-'0')*10UL + \
								(lcd_Line2_data[D2]-'0')*1UL;
					break;
				}
			}
		}

		//get time from user
		M1=label_length+3;
		M2=label_length+4;

		//Get from_time
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("Start Time:"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("HH/MM"));

		//< hour setting
		lcd_Line2_pointer = label_length;
		lcd_Line2_max = lcd_Line2_pointer+5;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;

		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		while(1)
		//< take user input
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					if (lcd_Line2_pointer==C1)
					{
						lcd_Line2_data[lcd_Line2_pointer]=':';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						if (lcd_Line2_pointer==(C1+1))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					if (lcd_Line2_pointer<lcd_Line2_max)
					{
						return;
					}
					//< store user input temporary
					from_time = (lcd_Line2_data[H1]-'0')*1000U + \
								(lcd_Line2_data[H2]-'0')*100U + \
								(lcd_Line2_data[M1]-'0')*10U + \
								(lcd_Line2_data[M2]-'0')*1U;
					break;
				}
			}
		}

		//Get to_time from user
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_gotoxy1(0);
		lcd_mega_StringF(PSTR("End Time:"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("HH/MM"));

		//< hour setting
		lcd_Line2_pointer = label_length;
		lcd_Line2_max = lcd_Line2_pointer+5;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;

		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		while(1)
		//< take user input
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					if (lcd_Line2_pointer==C1)
					{
						lcd_Line2_data[lcd_Line2_pointer]=':';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						if (lcd_Line2_pointer==(C1+1))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					if (lcd_Line2_pointer<lcd_Line2_max)
					{
						return;
					}
					//< store user input temporary
					to_time = (lcd_Line2_data[H1]-'0')*1000U + \
								(lcd_Line2_data[H2]-'0')*100U + \
								(lcd_Line2_data[M1]-'0')*10U + \
								(lcd_Line2_data[M2]-'0')*1U;
					break;
				}
			}
		}
		//From both date & time query get rst number query
		//Give 6 choices to user to compare from
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_Line1_data[0] = 'A';
		lcd_Line1_data[1] = 'l';
		lcd_Line1_data[2] = 'l';
		lcd_Line1_data[3] = '/';
		lcd_Line1_pointer = 4;
		i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
		if (i >= 1)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 2)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 3)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 4)
		{
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4);
			lcd_Line1_data[lcd_Line1_pointer++] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+1);
			lcd_Line1_data[lcd_Line1_pointer++] = '/';
		}
		if (i >= 5)
		{
			lcd_Line2_data[0] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5);
			lcd_Line2_data[1] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+1);
			lcd_Line2_data[2] = ' ';

			lcd_Line2_data[3] = '(';
			lcd_Line2_data[4] = '1';
			lcd_Line2_data[5] = '-';
			lcd_Line2_data[6] = '6';
			lcd_Line2_data[7] = ')';
			lcd_Line2_data[8] = ':';
			lcd_Line2_data[9] = ' ';
			if(get_character_from_user_P(&choice,PSTR("123456"),'1',10,LINE2)==RETURN_CAUSE_ESC)
			{
				return;
			}
		}
		else
		{
			lcd_Line2_data[0] = '(';
			lcd_Line2_data[1] = '1';
			lcd_Line2_data[2] = '-';
			lcd_Line2_data[3] = '6';
			lcd_Line2_data[4] = ')';
			lcd_Line2_data[5] = ':';
			lcd_Line2_data[6] = ' ';
			if(get_character_from_user_P(&choice,PSTR("123456"),'1',10,LINE2)==RETURN_CAUSE_ESC)
			{
				return;
			}
		}
		//get string from user to compare
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		if (choice == '2')
		//Entry 1 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '3')
		//Entry 2 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '4')
		//Entry 3 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '5')
		//Entry 4 Compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		if (choice == '6')
		//Entry 5 compare
		{
			for (i=0;i<12;i++)
			{
				lcd_Line1_data[i] = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY5+i);
			}
			//Take user input
			fun_ret = get_string_from_user(cmp_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
			if (fun_ret==RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
			{
				return;
			}
		}
		//Check for F / C / P
		set_string_in_lcd_dataF(LINE1,PSTR("Full/Comp/Pend"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("F/C/P : "),0,0);
		if(get_character_from_user_P(&f_c_p,PSTR("FCP"),'F',8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		if (report_type == 'G')
		//Print General Report
		{
			//Start printing
			clear_lcd_data(LINE_BOTH);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Printing Wait..."));
			no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
			//Print Report Header
			print_header_general_report(rst_date,from_date,to_date,choice);
			//Set font for records
			PRINTER_ESC;
			PRINTER_SET_REPORT_FONT;
			//Check all RST entries
			from_rst = 1;
			to_rst = RST_read() - 1;
			for (l1 = from_rst;l1 <= to_rst;l1++)
			//Loop through all rst queried
			{
				if (does_date_match(from_date,to_date,from_time,to_time,l1))
				//Check if rst in current date and time constrain
				{
					if(does_it_match(l1,choice,cmp_str,f_c_p))
					//Print this RST
					{
						//Print Right justified sr_no first
						ltoa(sr_no,lcd_Line1_data,10);
						j = strlen(lcd_Line1_data);
						for (i = 0; i < (4-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line1_data);
						//increment Sr_no
						sr_no++;
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');

						//print rst no
						ltoa(l1,lcd_Line1_data,10);
						j = strlen(lcd_Line1_data);
						for (i = 0; i < (7-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line1_data);
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//print Date
						printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
						printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
						printer_writeData('/');
						printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
						printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
						printer_writeData('/');
						printer_writeData('2');//<YYYY
						printer_writeData('0');
						printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
						printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//Start printing Entries
						if (no_entries >= 1)
						//Entry 1 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY1+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 2)
						//Entry 2 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY2+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 3)
						//Entry 3 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY3+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 4)
						//Entry 4 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY4+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 5)
						//Entry 5 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY5+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}

						//Print weight
						//Gross Weight print
						if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'G')
						//First weight is Gross print First weight
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
							}
						}
						else
						//Second Weight is Gross Print it first
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
							}
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
						//Tare Weight print
						if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'T')
						//First weight is Tare print First weight
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line1_data);
							}
						}
						else
						//Second Weight is Gross Print it
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
							}
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');
						//Net weight
						if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							sum_weight = sum_weight + 0;
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							//Save Net weight sum in temp variable
							sum_weight = sum_weight + save_var;
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//Print Charges
						if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							sum_charges = sum_charges + 0;
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
							ltoa(save_var,lcd_Line2_data,10);
							//Save Net weight sum in temp variable
							sum_charges = sum_charges + save_var;
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
					}
				}
			}
			print_footer_general_report(sum_weight,sum_charges);
		}
		else if (report_type == 'T')
		//Print Time report
		{
			//Start printing
			clear_lcd_data(LINE_BOTH);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Printing Wait..."));
			no_entries = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
			//Print Report Header
			print_header_time_report(rst_date,from_rst,to_rst,choice);
			//Set font for records
			PRINTER_ESC;
			PRINTER_SET_REPORT_FONT;
			for (l1 = from_rst;l1 <= to_rst;l1++)
			//Loop through all rst queried
			{
				if (does_date_match(from_date,to_date,from_time,to_time,l1))
				//Check if rst in current date and time constrain
				{
					if(does_it_match(l1,choice,cmp_str,f_c_p))
					//Print this RST
					{
						//Print Right justified sr_no first
						ltoa(sr_no,lcd_Line1_data,10);
						j = strlen(lcd_Line1_data);
						for (i = 0; i < (4-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line1_data);
						//increment Sr_no
						sr_no++;
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//print rst no
						ltoa(l1,lcd_Line1_data,10);
						j = strlen(lcd_Line1_data);
						for (i = 0; i < (7-j) ; i++)
						//For Right justified
						{
							printer_writeData(' ');
						}
						printer_writeString(lcd_Line1_data);
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//Start printing Entries
						if (no_entries >= 1)
						//Entry 1 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY1+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 2)
						//Entry 2 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY2+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}
						if (no_entries >= 3)
						//Entry 3 print
						{
							for (i=0;i<12;i++)//print only 12 characters
							{
								printer_writeData(SD_data_buf[RST_ENTRY3+i]);
							}
							//print field separator
							printer_writeData(' ');
							printer_writeData('|');
							printer_writeData(' ');
						}

						//Print weight
						//Gross Weight print
						if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'G')
						//First weight is Gross print First weight
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
								printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_FIRST_TIME]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
								printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_FIRST_TIME]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
						}
						else
						//Second Weight is Gross Print it first
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
								printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
								printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
								printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
								printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
								printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
								printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
						}

						//Tare Weight print
						if (SD_data_buf[RST_FIRST_ENTRY_TYPE] == 'T')
						//First weight is Tare print First weight
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
								printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_FIRST_TIME]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_FIRSTWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line1_data);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_FIRST_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_FIRST_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_FIRST_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_FIRST_DATE+4]);
								printer_writeData(SD_data_buf[RST_FIRST_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_FIRST_TIME]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_FIRST_TIME+2]);
								printer_writeData(SD_data_buf[RST_FIRST_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
						}
						else
						//Second Weight is Gross Print it
						{
							if (SD_data_buf[RST_FIRSTWEIGHT_IS_PRESENT] != 'Y')
							{
								printer_writeStringF(PSTR("------"));
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
								printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
								printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
								printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
							else
							{
								save_var = ((unsigned long int)SD_data_buf[RST_SECONDWEIGHT] << 24)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+1] << 16)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+2] << 8)+\
								((unsigned long int)SD_data_buf[RST_SECONDWEIGHT+3]);
								ltoa(save_var,lcd_Line2_data,10);
								j = strlen(lcd_Line2_data);
								for (i = 0; i < (6-j) ; i++)
								//For Right justified
								{
									printer_writeData(' ');
								}
								printer_writeString(lcd_Line2_data);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
								//print Date
								printer_writeData(SD_data_buf[RST_SECOND_DATE]);//<DD
								printer_writeData(SD_data_buf[RST_SECOND_DATE+1]);
								printer_writeData('/');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+2]);//<MM
								printer_writeData(SD_data_buf[RST_SECOND_DATE+3]);
								printer_writeData('/');
								printer_writeData('2');//<YYYY
								printer_writeData('0');
								printer_writeData(SD_data_buf[RST_SECOND_DATE+4]);
								printer_writeData(SD_data_buf[RST_SECOND_DATE+5]);
								printer_writeData(' ');
								//Print Time
								printer_writeData(SD_data_buf[RST_SECOND_TIME]);//<HH
								printer_writeData(SD_data_buf[RST_SECOND_TIME+1]);
								printer_writeData(':');
								printer_writeData(SD_data_buf[RST_SECOND_TIME+2]);//<mm
								printer_writeData(SD_data_buf[RST_SECOND_TIME+3]);
								//print field separator
								printer_writeData(' ');
								printer_writeData('|');
								printer_writeData(' ');
							}
						}

						//Net weight
						if (SD_data_buf[RST_NETWEIGHT_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							sum_weight = sum_weight + 0;
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_NETWEIGHT] << 24)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_NETWEIGHT+3]);
							ltoa(save_var,lcd_Line2_data,10);
							//Save Net weight sum in temp variable
							sum_weight = sum_weight + save_var;
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
						//print field separator
						printer_writeData(' ');
						printer_writeData('|');
						printer_writeData(' ');

						//Print Charges
						if (SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] != 'Y')
						{
							printer_writeStringF(PSTR("------"));
							sum_charges = sum_charges + 0;
						}
						else
						{
							save_var = ((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE] << 24)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+1] << 16)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+2] << 8)+\
							((unsigned long int)SD_data_buf[RST_TOTAL_CHARGE+3]);
							ltoa(save_var,lcd_Line2_data,10);
							//Save Net weight sum in temp variable
							sum_charges = sum_charges + save_var;
							j = strlen(lcd_Line2_data);
							for (i = 0; i < (6-j) ; i++)
							//For Right justified
							{
								printer_writeData(' ');
							}
							printer_writeString(lcd_Line2_data);
						}
						printer_writeData(' ');
						printer_writeData('|');
						PRINTER_ENTER;
					}
				}
			}
			//Print Footer
			print_footer_general_report(sum_weight,sum_charges);
		}
	}
}

void function_F4()
{
	unsigned char i,l1,fun_F4[18],pointer=0;
	unsigned long int rst;

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter RST No."),0,0);
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,7,USE_NUMERIC,STORE_UPTO_POINTER);
	if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}
	//Checks for valid RST input
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		rst = atol(lcd_Line2_data);
		if (rst > MAX_RST_VALUE)
		{
			return;
		}
	}
	SD_mega_ReadSingleBlock(SD_data_buf,rst);
	if (SD_data_buf[RST_ENTRY_IS_OCCUPIED] != 'Y')
	{
		return;
	}
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("View / Print"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("V / P : "),0,0);
	if(get_character_from_user_P(&i,PSTR("VP"),'V',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='P')
	//Print current RST entry
	{
		if (SD_data_buf[RST_BOTH_ENTRY_COMPLETED]=='Y')
		{
			F2_print_sleep_type1(rst);
		}
		else
		{
			F1_print_sleep_type1(rst);
		}
	}
	else
	//Only Show RST Entry
	{
		i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
		//Set up array to call wrapper
		for (l1=1 ; l1<=i ; l1++)
		{
			fun_F4[pointer++]=l1;
		}
		fun_F4[pointer++]=11; //Show Charges 1
		fun_F4[pointer++]=15; //Show Charges 2
		fun_F4[pointer++]=18; //Show First Weight
		if (SD_data_buf[RST_BOTH_ENTRY_COMPLETED]=='Y')
		//Show Second weight only if Both entry done
		{
			fun_F4[pointer++]=19;
		}
		fun_F4[pointer++]=21;  //Show Net Weight
		fun_F4[pointer++]=22;  //Show Total Charges
		fun_F4[pointer]=NULL;
		pointer = 0;
		i = strlen(fun_F4);
		from_where = '4';
		while(pointer<i)
		{
			//call appropriate section of F2 function
			fun_ret = function_get_data(fun_F4[pointer]);
			if (fun_ret==RETURN_CAUSE_ESC)
			{
				if (pointer==0)
				{
					from_where = '1';
					return;
				}
				pointer--;
			}
			else
			{
				pointer++;
			}
		}
	}
	from_where = '1';
}

void function_F6()
{
	unsigned char i,l1,fun_F4[18],pointer=0,ip_str[26];
	unsigned long int rst;
	//Point to last filled rst position
	rst = RST_read() - 1;

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Vehicle No"),0,0);
	//Get user input
	fun_ret = get_string_from_user(ip_str,0,0,25,USE_ALL_KEY,STORE_ALL_DATA);
	if (fun_ret == RETURN_CAUSE_ESC || fun_ret == RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
	{
		return;
	}

FIND_MORE:
	while(rst > 0)
	//Loop through from last rst no. to find a match for user input Veh no.
	{
		if (does_it_match(rst,'2',ip_str,'A'))  //2 for vehicle entry
		{
			break;
		}
		if (rst == 1)
		//Last entry not match so return with error
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("No Match Found"));
			_delay_ms(1000);
			return;
		}
		rst--;
	}
	lcd_mega_ClearDispaly();
	lcd_mega_String(ltoa(rst,lcd_Line2_data,10));
	_delay_ms(1000);
	//rst Found now show it or print it
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("View / Print"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("V / P : "),0,0);
	if(get_character_from_user_P(&i,PSTR("VP"),'V',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='P')
	//Print current RST entry
	{
		if (SD_data_buf[RST_BOTH_ENTRY_COMPLETED]=='Y')
		{
			F2_print_sleep_type1(rst);
		}
		else
		{
			F1_print_sleep_type1(rst);
		}
	}
	else
	//Only Show RST Entry
	{
		i = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
		//Set up array to call wrapper
		for (l1=1 ; l1<=i ; l1++)
		{
			fun_F4[pointer++]=l1;
		}
		fun_F4[pointer++]=11;  //Show Charges 1
		fun_F4[pointer++]=15;  //Show Charges 2
		fun_F4[pointer++]=18;  //Show First Weight
		if (SD_data_buf[RST_BOTH_ENTRY_COMPLETED]=='Y')
		//Show Second Weight only if both entry done
		{
			fun_F4[pointer++]=19;
		}
		fun_F4[pointer++]=21;  //Show Net Weight
		fun_F4[pointer++]=22;  //Show Total Charges
		fun_F4[pointer]=NULL;
		pointer = 0;
		i = strlen(fun_F4);
		from_where = '4';
		while(pointer<i)
		{
			//call appropriate section of F2 function
			fun_ret = function_get_data(fun_F4[pointer]);
			if (fun_ret==RETURN_CAUSE_ESC)
			{
				if (pointer==0)
				{
					from_where = '1';
					return;
				}
				pointer--;
			}
			else
			{
				pointer++;
			}
		}
	}
	//ask user to input next code
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Display Next?"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		goto FIND_MORE;
	}
	from_where = '1';
}

/*
void function_F1()
{
	unsigned int _rst_no;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned int i;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char supplier[13];
	unsigned char material[13];
	unsigned char address[13];
	unsigned char source[13];
	unsigned char is_charge1_present;
	unsigned char charge1[6];
	unsigned char gross_tare;
	unsigned char _weight[7];

	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);

	//< vehicle no.
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	vehicle_no[12]='\0';


	//< Supplier
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		supplier[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	supplier[12]='\0';


	//< Material
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		material[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	material[12]='\0';


	//< Address
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ADDRESS :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		address[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	address[12]='\0';


	//< Source
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		source[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	source[12]='\0';


	//< charge1
	//< check whether charge1 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge1_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));

		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge1_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge1[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge1[5]='\0';
		}
	}
	else
	{
		is_charge1_present=1;//< not present
	}


	//< calculate weight
	_weight[6] = '\0';
	_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_weight[0] = ((weight % 10)) + '0'; //< LSB

	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));

	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	//< GROSS-TARE
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("GROSS/TARE(G/T) :"));
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - GROSS
	lcd_Line2_data[lcd_Line2_pointer] = 'G';
	lcd_mega_SendData('G');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='G' || ps2_key.val=='T'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	gross_tare = lcd_Line2_data[lcd_Line2_pointer];


	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}


	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}

	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
		FUNCTION_F1_CHECK_RST:
		//< retrive RST no
		_rst_no = RST_read();

		if(_rst_no>9999)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Memory is Full"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("ENTER= Overwrite"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ESC  = Back"));
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==SPECIAL_KEY)
					{
						if(ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						else if(ps2_key.val==SPECIAL_KEY_ESC)
						{
							return;
						}
					}
				}
			}
			function_F9();
			goto FUNCTION_F1_CHECK_RST;
		}
		//< create ENTRY
		//< clear sd buffer
		for(i=0;i<512;i++)
		{
			SD_data_buf[i]=0xFF;
		}
		//< entry occupied
		SD_data_buf[RST_ENTRY_TYPE]=0;
		//< vehicle no
		i=0;
		while(vehicle_no[i])
		{
			SD_data_buf[RST_VEHICLE_NO+i]=vehicle_no[i];
			i++;
		}
		//< Supplier
		i=0;
		while(supplier[i])
		{
			SD_data_buf[RST_SUPPLIER+i]=supplier[i];
			i++;
		}
		//< material
		i=0;
		while(material[i])
		{
			SD_data_buf[RST_MATERIAL+i]=material[i];
			i++;
		}
		//< address
		i=0;
		while(address[i])
		{
			SD_data_buf[RST_ADDRESS+i]=address[i];
			i++;
		}
		//< source
		i=0;
		while(source[i])
		{
			SD_data_buf[RST_SOURCE+i]=source[i];
			i++;
		}
		//< charge1
		SD_data_buf[RST_CHARGE1_IS_PRESENT]=is_charge1_present;
		if(!is_charge1_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				SD_data_buf[RST_TOTALCHARGE+i]=charge1[i];
				i++;
			}
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0xFF;
		}

		//< GROSS_TARE
		SD_data_buf[RST_FIRSTENTRYTYPE]=gross_tare;
		if(gross_tare=='G')//< GROSS
		{
			SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_GROSSWEIGHT+i]=_weight[i];
				i++;
			}
		}
		else//< TARE
		{
			SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_TAREWEIGHT+i]=_weight[i];
				i++;
			}
		}

		//< Date and Time
		if(gross_tare=='G')//< GROSS
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
			SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		}
		else //<TARE
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
			SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		}

		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0xFF;

		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);

		//< increment rst_no
		RST_write(_rst_no+1);
	}

	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< retrive entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("RST No. is"));
	lcd_mega_gotoxy2(12);
	lcd_mega_SendData((_rst_no/1000)+'0');
	lcd_mega_SendData(((_rst_no%1000)/100)+'0');
	lcd_mega_SendData(((_rst_no%100)/10)+'0');
	lcd_mega_SendData((_rst_no%10)+'0');
	_delay_ms(1000);
}
*/

/*
void RST_entry_print(unsigned char *_sd_block,unsigned int _rst_no)
{
	unsigned char i;
	unsigned char temp;

	//< check if this related both entry completed or not
	//< if both entry completed
	if(!_sd_block[RST_BOTHENTRY_COMPLETED])
	{
		//< printing header
		//< check header position is top
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='T')
		{
			setting_print_header(0);
		}

		//< set font
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;

		//< RST No. printing
		printer_writeStringF(PSTR("RST NO      : "));
		temp=1;
		if((_rst_no/1000)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no/1000)+'0');
			temp=0;
		}
		if(((_rst_no%1000)/100)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%1000)/100)+'0');
			temp=0;
		}
		if(((_rst_no%100)/10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%100)/10)+'0');
			temp=0;
		}
		printer_writeData((_rst_no%10)+'0');

		//< 16-spaces
		for(i=0;i<16;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Vehicle No. Printing
		printer_writeStringF(PSTR("VEHICLE NO. : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
		}
		PRINTER_ENTER;

		//< Supplier
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SUPPLIER    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SUPPLIER+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Material
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("MATERIAL    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_MATERIAL+i]);
		}
		PRINTER_ENTER;

		//< Address
		printer_writeStringF(PSTR("ADDRESS     : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_ADDRESS+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Source
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SOURCE      : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SOURCE+i]);
		}
		PRINTER_ENTER;

		//< Putting line (------)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< gross weight
		printer_writeStringF(PSTR("GROSS   : "));

		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;

		//< check if gross weight presents
		if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));

		if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
		{
			//< Date
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("DATE:"));
			printer_writeData(_sd_block[RST_GROSS_DATE]);//<DD
			printer_writeData(_sd_block[RST_GROSS_DATE+1]);
			printer_writeData('/');
			printer_writeData(_sd_block[RST_GROSS_DATE+2]);//<MM
			printer_writeData(_sd_block[RST_GROSS_DATE+3]);
			printer_writeData('/');
			printer_writeData('2');//<YYYY
			printer_writeData('0');
			printer_writeData(_sd_block[RST_GROSS_DATE+4]);
			printer_writeData(_sd_block[RST_GROSS_DATE+5]);

			//< Time
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("TIME:"));
			printer_writeData(_sd_block[RST_GROSS_TIME]);//<HH
			printer_writeData(_sd_block[RST_GROSS_TIME+1]);
			printer_writeData(':');
			printer_writeData(_sd_block[RST_GROSS_TIME+2]);//<MM
			printer_writeData(_sd_block[RST_GROSS_TIME+3]);
		}
		PRINTER_ENTER;

		//< TARE weight
		printer_writeStringF(PSTR("TARE    : "));

		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;

		//< check if tare weight presents
		if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));

		if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
		{
			//< Date
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("DATE:"));
			printer_writeData(_sd_block[RST_TARE_DATE]);//<DD
			printer_writeData(_sd_block[RST_TARE_DATE+1]);
			printer_writeData('/');
			printer_writeData(_sd_block[RST_TARE_DATE+2]);//<MM
			printer_writeData(_sd_block[RST_TARE_DATE+3]);
			printer_writeData('/');
			printer_writeData('2');//<YYYY
			printer_writeData('0');
			printer_writeData(_sd_block[RST_TARE_DATE+4]);
			printer_writeData(_sd_block[RST_TARE_DATE+5]);

			//< Time
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("TIME:"));
			printer_writeData(_sd_block[RST_TARE_TIME]);//<HH
			printer_writeData(_sd_block[RST_TARE_TIME+1]);
			printer_writeData(':');
			printer_writeData(_sd_block[RST_TARE_TIME+2]);//<MM
			printer_writeData(_sd_block[RST_TARE_TIME+3]);
		}
		PRINTER_ENTER;

		//< net weight
		printer_writeStringF(PSTR("NET     : "));

		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;

		//< check if net weight presents
		if(!_sd_block[RST_NETWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_NETWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_NETWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));

		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< Charges
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge1 present or not
			if(!_sd_block[RST_CHARGE1_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE1+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}
		else if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge2 present or not
			if(!_sd_block[RST_CHARGE2_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE2+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}
		else if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge1 present or not
			if(!_sd_block[RST_CHARGE1_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE1+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}

			//< Charges2
			printer_writeStringF(PSTR("                "));
			printer_writeStringF(PSTR("Charges(2): Rs  "));
			//< check if charge2 present or not
			if(!_sd_block[RST_CHARGE2_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE2+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}

		printer_writeStringF(PSTR("Operator Signature :"));
		PRINTER_ENTER;
		PRINTER_ENTER;

		//< put line (-----)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< printing header
		//< check header position is bottom
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='B')
		{
			setting_print_header(0);
		}

		//< no.of LINE_FEED after slip print as per ALT_L setting
		for(i=0;i<eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_LINE_FEED);i++)
		{
			PRINTER_ENTER;
		}
	}
	//< if only first entry completed
	else
	{
		//< printing header
		//< check header position is top
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='T')
		{
			setting_print_header(0);
		}

		//< set font
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;

		//< RST No. printing
		//< RST No. printing
		printer_writeStringF(PSTR("RST NO      : "));
		temp=1;
		if((_rst_no/1000)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no/1000)+'0');
			temp=0;
		}
		if(((_rst_no%1000)/100)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%1000)/100)+'0');
			temp=0;
		}
		if(((_rst_no%100)/10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%100)/10)+'0');
			temp=0;
		}
		if((_rst_no%10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no%10)+'0');
			temp=0;
		}

		//< 16-spaces
		for(i=0;i<16;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Vehicle No. Printing
		printer_writeStringF(PSTR("VEHICLE NO. : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
		}
		PRINTER_ENTER;

		//< Supplier
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SUPPLIER    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SUPPLIER+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Material
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("MATERIAL    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_MATERIAL+i]);
		}
		PRINTER_ENTER;

		//< Address
		printer_writeStringF(PSTR("ADDRESS     : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_ADDRESS+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;

		//< Source
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SOURCE      : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SOURCE+i]);
		}
		PRINTER_ENTER;

		//< Putting line (------)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< Gross_Tare weight
		if(_sd_block[RST_FIRSTENTRYTYPE]=='G')
		{
			printer_writeStringF(PSTR("GROSS   : "));

			//< set weight font
			PRINTER_ESC;
			PRINTER_SET_BOLD;

			//< check if gross weight presents
			if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
			{
				temp=1;
				for(i=0;i<6;i++)
				{
					//< leading 0's removing
					if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
					{
						printer_writeData(' ');
					}
					else
					{
						printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
						temp=0;
					}
				}
			}
			else
			{
				printer_writeStringF(PSTR("------"));
			}
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			printer_writeStringF(PSTR(" Kg"));

			if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
			{
				//< Date
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("DATE:"));
				printer_writeData(_sd_block[RST_GROSS_DATE]);//<DD
				printer_writeData(_sd_block[RST_GROSS_DATE+1]);
				printer_writeData('/');
				printer_writeData(_sd_block[RST_GROSS_DATE+2]);//<MM
				printer_writeData(_sd_block[RST_GROSS_DATE+3]);
				printer_writeData('/');
				printer_writeData('2');//<YYYY
				printer_writeData('0');
				printer_writeData(_sd_block[RST_GROSS_DATE+4]);
				printer_writeData(_sd_block[RST_GROSS_DATE+5]);

				//< Time
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("TIME:"));
				printer_writeData(_sd_block[RST_GROSS_TIME]);//<HH
				printer_writeData(_sd_block[RST_GROSS_TIME+1]);
				printer_writeData(':');
				printer_writeData(_sd_block[RST_GROSS_TIME+2]);//<MM
				printer_writeData(_sd_block[RST_GROSS_TIME+3]);
			}
		}
		else
		{
			printer_writeStringF(PSTR("TARE    : "));

			//< set weight font
			PRINTER_ESC;
			PRINTER_SET_BOLD;

			//< check if tare weight presents
			if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
			{
				temp=1;
				for(i=0;i<6;i++)
				{
					//< leading 0's removing
					if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
					{
						printer_writeData(' ');
					}
					else
					{
						printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
						temp=0;
					}
				}
			}
			else
			{
				printer_writeStringF(PSTR("------"));
			}
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			printer_writeStringF(PSTR(" Kg"));

			if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
			{
				//< Date
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("DATE:"));
				printer_writeData(_sd_block[RST_TARE_DATE]);//<DD
				printer_writeData(_sd_block[RST_TARE_DATE+1]);
				printer_writeData('/');
				printer_writeData(_sd_block[RST_TARE_DATE+2]);//<MM
				printer_writeData(_sd_block[RST_TARE_DATE+3]);
				printer_writeData('/');
				printer_writeData('2');//<YYYY
				printer_writeData('0');
				printer_writeData(_sd_block[RST_TARE_DATE+4]);
				printer_writeData(_sd_block[RST_TARE_DATE+5]);

				//< Time
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("TIME:"));
				printer_writeData(_sd_block[RST_TARE_TIME]);//<HH
				printer_writeData(_sd_block[RST_TARE_TIME+1]);
				printer_writeData(':');
				printer_writeData(_sd_block[RST_TARE_TIME+2]);//<MM
				printer_writeData(_sd_block[RST_TARE_TIME+3]);
			}
		}

		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< Charges1
		printer_writeStringF(PSTR("Charges(1): Rs  "));
		//< check if charge1 present or not
		if(!_sd_block[RST_CHARGE1_IS_PRESENT])
		{
			for(i=0;i<5;i++)
			{
				printer_writeData(_sd_block[RST_CHARGE1+i]);
			}
		}
		else
		{
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData('-');
			printer_writeData('-');
			printer_writeData('-');
		}

		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		printer_writeStringF(PSTR("Operator Signature :"));
		PRINTER_ENTER;
		PRINTER_ENTER;

		//< put line (-----)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;

		//< printing header
		//< check header position is bottom
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='B')
		{
			setting_print_header(0);
		}

		//< no.of LINE_FEED after slip print as per ALT_L setting
		for(i=0;i<eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_LINE_FEED);i++)
		{
			PRINTER_ENTER;
		}
	}
}

void Report_entry_print(unsigned char *_sd_block,unsigned int _rst_no)
{
	unsigned char temp;
	unsigned char i;

	char str[8];

	//< set font
	PRINTER_ESC;
	PRINTER_SET_REPORT_FONT;

	//< RST NO. printing
	utoa(_rst_no,str,10);
	temp = strlen(str);
	for(i=0;i<4-temp;i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(str);
	printer_writeStringF(PSTR(" |"));

	//< Date Printing
	//< if first entry is gross entry then
	if(_sd_block[RST_FIRSTENTRYTYPE]=='G')
	{
		//< date
		printer_writeData(_sd_block[RST_GROSS_DATE]);
		printer_writeData(_sd_block[RST_GROSS_DATE+1]);
		//< month
		printer_writeData('/');
		printer_writeData(_sd_block[RST_GROSS_DATE+2]);
		printer_writeData(_sd_block[RST_GROSS_DATE+3]);
		//< year
		printer_writeStringF(PSTR("/20"));
		printer_writeData(_sd_block[RST_GROSS_DATE+4]);
		printer_writeData(_sd_block[RST_GROSS_DATE+5]);
	}
	//< if first entry is tare entry then
	else
	{
		//< date
		printer_writeData(_sd_block[RST_TARE_DATE]);
		printer_writeData(_sd_block[RST_TARE_DATE+1]);
		//< month
		printer_writeData('/');
		printer_writeData(_sd_block[RST_TARE_DATE+2]);
		printer_writeData(_sd_block[RST_TARE_DATE+3]);
		//< year
		printer_writeStringF(PSTR("/20"));
		printer_writeData(_sd_block[RST_TARE_DATE+4]);
		printer_writeData(_sd_block[RST_TARE_DATE+5]);
	}
	printer_writeStringF(PSTR(" |"));

	//< vehicle no.
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
	}
	printer_writeStringF(PSTR(" |"));

	//< Supplier
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_SUPPLIER+i]);
	}
	printer_writeStringF(PSTR(" |"));

	//< Material
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_MATERIAL+i]);
	}
	printer_writeStringF(PSTR(" |"));

	//< Address
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_ADDRESS+i]);
	}
	printer_writeStringF(PSTR(" |"));

	//< SOURCE
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_SOURCE+i]);
	}
	printer_writeStringF(PSTR(" |"));

	//< GROSS WEIGHT
	if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));

	//< TARE weight
	if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));

	//< NET weight
	if(!_sd_block[RST_BOTHENTRY_COMPLETED])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_NETWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_NETWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));

	//< Charges
	//< check if total charge present or not
	if(!_sd_block[RST_TOTALCHARGE_IS_PRESENT])
	{
		temp=0;
		while(temp!=5)
		{
			if(SD_data_buf[RST_TOTALCHARGE+temp]==' ')
			{
				break;
			}
			temp++;
		}
		for(i=0;i<5-temp;i++)
		{
			printer_writeData(' ');
		}
		for(i=0;i<temp;i++)
		{
			printer_writeData(_sd_block[RST_TOTALCHARGE+i]);
		}
	}
	else
	{
		printer_writeStringF(PSTR("     "));
	}
	printer_writeStringF(PSTR(" |"));

	PRINTER_ENTER;
}

void function_F1()
{
	unsigned int _rst_no;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned int i;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char supplier[13];
	unsigned char material[13];
	unsigned char address[13];
	unsigned char source[13];
	unsigned char is_charge1_present;
	unsigned char charge1[6];
	unsigned char gross_tare;
	unsigned char _weight[7];

	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);

	//< vehicle no.
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	vehicle_no[12]='\0';


	//< Supplier
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		supplier[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	supplier[12]='\0';


	//< Material
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		material[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	material[12]='\0';


	//< Address
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ADDRESS :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		address[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	address[12]='\0';


	//< Source
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		source[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	source[12]='\0';


	//< charge1
	//< check whether charge1 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge1_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));

		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge1_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge1[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge1[5]='\0';
		}
	}
	else
	{
		is_charge1_present=1;//< not present
	}


	//< calculate weight
	_weight[6] = '\0';
	_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_weight[0] = ((weight % 10)) + '0'; //< LSB

	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));

	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	//< GROSS-TARE
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("GROSS/TARE(G/T) :"));
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - GROSS
	lcd_Line2_data[lcd_Line2_pointer] = 'G';
	lcd_mega_SendData('G');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='G' || ps2_key.val=='T'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	gross_tare = lcd_Line2_data[lcd_Line2_pointer];


	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}


	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}

	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
FUNCTION_F1_CHECK_RST:
		//< retrive RST no
		_rst_no = RST_read();

		if(_rst_no>9999)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Memory is Full"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("ENTER= Overwrite"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ESC  = Back"));
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==SPECIAL_KEY)
					{
						if(ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						else if(ps2_key.val==SPECIAL_KEY_ESC)
						{
							return;
						}
					}
				}
			}
			function_F9();
			goto FUNCTION_F1_CHECK_RST;
		}
		//< create ENTRY
		//< clear sd buffer
		for(i=0;i<512;i++)
		{
			SD_data_buf[i]=0xFF;
		}
		//< entry occupied
		SD_data_buf[RST_ENTRY_TYPE]=0;
		//< vehicle no
		i=0;
		while(vehicle_no[i])
		{
			SD_data_buf[RST_VEHICLE_NO+i]=vehicle_no[i];
			i++;
		}
		//< Supplier
		i=0;
		while(supplier[i])
		{
			SD_data_buf[RST_SUPPLIER+i]=supplier[i];
			i++;
		}
		//< material
		i=0;
		while(material[i])
		{
			SD_data_buf[RST_MATERIAL+i]=material[i];
			i++;
		}
		//< address
		i=0;
		while(address[i])
		{
			SD_data_buf[RST_ADDRESS+i]=address[i];
			i++;
		}
		//< source
		i=0;
		while(source[i])
		{
			SD_data_buf[RST_SOURCE+i]=source[i];
			i++;
		}
		//< charge1
		SD_data_buf[RST_CHARGE1_IS_PRESENT]=is_charge1_present;
		if(!is_charge1_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				SD_data_buf[RST_TOTALCHARGE+i]=charge1[i];
				i++;
			}
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0xFF;
		}

		//< GROSS_TARE
		SD_data_buf[RST_FIRSTENTRYTYPE]=gross_tare;
		if(gross_tare=='G')//< GROSS
		{
			SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_GROSSWEIGHT+i]=_weight[i];
				i++;
			}
		}
		else//< TARE
		{
			SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_TAREWEIGHT+i]=_weight[i];
				i++;
			}
		}

		//< Date and Time
		if(gross_tare=='G')//< GROSS
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
			SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		}
		else //<TARE
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
			SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		}

		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0xFF;

		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);

		//< increment rst_no
		RST_write(_rst_no+1);
	}

	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< retrive entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("RST No. is"));
	lcd_mega_gotoxy2(12);
	lcd_mega_SendData((_rst_no/1000)+'0');
	lcd_mega_SendData(((_rst_no%1000)/100)+'0');
	lcd_mega_SendData(((_rst_no%100)/10)+'0');
	lcd_mega_SendData((_rst_no%10)+'0');
	_delay_ms(1000);
}

void function_F2()
{
	unsigned int _rst_no;
	unsigned char i;
	unsigned char is_charge2_present;
	unsigned char charge2[6];
	unsigned char _weight[7];
	unsigned int _charge1=0;
	unsigned int _charge2=0;
	unsigned int _tot_charge=0;
	unsigned char temp;
	unsigned char str[7];
	unsigned long int weight_grosstare;
	unsigned long int net_weight;

	//< ASK for RST No.
FUNCTION_F2_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));

	set_string_in_lcd_dataF(LINE2,PSTR("2nd Entry) : "),0,0);
	lcd_Line2_pointer = 13;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}

	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F2_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
			case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
			case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
			case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
		}

		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

		//< check this RST entry is valid or not
		if(SD_data_buf[RST_ENTRY_TYPE]!=0)
		{
			//< un occupied entry
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("RST No. Not Found!"));
			_delay_ms(1000);
			goto FUNCTION_F2_GET_RST;
		}

		if(SD_data_buf[RST_BOTHENTRY_COMPLETED]==0)
		{
			//< both entry is completed
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("3rd Entry Invalid"));
			_delay_ms(1000);
			goto FUNCTION_F2_GET_RST;
		}
	}

	//< now we have RST no. and related entry (tested)
	//< display vehicle no. from first entry
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_VEHICLE_NO+i]);
	}

	//< wait here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.val==SPECIAL_KEY_ENTER && ps2_key.type==SPECIAL_KEY)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	//< charge2
	//< check whether charge2 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge2_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));

		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge2_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge2[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge2[5]='\0';
		}
	}
	else
	{
		is_charge2_present=1;//< not present
	}

	//< calculate weight
	_weight[6] = '\0';
	_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_weight[0] = ((weight % 10)) + '0'; //< LSB

	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));

	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}


	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}


	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
		//< charge2
		SD_data_buf[RST_CHARGE2_IS_PRESENT]=is_charge2_present;
		if(!is_charge2_present)
		{
			i=0;
			while(charge2[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge2[i];
				i++;
			}
		}
		//< set total charge
		//< if both charge not exists
		if(SD_data_buf[RST_CHARGE1_IS_PRESENT] && SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge not present
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0xFF;
		}
		//< if only charge1 exists
		else if(!SD_data_buf[RST_CHARGE1_IS_PRESENT] && SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge1
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0;
			for(i=0;i<5;i++)
			{
				SD_data_buf[RST_TOTALCHARGE] = SD_data_buf[RST_CHARGE1];
			}
		}
		//< if only charge2 exists
		else if(SD_data_buf[RST_CHARGE1_IS_PRESENT] && !SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge2
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0;
			for(i=0;i<5;i++)
			{
				SD_data_buf[RST_TOTALCHARGE] = SD_data_buf[RST_CHARGE2];
			}
		}
		//< if both charge exist
		else if(!SD_data_buf[RST_CHARGE1_IS_PRESENT] && !SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge1 + charge2
			//< calculating charge1
			temp=0;
			while(temp!=5)
			{
				if(SD_data_buf[RST_CHARGE1+temp]==' ')
				{
					break;
				}
				temp++;
			}
			switch(temp)
			{
				case 1:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0');
					break;
				case 2:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0');
					break;
				case 3:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
				case 4:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 1000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
				case 5:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 10000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 1000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
			}
			//< calculating charge2
			temp=0;
			while(temp!=5)
			{
				if(SD_data_buf[RST_CHARGE2+temp]==' ')
				{
					break;
				}
				temp++;
			}
			switch(temp)
			{
				case 1:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0');
					break;
				case 2:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0');
					break;
				case 3:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
				case 4:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 1000 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 100 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 10 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
				case 5:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 10000 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 1000 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 100 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 10 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
			}
			_tot_charge = _charge1 + _charge2;
			utoa(_tot_charge,str,10);
			temp=1;
			for(i=0;i<5;i++)
			{
				if(str[i]=='\0')
				{
					temp=0;
				}

				if(temp)
				{
					SD_data_buf[RST_TOTALCHARGE+i] = str[i];
				}
				else
				{
					SD_data_buf[RST_TOTALCHARGE+i] = ' ';
				}
			}
		}

		//< gross_tare weight
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< now TARE weight
			SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_TAREWEIGHT+i]=_weight[i];
				i++;
			}
		}
		else//< TARE aleready done
		{
			//< now gross weight
			SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_GROSSWEIGHT+i]=_weight[i];
				i++;
			}
		}
		//< NET WEIGHT
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< then retrive GROSS weight
			weight_grosstare = (unsigned long int)(SD_data_buf[RST_GROSSWEIGHT]-'0') + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+1]-'0') * 10 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+2]-'0') * 100 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+3]-'0') * 1000 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+4]-'0') * 10000 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+5]-'0') * 100000;
			net_weight = weight_grosstare - weight;
		}
		else//< TARE aleready done
		{
			//< then retrive TARE weight
			weight_grosstare = (unsigned long int)(SD_data_buf[RST_TAREWEIGHT]-'0') + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+1]-'0') * 10 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+2]-'0') * 100 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+3]-'0') * 1000 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+4]-'0') * 10000 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+5]-'0') * 100000;
			net_weight = weight - weight_grosstare;
		}
		//< store net weight
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 0;
		SD_data_buf[RST_NETWEIGHT+5] = ((unsigned char)(net_weight / 100000UL)) + '0'; //< MSB
		SD_data_buf[RST_NETWEIGHT+4] = ((unsigned char)((net_weight % 100000UL) / 10000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+3] = ((unsigned char)((net_weight % 10000UL) / 1000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+2] = ((unsigned char)((net_weight % 1000UL) / 100UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+1] = ((unsigned char)((net_weight % 100UL) / 10UL)) + '0';
		SD_data_buf[RST_NETWEIGHT] = ((net_weight % 10)) + '0'; //< LSB

		//< Date and Time
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< then stor tare date
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
			SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		}
		else
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
			SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];

			//< Time
			SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		}


		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0;

		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
	}

	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}
}

unsigned char function_F3(unsigned char flag)
{
	unsigned int _rst_no;
	unsigned char i;
	unsigned char temp;
	unsigned char str[8];

	//< ASK for RST No.
FUNCTION_F3_GET_RST:
	if(flag)
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("Enter RST No. (For"));

		set_string_in_lcd_dataF(LINE2,PSTR("Entry View) : "),0,0);
		lcd_Line2_pointer = 14;
		lcd_Line2_max = lcd_Line2_pointer+4;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return 1;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Invalid RST No."));
			_delay_ms(1000);
			goto FUNCTION_F3_GET_RST;
		}
		//< if user has inputed RST no. then
		else
		{
			//< create RST no from user input
			switch(lcd_Line2_pointer-lcd_Line2_min)
			{
				case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
				case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
				case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
				case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
			}

			//< read RST entry
			SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

			//< check this RST entry is valid or not
			if(SD_data_buf[RST_ENTRY_TYPE]!=0)
			{
				//< un occupied entry
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("RST No. Not Found!"));
				_delay_ms(1000);
				goto FUNCTION_F3_GET_RST;
			}
		}

		//< now we have RST_no and its related entry
		//< show RST_no
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("At RST No. :"));
		lcd_mega_gotoxy2(13);
		lcd_mega_String(utoa(_rst_no,str,10));

		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}
	}

	//< show Vehicle No
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_VEHICLE_NO+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	//< show supplier
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_SUPPLIER+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	//< show material
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_MATERIAL+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	//< show address
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("ADDRESS :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_ADDRESS+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	//< show source
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_SOURCE+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	//< weight
	//< if both entry completed
	//< then show GROSS,TARE,NET weight
	if(!SD_data_buf[RST_BOTHENTRY_COMPLETED])
	{
		//< GROSS weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("GROSS WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_GROSSWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}

		//< TARE weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("TARE WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_TAREWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_TAREWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}

		//< NET weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("NET WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_NETWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_NETWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
	}
	//< if one entry is completed
	//< then show only GROSS or TERE entry
	else
	{
		//< if GROSS
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')
		{
			//< GROSS weight
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("GROSS WEIGHT :"));
			lcd_mega_gotoxy2(0);
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && SD_data_buf[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
				{
					lcd_mega_SendData(' ');
				}
				else
				{
					lcd_mega_SendData(SD_data_buf[RST_GROSSWEIGHT+5-i]);
					temp=0;
				}
			}
			lcd_mega_SendData(' ');
			lcd_mega_StringF(PSTR("KG"));
		}
		//< if TARE
		else
		{
			//< TARE weight
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("TARE WEIGHT :"));
			lcd_mega_gotoxy2(0);
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && SD_data_buf[RST_TAREWEIGHT+5-i]=='0' && i!=5)
				{
					lcd_mega_SendData(' ');
				}
				else
				{
					lcd_mega_SendData(SD_data_buf[RST_TAREWEIGHT+5-i]);
					temp=0;
				}
			}
			lcd_mega_SendData(' ');
			lcd_mega_StringF(PSTR("KG"));
		}
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}

	return 0;
}

void function_F4()
{
	unsigned int _rst_no;

	//< ASK for RST No.
FUNCTION_F4_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));

	set_string_in_lcd_dataF(LINE2,PSTR("Printing) : "),0,0);
	lcd_Line2_pointer = 12;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}

	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F4_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
			case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
			case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
			case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
		}

		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

		//< check this RST entry is valid or not
		if(SD_data_buf[RST_ENTRY_TYPE]!=0)
		{
			//< un occupied entry
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("RST No. Not Found!"));
			_delay_ms(1000);
			goto FUNCTION_F4_GET_RST;
		}
	}

	//< now we have RST No. and its related entry
	//< print entry
	RST_entry_print(SD_data_buf,_rst_no);
}

void function_F5()
{
	unsigned int _rst_no;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned int i;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char supplier[13];
	unsigned char material[13];
	unsigned char address[13];
	unsigned char source[13];
	unsigned char is_charge1_present=0xff;
	unsigned char charge1[6];
	unsigned char is_charge2_present=0xff;
	unsigned char charge2[6];
	unsigned char _gross_weight[7];
	unsigned char _tare_weight[7];
	unsigned long int tare_weight;
	unsigned long int net_weight;


	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);

	//< vehicle no.
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	vehicle_no[12]='\0';


	//< Supplier
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		supplier[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	supplier[12]='\0';


	//< Material
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		material[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	material[12]='\0';


	//< Address
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ADDRESS :"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		address[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	address[12]='\0';


	//< Source
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}

	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		source[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	source[12]='\0';

	//< charge1
	//< check whether charge1 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge1_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));

		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge1_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge1[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge1[5]='\0';
		}
	}
	else
	{
		is_charge1_present=1;//< not present
	}

	//< charge2
	//< check whether charge2 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S')
	{
		is_charge2_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));

		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}

		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge2_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge2[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge2[5]='\0';
		}
	}
	else
	{
		is_charge2_present=1;//< not present
	}

	//< calculate gross weight
	_gross_weight[6] = '\0';
	_gross_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_gross_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_gross_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_gross_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_gross_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_gross_weight[0] = ((weight % 10)) + '0'; //< LSB

	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _gross_weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_gross_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));

	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	//< MANUAL TARE INPUT
FUNCTION_F5_MANUAL_TARE:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter TARE WEIGHT :"));

	set_string_in_lcd_dataF(LINE2,PSTR("(In KG) : "),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+6;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}

	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid Entry"));
		_delay_ms(1000);
		goto FUNCTION_F5_MANUAL_TARE;
	}
	else
	{
		for(i=0;i<7;i++)
		{
			_tare_weight[i]='\0';
		}
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_pointer;i++)
		{
			_tare_weight[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		tare_weight=atol(_tare_weight);
		//< calculate gross weight
		_tare_weight[6] = '\0';
		_tare_weight[5] = ((unsigned char)(tare_weight / 100000UL)) + '0'; //< MSB
		_tare_weight[4] = ((unsigned char)((tare_weight % 100000UL) / 10000UL)) + '0';
		_tare_weight[3] = ((unsigned char)((tare_weight % 10000UL) / 1000UL)) + '0';
		_tare_weight[2] = ((unsigned char)((tare_weight % 1000UL) / 100UL)) + '0';
		_tare_weight[1] = ((unsigned char)((tare_weight % 100UL) / 10UL)) + '0';
		_tare_weight[0] = ((tare_weight % 10)) + '0'; //< LSB
	}

	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}


	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}

	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
		//< retrive RST no
FUNCTION_F5_CHECK_RST:
		_rst_no = RST_read();

		if(_rst_no>9999)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Memory is Full"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("ENTER= Overwrite"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ESC  = Back"));
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==SPECIAL_KEY)
					{
						if(ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						else if(ps2_key.val==SPECIAL_KEY_ESC)
						{
							return;
						}
					}
				}
			}
			function_F9();
			goto FUNCTION_F5_CHECK_RST;
		}
		//< create ENTRY
		//< clear sd buffer
		for(i=0;i<512;i++)
		{
			SD_data_buf[i]=0xFF;
		}
		//< entry occupied
		SD_data_buf[RST_ENTRY_TYPE]=0;
		//< vehicle no
		i=0;
		while(vehicle_no[i])
		{
			SD_data_buf[RST_VEHICLE_NO+i]=vehicle_no[i];
			i++;
		}
		//< Supplier
		i=0;
		while(supplier[i])
		{
			SD_data_buf[RST_SUPPLIER+i]=supplier[i];
			i++;
		}
		//< material
		i=0;
		while(material[i])
		{
			SD_data_buf[RST_MATERIAL+i]=material[i];
			i++;
		}
		//< address
		i=0;
		while(address[i])
		{
			SD_data_buf[RST_ADDRESS+i]=address[i];
			i++;
		}
		//< source
		i=0;
		while(source[i])
		{
			SD_data_buf[RST_SOURCE+i]=source[i];
			i++;
		}
		//< charge1
		SD_data_buf[RST_CHARGE1_IS_PRESENT]=is_charge1_present;
		if(!is_charge1_present)
		{
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				i++;
			}
		}
		//< charge2
		SD_data_buf[RST_CHARGE2_IS_PRESENT]=is_charge2_present;
		if(!is_charge2_present)
		{
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				i++;
			}
		}
		//< total charge
		if(!is_charge1_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_TOTALCHARGE+i]=charge1[i];
				i++;
			}
		}
		else if(!is_charge2_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge2[i])
			{
				SD_data_buf[RST_TOTALCHARGE+i]=charge2[i];
				i++;
			}
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0xFF;
		}
		//< GROSS_TARE weight
		SD_data_buf[RST_FIRSTENTRYTYPE]='G';
		//< GROSS
		SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
		i=0;
		while(_gross_weight[i])
		{
			SD_data_buf[RST_GROSSWEIGHT+i]=_gross_weight[i];
			i++;
		}
		//< TARE
		SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
		i=0;
		while(_tare_weight[i])
		{
			SD_data_buf[RST_TAREWEIGHT+i]=_tare_weight[i];
			i++;
		}
		//< NET Weight
		net_weight = weight - tare_weight;
		//< store net weight
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 0;
		SD_data_buf[RST_NETWEIGHT+5] = ((unsigned char)(net_weight / 100000UL)) + '0'; //< MSB
		SD_data_buf[RST_NETWEIGHT+4] = ((unsigned char)((net_weight % 100000UL) / 10000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+3] = ((unsigned char)((net_weight % 10000UL) / 1000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+2] = ((unsigned char)((net_weight % 1000UL) / 100UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+1] = ((unsigned char)((net_weight % 100UL) / 10UL)) + '0';
		SD_data_buf[RST_NETWEIGHT] = ((net_weight % 10)) + '0'; //< LSB

		//< Date and Time
		//< GROSS
		//< date
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
		SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];
		//< Time
		SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];

		//< TARE
		//< Date
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
		SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];

		//< Time
		SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];

		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0;

		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);

		//< increment rst_no
		RST_write(_rst_no+1);
	}

	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< retrive entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);

		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("RST No. is"));
	lcd_mega_gotoxy2(12);
	lcd_mega_SendData((_rst_no/1000)+'0');
	lcd_mega_SendData(((_rst_no%1000)/100)+'0');
	lcd_mega_SendData(((_rst_no%100)/10)+'0');
	lcd_mega_SendData((_rst_no%10)+'0');
	_delay_ms(1000);

}

void function_F6()
{
	unsigned char code_enable;
	unsigned char code_len;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char _vehicle_no[13];
	unsigned int i;
	unsigned char j;
	unsigned int _rst_no;
	unsigned char str[8];

	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);

	//< ASK for Vehicle no
FUNCTION_F6_GET_VEHICLE_NO:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter VEHICLE NO.(To"));

	set_string_in_lcd_dataF(LINE2,PSTR("Search):"),0,0);
	lcd_Line2_pointer = 8;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}

			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}

	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid VEHICLE NO."));
		_delay_ms(1000);
		goto FUNCTION_F6_GET_VEHICLE_NO;
	}
	//< if user has inputed vehicle no. then
	else
	{
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
		{
			vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		vehicle_no[12]='\0';
		//< remove space from vehicle no
		remove_space_from_string(vehicle_no);
	}

	//< retrive current RST no.
	_rst_no = RST_read();
	if(_rst_no>9999)
	{
		_rst_no = 9999;
	}
	_vehicle_no[12]='\0';

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Searching..."));
	//< search RST entry using Vehicle no.
	for(i=0;i<=_rst_no;i++)
	{
		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,i);
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}

		//< check this entry is occupid or not
		if(!SD_data_buf[RST_ENTRY_TYPE])
		{
			//< its occupied
			//< retrive _vehicle no
			for(j=0;j<12;j++)
			{
				_vehicle_no[j] = SD_data_buf[RST_VEHICLE_NO+j];
			}
			//< remove space from _vehicle no
			remove_space_from_string(_vehicle_no);
			//< compare two vehicle no
			if(!strcmp(vehicle_no,_vehicle_no))
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Entry Found..."));
				lcd_mega_gotoxy2(0);
				lcd_mega_StringF(PSTR("At RST No.: "));
				lcd_mega_String(utoa(i,str,10));
				//< stay here till user not pressed ENTER
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
						{
							return;
						}
					}
				}
				//< if both string same then show this RST entry
				if(function_F3(0))
				{
					return;
				}
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Searching..."));
			}
		}
	}
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Entry Not Found"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Search is Over"));
	_delay_ms(1000);
}

static void remove_space_from_string(unsigned char *str)
{
	unsigned char i=0,j=0;

	while(str[i])
	{
		//< if space found
		if(str[i]==' ')
		{
			j=0;
			while(str[i+j])
			{
				str[i+j]=str[i+j+1];
				j++;
			}
		}
		else
		{
			i++;
		}
	}
}

void function_F7()
{
	unsigned char report_type;
	unsigned char report_variable;
	unsigned int from_rst;
	unsigned int to_rst;
	unsigned long int from_date;
	unsigned long int to_date;
	unsigned long int temp_date;
	unsigned char _find_str[13];
	unsigned char _temp_str[13];
	unsigned int i;
	unsigned char temp;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned long int _net_weight;
	unsigned long int tot_NET_weight=0;
	unsigned long int _charge;
	unsigned long int tot_charge=0;
	unsigned char match;

	_find_str[12] = '\0';
	_temp_str[12] = '\0';
	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);

	//< password checking
	if(!check_password())
	{
		return;
	}

	//< Report Type
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("REPORT(ALL/RST/DATE)"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< default showing
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='3')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	report_type = lcd_Line2_data[lcd_Line2_pointer]-'0';

	//< inputing RST NO. or DATE
	switch(report_type)
	{
		//< if ALL
		case 1:
			from_rst = 0;
			to_rst = RST_read();
			break;

		//< if RST WISE
		case 2:
FUNCTION_F7_SET_FROM_RST:
			//< from RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("FROM RST NO.:"));

			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+4;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);

			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}

			//< if user not inputed anything then
			if(lcd_Line2_pointer==lcd_Line2_min)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid RST No."));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_FROM_RST;
			}
			//< if user has inputed RST no. then
			else
			{
				//< create RST no from user input
				switch(lcd_Line2_pointer-lcd_Line2_min)
				{
					case 1:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0');
					break;
					case 2:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
					break;
					case 3:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
					break;
					case 4:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
					break;
				}
			}

FUNCTION_F7_SET_TO_RST:
			//< TO RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("TO RST NO.:"));

			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+4;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);

			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}

			//< if user not inputed anything then
			if(lcd_Line2_pointer==lcd_Line2_min)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid RST No."));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_TO_RST;
			}
			//< if user has inputed RST no. then
			else
			{
				//< create RST no from user input
				switch(lcd_Line2_pointer-lcd_Line2_min)
				{
					case 1:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0');
					break;
					case 2:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
					break;
					case 3:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
					break;
					case 4:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
					break;
				}
			}
			break;

		//< DATE WISE
		case 3:
			from_rst = 0;
			to_rst = RST_read();
FUNCTION_F7_SET_FROM_DATE:
			//< from RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("FROM DATE:"));

			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+8;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);

			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_data[lcd_Line2_pointer]='/';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_Line2_pointer++;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}

			//< if user not inputed date properly
			if(lcd_Line2_pointer!=lcd_Line2_max)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid Date"));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_FROM_DATE;
			}
			//< if user has inputed Date then
			else
			{
				from_date = (lcd_Line2_data[lcd_Line2_min+6]-'0')*100000UL + \
							(lcd_Line2_data[lcd_Line2_min+7]-'0')*10000UL + \
							(lcd_Line2_data[lcd_Line2_min+3]-'0')*1000UL + \
							(lcd_Line2_data[lcd_Line2_min+4]-'0')*100UL + \
							(lcd_Line2_data[lcd_Line2_min+0]-'0')*10UL + \
							(lcd_Line2_data[lcd_Line2_min+1]-'0')*1UL;
			}

FUNCTION_F7_SET_TO_DATE:
			//< to RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("TO DATE:"));

			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+8;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);

			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_data[lcd_Line2_pointer]='/';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_Line2_pointer++;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}

			//< if user not inputed date properly
			if(lcd_Line2_pointer!=lcd_Line2_max)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid Date"));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_TO_DATE;
			}
			//< if user has inputed Date then
			else
			{
				to_date = (lcd_Line2_data[lcd_Line2_min+6]-'0')*100000UL + \
							(lcd_Line2_data[lcd_Line2_min+7]-'0')*10000UL + \
							(lcd_Line2_data[lcd_Line2_min+3]-'0')*1000UL + \
							(lcd_Line2_data[lcd_Line2_min+4]-'0')*100UL + \
							(lcd_Line2_data[lcd_Line2_min+0]-'0')*10UL + \
							(lcd_Line2_data[lcd_Line2_min+1]-'0')*1UL;
			}
			break;
	}


	//< take report_variable
	//< i.e.ALL			= 1
	//<		Supplier	= 2
	//<		source		= 3
	//<		vehicle no.	= 4
	//<		Material	= 5
	//<		Address		= 6
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("(ALL/SU/SO/VE/MA/AD)"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< default showing
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='6')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	report_variable = lcd_Line2_data[lcd_Line2_pointer]-'0';


	//< if not ALL
	//< take find_str i.e vehicle_no in the case of report as per vehicle_no
	if(report_variable!=1)
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		switch(report_variable)
		{
			//< SUPPLIER
			case 2:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("SUPPLIER :"));
				}
				break;

			//< SOURCE
			case 3:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("SOURCE :"));
				}
				break;

			//< Vehicle No
			case 4:
				lcd_mega_StringF(PSTR("VEHICLE NO :"));
				break;

			//< MATERIAL
			case 5:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("MATERIAL :"));
				}
				break;

			case 6:
				lcd_mega_StringF(PSTR("ADDRESS :"));
				break;
		}
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 12;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< normal key processing
			if(ps2_key.type==NORMAL_KEY)
			{
				//< code base system related
				if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
				{
					//< vehicle no related
					switch(report_variable)
					{
						//< SUPPLIER
						case 2:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;

						//< SOURCE
						case 3:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;

						//< VEHICLE NO
						case 4:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;

						//< MATERIAL
						case 5:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;

						//< ADDRESS
						case 6:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
					}
				}

				else if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					//< break with saving
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< break without saving
					return;
				}
			}
		}
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
		{
			_find_str[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		_find_str[12]='\0';
		//< remove space from find_str
		remove_space_from_string(_find_str);
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Report Generating..."));

	//< print header of report
	PRINTER_ESC;
	PRINTER_SET_REPORT_FONT;

	//< header printing
	//< line1
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_DATA+i));
		}
		PRINTER_ENTER;
	}
	//< line2
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_DATA+i));
		}
		PRINTER_ENTER;
	}
	//< line3
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_DATA+i));
		}
		PRINTER_ENTER;
	}

	switch(report_type)
	{
		//< ALL
		case 1:
			printer_writeStringF(PSTR("For ALL Records"));
			break;

		//< RST wise
		case 2:
			printer_writeStringF(PSTR("For RST NO. wise Records"));
			PRINTER_ENTER;
			printer_writeStringF(PSTR("RST:  "));
			printer_writeString(utoa(from_rst,_temp_str,10));
			printer_writeStringF(PSTR(" ... "));
			printer_writeString(utoa(to_rst,_temp_str,10));
			break;
		//< DATE wise
		case 3:
			printer_writeStringF(PSTR("For DATE wise Records"));
			PRINTER_ENTER;
			printer_writeStringF(PSTR("DATE:  "));
			//< from date
			ultoa(from_date,_temp_str,10);
			printer_writeData(_temp_str[4]);
			printer_writeData(_temp_str[5]);
			printer_writeData('/');
			printer_writeData(_temp_str[2]);
			printer_writeData(_temp_str[3]);
			printer_writeStringF(PSTR("/20"));
			printer_writeData(_temp_str[0]);
			printer_writeData(_temp_str[1]);
			printer_writeStringF(PSTR(" ... "));
			//< to date
			ultoa(to_date,_temp_str,10);
			printer_writeData(_temp_str[4]);
			printer_writeData(_temp_str[5]);
			printer_writeData('/');
			printer_writeData(_temp_str[2]);
			printer_writeData(_temp_str[3]);
			printer_writeStringF(PSTR("/20"));
			printer_writeData(_temp_str[0]);
			printer_writeData(_temp_str[1]);
			break;
	}
	PRINTER_ENTER;

	if(report_variable!=1)
	{
		switch(report_variable)
		{
			//< supplier
			case 2:
				printer_writeStringF(PSTR("SUPPLIER : "));
				break;

			//< source
			case 3:
				printer_writeStringF(PSTR("SOURCE : "));
				break;

			//< vehicle no
			case 4:
				printer_writeStringF(PSTR("VEHICLE NO. : "));
				break;

			//< material
			case 5:
				printer_writeStringF(PSTR("MATERIAL : "));
				break;

			//< ADDRESS
			case 6:
				printer_writeStringF(PSTR("ADDRESS : "));
				break;
		}
		printer_writeString(_find_str);
		PRINTER_ENTER;
	}
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	printer_writeStringF(PSTR("RST  |"));
	printer_writeStringF(PSTR("   DATE    |"));
	printer_writeStringF(PSTR("VEHICLE NO.  |"));
	printer_writeStringF(PSTR("SUPPLIER     |"));
	printer_writeStringF(PSTR("MATERIAL     |"));
	printer_writeStringF(PSTR("ADDRESS      |"));
	printer_writeStringF(PSTR("SOURCE       |"));
	printer_writeStringF(PSTR(" GROSS |"));
	printer_writeStringF(PSTR(" TARE  |"));
	printer_writeStringF(PSTR(" NETT  |"));
	printer_writeStringF(PSTR("CHARGE|"));

	PRINTER_ENTER;
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;



	//< report generating and printing
	//< if to_rst greater than cur_rst
	//< then set to_rst = cur_rst
	i=RST_read();
	if(to_rst>i)
	{
		to_rst=i;
	}

	for(i=from_rst;i<=to_rst;i++)
	{
		//< retrive RST Entry
		SD_mega_ReadSingleBlock(SD_data_buf,i);
		match = 0;

		//< check if entry is occupied
		if(!SD_data_buf[RST_ENTRY_TYPE])
		{
			//< date wise report related processing
			if(report_type==3)
			{
				//< calculate entry date
				if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')
				{
					temp_date = (SD_data_buf[RST_GROSS_DATE+4]-'0')*100000UL + \
								(SD_data_buf[RST_GROSS_DATE+5]-'0')*10000UL + \
								(SD_data_buf[RST_GROSS_DATE+2]-'0')*1000UL + \
								(SD_data_buf[RST_GROSS_DATE+3]-'0')*100UL + \
								(SD_data_buf[RST_GROSS_DATE+0]-'0')*10UL + \
								(SD_data_buf[RST_GROSS_DATE+1]-'0')*1UL;
				}
				else
				{
					temp_date = (SD_data_buf[RST_TARE_DATE+4]-'0')*100000UL + \
								(SD_data_buf[RST_TARE_DATE+5]-'0')*10000UL + \
								(SD_data_buf[RST_TARE_DATE+2]-'0')*1000UL + \
								(SD_data_buf[RST_TARE_DATE+3]-'0')*100UL + \
								(SD_data_buf[RST_TARE_DATE+0]-'0')*10UL + \
								(SD_data_buf[RST_TARE_DATE+1]-'0')*1UL;
				}

				//< check entry date is in range or not
				if(temp_date>to_date || temp_date<from_date)
				{
					//< then goto next entry
					continue;
				}
			}

			switch(report_variable)
			{
				//< if ALL
				case 1:
					match = 1;
					break;

				//< if SUPPLIER
				case 2:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_SUPPLIER+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;

				//< if SOURCE
				case 3:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_SOURCE+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;

				//< if VEHICLE NO
				case 4:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_VEHICLE_NO+temp];
					}
					//< remove space from
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;

				//< if MATERIAL
				case 5:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_MATERIAL+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;

				//< if ADDRESS
				case 6:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_ADDRESS+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
			}

			//< if occupied than print RST Entry
			if(match)
			{
				Report_entry_print(SD_data_buf,i);

				//< calculate total NET_weight
				if(!SD_data_buf[RST_BOTHENTRY_COMPLETED])
				{
					_net_weight = (unsigned long int)(SD_data_buf[RST_NETWEIGHT]-'0') + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+1]-'0') * 10 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+2]-'0') * 100 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+3]-'0') * 1000 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+4]-'0') * 10000 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+5]-'0') * 100000;
					tot_NET_weight += _net_weight;
				}

				//< calculate total_charge
				//< calculating charge
				temp=0;
				while(temp!=5)
				{
					if(SD_data_buf[RST_TOTALCHARGE+temp]==' ')
					{
						break;
					}
					temp++;
				}
				switch(temp)
				{
					case 0:
						_charge = 0;
					case 1:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0');
						break;
					case 2:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0');
						break;
					case 3:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
					case 4:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 1000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
					case 5:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 10000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 1000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
				}
				tot_charge += _charge;
			}
		}
	}

	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	//< total nett weight and total charge printing
	printer_writeStringF(PSTR("Total--------->"));//< 15 char
	for(int i=0;i<85;i++)
	{
		printer_writeData(' ');
	}
	//< total net weight printing
	ultoa(tot_NET_weight,_temp_str,10);
	temp = strlen(_temp_str);
	for(i=0;i<10-temp;i++)//< 10 char
	{
		printer_writeData(' ');
	}
	for(i=0;i<temp;i++)
	{
		printer_writeData(_temp_str[i]);
	}
	printer_writeStringF(PSTR(" | "));//< 3 char
	//< total charge printing
	ultoa(tot_charge,_temp_str,10);
	printer_writeString(_temp_str);//< 7 char

	PRINTER_ENTER;
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	PRINTER_ENTER;
}

void function_F8()
{
	unsigned int _rst_no;
	unsigned int i;

	//< ASK for RST No.
FUNCTION_F8_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));

	set_string_in_lcd_dataF(LINE2,PSTR("Entry Delete): "),0,0);
	lcd_Line2_pointer = 15;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}

	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F8_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
			break;
			case 2:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
			break;
			case 3:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
			break;
			case 4:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
			break;
		}
	}

	//< now we have rst no.
	//< erase thst rst no entry
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Erasing..."));
	for(i=0;i<512;i++)
	{
		SD_data_buf[i] = 0xFF;
	}
	SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
	_delay_ms(500);
}

void function_F9()
{
	unsigned int i;

	//< password checking
	if(!check_password())
	{
		return;
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("To ERASE Memory"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENTER"));
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Erasing..."));
	lcd_mega_gotoxy2(0);
	//< write 0xFF in all RST blocks
	for(i=0;i<512;i++)
	{
		SD_data_buf[i] = 0xFF;
	}
	for(i=0;i<10000;i++)
	{
		SD_mega_WriteSingleBlock(SD_data_buf,i);
		if(!(i%500))
		{
			lcd_mega_SendData('~');
		}
	}

	//< set RST no to 0
	RST_write(0);
}

void function_F10()
{
	unsigned char c;
	unsigned char str[20];
	unsigned char str_temp[8];
	unsigned char i;
	unsigned char j;
	unsigned int from_rst;
	unsigned int to_rst;

	//< password checking
	if(!check_password())
	{
		return;
	}

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Computer Data Backup"));

	lcd_mega_gotoxy2(0);
	while(1)
	{
		c = usart0_ReadData();
//		lcd_mega_SendData(c);

		if(c=='s')
		{
			usart0_WriteData('I');
			usart0_WriteData('T');
			//usart0_WriteData(0);

			c = usart0_ReadData();
			if(c=='o')
			{
				break;
			}
		}
  	}
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Connected..."));
	_delay_ms(1000);

	i=0;
	while(1)
	{
		c=usart0_ReadData();
		if(c=='o')
		{
			str[i] = c;
			str[i+1]='\0';
			break;
		}
		else
		{
			str[i]=c;
		}
		i++;
	}
	usart0_WriteData('o');

	//< rst number processing
	if(str[0]=='f')
	{
		//< calculate from rst
		j=0;
		while(str[j++]!='t');
		for(i=1;i<j;i++)
		{
			str_temp[i-1] = str[i];
		}
		str_temp[j] = '\0';
		from_rst = atoi(str_temp);

		//< calculate to rst
		c=0;
		while(str[c++]!='t');
		j=0;
		while(str[j++]!='o');
		for(i=(c+1);i<j;i++)
		{
			str_temp[(i-(c+1))] = str[i];
		}
		str_temp[j] = '\0';
		to_rst = atoi(str_temp);

		lcd_mega_ClearDispaly();
		lcd_mega_String("FromRST= ");
		lcd_mega_String(itoa(from_rst,str_temp,10));
		lcd_mega_gotoxy2(0);
		lcd_mega_String("ToRST= ");
		lcd_mega_String(itoa(to_rst,str_temp,10));
	}
	//< if all rst no selected
	else
	{

	}

	SD_mega_ReadSingleBlock(SD_data_buf,8);
	for(i=0;i<=121;i++)
	{
		usart0_WriteData(SD_data_buf[i]);
	}
	usart0_WriteData('#');
	_delay_ms(3000);
}
*/