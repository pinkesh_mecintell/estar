/*
 * eeprom_mega.h
 *
 * Created: 6/12/2012 7:17:18 PM
 *  Author: Pinkesh
 */ 


#ifndef EEPROM_MEGA_H_
#define EEPROM_MEGA_H_

#include <stdbool.h>

/**
* Macros used by ReadWriteData Function
*/
#define EEPROM_MEGA_READ true
#define EEPROM_MEGA_WRITE false



/************************************************************************/
/*  FUNCTION DECLARATION                                                */
/************************************************************************/

/**
* Read one byte from EEPROM with specified address.
* This function don't contain address checking. So that we must 
* specified valid address.
* /param	eeprom_address	address from which we want to read
* /return					reading data
*/
uint8_t eeprom_mega_ReadByte(uint16_t eeprom_address);

/**
* Write one byte to EEPROM with specified address.
* This function don't contain address checking. So that we must 
* specified valid address.
* /param	eeprom_address	address from which we want to read
* /param	eeprom_data		data(Byte) which we want to write
* /return					reading data
*/
void eeprom_mega_WriteByte(uint16_t eeprom_address,uint8_t eeprom_data);

/**
* Write into EEPROM from DATA_BUFFER.
* using this function we can write more then one Byte.
* /param	start_address	Starting EEPROM address
* /param	data_buffer		pointer to Data Buffer
* /param	number_data		specify how many Bytes to be write from Data Buffer
*/
void eeprom_mega_ReadData(uint16_t start_address,uint8_t *data_buffer,uint16_t number_data);

/**
* Read from EEPROM and place that data into DATA_BUFFER.
* using this function we can read more then one Byte.
* /param	start_address	Starting EEPROM address
* /param	data_buffer		pointer to Data Buffer
* /param	number_data		specify how many Bytes to be read from EEPROM
*/
void eeprom_mega_WriteData(uint16_t start_address,uint8_t *data_buffer,uint16_t number_data);

/**
* Read or Write EEPROM into/from DATA_BUFFER.
* when we need Read and Write Data_Buffer both functionality then use/include
* this function because in that case the performance of this function is higher
* then ReadData/WriteData function in terms of Program Size.
* when only one functionality is needed then don't use this function but
* use ReadData or WriteData function.
* /param	read_write		determine whether we want to read or write
							value = true(nonzero) then Read Operation
							value = false(zero) then Write Operation
* /param	start_address	Starting EEPROM address
* /param	data_buffer		pointer to Data Buffer
* /param	number_data		specify how many Bytes to be read from EEPROM
*/
void eeprom_mega_ReadWriteData(bool read_write,uint16_t start_address,uint8_t *data_buffer,uint16_t number_data);
#endif /* EEPROM_MEGA_H_ */