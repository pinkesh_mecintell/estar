/*
 * SD_mega_config.h
 *
 * Created: 6/24/2012 12:01:14 AM
 *  Author: Pinkesh
 */ 


#ifndef SD_MEGA_CONFIG_H_
#define SD_MEGA_CONFIG_H_


/**
* defines the which pin of Microcontroller is connected with CS pin of SD card
* \n this pin may be different then SS pin of SPI of Microcontroller 
*/
#define SD_mega_CSPORT PORTB
#define SD_mega_CSDDR DDRB
#define SD_mega_CSPin 0



#endif /* SD_MEGA_CONFIG_H_ */