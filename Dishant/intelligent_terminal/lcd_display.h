/*
 * lcd_display.h
 *
 * Created: 16/09/13 9:08:17 PM
 *  Author: Pinkesh
 */ 


#ifndef LCD_DISPLAY_H_
#define LCD_DISPLAY_H_


#define __LCD40_2__	0

/**** MACRO Definitions *****/
//< lcd related
#define LCD_ON	SET(PORTC,3)
#define LCD_OFF	CLR(PORTC,3)
#define LCD_LEN	40
#define LCD_DISPLAY_LEN 15
#define LINE1		1
#define LINE2		2
#define LINE_BOTH	0
////////////////////////////////
/*****************************/

/***** Function Declaration ****/
void clear_lcd_data(unsigned char line);
void set_string_in_lcd_data(unsigned char line,char *str,unsigned char from,unsigned char to);
void set_string_in_lcd_dataF(unsigned char line,const char *str,unsigned char from,unsigned char to);
void lcd_normal_display();
/*******************************/

#endif /* LCD_DISPLAY_H_ */