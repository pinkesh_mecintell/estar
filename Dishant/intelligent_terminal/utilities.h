/*
 * utilities.h
 *
 *  Created on: Jun 2, 2012
 *      Author: Pinkesh
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

//< common utilities macros
#define NO	0
#define YES	1
//////////////////////////////


#define SET(x,b) x|=(1<<b)
#define CLR(x,b) x=x&(~(1<<b))
#define CHK(x,b) (x&(1<<b))
#define TOG(x,b) x^=(1<<b)

//< returns integer which x-bit is one and all other bits are zero
#define _BIT_(x) (1<<x)

/**
* Input x and a both are Bytes.
* This macro Clears the bit of x which are set in a.
* example- if a = 0x03(b00000011) then _BIT_CLR(x,a)
* clears the bit-0 and bit-1 of the Byte-x.
* no other bits of x are affected.
*/
#define _BIT_CLR(x,a) (x &= (~(a)))

/**
* Input x and a both are Bytes.
* This macro Sets the bit of x which are set in a.
* example- if a = 0x03(b00000011) then _BIT_CLR(x,a)
* sets the bit-0 and bit-1 of the Byte-x.
* no other bits of x are affected.
*/
#define _BIT_SET(x,a) (x |= a)

#endif /* UTILITIES_H_ */
