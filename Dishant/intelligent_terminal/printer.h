/*
 * printer.h
 *
 * Created: 22/09/13 12:51:06 AM
 *  Author: Pinkesh
 */ 


#ifndef PRINTER_H_
#define PRINTER_H_

//< Printer Type related macros
#define PRINTER_USB				0
#define PRINTER_PARALLEL		1
#define PRINTER_BUS_TYPE		PRINTER_PARALLEL


//< status return used by printer_get_status()
#define STATUS_PRINTER_ERROR	0
#define STATUS_PRINTER_NO_PAPER	1
#define STATUS_PRINTER_OK		2
#define STATUS_PRINTER_NONE		3


//< cheak printer macro
#define CHK_PRINTER()		if(check_printer()==STATUS_PRINTER_ERROR){return;}

/*** macros ****/
#define PRINTER_ENTER	CHK_PRINTER(); printer_writeData(0X0A);
#define PRINTER_ESC		printer_writeData(0X1B);
#define PRINTER_TAB		printer_writeData(0X09);

//< font setting macross
#define PRINTER_SET_HEADER_FONT			printer_writeData(0X0E);
#define PRINTER_CANCEL_HEADER_FONT		printer_writeData(0X14);
#define PRINTER_SET_BOLD			printer_writeData(0X45);
#define PRINTER_CANCEL_BOLD			printer_writeData(0X46);
#define PRINTER_SET_RECEIPT_FONT	printer_writeData(0X50);
#define PRINTER_SET_REPORT_FONT		printer_writeData(0X0F); PRINTER_ESC; printer_writeData(0X50);
#define PRINTER_CANCLE_REPORT_FONT	printer_writeData(0x12)

//< printing related macros
#define PRINTER_REVERSE_PAGE		PRINTER_ESC; printer_writeData(0x6A); printer_writeData(216); PRINTER_ESC; printer_writeData(0x6A); printer_writeData(130);
#define PRINTER_FORWARD_PAGE		PRINTER_ESC; printer_writeData(0x4A); printer_writeData(216); PRINTER_ESC; printer_writeData(0x4A); printer_writeData(130);
#define PRINTER_SET_PAGE_LENGTH(n)	PRINTER_ESC; printer_writeData(0x43); printer_writeData(0x00); printer_writeData(n);	//< 0<= n <=22
#define PRINTER_GOTO_NEXT_PAGE		printer_writeData(0x0C);

//< how to use these macros
//< PRINTER_REVERSE_PAGE
//< PRINTER_SET_PAGE_LENGTH
//< print using - printer_set_horizontal_pos(mm) and 
//<					printer_set_verticle_pos(mm) functions
//< after printing last ENTRY
//< PRINTER_GOTO_NEXT_PAGE
//< PRINTER_FORWARD_PAGE
/***************/

void printer_init();
void printer_writeData(unsigned char pdata);
void printer_writeString(char *str);
void printer_writeStringF(const char *str);
unsigned char printer_get_status();
unsigned char check_printer();
//< absolute horizontal moving
void printer_set_horizontal_pos(unsigned int mm);
//< relative vartical moving
void printer_set_verticle_pos(unsigned int mm);

#endif /* PRINTER_H_ */