/*
 * lcd_mega.c
 *
 * Created: 6/15/2012 6:38:36 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "utilities.h"
#include "lcd_mega.h"
#include "lcd_mega_config.h"


/*
* Reads Busy Flag of LCD.
* return from the function when the LCD is not Busy in internal operation
* and it can take command or data from uC.
*/
//static void lcd_mega_BusyFlag(void);

/************************************************************************//**
*	FUNTION IMPLEMENTATION
*************************************************************************/

void lcd_mega_Initialize(void)
{
#if ( lcd_mega_configDATANibble == 0)
	_BIT_SET(lcd_mega_configDDR,0x0F+_BIT_(lcd_mega_configRS)+_BIT_(lcd_mega_configEN)+_BIT_(lcd_mega_configRW));
#else
	_BIT_SET(lcd_mega_configDDR,0xF0 + _BIT_(lcd_mega_configRS) + _BIT_(lcd_mega_configEN) + _BIT_(lcd_mega_configRW));
#endif
	
	//lcd_mega_SendCmd(0x01);
	lcd_mega_SendCmd(0x0E);
	lcd_mega_SendCmd(0x02);
	lcd_mega_SendCmd(0x06);
	lcd_mega_SendCmd(0x80);
	lcd_mega_ClearDispaly();
}

/*static void lcd_mega_BusyFlag(void)
{
	_BIT_CLR(lcd_mega_configPORT,_BIT_(lcd_mega_configRS)+_BIT_(lcd_mega_configRW));
	
#if ( lcd_mega_configDATANibble == 0)
	//< Make DB7 as input
	CLR(lcd_mega_configDDR,3);
	SET(lcd_mega_configPORT,3); //< pullup enable
	
	//< toggle EN
	TOGGLE_EN;
	
	//< check DB7 pin and wait until DB7 clears
	while(CHK(lcd_mega_configPIN,3));
	
	//< Make DB7 output
	SET(lcd_mega_configDDR,3);
#else
	//< Make DB7 as input
	CLR(lcd_mega_configDDR,7);
	SET(lcd_mega_configPORT,7); //< pullup enable
	
	//< toggle EN
	TOGGLE_EN;
	
	//< check DB7 pin and wait until DB7 clears
	while(CHK(lcd_mega_configPIN,7));
	
	//< Make DB7 output
	SET(lcd_mega_configDDR,7);
#endif
}
*/

void lcd_mega_SendCmd(uint8_t cmd)
{
	//lcd_mega_BusyFlag();
	
#if ( lcd_mega_configDATANibble==0 )
	_BIT_CLR(lcd_mega_configPORT,0x0F+_BIT_(lcd_mega_configRS)+_BIT_(lcd_mega_configEN)+_BIT_(lcd_mega_configRW));
	lcd_mega_configPORT |= ((cmd & 0xF0) >> 4);
	TOGGLE_EN;
	_BIT_CLR(lcd_mega_configPORT,0x0F);
	lcd_mega_configPORT |= (cmd & 0x0F);	
	TOGGLE_EN;
#else
	_BIT_CLR(lcd_mega_configPORT,0xF0 + _BIT_(lcd_mega_configRS) + _BIT_(lcd_mega_configEN) + _BIT_(lcd_mega_configRW));
	lcd_mega_configPORT |= (cmd & 0xF0);
	TOGGLE_EN;
	_BIT_CLR(lcd_mega_configPORT,0xF0);
	lcd_mega_configPORT |= ((cmd & 0x0F) << 4);	
	TOGGLE_EN;
#endif
}

void lcd_mega_SendData(uint8_t data)
{
	//lcd_mega_BusyFlag();
	
#if ( lcd_mega_configDATANibble==0 )
	_BIT_CLR(lcd_mega_configPORT,0x0F+_BIT_(lcd_mega_configRS)+_BIT_(lcd_mega_configEN)+_BIT_(lcd_mega_configRW));
	//< configure RS,RW for send command
	SET(lcd_mega_configPORT,lcd_mega_configRS);
	lcd_mega_configPORT |= ((data & 0xF0) >> 4);
	TOGGLE_EN;
	_BIT_CLR(lcd_mega_configPORT,0x0F);
	lcd_mega_configPORT |= (data & 0x0F);	
	TOGGLE_EN;
#else
	_BIT_CLR(lcd_mega_configPORT,0xF0 + _BIT_(lcd_mega_configRS) + _BIT_(lcd_mega_configEN) + _BIT_(lcd_mega_configRW));
	//< configure RS,RW for send command
	SET(lcd_mega_configPORT,lcd_mega_configRS);lcd_mega_configPORT |= (data & 0xF0);
	TOGGLE_EN;
	_BIT_CLR(lcd_mega_configPORT,0xF0);
	lcd_mega_configPORT |= ((data & 0x0F) << 4);	
	TOGGLE_EN;
#endif
}

#if ( lcd_mega_includeString==1 )
void lcd_mega_String(char *str)
{
	while(*str != '\0')
	{
		lcd_mega_SendData(*str);
		str++;
	}
}
#endif

#if ( lcd_mega_includeStringF==1 )
void lcd_mega_StringF(const char *str)
{
	while(pgm_read_byte(&(*str)))
	{
		lcd_mega_SendData(pgm_read_byte(&(*str++)));
	}
}
#endif

#if ( lcd_mega_includeShowvalue==1 )
void lcd_mega_Showvalue(uint8_t num)
{
	uint8_t H=0,T=0,O=0;
	H=num/100;
	T=(num - (H*100))/10;
	O=(num - (H*100) - (T*10));
	
	lcd_mega_SendData(H+48);
	lcd_mega_SendData(T+48);
	lcd_mega_SendData(O+48);
}
#endif