/*
 * time_date_setting.c
 *
 * Created: 17/09/13 5:59:10 PM
 *  Author: Pinkesh
 */
#include <avr/io.h>
#include <util/delay.h>
#include <math.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "lcd_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "password_strings.h"
#include "ps2_kb.h"
#include "buzzer.h"
#include "utilities.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "printer.h"
#include "function_keys.h"
#include "ADS123x.h"
#include "internal_display.h"
#include "SD_mega.h"
#include "SD_addresses.h"

/****** Macros Definition ****/
//< Password related
#define LEN_PASSWORD_MAX	16
/*****************************/


/*** Extern variables ****/
//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
extern unsigned char LCD_CODE[];
extern unsigned char SEG_CODE[];
///////////////////////////
extern unsigned char login_type;
extern unsigned char operator_no;
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////

///////////////////////////
//< ADS123x - ADC related
extern signed long int ADC_count;
extern signed long int ADC_buff[];
extern unsigned char ADC_buff_count;
///////////////////////////

/////////////////////////////
//< weight related
extern signed long int weight;
extern unsigned char weight_str[];
extern WEIGHT_CONSTANTS weight_const;
/////////////////////////////

extern unsigned char SD_data_buf[];
extern unsigned char fun_ret;
/*************************/


void setting_date_time()
{
	unsigned char date=0,month=0,year=0,hour=0,minute=0;
	unsigned char label_length = 5;
	unsigned char D1=label_length,D2=label_length+1,C1=label_length+2,M1=label_length+3,M2=label_length+4,C2=label_length+5,Y3=label_length+8,Y4=label_length+9;
	unsigned char H1=label_length,H2=label_length+1;
SET_DATE_DATE:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Date:"));
	lcd_mega_gotoxy2(label_length);
	lcd_mega_StringF(PSTR("DD/MM/YYYY"));

	//< Set LCD pointers
	lcd_Line1_pointer = label_length;
	lcd_Line1_max = lcd_Line1_pointer+10;
	lcd_Line1_min = lcd_Line1_pointer;
	lcd_CursorLine = LINE1;

	//< set cursor position
	lcd_mega_gotoxy1(lcd_Line1_pointer);

	while(1)
	//< take user input
	{
		while(1)
		//< get key pressed
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		//< numeric key processing
		{
			if(lcd_Line1_pointer<lcd_Line1_max)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
				lcd_Line1_pointer++;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
				if (lcd_Line1_pointer==C1 || lcd_Line1_pointer==C2)
				{
					lcd_Line1_data[lcd_Line1_pointer]='/';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_Line1_pointer++;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
		}
		else if(ps2_key.type==SPECIAL_KEY)
		//< special key processing
		{
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			//< backspace checking
			{
				if(lcd_Line1_pointer>lcd_Line1_min)
				{
					if (lcd_Line1_pointer==(C1+1) || lcd_Line1_pointer==(C2+1))
					{
						lcd_Line1_pointer--;
						lcd_mega_gotoxy1(lcd_Line1_pointer);
						lcd_Line1_data[lcd_Line1_pointer]=' ';
						lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
						lcd_mega_gotoxy1(lcd_Line1_pointer);
					}
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
					lcd_Line1_data[lcd_Line1_pointer]=' ';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			//< enter key processing
			{
				//< store user input temporary
				date=(lcd_Line1_data[D1]-'0')*10 + (lcd_Line1_data[D2]-'0');
				month=(lcd_Line1_data[M1]-'0')*10 + (lcd_Line1_data[M2]-'0');
				year=(lcd_Line1_data[Y3]-'0')*10 + (lcd_Line1_data[Y4]-'0');
				//< validate date
				if (lcd_Line1_data[D1]==' ')
				{
					goto SET_TIME;
				}
				if(date>31 || date==0)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Date"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				if(month>12 || month==0)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Month"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				if(year>99)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Year"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				//< storing date data into ds1307-RTC
				//< user has modified date
				ds1307_WriteRegister(4,(((date/10)<<4)+(date%10)));

				//< storing month data into ds1307-RTC
				//< user has modified month
				ds1307_WriteRegister(5,(((month/10)<<4)+(month%10)));

				//< storing year data into ds1307-RTC
				//< user has modified year
				ds1307_WriteRegister(6,(((year/10)<<4)+(year%10)));


				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting date
				return;//< unsuccessful return
			}
		}
	}
SET_TIME:
	M1=label_length+3;
	M2=label_length+4;

	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Time:"));
	lcd_mega_gotoxy2(label_length);
	lcd_mega_StringF(PSTR("HH:MM"));

	//< hour setting
	lcd_Line1_pointer = label_length;
	lcd_Line1_max = lcd_Line1_pointer+5;
	lcd_Line1_min = lcd_Line1_pointer;
	lcd_CursorLine = LINE1;

	//< set cursor position
	lcd_mega_gotoxy1(lcd_Line1_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line1_pointer<lcd_Line1_max)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
				lcd_Line1_pointer++;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
				if (lcd_Line1_pointer==C1)
				{
					lcd_Line1_data[lcd_Line1_pointer]=':';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_Line1_pointer++;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line1_pointer>lcd_Line1_min)
				{
					if (lcd_Line1_pointer==(C1+1))
					{
						lcd_Line1_pointer--;
						lcd_mega_gotoxy1(lcd_Line1_pointer);
						lcd_Line1_data[lcd_Line1_pointer]=' ';
						lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
						lcd_mega_gotoxy1(lcd_Line1_pointer);
					}
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
					lcd_Line1_data[lcd_Line1_pointer]=' ';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temporary
				hour=(lcd_Line1_data[H1]-'0')*10 + (lcd_Line1_data[H2]-'0');
				minute=(lcd_Line1_data[M1]-'0')*10 + (lcd_Line1_data[M2]-'0');

				//< validate time
				if (lcd_Line1_data[H1]==' ')
				{
					return;
				}
				if(minute>59)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Minute"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME;
				}
				if(hour>23)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Hour"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME;
				}
				//< storing hour data into ds1307-RTC
				//< user has modified hour
				ds1307_WriteRegister(2,(((hour/10)<<4)+(hour%10)));

				//< storing minute data into ds1307-RTC
				//< user has modified minute
				ds1307_WriteRegister(1,(((minute/10)<<4)+(minute%10)));
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting date
				return;//< unsuccessful return
			}
		}
	}

	return;//< successful return
}

unsigned char check_password()
{
	unsigned char pswd_len;

	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("-Enter Password-"));

	clear_lcd_data(LINE2);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = LEN_PASSWORD_MAX;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val!=' ')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without checking password
				return 0;//< unsuccessful return
			}
		}
	}

	//< count length of user seted password on lcd
	pswd_len=lcd_Line2_pointer;

	//< compare it with stored password len
	if(eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_LEN)==pswd_len)
	{
		//< compare password data with stored data
		while(1)
		{
			//< if there is no more password char then break
			if(pswd_len==0)
			{
				break;
			}
			//< compare each char
			if(lcd_Line2_data[pswd_len-1]!=eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_DATA+pswd_len-1))
			{
				break;
			}
			pswd_len--;
		}
		//< if all password chars are matched
		if(pswd_len==0)
		{
			return 1;
		}
		//< if there is mismatch found
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Password not Matched"));
			_delay_ms(1000);
			return 0;
		}
	}
	//< if both are not same
	else
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Password not Matched"));
		_delay_ms(1000);
		return 0;
	}

	//< successful return
	return 1;
}

void setting_ticket_header_entry()
{
	unsigned char line_len,str1[4],len,i=0,j=0,tkt_no;

#if (__LCD40_2__)
	len = 25;
#else
	len = 12;
#endif

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Tkt Entries:"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(0-10): "),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	if(line_len>10)
	{
		line_len=5;
	}
	utoa(line_len,str1,10);
	set_string_in_lcd_data(LINE2,str1,8,0);
	fun_ret=get_string_from_user(str1,8,8+strlen(str1),10,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		if(fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			tkt_no = atoi(str1);
		}
		else if(fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			tkt_no=line_len;
		}
		//save number of ticket entries
		if (tkt_no<=10)
		{
			eeprom_mega_WriteByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED,tkt_no);

			for (j=0;j<tkt_no;j++)
			{
				//< Ticket Header input
				clear_lcd_data(LINE_BOTH);
				set_string_in_lcd_dataF(LINE1,PSTR("Ticket Entry-"),0,0);
				utoa((j+1),str1,10);
				set_string_in_lcd_data(LINE1,str1,13,0);

				for(i=0;i<len;i++)
				{
					lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+1+(25*j)+i);
				}
				fun_ret =get_string_from_user(lcd_Line2_data,0,0,len,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
				if(fun_ret==RETURN_CAUSE_ESC)
				{
					return;
				}
				else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
				{
					//< save header string into EEPROM
					for(i=0;i<len;i++)
					{
						eeprom_mega_WriteByte(ADD_TICKET_HEADER+1+(25*j)+i,lcd_Line2_data[i]);
					}
				}
			}
		}
	}
}

void setting_printer_header_footer()
{
	unsigned char line_len;
	unsigned char i=0,j;

	//< header line1 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head1 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< header line1 writing in EEPROM
		//< header length
		eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_DATA+i,lcd_Line2_data[i]);
		}
	}

	//< header line1 - BOLD/NORMAL setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head1 Type B/N:"),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_BOLD_NORMAL);
	if(j!='B' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("BN"),j,0,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< header line1 - BOLD/NORMAL setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_BOLD_NORMAL,i);


	//< header line2 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head2 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< header line2 writing in EEPROM
		//< header length
		eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_DATA+i,lcd_Line2_data[i]);
		}
	}


	//< header line2 - BOLD/NORMAL setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head2 Type B/N:"),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_BOLD_NORMAL);
	if(j!='B' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("BN"),j,0,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< header line2 - BOLD/NORMAL setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_BOLD_NORMAL,i);


	//< header line3 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head3 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< header line3 writing in EEPROM
		//< header length
		eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_DATA+i,lcd_Line2_data[i]);
		}
	}

	//< header line3 - BOLD/NORMAL setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head3 Type B/N:"),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_BOLD_NORMAL);
	if(j!='B' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("BN"),j,0,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< header line3 - BOLD/NORMAL setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_BOLD_NORMAL,i);


	//< header position - disable/top/bottom setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Head-Pos 0/1/2:"),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_POSITION+PRINTER_HEADER_POSITION);
	if(j!='0' && j!='1' && j!='2')
	{
		j='1';
	}
	if(get_character_from_user_P(&i,PSTR("012"),j,0,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< header position - disable/top/bottom setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_PRINTER_HEADER_POSITION+PRINTER_HEADER_POSITION,i);


	//< footer line1 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Footer1 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< footer line1 writing in EEPROM
		//< footer length
		eeprom_mega_WriteByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_LEN,strlen(lcd_Line2_data));
		//< save footer string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_DATA+i,lcd_Line2_data[i]);
		}
	}

	//< footer line2 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Footer2 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< footer line2 writing in EEPROM
		//< footer length
		eeprom_mega_WriteByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_LEN,strlen(lcd_Line2_data));
		//< save footer string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_DATA+i,lcd_Line2_data[i]);
		}
	}

#if (__LCD40_2__ == 1)
	//< lcd40x2 header line1 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("LCD Hdr1 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_LCD40_HEADER_LINE1+LCD40_HEADER_LINE1_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_LCD40_HEADER_LINE1+LCD40_HEADER_LINE1_DATA+i);
		}
	}
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< lcd40x2 header line1 writing in EEPROM
		//< header length
		eeprom_mega_WriteByte(ADD_LCD40_HEADER_LINE1+LCD40_HEADER_LINE1_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_LCD40_HEADER_LINE1+LCD40_HEADER_LINE1_DATA+i,lcd_Line2_data[i]);
		}
	}

	//< lcd40x2 header line2 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("LCD Hdr2 (Max 40)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_LCD40_HEADER_LINE2+LCD40_HEADER_LINE2_LEN);
	if(line_len<=40)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_LCD40_HEADER_LINE2+LCD40_HEADER_LINE2_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,40,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< lcd40x2 header line2 writing in EEPROM
		//< header length
		eeprom_mega_WriteByte(ADD_LCD40_HEADER_LINE2+LCD40_HEADER_LINE2_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<40;i++)
		{
			eeprom_mega_WriteByte(ADD_LCD40_HEADER_LINE2+LCD40_HEADER_LINE2_DATA+i,lcd_Line2_data[i]);
		}
	}
#endif
}

void print_header_type1()
{
	unsigned char i,len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_LEN),font_len;
	//Printing Header Line 1
	if (len >= 1) //if Header line has data
	{
		//set Header fonts
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		font_len=80;
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//Set bold for next line
			PRINTER_ESC;
			PRINTER_SET_BOLD;
			PRINTER_ESC;
			PRINTER_SET_HEADER_FONT;
			font_len=40;
		}
		for (i=0;i<((font_len-len)/2);i++)
		{
			printer_writeData(' ');
		}
		for (i=0;i<len;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_DATA+i));
		}
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE1+PRINTER_HEADER_LINE1_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//reset bold for next line
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			PRINTER_ESC;
			PRINTER_SET_RECEIPT_FONT;
			font_len=80;
		}
		PRINTER_ENTER;
	}
	//printing header Line 2
	len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_LEN);
	if (len >= 1)
	{
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//Set bold for next line
			PRINTER_ESC;
			PRINTER_SET_BOLD;
			PRINTER_ESC;
			PRINTER_SET_HEADER_FONT;
			font_len=40;
		}
		for (i=0;i<((font_len-len)/2);i++)
		{
			printer_writeData(' ');
		}
		for (i=0;i<len;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_DATA+i));
		}
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE2+PRINTER_HEADER_LINE2_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//reset bold for next line
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			PRINTER_ESC;
			PRINTER_SET_RECEIPT_FONT;
			font_len=80;
		}
		PRINTER_ENTER;
	}
	//print Header Line 3
	len=eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_LEN);
	if (len >= 1)
	{
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//Set bold for next line
			PRINTER_ESC;
			PRINTER_SET_BOLD;
			PRINTER_ESC;
			PRINTER_SET_HEADER_FONT;
			font_len=40;
		}
		for (i=0;i<((font_len-len)/2);i++)
		{
			printer_writeData(' ');
		}
		for (i=0;i<len;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_DATA+i));
		}
		if (eeprom_mega_ReadByte(ADD_PRINTER_HEADER_LINE3+PRINTER_HEADER_LINE3_BOLD_NORMAL)=='B') //if Header line is Bold
		{
			//reset bold for next line
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			PRINTER_ESC;
			PRINTER_SET_RECEIPT_FONT;
			font_len=80;
		}
		PRINTER_ENTER;
	}
}

void print_footer_type1()
{
	unsigned char i,len=eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_LEN);
	//Printing Footer Line 1
	if (len >= 1) //if Footer line has data
	{
		//set Footer fonts
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		//loop to set Footer in middle
		for (i=0;i<((80-len)/2);i++)
		{
			printer_writeData(' ');
		}
		//Print Footer1
		for (i=0;i<len;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE1+PRINTER_FOOTER_LINE1_DATA+i));
		}
		PRINTER_ENTER;
	}

	//Printing Footer line 2
	len = eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_LEN);
	//Printing Footer Line 2
	if (len >= 1) //if Footer line has data
	{
		//set Footer fonts
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		//loop to set Footer in middle
		for (i=0;i<((80-len)/2);i++)
		{
			printer_writeData(' ');
		}
		//Print Footer1
		for (i=0;i<len;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_PRINTER_FOOTER_LINE2+PRINTER_FOOTER_LINE2_DATA+i));
		}
		PRINTER_ENTER;
	}
}

void print_signature_line_type1()
{
	unsigned char i;

	CHK_PRINTER();
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;
	//First Signature space
	for (i=0;i<25;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_SIGNATURE_LINE1+SIGNATURE_LINE1_DATA+i));
	}
	printer_writeData(' ');printer_writeData(' ');
	//Second Signature space
	for (i=0;i<25;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_SIGNATURE_LINE2+SIGNATURE_LINE2_DATA+i));
	}
	printer_writeData(' ');printer_writeData(' ');
	//Third Signature space
	for (i=0;i<25;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_SIGNATURE_LINE3+SIGNATURE_LINE3_DATA+i));
	}
	PRINTER_ENTER;
}

unsigned char get_code_location(char* input_str,unsigned char table_index)
{
	unsigned char temp[4],cmp_str[4],position,local_buff[512];
	//Get only First 3 chars of input string
	cmp_str[0]=input_str[0];
	cmp_str[1]=input_str[1];
	cmp_str[2]=input_str[2];
	cmp_str[3]='\0';

	//read SD block from where address is to be found
	SD_mega_ReadSingleBlock(local_buff,CODE_ENTRY_TABLE_START+table_index);
	//set pointer to last entry starting point
	position = local_buff[CODE_TABLE_POINTER];
	if (position >= 4)
	{
		position = position - 4;
	}
	//lcd_mega_String("Pot value: ");
	//lcd_mega_Showvalue(pot);
	while(position>=0 && position<200)
	{
		//get current string in temporary string to compare
		temp[0]=local_buff[position];
		temp[1]=local_buff[position+1];
		temp[2]=local_buff[position+2];
		temp[3]='\0';
		//Compare input string with current string
		if(!strcmp(cmp_str, temp))
		{
			//if string matches return its location
			return local_buff[position+3];
		}
		position = position-4;
	}
	//No string matched return something else
	return NO_MATCH_FOUND;
}

void setting_weightCallibration()
{
	unsigned long int capacity;
	unsigned char max_steps;
	unsigned char division;
	signed long int weight_zero_ADC_count;
	double unit_weight_count1;
	double unit_weight_count2;
	unsigned long int steps_weight[5]={0};
	signed long int steps_ADC_count[5]={0};
	double steps_slope[5]={0};
	double steps_distace[5]={0};
	double slope_nume=0.0;
	double slope_denom=0.0;
	double slope;
	unsigned char i,j;

	//< password checking
	if(!check_password())
	{
		return;
	}

	//< Capacity inputing
SET_WEIGHT_CALIB_CAPACITY:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("CAPACITY (in KG):"));

	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+6;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temporary
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					lcd_mega_ClearDispaly();
					lcd_mega_StringF(PSTR("Invalid Entry"));
					goto SET_WEIGHT_CALIB_CAPACITY;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				return;//< unsuccessful return
			}
		}
	}
	lcd_Line2_data[lcd_Line2_pointer] = '\0';
	capacity = (unsigned long int)(atol(&lcd_Line2_data[lcd_Line2_min]));


	//< MAX_STEP inputing
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("MAX_STEPS (1-5):"));

	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;

	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='5')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	max_steps = lcd_Line2_data[lcd_Line2_pointer] - '0';


	//< Division Inputing
SET_DIVISION:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Division :"));

	set_string_in_lcd_dataF(LINE2,PSTR("5"),0,0);
	lcd_Line2_pointer = 1;
	lcd_Line2_max = 2;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
			ADC_getReading();
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					lcd_mega_ClearDispaly();
					lcd_mega_StringF(PSTR("Invalid Entry"));
					_delay_ms(1000);
					goto SET_DIVISION;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				return;//< unsuccessful return
			}
		}
	}
	if(lcd_Line2_pointer==(lcd_Line2_min+1))
	{
		division = lcd_Line2_data[lcd_Line2_min]-'0';
	}
	else if(lcd_Line2_pointer==(lcd_Line2_min+2))
	{
		division = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');;
	}
	if(division==0)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid Entry"));
		_delay_ms(1000);
		goto SET_DIVISION;
	}

	//< ZERO weight ADC_count calculation
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Clear LoadCell"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENTER"));

	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("-Calibrating-"));
	ADS123x_Calibrate();
	for(i=0;i<=ADC_BUFF_LENGTH;i++)
	{
		//< getting ADC_count from ADS123x
		ADC_getReading();
	}
	weight_zero_ADC_count = ADC_count;


	//< STEPS : USER enters weight
	for(i=0;i<max_steps;i++)
	{
SET_WEIGHT_STEPS:
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("STEP-X(Enter Weight)"));
		lcd_mega_gotoxy1(5);
		lcd_mega_SendData((i+1)+'0');

		set_string_in_lcd_dataF(LINE2,PSTR("(In KG): "),0,0);
		lcd_Line2_pointer = 9;
		lcd_Line2_max = lcd_Line2_pointer+6;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);

		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					//< store user input temperory
					//< if user has entered without typing anything
					if(lcd_Line2_pointer==lcd_Line2_min)
					{
						lcd_mega_ClearDispaly();
						lcd_mega_StringF(PSTR("Invalid Entry"));
						goto SET_WEIGHT_STEPS;
					}
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					return;//< unsuccessful return
				}
			}
		}
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("-Calibrating-"));
		ADS123x_Calibrate();
		for(j=0;j<=ADC_BUFF_LENGTH;j++)
		{
			//< getting ADC_count from ADS123x
			ADC_getReading();
		}
		lcd_Line2_data[lcd_Line2_pointer] = '\0';
		steps_weight[i] = (unsigned long int)(atol(&lcd_Line2_data[lcd_Line2_min]));
		steps_ADC_count[i] = ADC_count;
		steps_slope[i] = (double)(((double)steps_weight[i])/((double)(steps_ADC_count[i]-weight_zero_ADC_count)));
		steps_distace[i] = sqrt(square((double)(steps_ADC_count[i]-weight_zero_ADC_count)) + square((double)(steps_weight[i])));
	}

	//< set LCD Display
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Configuring..."));

	//< calculate final slope
	for(i=0;i<max_steps;i++)
	{
		slope_denom	+= (steps_distace[i]);					//< denominator of final slope formula
		slope_nume	+= (steps_slope[i] * steps_distace[i]);	//< numerator of final slope formula, slope used here temperory
	}
	slope = (slope_nume / slope_denom);

	///////////////////////////////
	//< WEIGHT_CONSTANTS Setting //
	///////////////////////////////
	//< unit_weight_value
	weight_const.unit_weight_value = division;

	//< unit_weight_count
	unit_weight_count1 = (((((double)(weight_const.unit_weight_value)) * 1.0)/slope) + (double)weight_zero_ADC_count);
	unit_weight_count2 = (((((double)(weight_const.unit_weight_value)) * 2.0)/slope) + (double)weight_zero_ADC_count);
	weight_const.unit_weight_count = (unsigned long int)((unit_weight_count2 - unit_weight_count1)*1000.0);

	//< ADC_min_count
	weight_const.ADC_min_count = weight_zero_ADC_count;

	//< max_weight_value
	weight_const.max_weight_value = capacity;

	//< ADC_max_count
	weight_const.ADC_max_count = (unsigned long int)((((double)capacity)/slope) + (double)weight_zero_ADC_count);

	//< store all constant into EEPROM
	WEIGHT_CONSTANTS_Write();

	_delay_ms(1000);
}

void setting_smartCallibration()
{
	signed long int wrong_weight;
	signed long int actual_weight;
	double _adc_count;
	double _unit_weight_count;

	//< take wrong weight input from user
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Wrong Wt(in Kg)"),0,0);
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		wrong_weight = atol(lcd_Line2_data);
		if(!wrong_weight)
		{
			return;
		}
	}

	//< take actual weight input from user
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Actual Wt(in Kg)"),0,0);
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		actual_weight = atol(lcd_Line2_data);
		if(!actual_weight)
		{
			return;
		}
	}

	//< check wrong_weight and actual_weight are multiple of unit_weight_value
	//< if not convert it into multiple
	if((wrong_weight%weight_const.unit_weight_value)>(weight_const.unit_weight_value/2))
	{
		wrong_weight = ((wrong_weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value))+(weight_const.unit_weight_value);
	}
	else
	{
		wrong_weight = ((wrong_weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value));
	}
	if((actual_weight%weight_const.unit_weight_value)>(weight_const.unit_weight_value/2))
	{
		actual_weight = ((actual_weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value))+(weight_const.unit_weight_value);
	}
	else
	{
		actual_weight = ((actual_weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value));
	}

	//< calculate ADC count
	_adc_count = (((double)(wrong_weight)*(double)(weight_const.unit_weight_count))/((double)(weight_const.unit_weight_value) * 1000.0) + weight_const.ADC_min_count);

	//< calculate new unit_weight_count(slope)
	_unit_weight_count = ((((_adc_count)-(double)(weight_const.ADC_min_count))/(actual_weight)) * (double)(weight_const.unit_weight_value));

	//< calculate adc_max_count
	weight_const.ADC_max_count = (signed long int)((_unit_weight_count)*(weight_const.max_weight_value)+(weight_const.ADC_min_count));

	//< store in eeprom
	weight_const.unit_weight_count = (unsigned long int)(_unit_weight_count*1000.0);

	WEIGHT_CONSTANTS_Write();
}


void setting_AutoZero()
{
	unsigned char i;

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("-Auto Zero-"));

	ADS123x_Calibrate();
	for(i=0;i<=ADC_BUFF_LENGTH;i++)
	{
		//< getting ADC_count from ADS123x
		ADC_getReading();
	}

	weight_const.ADC_min_count = ADC_count;
	//WEIGHT_CONSTANTS_Write();
}

void setting_view_ADC_count()
{
	char str[10];
	unsigned char len,i=0;

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Count: "));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENT to Exit"));

	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				return;
			}
		}

		//< getting ADC_count from ADS123x
		ADC_getReading();
		ltoa(ADC_count,str,10);
		len = strlen(str);
		lcd_mega_gotoxy1(7);
		lcd_mega_StringF(PSTR("           "));
		lcd_mega_gotoxy1(7);
		lcd_mega_String(str);

		/*
		if(len>6)
		{
			len=6;
		}
		for(i=0;i<6-len;i++)
		{
			str[i]=DIGIT_OFF;
		}
		for(i=6-len;i<6;i++)
		{
			if(str[i-(6-len)]=='-')
			{
				str[i] = DIGIT_MINUS;
			}
			else
			{
				str[i] = str[i-(6-len)]-'0';
			}
		}

		//< displaying count on LCD
		lcd_mega_gotoxy1(7);
		lcd_mega_SendData(LCD_CODE[str[0]]);//< MSB
		lcd_mega_SendData(LCD_CODE[str[1]]);
		lcd_mega_SendData(LCD_CODE[str[2]]);
		lcd_mega_SendData(LCD_CODE[str[3]]);
		lcd_mega_SendData(LCD_CODE[str[4]]);
		lcd_mega_SendData(LCD_CODE[str[5]]);//< LSB


		//< displaying count on internal 7-segment display
		shift_register_WriteByte(SEG_CODE[str[5]]);//< LSB
		shift_register_WriteByte(SEG_CODE[str[4]]);
		shift_register_WriteByte(SEG_CODE[str[3]]);
		shift_register_WriteByte(SEG_CODE[str[2]]);
		shift_register_WriteByte(SEG_CODE[str[1]]);
		shift_register_WriteByte(SEG_CODE[str[0]]);//< MSB
		shift_register_LatchPulse();
		*/
	}
}

void setting_code_base_system()
{
	unsigned char j,i;

	//< CODE1 Enable/Disable //////////////////////////////////
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code base Sys1"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY1_ENABLE);
	if(j!='Y' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< Code 1 setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY1_ENABLE,i);
	///////////////////////////////////////////////////////////

	//< CODE2 Enable/Disable //////////////////////////////////
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code base Sys2"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY2_ENABLE);
	if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< Code 2 setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY2_ENABLE,i);

	if (i=='Y')
	{
		//if code 2 system enabled ask to associate Charges or Mob. no. options
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code2 Charges"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
		//< showing stored data from EEPROM
		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE);
		if(j!='2')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}

		//< Code charges Enabled/Disabled setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'2');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'N');
			}
		}

		//Check if mobile no. integration is done
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code2 Mobile Ent"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
		//< showing stored data from EEPROM
		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE);
		if(j!='2')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//< Mobile no. association setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'2');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'N');
			}
		}
	}
	///////////////////////////////////////////////////////////

	//< CODE3 Enable/Disable //////////////////////////////////
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code base Sys3"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY3_ENABLE);
	if(j!='Y' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< Code 3 setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY3_ENABLE,i);

	//if code 3 is enabled ask to associate charges / mobile no.
	if (i=='Y')
	{
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code3 Charges"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);

		//< showing stored data from EEPROM
		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE);
		if(j!='3')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}

		//< Code charges Enabled/Disabled setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'3');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'N');
			}
		}

		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code3 Mobile Ent"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
		//< showing stored data from EEPROM
		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE);
		if(j!='3')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//< Mobile no. association setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'3');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'N');
			}
		}
	}
	////////////////////////////////////////////////////////////////

	//< CODE4 Enable/Disable ///////////////////////////////////////
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code base Sys4"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
	//< showing stored data from EEPROM
	j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY4_ENABLE);
	if(j!='Y' && j!='N')
	{
		j='N';
	}
	if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//< Code 4 setting writing in EEPROM
	eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_ENTRY4_ENABLE,i);

	if (i=='Y')
	{
		//if code 4 system enabled ask to associate Charges or Mob. no. options
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code4 Charges"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
		//< showing stored data from EEPROM

		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE);
		if(j!='4')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}

		//< Code charges Enabled/Disabled setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'4');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE,'N');
			}
		}

		//mobile no integration for code 4
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code4 Mobile Ent"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N): "),0,0);
		//< showing stored data from EEPROM
		j=eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE);
		if(j!='4')
		{
			j='N';
		}
		else
		{
			j='Y';
		}
		//get user input
		if(get_character_from_user_P(&i,PSTR("YN"),j,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//< Mobile no. association setting writing in EEPROM
		if (i=='Y')
		{
			eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'4');
		}
		else if (i=='N')
		{
			if (j=='Y')
			{
				eeprom_mega_WriteByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE,'N');
			}
		}
	}
	///////////////////////////////////////////////////////////////
}

void setting_code1_input_system()
{
	unsigned char i,location,str[11],pot,flag=0;
	signed long int _tare_weight,tare;

//< label to get back to next code entry if needed
SET_ANOTHER_CODE1:
	//get code table in sd buffer
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
	//get current pointer in the database
	pot = SD_data_buf[CODE_TABLE_POINTER];
	//if pointer value is wrong
	if (pot>200)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE1 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//location of data block where to store current code1 entry
	location = SD_data_buf[CODE_ENTRY_POINTER];
	//if location value is wrong than reset it to 0
	if (location>=32)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE1 Mem Full"));
		_delay_ms(1000);
		return;
	}

	//set LCD line 1 from eeprom data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
	}
	//set LCD line 2
	set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);

	//take user input
	fun_ret = get_string_from_user(lcd_Line2_data,6,6,9,USE_CHARACTERS|USE_NUMERIC,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//Check if input code already exists
		fun_ret=get_code_location(lcd_Line2_data,CODE1_ENTRY_TABLE);
		if (fun_ret == NO_MATCH_FOUND)
		{
			//update data buffer for Digits as per user input
			for (i=0;i<3;i++)
			{
				SD_data_buf[pot++]=lcd_Line2_data[i];
			}
			//save location of the Data for this code
			SD_data_buf[pot++]=location;
			//increment data pointer
			SD_data_buf[CODE_ENTRY_POINTER]=location+1;
			//increment and save the next pointer in current table
			SD_data_buf[CODE_TABLE_POINTER]=pot;
			//save current buffer value and update Table
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
		}
		else
		{
			location = fun_ret;
			flag = 1;
		}
	}

	//get values of next table to store Data for current code
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//Set LCD line 1 data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i);
	}
	//if Existing entry set line 2 data
	if (flag == 1)
	{
		for (i=0;i<25;i++)
		{
			lcd_Line2_data[i]=SD_data_buf[CODE1_ENTRY_DATA+i];
		}
	}
	//get user input
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//save user input data in buffer
		for (i=0;i<25;i++)
		{
			SD_data_buf[CODE1_ENTRY_DATA+i]=lcd_Line2_data[i];
		}
	}
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Tare Wt A/M:"),0,0);
	if(get_character_from_user_P(&i,PSTR("AM"),'M',0,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	//get tare weight
	if (i=='A')
	{
		//get Tare Weight automatically from scale
		lcd_mega_ClearDispaly();
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Press ENTER"));
		while(1)
		{
			//< getting ADC_count from ADS123x
			ADC_getReading();

			//< get current weight from ADC
			ADC_getWeight();

			//< displaying weight on LCD
			lcd_mega_gotoxy1(0);
			lcd_mega_SendData(LCD_CODE[weight_str[0]]);//< MSB
			lcd_mega_SendData(LCD_CODE[weight_str[1]]);
			lcd_mega_SendData(LCD_CODE[weight_str[2]]);
			lcd_mega_SendData(LCD_CODE[weight_str[3]]);
			lcd_mega_SendData(LCD_CODE[weight_str[4]]);
			lcd_mega_SendData(LCD_CODE[weight_str[5]]);//< LSB
			lcd_mega_SendData(' ');
			lcd_mega_SendData('K');
			lcd_mega_SendData('G');

			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
			}
		}
		_tare_weight = weight;
	}
	else if (i=='M')
	{
		//get tare weight manually
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Tare Weight :"),0,0);
		//if an existing entry show current saved tare weight
		if (flag == 1)
		{
			tare = ((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE] << 24)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+1] << 16)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+2] << 8)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+3]);
			ltoa(tare,lcd_Line2_data,10);
		}
		//take user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else
		{
			_tare_weight = atol(lcd_Line2_data);
		}
	}
	//< save tare weight
	SD_data_buf[CODE1_ENTRY_TARE] = (unsigned char)(_tare_weight >> 24);//< MSB
	SD_data_buf[CODE1_ENTRY_TARE+1] = (unsigned char)(_tare_weight >> 16);
	SD_data_buf[CODE1_ENTRY_TARE+2] = (unsigned char)(_tare_weight >> 8);
	SD_data_buf[CODE1_ENTRY_TARE+3] = (unsigned char)(_tare_weight);		//< LSB

	//save Buffer to appropriate location
	SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//ask user to input next code
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter next code"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		goto SET_ANOTHER_CODE1;
	}
	else
	{
		return;
	}
}

void setting_code2_input_system()
{
	unsigned char i,location,pot,flag=0;
	signed long int charges;

//< label to get back to next code entry if needed
SET_ANOTHER_CODE2:
	//get code table in sd buffer
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE2_ENTRY_TABLE);
	//get current pointer in the database
	pot = SD_data_buf[CODE_TABLE_POINTER];
	//if pointer value is wrong
	if (pot>200)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//location of data block where to store current code2 entry
	location = SD_data_buf[CODE_ENTRY_POINTER];
	//if location value is wrong
	if (location>=32)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//set LCD line 1 from eeprom data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
	}
	//set LCD line 2
	set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);

	//take user input
	fun_ret=get_string_from_user(lcd_Line2_data,6,6,9,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//Check if input code already exists
		fun_ret=get_code_location(lcd_Line2_data,CODE2_ENTRY_TABLE);
		if (fun_ret == NO_MATCH_FOUND)
		{
			//update data buffer for Digits as per user input
			for (i=0;i<3;i++)
			{
				SD_data_buf[pot++]=lcd_Line2_data[i];
			}
			//save location of the Data for this code
			SD_data_buf[pot++]=location;
			//increment data pointer
			SD_data_buf[CODE_ENTRY_POINTER]=location+1;
			//increment and save the next pointer in current table
			SD_data_buf[CODE_TABLE_POINTER]=pot;
			//save current buffer value and update Table
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE2_ENTRY_TABLE);
		}
		else
		{
			location = fun_ret;
			flag = 1;
		}
	}

	//get values of next table to store Data for current code
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//Set LCD line 1 data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i);
	}
	//if Existing entry set line 2 data
	if (flag == 1)
	{
		for (i=0;i<25;i++)
		{
			lcd_Line2_data[i]=SD_data_buf[CODE2_ENTRY_DATA+i];
		}
	}
	//get user input
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//save user input data in buffer
		for (i=0;i<25;i++)
		{
			SD_data_buf[CODE2_ENTRY_DATA+i]=lcd_Line2_data[i];
		}
	}
	//if charges are associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='2')
	{
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Charges:"),0,0);
		if (flag==1)
		{
			charges = ((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES] << 24)+\
						((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+1] << 16)+\
						((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+2] << 8)+\
						((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		//take user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			charges = atol(lcd_Line2_data);
			//save Charges
			SD_data_buf[CODE2_ENTRY_CHARGES] = (unsigned char)(charges >> 24);//< MSB
			SD_data_buf[CODE2_ENTRY_CHARGES+1] = (unsigned char)(charges >> 16);
			SD_data_buf[CODE2_ENTRY_CHARGES+2] = (unsigned char)(charges >> 8);
			SD_data_buf[CODE2_ENTRY_CHARGES+3] = (unsigned char)(charges);		//< LSB
		}
	}
	//if Mob. no. is associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='2')
	{
		//set LCD data
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Mobile No.:"),0,0);
		//if an existing entry
		if (flag==1)
		{
			//show stored mobile no.
			for (i=0;i<12;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[CODE2_ENTRY_MOB_NO+i];
			}
		}
		//get user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,12,USE_NUMERIC|USE_ARROW_KEY,STORE_ALL_DATA);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//save user input in SD buffer
			for (i=0;i<12;i++)
			{
				SD_data_buf[CODE2_ENTRY_MOB_NO+i]=lcd_Line2_data[i];
			}
		}
	}
	//save SD buffer with updates
	SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//ask user to input next code
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter next code"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		goto SET_ANOTHER_CODE2;
	}
	else
	{
		return;
	}
}

void setting_code3_input_system()
{
	unsigned char i,location,pot,flag=0;
	signed long int charges;

SET_ANOTHER_CODE3:  //label to get back to next code entry if needed
	//get code table in sd buffer
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE3_ENTRY_TABLE);
	//get current pointer in the database
	pot = SD_data_buf[CODE_TABLE_POINTER];
	//if pointer value is wrong than reset it to 0
	if (pot>200)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//location of data block where to store current code1 entry
	location = SD_data_buf[CODE_ENTRY_POINTER];
	//if location value is wrong than reset it to 0
	if (location>=32)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//set LCD line 1 from eeprom data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
	}
	//set LCD line 2
	set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);

	//take user input
	fun_ret=get_string_from_user(lcd_Line2_data,6,6,9,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if (fun_ret == RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//Check if input code already exists
		fun_ret=get_code_location(lcd_Line2_data,CODE3_ENTRY_TABLE);
		if (fun_ret == NO_MATCH_FOUND)
		{
			//update data buffer for Digits as per user input
			for (i=0;i<3;i++)
			{
				SD_data_buf[pot++]=lcd_Line2_data[i];
			}
			//save location of the Data for this code
			SD_data_buf[pot++]=location;
			//increment data pointer
			SD_data_buf[CODE_ENTRY_POINTER]=location+1;
			//increment and save the next pointer in current table
			SD_data_buf[CODE_TABLE_POINTER]=pot;
			//save current buffer value and update Table
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE3_ENTRY_TABLE);
		}
		else
		{
			location = fun_ret;
			flag = 1;
		}
	}

	//get values of next table to store Data for current code
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//Set LCD line 1 data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i);
	}
	//if Existing entry set line 2 data
	if (flag == 1)
	{
		for (i=0;i<25;i++)
		{
			lcd_Line2_data[i]=SD_data_buf[CODE3_ENTRY_DATA+i];
		}
	}
	//get user input
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//save user input data in buffer
		for (i=0;i<25;i++)
		{
			SD_data_buf[CODE3_ENTRY_DATA+i]=lcd_Line2_data[i];
		}
	}

	//if charges are associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='3')
	{
		//get tare weight manually
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Charges:"),0,0);
		if (flag==1)
		{
			charges = ((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES] << 24)+\
						((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+1] << 16)+\
						((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+2] << 8)+\
						((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		//take user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret)
		{
			charges = atol(lcd_Line2_data);
			//save Charges
			SD_data_buf[CODE3_ENTRY_CHARGES] = (unsigned char)(charges >> 24);//< MSB
			SD_data_buf[CODE3_ENTRY_CHARGES+1] = (unsigned char)(charges >> 16);
			SD_data_buf[CODE3_ENTRY_CHARGES+2] = (unsigned char)(charges >> 8);
			SD_data_buf[CODE3_ENTRY_CHARGES+3] = (unsigned char)(charges);		//< LSB
		}
	}
	//if Mob. no. is associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='3')
	{
		//set LCD data
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Mobile No.:"),0,0);
		//if an existing entry
		if (flag==1)
		{
			//show stored mobile no.
			for (i=0;i<12;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[CODE3_ENTRY_MOB_NO+i];
			}
		}
		//get user input
		fun_ret=get_string_from_user(lcd_Line2_data,0,0,12,USE_NUMERIC|USE_ARROW_KEY,STORE_ALL_DATA);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//save user input in SD buffer
			for (i=0;i<12;i++)
			{
				SD_data_buf[CODE3_ENTRY_MOB_NO+i]=lcd_Line2_data[i];
			}
		}
	}
	//save SD buffer with updates
	SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//ask user to input next code
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter next code"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		goto SET_ANOTHER_CODE3;
	}
	else
	{
		return;
	}
}

void setting_code4_input_system()
{
	unsigned char i,location,pot,flag=0;
	signed long int charges;

SET_ANOTHER_CODE4:  //label to get back to next code entry if needed
	//get code table in sd buffer
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE4_ENTRY_TABLE);
	//get current pointer in the database
	pot = SD_data_buf[CODE_TABLE_POINTER];
	//if pointer value is wrong than reset it to 0
	if (pot>200)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//location of data block where to store current code4 entry
	location = SD_data_buf[CODE_ENTRY_POINTER];
	//if location value is wrong than reset it to 0
	if (location>=32)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("CODE2 Mem Full"));
		_delay_ms(1000);
		return;
	}
	//set LCD line 1 from eeprom data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
	}
	//set LCD line 2
	set_string_in_lcd_dataF(LINE2,PSTR("Code: "),0,0);

	//take user input
	fun_ret=get_string_from_user(lcd_Line2_data,6,6,9,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if (fun_ret == RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//Check if input code already exists
		fun_ret=get_code_location(lcd_Line2_data,CODE4_ENTRY_TABLE);
		if (fun_ret == NO_MATCH_FOUND)
		{
			//update data buffer for Digits as per user input
			for (i=0;i<3;i++)
			{
				SD_data_buf[pot++]=lcd_Line2_data[i];
			}
			//save location of the Data for this code
			SD_data_buf[pot++]=location;
			//increment data pointer
			SD_data_buf[CODE_ENTRY_POINTER]=location+1;
			//increment and save the next pointer in current table
			SD_data_buf[CODE_TABLE_POINTER]=pot;
			//save current buffer value and update Table
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE4_ENTRY_TABLE);
		}
		else
		{
			location = fun_ret;
			flag = 1;
		}
	}


	//get values of next table to store Data for current code
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//Set LCD line 1 data
	clear_lcd_data(LINE_BOTH);
	for (i=0;i<12;i++)
	{
		lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i);
	}
	//if Existing entry set line 2 data
	if (flag == 1)
	{
		for (i=0;i<25;i++)
		{
			lcd_Line2_data[i]=SD_data_buf[CODE4_ENTRY_DATA+i];
		}
	}
	//get user input
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//save user input data in buffer
		for (i=0;i<25;i++)
		{
			SD_data_buf[CODE4_ENTRY_DATA+i]=lcd_Line2_data[i];
		}
	}
	//if charges are associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='4')
	{
		//get tare weight manually
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Charges:"),0,0);
		if (flag==1)
		{
			charges = ((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES] << 24)+\
						((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+1] << 16)+\
						((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+2] << 8)+\
						((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+3]);
			ltoa(charges,lcd_Line2_data,10);
		}
		//take user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,6,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			charges = atol(lcd_Line2_data);
			//save Charges
			SD_data_buf[CODE3_ENTRY_CHARGES] = (unsigned char)(charges >> 24);//< MSB
			SD_data_buf[CODE3_ENTRY_CHARGES+1] = (unsigned char)(charges >> 16);
			SD_data_buf[CODE3_ENTRY_CHARGES+2] = (unsigned char)(charges >> 8);
			SD_data_buf[CODE3_ENTRY_CHARGES+3] = (unsigned char)(charges);		//< LSB
		}
	}
	//if Mob. no. is associated with second entry than get user input
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='4')
	{
		//set LCD data
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Mobile No.:"),0,0);
		if (flag==1)
		{
			//show stored mobile no.
			for (i=0;i<12;i++)
			{
				lcd_Line2_data[i]=SD_data_buf[CODE4_ENTRY_MOB_NO+i];
			}
		}
		//get user input
		fun_ret = get_string_from_user(lcd_Line2_data,0,0,12,USE_NUMERIC|USE_ARROW_KEY,STORE_ALL_DATA);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			//save user input in SD buffer
			for (i=0;i<12;i++)
			{
				SD_data_buf[CODE4_ENTRY_MOB_NO+i]=lcd_Line2_data[i];
			}
		}
	}
	//save SD buffer with updates
	SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+location);
	//ask user to input next code
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter next code"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		goto SET_ANOTHER_CODE4;
	}
	else
	{
		return;
	}
}

void setting_reset_code1()
{
	unsigned char i;
	unsigned int j;

	//set LCD display
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code1 Memory Clr."),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		//First reset the Code table
		for (j=0;j<512;j++)
		{
			SD_data_buf[j]=0xFF;
		}
		SD_data_buf[CODE_ENTRY_POINTER]=0;
		SD_data_buf[CODE_TABLE_POINTER]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
		//read Each 32 code blocks and reset them
		for (j=0;j<32;j++)
		{
			SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
			for (i=0;i<CODE2_ENTRY_DATA;i++)
			{
				SD_data_buf[i]=0xFF;
			}
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+j);
		}
	}
	else
	{
		return;
	}
}

void setting_reset_code2()
{
	unsigned char i;
	unsigned int j;
	//set LCD display
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code2 Memory Clr."),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		//First reset the Code table
		for (j=0;j<=512;j++)
		{
			SD_data_buf[i]=0xFF;
		}
		SD_data_buf[CODE_ENTRY_POINTER]=0;
		SD_data_buf[CODE_TABLE_POINTER]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE2_ENTRY_TABLE);
		//read Each 32 code blocks and reset them
		for (i=0;i<32;i++)
		{
			SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
			for (j=CODE2_ENTRY_DATA;j<CODE3_ENTRY_DATA;j++)
			{
				SD_data_buf[j]=0xFF;
			}
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		}
	}
	else
	{
		return;
	}
}

void setting_reset_code3()
{
	unsigned char i;
	unsigned int j;
	//set LCD display
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code3 Memory Clr."),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		//First reset the Code table
		for (j=0;j<=512;j++)
		{
			SD_data_buf[j]=0xFF;
		}
		SD_data_buf[CODE_ENTRY_POINTER]=0;
		SD_data_buf[CODE_TABLE_POINTER]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE3_ENTRY_TABLE);
		//read Each 32 code blocks and reset them
		for (i=0;i<32;i++)
		{
			SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
			for (j=CODE3_ENTRY_DATA;j<CODE4_ENTRY_DATA;j++)
			{
				SD_data_buf[j]=0xFF;
			}
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		}
	}
	else
	{
		return;
	}
}

void setting_reset_code4()
{
	unsigned char i;
	unsigned int j;
	//set LCD display
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Code4 Memory Clr."),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	//get User input
	if(get_character_from_user_P(&i,PSTR("YN"),'N',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		//First reset the Code table
		for (j=0;j<=512;j++)
		{
			SD_data_buf[j]=0xFF;
		}
		SD_data_buf[CODE_ENTRY_POINTER]=0;
		SD_data_buf[CODE_TABLE_POINTER]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE4_ENTRY_TABLE);
		//read Each 32 code blocks and reset them
		for (i=0;i<32;i++)
		{
			SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
			for (j=CODE4_ENTRY_DATA;j<(CODE4_ENTRY_MOB_NO+12);j++)
			{
				SD_data_buf[i]=0xFF;
			}
			SD_mega_WriteSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		}
	}
	else
	{
		return;
	}
}

void setting_code1_print()
{
	unsigned char i,count=0,j,location;
	signed long int tare;
	char str[11];

	//lcd_mega_StringF("Printing.......");
	//Print header - Footer function goes here --------------------------
	//set Printer fonts
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;

	//Print The Title
	printer_writeStringF(PSTR("ENTRY1 CODES: "));
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
	}
	PRINTER_ENTER;

	//Print Table Header
	printer_writeStringF(PSTR("CODE | ")); //CODE |
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+i));
	}       //CODE | Vehicle no.
	for (i=12;i<25;i++)
	{
		printer_writeData(' ');
	}
	printer_writeStringF(PSTR(" | Tare Weight"));
	PRINTER_ENTER;
	//Print Separator line next
	for(i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	//read table data to start printing
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
	location=SD_data_buf[CODE_ENTRY_POINTER];
	//Start Printing Data
	for (i=0;i<location;i++)
	{
		count=i*4;
		//read table to get 3 digit data
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE1_ENTRY_TABLE);
		//print Code digits (3 chars)
		printer_writeData(' ');
		printer_writeData(SD_data_buf[count]);
		printer_writeData(SD_data_buf[count+1]);
		printer_writeData(SD_data_buf[count+2]);
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		//Print Data (25 chars)
		for (j=0;j<25;j++)
		{
			printer_writeData(SD_data_buf[CODE1_ENTRY_DATA+j]);
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Print Tare weight
		tare = ((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE] << 24)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+1] << 16)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+2] << 8)+\
					((unsigned long int)SD_data_buf[CODE1_ENTRY_TARE+3]);
		printer_writeString(ltoa(tare,str,10));
		PRINTER_ENTER;
	}
	PRINTER_ENTER;
}

void setting_code2_print()
{
	unsigned char i,count=0,j,location;
	signed long int valch;
	char str[11];
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Printing......."));
	//Print header - Footer function goes here --------------------------
	//set Printer fonts
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;

	//Print The Title
	printer_writeStringF(PSTR("ENTRY2 CODES: "));
	for (i=0;i<25;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
	}
	PRINTER_ENTER;

	//Print Table Header
	printer_writeStringF(PSTR("CODE | ")); //CODE |
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY2+i));
	}       //CODE | Vehicle Type (example)
	for (i=12;i<25;i++)
	{
		printer_writeData(' ');
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='2')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("CHARGES")); //CODE | VEHICLE TYPE | CHARGES
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='2')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("MOBILE NO.")); //CODE | VEHICLE TYPE | CHARGES | MOBILE NO.
	}
	PRINTER_ENTER;
	//Print Separator line next
	for(i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	//read table data to start printing
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE2_ENTRY_TABLE);
	location=SD_data_buf[CODE_ENTRY_POINTER];
	//Start Printing Data
	for (i=0;i<location;i++)
	{
		count=i*4;
		//read table to get 3 digit data
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE2_ENTRY_TABLE);
		//print Code digits (3 chars)
		printer_writeData(' ');
		printer_writeData(SD_data_buf[count]);
		printer_writeData(SD_data_buf[count+1]);
		printer_writeData(SD_data_buf[count+2]);
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Read data table next
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		//Print Data (25 chars)
		for (j=0;j<25;j++)
		{
			printer_writeData(SD_data_buf[CODE2_ENTRY_DATA+j]);
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Print Charges if enabled weight
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='2')
		{
			valch = ((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES] << 24)+\
			((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+1] << 16)+\
			((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+2] << 8)+\
			((unsigned long int)SD_data_buf[CODE2_ENTRY_CHARGES+3]);
			ltoa(valch,str,10);
			for (j=0;j<(6-strlen(str));j++)
			{
				printer_writeData(' ');
			}
			printer_writeString(str);

		}
		//print mobile no. if associated
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='2')
		{
			printer_writeData(' ');
			printer_writeData('|');
			printer_writeData(' ');
			for (j=0;j<12;j++)
			{
				printer_writeData(SD_data_buf[CODE2_ENTRY_MOB_NO+j]);
			}
		}

		PRINTER_ENTER;
	}
	PRINTER_ENTER;
}

void setting_code3_print()
{
	unsigned char i,count=0,j,location;
	signed long int valch;
	char str[11];
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Printing......."));
	//Print header - Footer function goes here --------------------------
	//set Printer fonts
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;

	//Print The Title
	printer_writeStringF(PSTR("ENTRY3 CODES: "));
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
	}
	PRINTER_ENTER;

	//Print Table Header
	printer_writeStringF(PSTR("CODE | ")); //CODE |
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY3+i));
	}       //CODE | COMODITY (example)
	for (i=12;i<25;i++)
	{
		printer_writeData(' ');
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='3')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("CHARGES")); //CODE | VEHICLE TYPE | CHARGES
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='3')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("MOBILE NO.")); //CODE | VEHICLE TYPE | CHARGES | MOBILE NO.
	}
	PRINTER_ENTER;
	//Print Separator line next
	for(i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	//read table data to start printing
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE3_ENTRY_TABLE);
	location=SD_data_buf[CODE_ENTRY_POINTER];
	//Start Printing Data
	for (i=0;i<location;i++)
	{
		count=i*4;
		//read table to get 3 digit data
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE3_ENTRY_TABLE);
		//print Code digits (3 chars)
		printer_writeData(' ');
		printer_writeData(SD_data_buf[count]);
		printer_writeData(SD_data_buf[count+1]);
		printer_writeData(SD_data_buf[count+2]);
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Read data table next
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		//Print Data (25 chars)
		for (j=0;j<25;j++)
		{
			printer_writeData(SD_data_buf[CODE3_ENTRY_DATA+j]);
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Print Charges if enabled weight
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='3')
		{
			valch = ((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES] << 24)+\
			((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+1] << 16)+\
			((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+2] << 8)+\
			((unsigned long int)SD_data_buf[CODE3_ENTRY_CHARGES+3]);
			ltoa(valch,str,10);
			for (j=0;j<(6-strlen(str));j++)
			{
				printer_writeData(' ');
			}
			printer_writeString(str);
		}
		//print mobile no. if associated
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='3')
		{
			printer_writeData(' ');
			printer_writeData('|');
			printer_writeData(' ');
			for (j=0;j<12;j++)
			{
				printer_writeData(SD_data_buf[CODE3_ENTRY_MOB_NO+j]);
			}
		}

		PRINTER_ENTER;
	}
	PRINTER_ENTER;
}

void setting_code4_print()
{
	unsigned char i,count=0,j,location;
	signed long int valch;
	char str[11];

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Printing......."));
	//Print header - Footer function goes here --------------------------
	//set Printer fonts
	PRINTER_ESC;
	PRINTER_SET_RECEIPT_FONT;

	//Print The Title
	printer_writeStringF(PSTR("ENTRY4 CODES: "));
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i));
	}
	PRINTER_ENTER;
	//Print Table Header
	printer_writeStringF(PSTR("CODE | ")); //CODE |
	for (i=0;i<12;i++)
	{
		printer_writeData(eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY4+i)); //CODE | Vehicle Type (example)
	}
	for (i=12;i<25;i++)
	{
		printer_writeData(' ');
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='4')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("CHARGES")); //CODE | VEHICLE TYPE | CHARGES
	}
	if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='4')
	{
		printer_writeStringF(PSTR(" | "));
		printer_writeStringF(PSTR("MOBILE NO.")); //CODE | VEHICLE TYPE | CHARGES | MOBILE NO.
	}
	PRINTER_ENTER;
	//Print Separator line next
	for(i=0;i<80;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;

	//read table data to start printing
	SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE4_ENTRY_TABLE);
	location=SD_data_buf[CODE_ENTRY_POINTER];
	//Start Printing Data
	for (i=0;i<location;i++)
	{
		count=i*4;
		//read table to get 3 digit data
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_TABLE_START+CODE4_ENTRY_TABLE);
		//print Code digits (3 chars)
		printer_writeData(' ');
		printer_writeData(SD_data_buf[count]);
		printer_writeData(SD_data_buf[count+1]);
		printer_writeData(SD_data_buf[count+2]);
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Read data table next
		SD_mega_ReadSingleBlock(SD_data_buf,CODE_ENTRY_START+i);
		//Print Data (25 chars)
		for (j=0;j<25;j++)
		{
			printer_writeData(SD_data_buf[CODE4_ENTRY_DATA+j]);
		}
		printer_writeData(' ');
		printer_writeData('|');
		printer_writeData(' ');
		//Print Charges if enabled weight
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_CHARGES_ENABLE)=='4')
		{
			valch = ((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES] << 24)+\
			((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+1] << 16)+\
			((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+2] << 8)+\
			((unsigned long int)SD_data_buf[CODE4_ENTRY_CHARGES+3]);
			ltoa(valch,str,10);
			for (j=0;j<(6-strlen(str));j++)
			{
				printer_writeData(' ');
			}
			printer_writeString(str);
		}
		//print mobile no. if associated
		if (eeprom_mega_ReadByte(ADD_CODE_ENABLED_ENTRIES+CODE_MOBILE_NO_ENABLE)=='4')
		{

			printer_writeData(' ');
			printer_writeData('|');
			printer_writeData(' ');
			for (j=0;j<12;j++)
			{
				printer_writeData(SD_data_buf[CODE4_ENTRY_MOB_NO+j]);
			}
		}

		PRINTER_ENTER;
	}
	PRINTER_ENTER;
}

void setting_charge_type()
{
	unsigned char i,temp,line_len;

	//set LCD
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Bag Base System"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N):"),0,0);
	i = eeprom_mega_ReadByte(ADD_BAG_BASE_SYSTEM);
	//get User input
	if (get_character_from_user_P(&i,PSTR("YN"),i,8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	eeprom_mega_WriteByte(ADD_BAG_BASE_SYSTEM,i);


	i = eeprom_mega_ReadByte(ADD_BAG_BASE_SYSTEM);
	if (i == 'Y')
	{
		//set LCD
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Type of Bags"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(max 3):"),0,0);
		i = eeprom_mega_ReadByte(ADD_CHARGE_TYPE);
		//get User input
		if (get_character_from_user_P(&i,PSTR("0123"),i,9,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		eeprom_mega_WriteByte(ADD_CHARGE_TYPE,i);
	}
	else
	{
		//set LCD
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Type of Charges"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(max 3):"),0,0);
		i = eeprom_mega_ReadByte(ADD_CHARGE_TYPE);
		//get User input
		if (get_character_from_user_P(&i,PSTR("0123"),i,9,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		eeprom_mega_WriteByte(ADD_CHARGE_TYPE,i);

		//Code charges Editable settings
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code Charge Edit"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N):"),0,0);
		i = eeprom_mega_ReadByte(ADD_CODE_CHARGES_EDIT);
		//get User input
		if (get_character_from_user_P(&i,PSTR("YN"),i,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_CODE_CHARGES_EDIT,i);

		//Code mobile editable settings
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Code Mobile Edit"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N):"),0,0);
		i = eeprom_mega_ReadByte(ADD_CODE_MOBILE_EDIT);
		//get User input
		if (get_character_from_user_P(&i,PSTR("YN"),i,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_CODE_MOBILE_EDIT,i);

			//< Ticket Type
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Ticket Type:"),0,0);
		i = eeprom_mega_ReadByte(ADD_TICKET_TYPE);
		//get User input
		if (get_character_from_user_P(&i,PSTR("1234567"),i,0,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		eeprom_mega_WriteByte(ADD_TICKET_TYPE,i);

		//< Line Feed
		//< Set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Line Feed"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(max 20): "),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+NO_LINE_FEED);
		lcd_Line2_data[10]=(i/10)+'0';
		lcd_Line2_data[11]=(i%10)+'0';
		lcd_Line2_data[12]=NULL;
		//get User input
		fun_ret = get_string_from_user(lcd_Line2_data,10,10,12,USE_NUMERIC,STORE_ALL_DATA);
		if (fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (lcd_Line2_data[1]==' ')
			{
				eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+NO_LINE_FEED,lcd_Line2_data[0]-'0');
			}
			else
			{
				temp=(lcd_Line2_data[0]-'0')*10 + lcd_Line2_data[1]-'0';
				if(temp>20)
				{
					temp=20;
				}
				eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+NO_LINE_FEED,temp);
			}
		}

		//WT Bold/Normal setting
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Wt. Bold/Normal"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(B/N):"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL);
		//get User input
		if (get_character_from_user_P(&i,PSTR("BN"),i,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+WT_BOLD_NORMAL,i);

		//set LCD
		//gET PRINTER TYPE
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Printer type:"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(1/2/3/4):"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+PRINTER_TYPE);
		//get User input
		if (get_character_from_user_P(&i,PSTR("1234"),i,12,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+PRINTER_TYPE,i);

		//get no. of weighment1 copy
		//set LCD
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("No. of Copies"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("1st Weighment:"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+COPY_NO_WEIGHMENT1);
		//get User input
		if (get_character_from_user_P(&i,PSTR("123456789"),i,15,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+COPY_NO_WEIGHMENT1,i);

		//get no. of weighment2 copy
		//set LCD
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("No. of Copies"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("2nd Weighment:"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+COPY_NO_WEIGHMENT2);
		//get User input
		if (get_character_from_user_P(&i,PSTR("123456789"),i,15,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+COPY_NO_WEIGHMENT2,i);

		//report date setting
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Use Report Date"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(1/2):"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+USE_REPORT_DATE);
		//get User input
		if (get_character_from_user_P(&i,PSTR("12"),i,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+USE_REPORT_DATE,i);

		//Duplicate copy setting
		//set LCD
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Print Duplicate"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Copy (Y/N):"),0,0);
		i = eeprom_mega_ReadByte(ADD_PRINTER_SETTINGS+PRINT_DUPLICATE_COPY);
		//get User input
		if (get_character_from_user_P(&i,PSTR("YN"),i,13,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		//save user data
		eeprom_mega_WriteByte(ADD_PRINTER_SETTINGS+PRINT_DUPLICATE_COPY,i);
	}

	//< Signature line1 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Signature Line1:"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE1+SIGNATURE_LINE1_LEN);
	if(line_len<=25)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE1+SIGNATURE_LINE1_DATA+i);
		}
	}
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< Signature line1 writing in EEPROM
		eeprom_mega_WriteByte(ADD_SIGNATURE_LINE1+SIGNATURE_LINE1_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<25;i++)
		{
			eeprom_mega_WriteByte(ADD_SIGNATURE_LINE1+SIGNATURE_LINE1_DATA+i,lcd_Line2_data[i]);
		}
	}


	//< Signature line2 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Signature Line2:"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE2+SIGNATURE_LINE2_LEN);
	if(line_len<=25)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE2+SIGNATURE_LINE2_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< Signature line2 writing in EEPROM
		eeprom_mega_WriteByte(ADD_SIGNATURE_LINE2+SIGNATURE_LINE2_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<25;i++)
		{
			eeprom_mega_WriteByte(ADD_SIGNATURE_LINE2+SIGNATURE_LINE2_DATA+i,lcd_Line2_data[i]);
		}
	}

	//< Signature line3 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Signature Line3:"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE3+SIGNATURE_LINE3_LEN);
	if(line_len<=25)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_SIGNATURE_LINE3+SIGNATURE_LINE3_DATA+i);
		}
	}
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< Signature line3 writing in EEPROM
		eeprom_mega_WriteByte(ADD_SIGNATURE_LINE3+SIGNATURE_LINE3_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<25;i++)
		{
			eeprom_mega_WriteByte(ADD_SIGNATURE_LINE3+SIGNATURE_LINE3_DATA+i,lcd_Line2_data[i]);
		}
	}
}

void setting_startup_header()
{
	unsigned char line_len;
	unsigned char i;

	//< startup header line1 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("S.Head1 (Max 16)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE1_LEN);
	if(line_len<=16)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE2_DATA+i);
		}
	}
	//< take user input
	fun_ret=get_string_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< if user pressed ENTER then save
		//< startup header line1 writing
		//< header length
		eeprom_mega_WriteByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE1_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<strlen(lcd_Line2_data);i++)
		{
			eeprom_mega_WriteByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE1_DATA+i,lcd_Line2_data[i]);
		}
	}


	//< startup header line2 setting
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("S.Head2 (Max 16)"),0,0);
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_LEN);
	if(line_len<=16)
	{
		for(i=0;i<line_len;i++)
		{
			lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_DATA+i);
		}
	}
	//< take user input
	fun_ret = get_string_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		//< if user pressed ENTER then save
		//< startup header line2 writing
		//< header length
		eeprom_mega_WriteByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_LEN,strlen(lcd_Line2_data));
		//< save header string into EEPROM
		for(i=0;i<strlen(lcd_Line2_data);i++)
		{
			eeprom_mega_WriteByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_DATA+i,lcd_Line2_data[i]);
		}
	}
}

void display_startup_header()
{
	unsigned char len,i,j;

	///////////////////////////////
	//< Displaying Startup Header//
	///////////////////////////////
	//< Header Line - 1
	lcd_mega_ClearDispaly();
	j=0;
	len=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE1_LEN);
	if(len<=16)
	{
		for(i=0;i<len;i++)
		{
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE1+STARTUP_HEADER_LINE1_DATA+i));
		}
		j=1;
	}
	//< Header Line - 2
	len=eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_LEN);
	if(len<=16)
	{
		for(i=0;i<len;i++)
		{
			lcd_mega_gotoxy2(i);
			lcd_mega_SendData(eeprom_mega_ReadByte(ADD_STARTUP_HEADER_LINE2+STARTUP_HEADER_LINE2_DATA+i));
		}
		j=1;
	}
	if(j)
	{
		_delay_ms(2000);
	}
}

void view_rst_no()
{
	unsigned long int rst;

	rst = RST_read();
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Current RST:"));
	lcd_mega_gotoxy2(0);
	ltoa(rst,lcd_Line2_data,10);
	lcd_mega_String(lcd_Line2_data);
	fun_ret = wait_for_enter();
	return;
}

void setting_clear_rst_mem()
{
	unsigned char i;
	unsigned long int clc;
	//Set LCd display
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Data Memory clr."),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N):"),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'N',7,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	if (i=='Y')
	{
		//clear all rst entries
		for (clc=0;clc<512;clc++)
		{
			SD_data_buf[clc]=0xFF;
		}
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Erasing Memory.."));
		clc = RST_read();
		while(clc != 0)
		{
			SD_mega_WriteSingleBlock(SD_data_buf,clc);
			clc--;
		}
		RST_write(1);
	}
}

void operator_login_system()
{
	unsigned char i,name[16],op_no=eeprom_mega_ReadByte(ADD_OPERATOR_SYSTEM+NO_OF_OPERATOR);
	if (op_no>=10)
	{
		op_no=0;
	}
	if (op_no<9)
	{
		//Get Operator name
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Operator Name"),0,0);
		fun_ret=get_string_from_user(lcd_Line2_data,0,0,16,USE_ALL_KEY,STORE_ALL_DATA);
		if(fun_ret==RETURN_CAUSE_ESC || fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		//save the login name temporarily
		{
			for (i=0;i<16;i++)
			{
				name[i]=lcd_Line2_data[i];
			}
		}
		//Get operator password
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Oprtor PassWord"),0,0);
		fun_ret = get_password_from_user(lcd_Line2_data,0,0,16,USE_NUMERIC|USE_CHARACTERS,STORE_ALL_DATA);
		if(fun_ret==RETURN_CAUSE_ESC || fun_ret==RETURN_CAUSE_ENTER_WITHOUT_CHANGE)
		{
			return;
		}
		else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
		//save the login passWord temporarily
		{
			for (i=0;i<16;i++)
			//Update operator data in eeprom
			{
				eeprom_mega_WriteByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*op_no)+OPERATOR_PASS+i,lcd_Line2_data[i]);
			}
		}
		for (i=0;i<16;i++)
		{
			eeprom_mega_WriteByte(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*op_no)+OPERATOR_NAME+i,name[i]);
		}
		op_no++;
		eeprom_mega_WriteByte(ADD_OPERATOR_SYSTEM+NO_OF_OPERATOR,op_no);
	}
	else
	{
		//All 10 operators are assigned
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Operators Full"));
		_delay_ms(1000);
	}
}

void operator_mem_clear()
{
	unsigned int i;
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Clearing Memory.."));
	eeprom_mega_WriteByte(ADD_OPERATOR_SYSTEM+NO_OF_OPERATOR,0);
	for (i=ADD_OPERATOR_SYSTEM+1;i<=(ADD_OPERATOR_SYSTEM+(OPERATOR_OFFSET*10));i++)
	{
		eeprom_mega_WriteByte(i,0xFF);
	}
}

void setting_custom_sleep()
{
	unsigned char i,j,k,str1[4],no_entries;

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Pre Printed Tkt"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Setting (Y/N): "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',15,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(i == 'Y')
	//Set Tkt type to custom
	{
		eeprom_mega_WriteByte(ADD_TICKET_TYPE,'C');
	}
	else if(i == 'N')
	{
		return;
	}
	//Get Tkt Length from user
	i = eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+TKT_LENGTH_INCHES);
	utoa(i,str1,10);
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Ticket Length"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("In Inches : "),0,0);
	set_string_in_lcd_data(LINE2,str1,12,0);
	fun_ret = get_string_from_user(lcd_Line2_data,12,12+strlen(str1),15,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret == RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		i = atoi(lcd_Line2_data);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+TKT_LENGTH_INCHES,i); //------------------------check works for char?
	}
	//Print Kg
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Print Kg"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+PRINT_KG,i);
	}
	//Serial No settings
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Print Serial No"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+PRINT_SERIAL_NO,i);
	}
	if (i == 'Y')
	//Take Input if yes
	{
	SERIAL_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Serial No: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_SERIAL_NO);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto SERIAL_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_SERIAL_NO,i);
		}
	SERIAL_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Serial No: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_SERIAL_NO);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto SERIAL_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_SERIAL_NO,i);
		}
	}
	//RST No settings
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Print RST No"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',8,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+PRINT_RST_NO,i);
	}
	if (i == 'Y')
	//Take Input if yes
	{
	RST_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("RST No: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_RST_NO);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,8,0);
		fun_ret=get_string_from_user(str1,8,8+strlen(str1),12,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto RST_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_RST_NO,i);
		}
	RST_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("RST No: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_RST_NO);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,8,0);
		fun_ret=get_string_from_user(str1,8,8+strlen(str1),12,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto RST_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_RST_NO,i);
		}
	}

	//Ticket Entries settings
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Tkt Entries:"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("(0-10): "),0,0);
	no_entries=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	utoa(no_entries,str1,10);
	set_string_in_lcd_data(LINE2,str1,8,0);
	fun_ret=get_string_from_user(str1,8,8+strlen(str1),10,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		no_entries = atoi(str1);
		eeprom_mega_WriteByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED,no_entries);
	}
	for (j=1;j<=no_entries;j++)
	//Get input for all Ticket entries enabled
	{
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Print Entry-"),0,0);
		utoa(j,str1,10);
		set_string_in_lcd_data(LINE1,str1,12,0);
		set_string_in_lcd_dataF(LINE2,PSTR("(Y/N) : "),0,0);
		k = eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+PRINT_ENTRY1+((j-1)*3));
		if(get_character_from_user_P(&i,PSTR("YN"),k,8,LINE2)==RETURN_CAUSE_ESC)
		{
			return;
		}
		else
		{
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+PRINT_ENTRY1+((j-1)*3),i);
		}
		if (i == 'Y')
		//Take input only if going to be printed
		{
			//< Ticket Entry change
			clear_lcd_data(LINE_BOTH);
			set_string_in_lcd_dataF(LINE1,PSTR("Ticket Entry-"),0,0);
			set_string_in_lcd_data(LINE1,str1,13,0);
			for(i=0;i<25;i++)
			{
				lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+1+(25*(j-1))+i);
			}
			fun_ret =get_string_from_user(lcd_Line2_data,0,0,25,USE_ALL_KEY|USE_ARROW_KEY,STORE_ALL_DATA);
			if(fun_ret==RETURN_CAUSE_ESC)
			{
				return;
			}
			else if (fun_ret==RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				for(i=0;i<25;i++)
				//< save string into EEPROM
				{
					eeprom_mega_WriteByte(ADD_TICKET_HEADER+1+(25*(j-1))+i,lcd_Line2_data[i]);
				}
			}
		ENTRY_VERT:
			clear_lcd_data(LINE_BOTH);
			set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
			for(i=0;i<12;i++)
			{
				lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+1+(25*(j-1))+i);
			}
			set_string_in_lcd_dataF(LINE2,PSTR(":"),12,0);
			i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_ENTRY1+((j-1)*3));
			utoa(i,str1,10);
			set_string_in_lcd_data(LINE2,str1,13,0);
			fun_ret=get_string_from_user(str1,13,13+strlen(str1),16,USE_NUMERIC,STORE_UPTO_POINTER);
			if(fun_ret==RETURN_CAUSE_ESC)
			{
				return;
			}
			else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				if (atoi(str1) > 255)
				{
					goto ENTRY_VERT;
				}
				i = atoi(str1);
				eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_ENTRY1+((j-1)*3),i);
			}
		ENTRY_HORIZ:
			clear_lcd_data(LINE_BOTH);
			set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
			for(i=0;i<12;i++)
			{
				lcd_Line2_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+1+(25*(j-1))+i);
			}
			set_string_in_lcd_dataF(LINE2,PSTR(":"),12,0);
			i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_ENTRY1+((j-1)*3));
			utoa(i,str1,10);
			set_string_in_lcd_data(LINE2,str1,13,0);
			fun_ret=get_string_from_user(str1,13,13+strlen(str1),16,USE_NUMERIC,STORE_UPTO_POINTER);
			if(fun_ret==RETURN_CAUSE_ESC)
			{
				return;
			}
			else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				if (atoi(str1) > 255)
				{
					goto ENTRY_HORIZ;
				}
				i = atoi(str1);
				eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_ENTRY1+((j-1)*3),i);
			}
		ENTRY_SIZE:
			clear_lcd_data(LINE_BOTH);
			for(i=0;i<12;i++)
			{
				lcd_Line1_data[i]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+1+(25*(j-1))+i);
			}
			set_string_in_lcd_dataF(LINE2,PSTR("Char (max 25):"),0,0);
			i = eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+ENTRY1_SIZE+(j-1));
			utoa(i,str1,10);
			set_string_in_lcd_data(LINE2,str1,14,0);
			fun_ret=get_string_from_user(str1,14,14+strlen(str1),16,USE_NUMERIC,STORE_UPTO_POINTER);
			if(fun_ret==RETURN_CAUSE_ESC)
			{
				return;
			}
			else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
			{
				if (atoi(str1) > 25)
				{
					goto ENTRY_SIZE;
				}
				i = atoi(str1);
				eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+ENTRY1_SIZE+(j-1),i);
			}
		}
	}

	//Gross Wt settings
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Gross Wt"));
	_delay_ms(1000);
	ps2_kb_flush_keybuffer();
GROSS_VERT:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Gross Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_WT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,10,0);
	fun_ret=get_string_from_user(str1,10,10+strlen(str1),13,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto GROSS_VERT;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_WT,i);
	}
GROSS_HORIZ:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Gross Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_WT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,10,0);
	fun_ret=get_string_from_user(str1,10,10+strlen(str1),13,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto GROSS_HORIZ;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_WT,i);
	}
	//Gross Date
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Date Print With"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Wt(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',10,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+DATE_PRINT_WITH_GROSS_WT,i);
	}
	if (i == 'Y')
	{
	GDATE_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Gross Date: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_DATE);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,12,0);
		fun_ret=get_string_from_user(str1,12,12+strlen(str1),15,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto GDATE_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_DATE,i);
		}
	GDATE_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Gross Date: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_DATE);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,12,0);
		fun_ret=get_string_from_user(str1,12,12+strlen(str1),15,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto GDATE_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_DATE,i);
		}
	}	
	//Gross Time
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Time Print With"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Wt(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',10,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+TIME_PRINT_WITH_GROSS_WT,i);
	}
	if (i == 'Y')
	{
	GTIME_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Gross Time: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_TIME);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,12,0);
		fun_ret=get_string_from_user(str1,12,12+strlen(str1),15,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto GTIME_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_GROSS_TIME,i);
		}
	GTIME_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Gross Time: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_TIME);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,12,0);
		fun_ret=get_string_from_user(str1,12,12+strlen(str1),15,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto GTIME_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_GROSS_TIME,i);
		}
	}	
	
	//Tare Wt settings
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Tare Wt"));
	_delay_ms(1000);
	ps2_kb_flush_keybuffer();
TARE_VERT:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Tare Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_WT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,9,0);
	fun_ret=get_string_from_user(str1,9,9+strlen(str1),12,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto TARE_VERT;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_WT,i);
	}
TARE_HORIZ:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Tare Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_WT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,9,0);
	fun_ret=get_string_from_user(str1,9,9+strlen(str1),12,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto TARE_HORIZ;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_WT,i);
	}
	
	//Tare Date
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Date Print With"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Wt(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',10,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+DATE_PRINT_WITH_TARE_WT,i);
	}
	if (i == 'Y')
	{
	TDATE_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Tare Date: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_DATE);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto TDATE_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_DATE,i);
		}
	TDATE_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Tare Date: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_DATE);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto TDATE_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_DATE,i);
		}
	}	
	//Tare Time
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Time Print With"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Wt(Y/N) : "),0,0);
	if(get_character_from_user_P(&i,PSTR("YN"),'Y',10,LINE2)==RETURN_CAUSE_ESC)
	{
		return;
	}
	else
	{
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+TIME_PRINT_WITH_TARE_WT,i);
	}
	if (i == 'Y')
	{
	TTIME_VERT:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Tare Time: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_TIME);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto TTIME_VERT;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_TARE_TIME,i);
		}
	TTIME_HORIZ:
		clear_lcd_data(LINE_BOTH);
		set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
		set_string_in_lcd_dataF(LINE2,PSTR("Tare Time: "),0,0);
		i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_TIME);
		utoa(i,str1,10);
		set_string_in_lcd_data(LINE2,str1,11,0);
		fun_ret=get_string_from_user(str1,11,11+strlen(str1),14,USE_NUMERIC,STORE_UPTO_POINTER);
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
		{
			if (atoi(str1) > 255)
			{
				goto TTIME_HORIZ;
			}
			i = atoi(str1);
			eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_TARE_TIME,i);
		}
	}	
	
	//Net Wt settings
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Net Wt"));
	_delay_ms(1000);
	ps2_kb_flush_keybuffer();
NET_VERT:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Net Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_NETWEIGHT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,8,0);
	fun_ret=get_string_from_user(str1,8,8+strlen(str1),11,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto NET_VERT;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_NETWEIGHT,i);
	}
NET_HORIZ:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Net Wt: "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_NETWEIGHT);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,8,0);
	fun_ret=get_string_from_user(str1,8,8+strlen(str1),11,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto NET_HORIZ;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_NETWEIGHT,i);
	}
	
	//Charges settings
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Charges"));
	_delay_ms(1000);
	ps2_kb_flush_keybuffer();
CHARGES_VERT:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Vert. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Charges : "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_CHARGES);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,10,0);
	fun_ret=get_string_from_user(str1,10,10+strlen(str1),13,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto CHARGES_VERT;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+VERT_DIST_CHARGES,i);
	}
CHARGES_HORIZ:
	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Horiz. Dist.(mm)"),0,0);
	set_string_in_lcd_dataF(LINE2,PSTR("Charges : "),0,0);
	i=eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_CHARGES);
	utoa(i,str1,10);
	set_string_in_lcd_data(LINE2,str1,10,0);
	fun_ret=get_string_from_user(str1,10,10+strlen(str1),13,USE_NUMERIC,STORE_UPTO_POINTER);
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}
	else if(fun_ret == RETURN_CAUSE_ENTER_WITH_CHANGE)
	{
		if (atoi(str1) > 255)
		{
			goto CHARGES_HORIZ;
		}
		i = atoi(str1);
		eeprom_mega_WriteByte(ADD_PRE_PRINTED_SETTINGS+HORI_DIST_CHARGES,i);
	}
}

void setting_pre_printed_sort()
{
	unsigned char i,prime_pointer = PRINT_SERIAL_NO,min_v=255,min_h=255,current_point=0,curr_v[21],curr_h[21],curr_index;
	
	for (i=0;i<20;i++)
	//read all the values from eeprom
	{
		if (eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+prime_pointer+(i*3))=='Y')
		//Check if entry is to be printed
		{
			curr_v[i] = eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+prime_pointer+(i*3)+1);
			curr_h[i] = eeprom_mega_ReadByte(ADD_PRE_PRINTED_SETTINGS+prime_pointer+(i*3)+2);
		}
		else
		//ignore this entry
		{
			curr_v[i] = 255;
			curr_h[i] = 255;
		}
	}
	while(current_point<20)
	//Sorting starts here
	{
		for (i=0;i<20;i++)
		//poll each record
		{
			if (curr_v[i] != 255 && curr_h[i] != 255)
			//Check if entry is done or not to be printed
			{
				if (curr_v[i] == min_v)
				//Same vertical distance now compare horizontal				
				{
					if (curr_h[i] < min_h)
					//Horizontal compare  ---------------do smt for same values
					{
						//save current index
						curr_index = i;
						min_v = curr_v[i];
						min_h = curr_h[i];
					}
				}
				else if (curr_v[i] < min_v)
				//Compare horizontal value
				{
					//save current index
					curr_index = i;
					min_v = curr_v[i];
					min_h = curr_h[i];
				}
			}
		}
		//after whole list polling
		eeprom_mega_WriteByte(ADD_SORTED_ARRAY+current_point,(prime_pointer+(curr_index*3)));  //write offset to current point
		curr_h[curr_index] = 255;	//reset values of sorted records
		curr_v[curr_index] = 255;
		current_point++;			//Increment point
		min_v = 255;				//reset min & max value
		min_h = 255;
	}
}

void setting_ALT_F6()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __STARTUP_HEADER__))
	{
		setting_startup_header();
	}
	else if (!strcmp_P(pswd_str, __CODE_SYSTEM_PERMS__))
	{
		setting_code_base_system();
	}
	else if (!strcmp_P(pswd_str, __LOGIN_SYSTEM__))
	{
		operator_login_system();
	}
	else if (!strcmp_P(pswd_str, __CUSTOM_TKT_SETTING__))
	{
		setting_custom_sleep();
	}
	else
	{
		return;
	}
}

void setting_ALT_F4()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __SIGNATURE_LINES__))
	{
		setting_charge_type();
	}
}

void setting_ALT_F3()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __TICKET_HEADER_ENTRY__))
	{
		setting_ticket_header_entry();
	}
}

void setting_ALT_F10()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __CODE1_INPUT_SYSTEM__))
	{
		setting_code1_input_system();
	}
	else if(!strcmp_P(pswd_str, __CODE2_INPUT_SYSTEM__))
	{
		setting_code2_input_system();
	}
	else if(!strcmp_P(pswd_str, __CODE3_INPUT_SYSTEM__))
	{
		setting_code3_input_system();
	}
	else if(!strcmp_P(pswd_str, __CODE4_INPUT_SYSTEM__))
	{
		setting_code4_input_system();
	}
	else if(!strcmp_P(pswd_str, __CODE1_PRINT_ALL__))
	{
		setting_code1_print();
	}
	else if(!strcmp_P(pswd_str, __CODE2_PRINT_ALL__))
	{
		setting_code2_print();
	}
	else if(!strcmp_P(pswd_str, __CODE3_PRINT_ALL__))
	{
		setting_code3_print();
	}
	else if(!strcmp_P(pswd_str, __CODE4_PRINT_ALL__))
	{
		setting_code4_print();
	}
	else
	{
		return;
	}
}

void setting_ALT_F7()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __PRINTER_HEADER_FOOTER__))
	{
		setting_printer_header_footer();
	}
}

void setting_ALT_F8()
{
	char pswd_str[LEN_STRING_PASSWORD+1];

	clear_lcd_data(LINE_BOTH);
	set_string_in_lcd_dataF(LINE1,PSTR("Enter Password:"),0,0);
	get_password_from_user(pswd_str,0,0,LEN_STRING_PASSWORD,USE_NUMERIC|USE_CHARACTERS,STORE_UPTO_POINTER);
	lcd_mega_ClearDispaly();

	if(!strcmp_P(pswd_str, __CODE1_RESET_SYSTEM__))
	{
		setting_reset_code1();
	}
	else if(!strcmp_P(pswd_str, __CODE2_RESET_SYSTEM__))
	{
		setting_reset_code2();
	}
	else if(!strcmp_P(pswd_str, __CODE3_RESET_SYSTEM__))
	{
		setting_reset_code3();
	}
	else if(!strcmp_P(pswd_str, __CODE4_RESET_SYSTEM__))
	{
		setting_reset_code4();
	}
	else if (!strcmp_P(pswd_str, __RST_CLEAR_SYSTEM__))
	{
		setting_clear_rst_mem();
	}
	else if (!strcmp_P(pswd_str, __CLR_OPERATOR_MEM__))
	{
		operator_mem_clear();
	}
	else
	{
		return;
	}
}

