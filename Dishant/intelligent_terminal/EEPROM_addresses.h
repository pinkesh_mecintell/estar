/*
 * EEPROM_addresses.h
 *
 * Created: 18/09/13 6:59:16 PM
 *  Author: Pinkesh
 */ 


#ifndef EEPROM_ADDRESSES_H_
#define EEPROM_ADDRESSES_H_

/**
* all the bellow field has two components
* first #define is 'base address'
* and all subsequent #define is member
* parameters of field
* member define value is offset to base value
* its comment has (length(in byte))/(default value)/(all values) format
*/

//< first copy at 0
//< second copy at 0+copy_offset
#define COPY_OFFSET	2000
//< code 1 address - code 0 address (i.e. address difference between two successive code)
#define CODE_OFFSET	75


////////////////////
//< Field: PASSWORD
#define ADD_PASSWORD	1982//< base address(0x07BE)
#define PASSWORD_LEN	1	//< (1)/(1)/(0-16)
#define PASSWORD_DATA	2	//< (16)/('1')/('xxxxxxxxxxxxxxxx')
////////////////////

//////////////////////
//< Field: HEADER_LINE1
#define ADD_STARTUP_HEADER_LINE1	0//< base address
#define STARTUP_HEADER_LINE1_LEN	0//< (1)/(0)/(0-16)
#define STARTUP_HEADER_LINE1_DATA	1//< (16)/(none)/(any printable character uto 20 char)
///////////////////////

//////////////////////
//< Field: HEADER_LINE2
#define ADD_STARTUP_HEADER_LINE2	17//< base address
#define STARTUP_HEADER_LINE2_LEN	0//< (1)/(0)/(0-20)
#define STARTUP_HEADER_LINE2_DATA	1//< (16)/(none)/(any printable character uto 20 char)
///////////////////////

//////////////////////
//< Field: HEADER_LINE1
#define ADD_PRINTER_HEADER_LINE1			34//< base address
#define PRINTER_HEADER_LINE1_LEN			0//< (1)/(0)/(0-40)
#define PRINTER_HEADER_LINE1_DATA			1//< (40)/(none)/(any printable character upto 40 char)
#define PRINTER_HEADER_LINE1_BOLD_NORMAL	41//< (1)/('N')/('N'/'B')
///////////////////////

//////////////////////
//< Field: HEADER_LINE2
#define ADD_PRINTER_HEADER_LINE2			76//< base address
#define PRINTER_HEADER_LINE2_LEN			0//< (1)/(0)/(0-20)
#define PRINTER_HEADER_LINE2_DATA			1//< (40)/(none)/(any printable character upto 40 char)
#define PRINTER_HEADER_LINE2_BOLD_NORMAL	41//< (1)/('N')/('N'/'B')
///////////////////////

//////////////////////
//< Field: HEADER_LINE3
#define ADD_PRINTER_HEADER_LINE3			118//< base address
#define PRINTER_HEADER_LINE3_LEN			0//< (1)/(0)/(0-20)
#define PRINTER_HEADER_LINE3_DATA			1//< (40)/(none)/(any printable character upto 40 char)
#define PRINTER_HEADER_LINE3_BOLD_NORMAL	41//< (1)/('N')/('N'/'B')
///////////////////////

//////////////////////
//< Field: HEADER_POSITION
#define ADD_PRINTER_HEADER_POSITION		160//< base address
#define PRINTER_HEADER_POSITION			0//< (1)/(1)/('0'/'1'/'2'(char))
///////////////////////

//////////////////////
//< Field: FOOTER_LINE1
#define ADD_PRINTER_FOOTER_LINE1			202//< base address
#define PRINTER_FOOTER_LINE1_LEN			0//< (1)/(0)/(0-20)
#define PRINTER_FOOTER_LINE1_DATA			1//< (40)/(none)/(any printable character upto 40 char)
///////////////////////

//////////////////////
//< Field: FOOTER_LINE2
#define ADD_PRINTER_FOOTER_LINE2			243//< base address
#define PRINTER_FOOTER_LINE2_LEN			0//< (1)/(0)/(0-20)
#define PRINTER_FOOTER_LINE2_DATA			1//< (40)/(none)/(any printable character upto 40 char)
///////////////////////

//////////////////////
//< Field: LCD40_HEADER_LINE1
#define ADD_LCD40_HEADER_LINE1			284//< base address
#define LCD40_HEADER_LINE1_LEN			0//< (1)/(0)/(0-20)
#define LCD40_HEADER_LINE1_DATA			1//< (40)/(none)/(any printable character upto 40 char)
///////////////////////

//////////////////////
//< Field: LCD40_HEADER_LINE2
#define ADD_LCD40_HEADER_LINE2			325//< base address
#define LCD40_HEADER_LINE2_LEN			0//< (1)/(0)/(0-20)
#define LCD40_HEADER_LINE2_DATA			1//< (40)/(none)/(any printable character upto 40 char)
///////////////////////

//////////////////////
//< Field: SIGNATURE_LINE1
#define ADD_SIGNATURE_LINE1			366//< base address
#define SIGNATURE_LINE1_LEN			0//< (1)/(0)/(0-20)
#define SIGNATURE_LINE1_DATA		1//< (25)/(none)/(any printable character upto 25 char)
///////////////////////

//////////////////////
//< Field: SIGNATURE_LINE2
#define ADD_SIGNATURE_LINE2			392//< base address
#define SIGNATURE_LINE2_LEN			0//< (1)/(0)/(0-20)
#define SIGNATURE_LINE2_DATA		1//< (25)/(none)/(any printable character upto 25 char)
///////////////////////

//////////////////////
//< Field: SIGNATURE_LINE3
#define ADD_SIGNATURE_LINE3			418//< base address
#define SIGNATURE_LINE3_LEN			0//< (1)/(0)/(0-20)
#define SIGNATURE_LINE3_DATA		1//< (25)/(none)/(any printable character upto 25 char)
///////////////////////

//////////////////////
//< Field: Ticket Header Entries
#define ADD_TICKET_HEADER			444//< base address
#define TKT_ENTRIES_ENABLED			0//< (1)/(5)/(0-10)
#define TKT_HEADER_ENTRY1    		1//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY2    		26//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY3    		51//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY4    		76//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY5    		101//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY6    		126//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY7    		151//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY8    		176//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY9    		201//< (12/25)/(none)/(any printable character upto 12 char)
#define TKT_HEADER_ENTRY10    		226//< (12/25)/(none)/(any printable character upto 12 char)
///////////////////////

//////////////////////
//< Field: CODE BASE SYSTEM ENABLED DATA
#define ADD_CODE_ENABLED_ENTRIES	695//< base address
#define CODE_ENTRY1_ENABLE			0//< (1)/('N')/("Y/N")
#define CODE_ENTRY2_ENABLE			1//< (1)/('N')/("Y/N")
#define CODE_ENTRY3_ENABLE			2//< (1)/('N')/("Y/N")
#define CODE_ENTRY4_ENABLE			3//< (1)/('N')/("Y/N")
#define CODE_CHARGES_ENABLE			4//< (1)/('N')/("2/3/4/N")
#define CODE_MOBILE_NO_ENABLE		5//< (1)/('N')/("2/3/4/N")
///////////////////////

//////////////////////
//< Field: F4 few settings
#define ADD_CHARGE_TYPE          	700//< (1)/('1')/('0','1','2','3')
#define ADD_TICKET_TYPE				701//< (1)/('1')/("1-7")
#define ADD_BAG_BASE_SYSTEM			702//< (1)/('N')/("Y/N")
#define ADD_CODE_CHARGES_EDIT		703//< (1)/('Y')/("Y/N")
#define ADD_CODE_MOBILE_EDIT		704//< (1)/('Y')/("Y/N")
///////////////////////

//////////////////////
//< Field: Printer settings
#define ADD_PRINTER_SETTINGS       	705//< base address
#define PRINTER_TYPE				0//< (1)/('1')/("1-4")
#define COPY_NO_WEIGHMENT1			1// (1)/(1)/(0-20)
#define COPY_NO_WEIGHMENT2			2// (1)/(1)/(0-20)
#define USE_REPORT_DATE				3//(1)/(1)/(1/2)
#define PRINT_DUPLICATE_COPY		4//(1)/('N')/(Y/N)
#define NO_LINE_FEED				5//(1)/(1)/(0-20)
#define WT_BOLD_NORMAL				6//(1)/('B')/('B/N')
///////////////////////

/////////////////////////
//< Field: OPRATOR Login System
#define ADD_OPERATOR_SYSTEM		    712//< base address
#define NO_OF_OPERATOR				0//< (1)/(0)/(0-10)
#define OPERATOR_NAME      			1//< (16)/(0)/(any printable character upto 16 char)
#define OPERATOR_PASS				17//< (16)/(none)/(" ")
#define OPERATOR_OFFSET				32//< Offset Constant
///////////////////////				total 10 records

#define ENGG_LOGIN_PASSWORD			1023 //< (16)/(none)/(any printable character upto 16 char)

////////////////////////
//< Field: Pre-printed tkt parameters
#define ADD_PRE_PRINTED_SETTINGS	1039//<base Address
#define TKT_LENGTH_INCHES			0//< (1)/(4)/(0-255)
#define PRINT_KG					1//< (1)/('Y')/(Y/N)
#define PRINT_SERIAL_NO				2//< (1)/('Y')/(Y/N)
#define VERT_DIST_SERIAL_NO			3//< (1)/(0)/(0-255)
#define HORI_DIST_SERIAL_NO			4//< (1)/(0)/(0-255)
#define PRINT_RST_NO				5//< (1)/('Y')/(Y/N)
#define VERT_DIST_RST_NO			6//< (1)/(0)/(0-255)
#define HORI_DIST_RST_NO			7//< (1)/(0)/(0-255)
#define PRINT_ENTRY1				8//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY1			9//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY1			10//< (1)/(0)/(0-255)
#define PRINT_ENTRY2				11//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY2			12//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY2			13//< (1)/(0)/(0-255)
#define PRINT_ENTRY3				14//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY3			15//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY3			16//< (1)/(0)/(0-255)
#define PRINT_ENTRY4				17//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY4			18//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY4			19//< (1)/(0)/(0-255)
#define PRINT_ENTRY5				20//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY5			21//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY5			22//< (1)/(0)/(0-255)
#define PRINT_ENTRY6				23//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY6			24//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY6			25//< (1)/(0)/(0-255)
#define PRINT_ENTRY7				26//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY7			27//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY7			28//< (1)/(0)/(0-255)
#define PRINT_ENTRY8				29//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY8			30//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY8			31//< (1)/(0)/(0-255)
#define PRINT_ENTRY9				32//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY9			33//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY9			34//< (1)/(0)/(0-255)
#define PRINT_ENTRY10				35//< (1)/('Y')/(Y/N)
#define VERT_DIST_ENTRY10			36//< (1)/(0)/(0-255)
#define HORI_DIST_ENTRY10			37//< (1)/(0)/(0-255)
#define PRINT_GROSS_WT				38//< (1)/('Y')/(Y/N)
#define VERT_DIST_GROSS_WT			39//< (1)/(0)/(0-255)
#define HORI_DIST_GROSS_WT			40//< (1)/(0)/(0-255)
#define DATE_PRINT_WITH_GROSS_WT	41//< (1)/('Y')/(Y/N)
#define VERT_DIST_GROSS_DATE		42//< (1)/(0)/(0-255)
#define HORI_DIST_GROSS_DATE		43//< (1)/(0)/(0-255)
#define TIME_PRINT_WITH_GROSS_WT	44//< (1)/('Y')/(Y/N)
#define VERT_DIST_GROSS_TIME		45//< (1)/(0)/(0-255)
#define HORI_DIST_GROSS_TIME		46//< (1)/(0)/(0-255)
#define PRINT_TARE_WT				47//< (1)/('Y')/(Y/N)
#define VERT_DIST_TARE_WT			48//< (1)/(0)/(0-255)
#define HORI_DIST_TARE_WT			49//< (1)/(0)/(0-255)
#define DATE_PRINT_WITH_TARE_WT		50//< (1)/('Y')/(Y/N)
#define VERT_DIST_TARE_DATE			51//< (1)/(0)/(0-255)
#define HORI_DIST_TARE_DATE			52//< (1)/(0)/(0-255)
#define TIME_PRINT_WITH_TARE_WT		53//< (1)/('Y')/(Y/N)
#define VERT_DIST_TARE_TIME			54//< (1)/(0)/(0-255)
#define HORI_DIST_TARE_TIME			55//< (1)/(0)/(0-255)
#define PRINT_NETWEIGHT				56//< (1)/('Y')/(Y/N)
#define VERT_DIST_NETWEIGHT			57//< (1)/(0)/(0-255)
#define HORI_DIST_NETWEIGHT			58//< (1)/(0)/(0-255)
#define PRINT_CHARGES				59//< (1)/('Y')/(Y/N)
#define VERT_DIST_CHARGES			60//< (1)/(0)/(0-255)
#define HORI_DIST_CHARGES			61//< (1)/(0)/(0-255)
#define ENTRY1_SIZE					62//< (1)/(0)/(0-25)
#define ENTRY2_SIZE					63//< (1)/(0)/(0-25)
#define ENTRY3_SIZE					64//< (1)/(0)/(0-25)
#define ENTRY4_SIZE					65//< (1)/(0)/(0-25)
#define ENTRY5_SIZE					66//< (1)/(0)/(0-25)
#define ENTRY6_SIZE					67//< (1)/(0)/(0-25)
#define ENTRY7_SIZE					68//< (1)/(0)/(0-25)
#define ENTRY8_SIZE					69//< (1)/(0)/(0-25)
#define ENTRY9_SIZE					70//< (1)/(0)/(0-25)
#define ENTRY10_SIZE				71//< (1)/(0)/(0-25)
////////////////////////

////////////////////////
//< Field: Pre printed Tkt field sort
#define ADD_SORTED_ARRAY			1110//< Base adress of array
////////////////////////			total 20 fields

////////////////////////
//< Field: WEIGHT_CONSTANTS
#define ADD_WEIGHT_CONSTANTS				3914 //< base address
#define WEIGHT_CONSTANTS_UNIT_WEIGHT_VALUE	1	//< (1)/()/(5 or 10)-unsigned char
#define WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT	2	//< (4)/()/()-unsigned long
#define WEIGHT_CONSTANTS_ADC_MAX_COUNT		6	//< (4)/()/()-signed long
#define WEIGHT_CONSTANTS_ADC_MIN_COUNT		10	//< (4)/()/()-signed long
#define WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE	14	//< (4)/()/()-unsigned long
/////////////////////////

#endif /* EEPROM_ADDRESSES_H_ */