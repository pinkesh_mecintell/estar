/*
 * buzzer.c
 *
 * Created: 16/09/13 8:55:30 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "utilities.h"
#include "buzzer.h"


void buzzer_init()
{
	SET(DDRD,5);
	BUZZER_OFF;
	//< Timer-2(8-bit) initialization for Buzzer
	TCCR2 = (1<<WGM21);
	SET(TIMSK,OCIE2);
	OCR2 = 254;
}

//< Buzzer timer ISR
ISR(TIMER2_COMP_vect)
{
	static unsigned char val=0;
	
	if(val<10)
	{
		val++;
		return;
	}
	
	val=0;
	BUZZER_OFF;
	BUZZER_TIMER_OFF;
}