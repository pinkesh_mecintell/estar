/*
 * ps2_kb.c
 *
 * Created: 11/08/13 1:21:46 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <stddef.h>
#include "ps2_kb.h"
#include "utilities.h"
#include "buzzer.h"
#include "ps2_kb_scancodes.h"


#if (KB_TYPE==KB_PS2)
	//< ps2_kb timer (no response) related
	#define PS2_KB_TIMER_ON		SET(TCCR0,CS02); SET(TCCR0,CS01); SET(TCCR0,CS00)
	#define PS2_KB_TIMER_OFF	CLR(TCCR0,CS02); CLR(TCCR0,CS01); CLR(TCCR0,CS00)
	#define PS2_KB_TIMER_CLEAR	(TCNT0=0)
#endif

/**** static function ****/
#if (KB_TYPE==KB_PS2)
	static void ps2_kb_decode(unsigned char sc);
#endif

void ps2_kb_put_kbbuff(unsigned char c);
/*************************/

/**** static variables *****/
#if (KB_TYPE==KB_PS2)
	//< Holds the received scan code
	static unsigned char ps2_kb_data;
	//< 0 = negative  1 = positive
	static unsigned char edge;
	static unsigned char bitcount;
	static unsigned char is_up=0,spec_key=0,pre_is_alt=0;
#endif

static volatile unsigned char ps2_kb_buffer[PS2_KB_BUFF_SIZE];
static volatile unsigned char *inpt, *outpt;
static volatile unsigned char buffcnt;
/***************************/

/**** Function Definitions *****/
void ps2_kb_Initialize(void)
{
#if (KB_TYPE==KB_PS2)
	//< I/O pin setting
	//< ATmega128
	//< Clock(ps2) - E4(INT4) - INPUT
	CLR(DDRE,4);
	SET(PORTE,4);
	//< data(ps2) - E3 - input
	CLR(DDRE,3);
	SET(PORTE,3);
	
	//< Timer-0(8-bit) initialization for ps2 keyboard no response
	//< delay value=1.5 ms
	TCCR0 = (1<<WGM01);
	SET(TIMSK,OCIE0);
	PS2_KB_TIMER_CLEAR;
	PS2_KB_TIMER_OFF;
	//< FCP=14.7456MHz
	OCR0 = 9;

#elif (KB_TYPE==KB_USB)
//< nothing
#endif
}

#if (KB_TYPE==KB_PS2)
ISR(INT4_vect)
{	
	//< Routine entered at falling edge
	if(!edge)
	{
		//< Bit 3 to 10 is data. Parity bit,
		//< start and stop bits are ignored.
		if(bitcount<11 && bitcount>2)
		{
			ps2_kb_data = (ps2_kb_data >> 1);
			if(CHK(PINE,3))
			{
				ps2_kb_data = ps2_kb_data | 0x80;//< Store a '1'
			}				
		}
		//< Set interrupt on rising edge
		//< atmega128
		SET(EICRB,ISC40);
		edge = 1;
		
	}
	//< Routine entered at rising edge
	else
	{
		//< Set interrupt on falling edge
		//< atmega128
		CLR(EICRB,ISC40);
		edge = 0;
		
		//< All bits received decode data
		if(--bitcount == 0)
		{
			ps2_kb_decode(ps2_kb_data);
			bitcount = 11;
		}
	}
	
	//< set timer
	PS2_KB_TIMER_CLEAR;
	PS2_KB_TIMER_ON;
}

ISR(TIMER0_COMP_vect)
{
	//< if interrupt generate means ps2 keyboard
	//< not responced in time(i.e. 1.5ms)
	//< so that we should reinitialize all flags
	//< and ps2 keyboard related variables
	edge = 0;
	bitcount = 11;
	ps2_kb_data=0;
	//< Set interrupt on falling edge
	//< atmega128
	CLR(EICRB,ISC40);
	
	//< timer off
	PS2_KB_TIMER_OFF;
}

static void ps2_kb_decode(unsigned char sc)
{
	unsigned char i;
	
	//< Last data received was the up-key identifier
	if(!is_up)
	{
		switch (sc)
		{
			//< The up-key identifier
			case 0xF0:
				spec_key=0;
				is_up = 1;
				break;
			
			//< special key identifier
			//< not used here
			case 0xE0:
				spec_key=1;
				break;
			
			//< key press finding
			default:
// 				/////// test //////
// 				ps2_kb_put_kbbuff(sc);
// 				//////////////////
				
				//< special keys which are preceded by 0xE0
				if(spec_key==1)
				{
					//< do a table look-up for special key
					for(i = 0; pgm_read_byte(&ps2_scancodes_special_key_list1[i][0])!=sc && pgm_read_byte(&ps2_scancodes_special_key_list1[i][0]); i++);
					//< if scancode for special key found in table
					if(pgm_read_byte(&ps2_scancodes_special_key_list1[i][0]) == sc)
					{
						if(pgm_read_byte(&ps2_scancodes_special_key_list1[i][1])==SPECIAL_KEY_ALT)
						{
							if(pre_is_alt)
							{
								return;
							}
							pre_is_alt=1;
						}
						//< store special key identifier in buffer
						ps2_kb_put_kbbuff(SPECIAL_KEY_IDENTIFIER);
						//< store special key in buffer into buffer
						ps2_kb_put_kbbuff(pgm_read_byte(&ps2_scancodes_special_key_list1[i][1]));
						if(pgm_read_byte(&ps2_scancodes_special_key_list1[i][1])!=SPECIAL_KEY_ALT)
						{
							pre_is_alt=0;
						}
					}
					spec_key=0;
				}
				else
				{
					//< do a table look-up for printable character
					for(i = 0; pgm_read_byte(&ps2_scancodes[i][0])!=sc && pgm_read_byte(&ps2_scancodes[i][0]); i++);
					//< if scancode found in table
					if(pgm_read_byte(&ps2_scancodes[i][0]) == sc)
					{
						//< store into buffer
						ps2_kb_put_kbbuff(pgm_read_byte(&ps2_scancodes[i][1]));
						//< clear alt related flag
						pre_is_alt=0;
					}
					//< if printable character not found
					//< do table lookup for special
					//< key which are not preceded by 0xE0
					else
					{
						//< do a table look-up for special key
						for(i = 0; pgm_read_byte(&ps2_scancodes_special_key_list2[i][0])!=sc && pgm_read_byte(&ps2_scancodes_special_key_list2[i][0]); i++);
						//< if scancode for special key found in table
						if(pgm_read_byte(&ps2_scancodes_special_key_list2[i][0]) == sc)
						{
							if(pgm_read_byte(&ps2_scancodes_special_key_list2[i][1])==SPECIAL_KEY_ALT)
							{
								if(pre_is_alt)
								{
									return;
								}
								pre_is_alt=1;
							}
							//< store special key identifier in buffer
							ps2_kb_put_kbbuff(SPECIAL_KEY_IDENTIFIER);
							//< store special key in buffer into buffer
							ps2_kb_put_kbbuff(pgm_read_byte(&ps2_scancodes_special_key_list2[i][1]));
							if(pgm_read_byte(&ps2_scancodes_special_key_list2[i][1])!=SPECIAL_KEY_ALT)
							{
								pre_is_alt=0;
							}
						}
					}
				}
				break;//< default case break
		}//< switch end }
	}
	else
	{
		is_up = 0;//< Two 0xF0 in a row not allowed
	}
}
#endif

void ps2_kb_put_kbbuff(unsigned char c)
{
#if (KB_TYPE==KB_PS2)
	//< If buffer not full
    if(buffcnt<PS2_KB_BUFF_SIZE)
    {
		//< Put character into buffer
        *inpt = c;
        //< Increment pointer
		inpt++;
        buffcnt++;
		//< Pointer validating
        if(inpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
		{
            inpt = ps2_kb_buffer;
		}
		//< buzzer related
		BUZZER_ON;
		BUZZER_TIMER_ON;
	}
	
#elif (KB_TYPE==KB_USB)
	//< If buffer not full
    if(buffcnt<PS2_KB_BUFF_SIZE)
    {
		//< Put character into buffer
        *inpt = c;
        //< Increment pointer
		inpt++;
        buffcnt++;
		//< Pointer validating
        if(inpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
		{
            inpt = ps2_kb_buffer;
		}
	}
#endif		
}

unsigned char ps2_kb_getkey(PS2_KEY *ps2_key)
{
	unsigned char key;
	
#if (KB_TYPE==KB_PS2)
	//< Wait for data
    if(buffcnt==0)
	{
		return 0;
	}
	
	//< Get byte
    key = *outpt;
    //< Increment pointer
    outpt++;
    //< Pointer validating
    if (outpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
    {
	    outpt = ps2_kb_buffer;
    }
    //< Decrement buffer count
    buffcnt--;
	
	//< ps2 key type
	ps2_key->type = NORMAL_KEY;

	if(key==SPECIAL_KEY_IDENTIFIER)
	{
		while(!ps2_kb_available_key());
		//< Get byte
		key = *outpt;
		//< Increment pointer
		outpt++;
		//< Pointer validating
		if (outpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
		{
			outpt = ps2_kb_buffer;
		}
		//< Decrement buffer count	
		buffcnt--;
		
		//< ps2 key type
		ps2_key->type=SPECIAL_KEY;
	}
	ps2_key->val = key;

	//< successful return
	return 1;

#elif (KB_TYPE==KB_USB)
	//< Wait for data
	if(buffcnt==0)
	{
		return 0;
	}
	
	//< Get byte
	key = *outpt;
	//< Increment pointer
	outpt++;
	//< Pointer validating
	if (outpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
	{
		outpt = ps2_kb_buffer;
	}
	//< Decrement buffer count
	buffcnt--;
	
	//< ps2 key type
	ps2_key->type = key;

	//< Get byte
	key = *outpt;
	//< Increment pointer
	outpt++;
	//< Pointer validating
	if (outpt >= ps2_kb_buffer + PS2_KB_BUFF_SIZE)
	{
		outpt = ps2_kb_buffer;
	}
	//< Decrement buffer count
	buffcnt--;
	
	//< ps2 key type
	ps2_key->val = key;

	//< successful return
	return 1;
#endif
}

unsigned char ps2_kb_available_key()
{
#if (KB_TYPE==KB_PS2 || KB_TYPE==KB_USB)
	return buffcnt;
#endif
}

void ps2_kb_flush_keybuffer()
{
#if (KB_TYPE==KB_PS2 || KB_TYPE==KB_USB)
	//< disable global interrupt
	cli();
	//< Initialize buffer
	inpt =  ps2_kb_buffer;
	outpt = ps2_kb_buffer;
	buffcnt = 0;
	//< enable global interrupt
	sei();
#endif
}

void ps2_kb_start()
{
#if (KB_TYPE==KB_PS2)
	//< Initialize buffer
	inpt =  ps2_kb_buffer;
	outpt = ps2_kb_buffer;
	buffcnt = 0;
	
	//< Initialize all flags
	edge = 0;
	bitcount = 11;
	ps2_kb_data=0;
	spec_key=0;
	is_up=0;
	pre_is_alt=0;
	
	//< disable global interrupt
	cli();
	//< INT4 interrupt on falling edge
	//< atmega128
	SET(EICRB,ISC41);
	CLR(EICRB,ISC40);
	SET(EIMSK,INT4);
	//< clear any pending interrupt-4 flag
	if(CHK(EIFR,INTF4))
	{
		//< clear flag by setting - 1
		SET(EIFR,INTF4);
	}
	//< enable global interrupt
	sei();
	
#elif (KB_TYPE==KB_USB)
	//< Initialize buffer
	inpt =  ps2_kb_buffer;
	outpt = ps2_kb_buffer;
	buffcnt = 0;
#endif
}

void ps2_kb_stop()
{
#if (KB_TYPE==KB_PS2)
	//< disable global interrupt
	cli();
	//< clear int-4
	CLR(EIMSK,INT4);
	//< enable global interrupt
	sei();

#elif (KB_TYPE==KB_USB)
//< nothing
#endif
}