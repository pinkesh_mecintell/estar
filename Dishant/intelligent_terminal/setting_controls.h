/*
 * time_date_Setting.h
 *
 * Created: 17/09/13 5:57:59 PM
 *  Author: Pinkesh
 */ 


#ifndef SETTING_CONTROLS_H_
#define SETTING_CONTROLS_H_

//Defines for login type variable
#define LOGIN_NONE 255
#define LOGIN_ENGG 0
#define LOGIN_OPRT 1

void setting_date_time();
unsigned char check_password();
void setting_printer_header_footer();
void setting_weightCallibration();
void setting_smartCallibration();
void setting_AutoZero();
void setting_view_ADC_count();
void setting_startup_header();
void display_startup_header();
void setting_ALT_F6();
void setting_ALT_F10();
void setting_ALT_F7();
void setting_ALT_F8();
void setting_ALT_F4();
void setting_ALT_F3();
void setting_ticket_header_entry();
void setting_code_base_system();
void setting_code1_input_system();
void setting_code2_input_system();
void setting_code3_input_system();
void setting_code4_input_system();
void setting_reset_code1();
void setting_reset_code2();
void setting_reset_code3();
void setting_reset_code4();
void setting_code1_print();
void setting_code2_print();
void setting_code3_print();
void setting_code4_print();
void print_header_type1();
void print_footer_type1();
void setting_charge_type();
void view_rst_no();
void setting_clear_rst_mem();
void operator_login_system();
void operator_mem_clear();
void print_signature_line_type1();
void setting_custom_sleep();
void setting_pre_printed_sort();

/**
* this function takes 3 char long input string
* and finds its match in appropriate code table
* returns location if match found
* \param	input_str	        input String to compare
* \param	table_index			Which table to find location in (0-3)
* \return			0-31 = location of the data Entry
*					40(NO_MATCH_FOUND) = Data not found
*/
unsigned char get_code_location(char* input_str,unsigned char table_index);




#endif /* TIME_DATE_SETTING_H_ */