/*
 * pendrive_dump.c
 *
 * Created: 19/01/14 12:44:23 PM
 *  Author: Pinkesh
 */ 

#include <string.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>
#include "pendrive_dump.h"
#include "lcd_mega.h"
#include "password_strings.h"
#include "ds1307.h"
#include "function_keys.h"
#include "eeprom_mega.h"
#include "EEPROM_addresses.h"
#include "SD_addresses.h"


//< extern variables
extern unsigned char SD_data_buf[];
extern unsigned char fun_ret;

///////////////////////////
//< RTC related
extern RTC_DATA RTC_data;
///////////////////////////

//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
///////////////////////////


#if (USE_PENDRIVE==1)

//< global variable definition
volatile unsigned char pendrive_response;

//< function definition
void setting_pendrive_dumping()
{
	unsigned long int i;
	unsigned char res;
	unsigned char etries_enabled;
	unsigned long int cur_rst;
	unsigned long int sr_no;
	char _buf[150];
	char _str[12];
	
	//< pendrive dumping stastup message
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Pendrive Dumping"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENTER"));
	//< wait here till user not press enter
	fun_ret = wait_for_enter();
	if(fun_ret==RETURN_CAUSE_ESC)
	{
		return;
	}

PENDRIVE_DUMPING_SEARCH_PENDRIVE:
	//< if user pressed ENTER then
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Pendrive Search"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Start..."));
	
	//< send pendrive communication start command
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_COM_START);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;
	
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
		lcd_mega_StringF(PSTR("Error"));
		_delay_ms(1000);

		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Search Again"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Yes=ENT  No=ESC"));
		//< wait here till user not press enter
		fun_ret = wait_for_enter();
		if(fun_ret==RETURN_CAUSE_ESC)
		{
			return;
		}
		//< if user pressed enter
		else
		{
			goto PENDRIVE_DUMPING_SEARCH_PENDRIVE;
		}
	}
	
	//< if successfully pendrive is found
	lcd_mega_StringF(PSTR("Found"));
	_delay_ms(1000);
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Data Dump Start"));
	
	//< send file name
	//< filename = IT<DD><MM><YY>.CSV - DD=date,MM=month,YY=last two digit of year
	ds1307_RefreshRTC_data();
	//< lcd_Line1_data used temporary
	lcd_Line1_data[0] = 'I';
	lcd_Line1_data[1] = 'T';
	lcd_Line1_data[2] = RTC_data.date[0];
	lcd_Line1_data[3] = RTC_data.date[1];
	lcd_Line1_data[4] = RTC_data.month[0];
	lcd_Line1_data[5] = RTC_data.month[1];
	lcd_Line1_data[6] = RTC_data.year[0];
	lcd_Line1_data[7] = RTC_data.year[1];
	lcd_Line1_data[8] = '.';
	lcd_Line1_data[9] = 'C';
	lcd_Line1_data[10] = 'S';
	lcd_Line1_data[11] = 'V';
	lcd_Line1_data[12] = '\0';
	
	//< send create filename start command to pendrive
	uart_vnc2_WriteData(CMD_PENDRIVE_CREATE_FILE_START);//< no response is needed
	uart_vnc2_WriteString(lcd_Line1_data);//< send file name to pendrive
	//< send create filename stop command to pendrive
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_CREATE_FILE_STOP);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;	
	
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Error"));
		
		//< pendrive is already opened
		//< close the pendrive
		//< send close pendrive command to pendrive
		pendrive_response = RESPONSE_PENDRIVE_NONE;
		uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
		//< wait for response
		i=0;
		while(pendrive_response==RESPONSE_PENDRIVE_NONE)
		{
			_delay_ms(100);
			i++;
			if(i>100)
			{
				pendrive_response=RESPONSE_PENDRIVE_ERROR;
				break;
			}
		}
		res=pendrive_response;
		
		//< if error response is received
		if(res==RESPONSE_PENDRIVE_ERROR)
		{
		}
		_delay_ms(1000);
		return;
	}
	
	//< if file is opened with filename then 
	//< write header in file
	SD_data_buf[0] = '\0';
	strcat_P(SD_data_buf,PSTR("SrNO,"));//< 1
	strcat_P(SD_data_buf,PSTR("RST No,"));//< 2
	//< write partial header into file
	//< write header into file
	uart_vnc2_WriteString(SD_data_buf);
	//< send CMD_PENDRIVE_WRITE_DATA_IN_FILE command to pendrive
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_WRITE_DATA_IN_FILE);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Error"));
		
		//< pendrive is already opened
		//< close the pendrive
		//< send close pendrive command to pendrive
		pendrive_response = RESPONSE_PENDRIVE_NONE;
		uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
		//< wait for response
		i=0;
		while(pendrive_response==RESPONSE_PENDRIVE_NONE)
		{
			_delay_ms(100);
			i++;
			if(i>100)
			{
				pendrive_response=RESPONSE_PENDRIVE_ERROR;
				break;
			}
		}
		res=pendrive_response;
		
		//< if error response is received
		if(res==RESPONSE_PENDRIVE_ERROR)
		{
		}
		_delay_ms(1000);
		return;
	}
	
	
	SD_data_buf[0] = '\0';
	//< check how many entries enabled
	etries_enabled = eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_ENTRIES_ENABLED);
	lcd_Line1_data[12]=',';
	lcd_Line1_data[13]='\0';
	for(i=0;i<etries_enabled;i++)
	{
		//< res and lcd_line1_data used here temporary
		for (res=0;res<12;res++)
		{
			lcd_Line1_data[res]=eeprom_mega_ReadByte(ADD_TICKET_HEADER+TKT_HEADER_ENTRY1+25*i+res);
		}
		strcat(SD_data_buf,lcd_Line1_data);//< n
	}
	//< write partial header into file
	//< write header into file
	uart_vnc2_WriteString(SD_data_buf);
	//< send CMD_PENDRIVE_WRITE_DATA_IN_FILE command to pendrive
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_WRITE_DATA_IN_FILE);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Error"));
		
		//< pendrive is already opened
		//< close the pendrive
		//< send close pendrive command to pendrive
		pendrive_response = RESPONSE_PENDRIVE_NONE;
		uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
		//< wait for response
		i=0;
		while(pendrive_response==RESPONSE_PENDRIVE_NONE)
		{
			_delay_ms(100);
			i++;
			if(i>100)
			{
				pendrive_response=RESPONSE_PENDRIVE_ERROR;
				break;
			}
		}
		res=pendrive_response;
		
		//< if error response is received
		if(res==RESPONSE_PENDRIVE_ERROR)
		{
		}
		_delay_ms(1000);
		return;
	}
	
	
	SD_data_buf[0] = '\0';
	strcat_P(SD_data_buf,PSTR("Gross Wt(KG),"));//< 3
	strcat_P(SD_data_buf,PSTR("Tare Wt(KG),"));//< 4
	strcat_P(SD_data_buf,PSTR("Net Wt(KG),"));//< 5
	strcat_P(SD_data_buf,PSTR("Charge1(Rs),"));//< 6
	strcat_P(SD_data_buf,PSTR("Charge2(Rs),"));//< 7
	strcat_P(SD_data_buf,PSTR("Total Charge(Rs)\n\n"));//< 8
	//< write header into file
	uart_vnc2_WriteString(SD_data_buf);
	//< send CMD_PENDRIVE_WRITE_DATA_IN_FILE command to pendrive
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_WRITE_DATA_IN_FILE);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Error"));
		
		//< pendrive is already opened
		//< close the pendrive
		//< send close pendrive command to pendrive
		pendrive_response = RESPONSE_PENDRIVE_NONE;
		uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
		//< wait for response
		i=0;
		while(pendrive_response==RESPONSE_PENDRIVE_NONE)
		{
			_delay_ms(100);
			i++;
			if(i>100)
			{
				pendrive_response=RESPONSE_PENDRIVE_ERROR;
				break;
			}
		}
		res=pendrive_response;
		
		//< if error response is received
		if(res==RESPONSE_PENDRIVE_ERROR)
		{
		}
		_delay_ms(1000);
		return;
	}
		
	
	//< write RST records to pendrive
	cur_rst = RST_read();
	sr_no=1;
	for(i=0;i<cur_rst;i++)
	{
		//< fetch RST record first
		SD_mega_ReadSingleBlock(SD_data_buf,i);
		//< if record is occupied then
		if(SD_data_buf[RST_ENTRY_IS_OCCUPIED]=='Y')
		{
			_buf[0]='\0';
			//< write SR_No # 9char
			//< convert current sr_no into string
			ultoa(sr_no,_str,10);
			strcat(_buf,_str);
			strcat_P(_buf,PSTR(","));
			sr_no++;
			
			//< write RST_No # 9char
			//< convert current RST_no into string
			ultoa((i+1),_str,10);
			strcat(_buf,_str);
			strcat_P(_buf,PSTR("\n"));
		}
		//< write partial record into file
		uart_vnc2_WriteString(_buf);
		//< send CMD_PENDRIVE_WRITE_DATA_IN_FILE command to pendrive
		pendrive_response = RESPONSE_PENDRIVE_NONE;
		uart_vnc2_WriteData(CMD_PENDRIVE_WRITE_DATA_IN_FILE);
		//< wait for response
		i=0;
		while(pendrive_response==RESPONSE_PENDRIVE_NONE)
		{
			_delay_ms(100);
			i++;
			if(i>100)
			{
				pendrive_response=RESPONSE_PENDRIVE_ERROR;
				break;
			}
		}
		res=pendrive_response;
		//< if error response is received
		if(res==RESPONSE_PENDRIVE_ERROR)
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Error"));
		
			//< pendrive is already opened
			//< close the pendrive
			//< send close pendrive command to pendrive
			pendrive_response = RESPONSE_PENDRIVE_NONE;
			uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
			//< wait for response
			i=0;
			while(pendrive_response==RESPONSE_PENDRIVE_NONE)
			{
				_delay_ms(100);
				i++;
				if(i>100)
				{
					pendrive_response=RESPONSE_PENDRIVE_ERROR;
					break;
				}
			}
			res=pendrive_response;
		
			//< if error response is received
			if(res==RESPONSE_PENDRIVE_ERROR)
			{
			}
			_delay_ms(1000);
			return;
		}
		
		
		
	}
	
	
	//< close the file and stop communication with pendrive
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("                "));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Process Over"));
	
	//< pendrive is already opened
	//< close the pendrive
	//< send close pendrive command to pendrive
	pendrive_response = RESPONSE_PENDRIVE_NONE;
	uart_vnc2_WriteData(CMD_PENDRIVE_COM_STOP);
	//< wait for response
	i=0;
	while(pendrive_response==RESPONSE_PENDRIVE_NONE)
	{
		_delay_ms(100);
		i++;
		if(i>100)
		{
			pendrive_response=RESPONSE_PENDRIVE_ERROR;
			break;
		}
	}
	res=pendrive_response;
	
	//< if error response is received
	if(res==RESPONSE_PENDRIVE_ERROR)
	{
	}
	_delay_ms(1000);
	return;
}

#endif