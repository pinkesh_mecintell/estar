/*
 * ADS123x.h
 *
 * Created: 11/10/13 1:03:48 PM
 *  Author: Pinkesh
 */ 


#ifndef ADS1230_H_
#define ADS1230_H_

#define _ADS1230_	1
#define _ADS1232_	2
#define ADS123x_NAN			0xFFFFFFFF


/*******************************************************/
/**** Configure below as per ADC used and its buffer ***/
#define ADS_USED			_ADS1230_
#define ADC_BUFF_LENGTH		30
/*******************************************************/
/*******************************************************/


typedef struct
{
	unsigned char		unit_weight_value;
	unsigned long int	unit_weight_count;
	signed long int		ADC_max_count;
	signed long int		ADC_min_count;
	unsigned long int	max_weight_value;
}WEIGHT_CONSTANTS;


void ADS123x_Initialize();
void ADS123x_Calibrate();
signed long int ADS123x_getReading();
void ADC_getReading();
void WEIGHT_CONSTANTS_Read();
void WEIGHT_CONSTANTS_Write();
void ADC_getWeight();

#endif /* ADS1230_H_ */