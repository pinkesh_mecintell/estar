/*
 * uart_vnc2.h
 *
 * Created: 03/12/13 2:15:43 PM
 *  Author: Pinkesh
 */ 


#ifndef UART_VNC2_H_
#define UART_VNC2_H_

//< Command to VNC2 related macros
#define CMD_PRINTER_STATUS	0xFE
	

/******* Macros ******/
//< usart receive buffer
#define UART_VNC2_RECEIVE_BUFF_SIZE 128

//< uart_vnc2 communication - start characters
//< USB Key Board
#define USB_KEY_BOARD_START_CHAR	0xF0
//< USB printer
#define PRINTER_START_CHAR			0xF1
//< Pen drive
#define PENDRIVE_START_CHAR			0xF2


//< currently which device is communicating
#define RUNNING_NONE			0
#define RUNNING_USB_KEY_BOARD	1
#define RUNNING_PRINTER			2
#define RUNNING_PENDRIVE		3
/*********************************/


/********* Functions *********/
void uart_vnc2_Init();
uint8_t uart_vnc2_DataAvailable();
//char uart_vnc2_ReadData();
void uart_vnc2_WriteData(char data);
void uart_vnc2_WriteString(char *str);
void uart_vnc2_WriteStringF(const char *str);
//void uart_vnc2_ReadBuffer(void *buff,uint16_t len);
//void uart_vnc2_FlushBuffer();
/*******************************/


#endif /* UART_VNC2_H_ */