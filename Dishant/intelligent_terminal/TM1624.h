/*
 * TM1624.h
 *
 * Created: 07/01/14 6:09:39 PM
 *  Author: Pinkesh
 */ 


#ifndef TM1624_H_
#define TM1624_H_


#include <avr/io.h>
#include "utilities.h"

/********************************/
/**** Configuration *************/
/********************************/

#define TM1624_PORT	PORTB
#define TM1624_DDR	DDRB

#define TM1624_DATA_Pin		6
#define TM1624_CLOCK_Pin	5
#define TM1624_STROBE_Pin	7

#define TM1624_CLOCK_Delay	50	//< delay in microsecond
#define TM1624_STROBE_Delay	50	//< delay in microsecond
#define TM1624_DATA_Delay	50	//< delay in microsecond
/*********************************/


/*** Macros Related to TM1624 STROBE Pin *****/
#define TM1624_STROBE_DEACTIVE	SET(TM1624_PORT,TM1624_STROBE_Pin)
#define TM1624_STROBE_ACTIVE	CLR(TM1624_PORT,TM1624_STROBE_Pin)
/*********************************************/


/***** Function Declaration ******/
/**
* Initializes Shift Register interfacing pins
* Data, Clock, Strobe(total 3 pin)
*/
void TM1624_Initialize();

/**
* Low level function to write a single byte to TM1624.
* The byte is serially transfered to TM1624.
* but STROBE Pin is not controlled by this function
* it is high level function's responsibility
* to control STROBE pin appropriately
*/
void TM1624_WriteByte(unsigned char data);


/**
* High Level function to display data on
* TM1624 based internal display
*/
void TM1624_DisplayData();

#endif /* TM1624_H_ */