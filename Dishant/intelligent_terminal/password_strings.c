/*
 * password_strings.c
 *
 * Created: 18/11/13 11:22:15 PM
 *  Author: Pinkesh
 */ 

#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "lcd_mega.h"
#include "lcd_display.h"
#include "password_strings.h"
#include "ps2_kb.h"
#include "utilities.h"


/*** Extern variables ****/
//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////
/*********************************/

unsigned char wait_for_enter()
{
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		if (ps2_key.type==SPECIAL_KEY)
		{
			if (ps2_key.val==SPECIAL_KEY_ESC)
			{
				return RETURN_CAUSE_ESC;
			}
			else if (ps2_key.val==SPECIAL_KEY_ENTER)
			{
				return RETURN_CAUSE_ENTER;
			}
		}
	}
}

unsigned char get_string_from_user(char* str,unsigned char from,unsigned char cur,unsigned char to,unsigned char key_flag,unsigned char store_flag)
{
	unsigned char i,c=1;
	
	lcd_mega_ClearDispaly();
	lcd_mega_String(lcd_Line1_data);
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	lcd_Line2_min = from;
	lcd_Line2_pointer = cur;
	lcd_Line2_max = to;
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	
	//< clear LCD data on first key press
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (((ps2_key.val==' ')&&(key_flag&USE_SPACE)) || \
										(((ps2_key.val>='A')&&(ps2_key.val<='Z'))&&(key_flag&USE_CHARACTERS)) || \
										(((ps2_key.val>='0')&&(ps2_key.val<='9'))&&(key_flag&USE_NUMERIC))))
		{
			set_string_in_lcd_dataF(LINE2,PSTR("                                          "),from,0);
			lcd_Line2_data[from] = ps2_key.val;
			lcd_Line2_pointer=from+1;
			break;
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				set_string_in_lcd_dataF(LINE2,PSTR("                                          "),from,0);
				lcd_Line2_pointer=from;
				break;
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			//return user typed string at press of enter
			{
				str[0] = '\0';
				return RETURN_CAUSE_ENTER_WITHOUT_CHANGE;//< successful return
			}
			
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			//return an null string to indicate user giving no password
			{
				str[0] = '\0';
				return RETURN_CAUSE_ESC;//< unsuccessful return
			}
		}
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_String(lcd_Line1_data);
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (((ps2_key.val==' ')&&(key_flag&USE_SPACE)) || \
										(((ps2_key.val>='A')&&(ps2_key.val<='Z'))&&(key_flag&USE_CHARACTERS)) || \
										(((ps2_key.val>='0')&&(ps2_key.val<='9'))&&(key_flag&USE_NUMERIC))))
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
				if (lcd_Line2_pointer > LCD_DISPLAY_LEN)
				{
					lcd_mega_gotoxy1(0);
					for (i=0;i<40;i++)
					{
						lcd_mega_SendData(' ');
					}
					lcd_mega_gotoxy1(lcd_Line2_pointer-LCD_DISPLAY_LEN);
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_MoveDisplayLEFT();
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					//loop for added delete functionality rather than using insert with backspace
					while (lcd_Line2_pointer+(c-1) <= 40)
					{
						lcd_Line2_data[lcd_Line2_pointer+(c-1)]=lcd_Line2_data[lcd_Line2_pointer+c];
						c++;
					}
					c=1;
					lcd_mega_gotoxy2(0);
					//change the whole string in the second line of LCD data
					lcd_mega_String(lcd_Line2_data);
					//if display needs a back shift
					if (lcd_Line2_pointer>=LCD_DISPLAY_LEN)
					{
						lcd_mega_gotoxy1(0);
						for (i=0;i<40;i++)
						{
							lcd_mega_SendData(' ');
						}
						lcd_mega_gotoxy1(lcd_Line2_pointer-LCD_DISPLAY_LEN);
						lcd_mega_String(lcd_Line1_data);
						lcd_mega_MoveDisplayRIGHT();
					}
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			//return user typed string at press of enter
			{
				if(store_flag==STORE_UPTO_POINTER)
				{
					for(i=from;i<lcd_Line2_pointer;i++)
					{
						str[i-from] = lcd_Line2_data[i];
					}
					str[lcd_Line2_pointer-from] = '\0';
				}
				else if(store_flag==STORE_ALL_DATA)
				{
					for(i=from;i<to;i++)
					{
						str[i-from] = lcd_Line2_data[i];
					}
					str[to] = '\0';
				}
				return RETURN_CAUSE_ENTER_WITH_CHANGE;//< successful return
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			//return an null string to indicate user giving no password
			{
				str[0] = '\0';
				return RETURN_CAUSE_ESC;//< unsuccessful return
			}
		}
	}
}

unsigned char get_character_from_user_P(char* chr,const char* chr_str_P,char default_chr,unsigned char cur,unsigned char cur_line)
{
	lcd_mega_ClearDispaly();
	lcd_mega_String(lcd_Line1_data);
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	if(cur_line==LINE1)
	{
		lcd_Line1_pointer = cur;
		lcd_Line1_min = lcd_Line1_pointer;
		lcd_Line1_max = lcd_Line1_pointer+1;
		//< set cursor position
		lcd_mega_gotoxy1(lcd_Line1_pointer);
		//< setting default character
		lcd_Line1_data[lcd_Line1_pointer] = default_chr;
		lcd_mega_SendData(default_chr);
	}
	else
	{
		lcd_Line2_pointer = cur;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_Line2_max = lcd_Line2_pointer+1;
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		//< setting default character
		lcd_Line2_data[lcd_Line2_pointer] = default_chr;
		lcd_mega_SendData(default_chr);
	}
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (strchr_P(chr_str_P,ps2_key.val)))
		{
			if(cur_line==LINE1)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
			}
			else
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				if(cur_line==LINE1)
				{
					*chr = lcd_Line1_data[lcd_Line1_pointer];
				}
				else
				{
					*chr = lcd_Line2_data[lcd_Line2_pointer];
				}
				return RETURN_CAUSE_ENTER_WITH_CHANGE;//< Successful return
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				*chr = 0;
				return RETURN_CAUSE_ESC;//< unsuccessful return
			}
		}
	}
}

unsigned char get_password_from_user(char* str,unsigned char from,unsigned char cur,unsigned char to,unsigned char key_flag,unsigned char store_flag)
{
	unsigned char i,c=1;
	
	lcd_mega_ClearDispaly();
	lcd_mega_String(lcd_Line1_data);
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	lcd_Line2_min = from;
	lcd_Line2_pointer = cur;
	lcd_Line2_max = to;
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (((ps2_key.val==' ')&&(key_flag&USE_SPACE)) || \
		(((ps2_key.val>='A')&&(ps2_key.val<='Z'))&&(key_flag&USE_CHARACTERS)) || \
		(((ps2_key.val>='0')&&(ps2_key.val<='9'))&&(key_flag&USE_NUMERIC))))
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData('*');
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					//Shift whole string after backspace pointer to left
					while (lcd_Line2_pointer+(c-1) <= 40)
					{
						lcd_Line2_data[lcd_Line2_pointer+(c-1)]=lcd_Line2_data[lcd_Line2_pointer+c];
						c++;
					}
					c=1;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					//change the whole string in the second line of LCD data
					lcd_mega_SendData(' ');
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			//return user typed string at press of enter
			{
				if(store_flag==STORE_UPTO_POINTER)
				{
					for(i=from;i<lcd_Line2_pointer;i++)
					{
						str[i-from] = lcd_Line2_data[i];
					}
					str[lcd_Line2_pointer-from] = '\0';
				}
				else if(store_flag==STORE_ALL_DATA)
				{
					for(i=from;i<to;i++)
					{
						str[i-from] = lcd_Line2_data[i];
					}
					str[to] = '\0';
				}
				return RETURN_CAUSE_ENTER_WITH_CHANGE;//< successful return
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			//return an null string to indicate user giving no password
			{
				str[0] = '\0';
				return RETURN_CAUSE_ESC;//< unsuccessful return
			}
		}
	}
}