/*
 * spi_mega.c
 *
 * Created: 6/10/2012 11:12:34 AM
 *  Author: Pinkesh
 */ 
#include <avr/io.h>
#include "spi_mega.h"
#include "utilities.h"

void spi_mega_Initialize(void)
{
//< slave mode configuration
#if ( spi_mega_configSPIMode == 0)
	SET(spi_mega_SPIDDR,spi_mega_MISOPin);
	CLR(spi_mega_SPIPort,spi_mega_MISOPin);
//< master mode configuration
#else
	SET(spi_mega_SPIDDR,spi_mega_SSPin);
	SET(spi_mega_SPIPort,spi_mega_SSPin);
	SET(spi_mega_SPIDDR,spi_mega_MOSIPin);
	CLR(spi_mega_SPIPort,spi_mega_MOSIPin);
	SET(spi_mega_SPIDDR,spi_mega_SCKPin);
	CLR(spi_mega_SPIPort,spi_mega_SCKPin);
#endif

//< common configuration
	SPCR = (spi_mega_configSPIInterrupt << SPIE) | (1 << SPE) | 
			(spi_mega_configDataOrder << DORD) | 
			(spi_mega_configSPIMode << MSTR) | 
			(spi_mega_DataMode) | 
			(spi_mega_SPR1 << SPR1) | 
			(spi_mega_SPR0 << SPR0);
			
	SPSR = (spi_mega_SPI2X << SPI2X);
}

uint8_t	spi_mega_TransmittData(uint8_t spi_byte)
{
	uint8_t temp;

	SPDR = spi_byte;
	while(!CHK(SPSR,SPIF));
	temp = SPDR;

	return temp;
}