/*
 * SD_mega.h
 *
 * Created: 6/23/2012 11:50:07 PM
 *  Author: Pinkesh
 */ 


#ifndef SD_MEGA_H_
#define SD_MEGA_H_

#include "SD_mega_config.h"


//< fixed vlock length used in SD data communication
#define SD_mega_BLOCK_LEN 512

#define SD_mega_CS_LOW CLR(SD_mega_CSPORT,SD_mega_CSPin)
#define SD_mega_CS_HIGH SET(SD_mega_CSPORT,SD_mega_CSPin)

#define SD_mega_NOCARD 1
#define SD_mega_INITERROR 2
#define SD_mega_VOTAGEMISMATCH 3

#define SD_mega_SDVERSION_1 0
#define SD_mega_SDVERSION_2 1

#define SD_mega_SDSTANDARD 0
#define SD_mega_SDHC_SDXC 1


//SD commands, many of these are not used here
#define GO_IDLE_STATE            0
#define SEND_OP_COND             1
#define SEND_IF_COND			 8
#define SEND_CSD                 9
#define STOP_TRANSMISSION        12
#define SEND_STATUS              13
#define SET_BLOCK_LEN            16
#define READ_SINGLE_BLOCK        17
#define READ_MULTIPLE_BLOCKS     18
#define WRITE_SINGLE_BLOCK       24
#define WRITE_MULTIPLE_BLOCKS    25
#define ERASE_BLOCK_START_ADDR   32
#define ERASE_BLOCK_END_ADDR     33
#define ERASE_SELECTED_BLOCKS    38
#define SD_SEND_OP_COND			 41   //ACMD
#define APP_CMD					 55
#define READ_OCR				 58
#define CRC_ON_OFF               59


typedef struct
{
	uint8_t version	:1;	//< define version of SD card
						//< value = 0 then version 1.x compatible
						//< value = 1 then version 2.x or later compatible
						
	uint8_t	type	:1;	//< define type of SD card
						//< value = 0 then STANDARD SD CARD
						//< value = 1 then HIGH CAPACITY  (SDHC) or Extended Capacity (SDXC) SD CARD
	
	uint8_t success_init	:1;	//< defines the successful initialization of card
								//< value = 1 indicates successful initialization
								
	uint8_t error_type		:2;	//< defines which type of error occured during card initialization
								//< value = 1 then No SD card in slot
								//< value = 2 then card exist but error in initialization
								//< value = 3 then card exist but initialization is aborted due to voltage mismatch
								
	uint8_t			:3;
}SD_mega_CardInfo;


/**
* common structure contains fields related to the error when BLOCK write
* and block read operations are executed.
* \n all fields are used into read and write operation
* \n dataRejected field is not used in Read operation
*/
typedef struct 
{
	uint8_t error_occure		:1;	//< defines if error occurred or not
	
	uint8_t	commandExecution	:1;	//< defines error occurred in Command Execution or not
	
	uint8_t	timeout				:1; //< defines error occurred due to timeout or not
	
	uint8_t dataRejected		:1; //< defines error occurred due to data rejection or not
									//< only related to write operation
	uint8_t						:4;
}SD_mega_CommError;

/**
* This function initialize SD cards and detect the card type
* \return	SD_mega_CardInfo structure which defines various fields related to SD card
*/
SD_mega_CardInfo SD_mega_Initialize(void);


/**
* This function sends commands and return the response obtained
* This function implements commands which return R1 and R3 response only
* \return	response obtained
*			\nIn the case of R1 response command it return R1(8-bit) response obtained
*			\nIn the case of R3 response command(READ_OCR-CMD58) the response is 5-byte
*			long. so that it only return 2nd byte of response which contains the bits
*			related to the capacity of the card (CSC bit).
*			\n if timeout occure then it returns 0xFF.
*/
uint8_t SD_mega_SendCommand(uint8_t cmd, unsigned long arg);


/**
* This function reads a single block in data_buf of size SD_mega_BLOCK_LEN=512 bytes
* \param	data_buf	data buffer at which read data are stored
*						\n minimum size must be equal or greater than SD_mega_BLOCK_LEN=512
* \param	block_add	specify which block of SD card memory want to read
*						\n value must be in BLOCK unit means
*						\n if value=0 then address=0 to 511 are read
*						\n if value=1 then address=512 to 1023 are read
*						\n and so on..
*\return				return the SD_mega_CommError structure which contains
*						error related fields and if any error occurred then
*						corresponding bit is set
*/
SD_mega_CommError SD_mega_ReadSingleBlock(uint8_t* data_buf,uint32_t block_add);


/**
* This function writes a single block from data_buf of size SD_mega_BLOCK_LEN=512 bytes
* \param	data_buf	data buffer from which data are written
*						\n minimum size must be equal or greater than SD_mega_BLOCK_LEN=512
* \param	block_add	specify at which block of SD card memory want to write
*						\n value must be in BLOCK unit means
*						\n if value=0 then address=0 to 511 are written
*						\n if value=1 then address=512 to 1023 are written
*						\n and so on..
*\return				return the SD_mega_CommError structure which contains
*						error related fields and if any error occurred then
*						corresponding bit is set
*/
SD_mega_CommError SD_mega_WriteSingleBlock(const uint8_t* data_buf,uint32_t block_add);


#endif /* SD_MEGA_H_ */