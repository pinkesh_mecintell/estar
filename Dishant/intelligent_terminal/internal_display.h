/*
 * internal_display.h
 *
 * Created: 04/10/13 7:05:20 PM
 *  Author: Pinkesh
 */ 


#ifndef SHIFT_REGISTER_H_
#define SHIFT_REGISTER_H_


#include <avr/io.h>
#include "utilities.h"

#define DIGIT_OFF	0x0A
#define DIGIT_MINUS	0x0B

/********************************/
/**** Configuration *************/
/********************************/

#define INTERNAL_DISPLAY_PORT	PORTB
#define INTERNAL_DISPLAY_DDR	DDRB

#define INTERNAL_DISPLAY_DATA_Pin		6
#define INTERNAL_DISPLAY_CLOCK_Pin		5
#define INTERNAL_DISPLAY_LATCH_Pin		7

#define INTERNAL_DISPLAY_CLOCK_Delay	5	//< delay in microsecond
#define INTERNAL_DISPLAY_LATCH_DELAY	5	//< delay in microsecond

/*********************************/


/***** Function Declaration ******/
/**
* Initializes Shift Register interfacing pins
* Data, Clock, Latch(total 3 pin)
*/
void internal_display_Initialize();

/**
* Main High level function to write a single byte to
* Output shift register.
* The byte is serially transfered to shift register
* but it is not latched i.e. The byte is not available on
* output line Q0 to Q7 of the shift register IC.
* To set output pin as per new written byte, LatchPulse()
* function should be called.
*/
void internal_display_WriteByte(unsigned char data);

/**
* Sends pulse on LATCH PIN of shift register
* so that new written byte is latched and
* its output appeared on output pins.
*/
void internal_display_LatchPulse();


#endif /* SHIFT_REGISTER_H_ */