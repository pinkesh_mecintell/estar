/*
 * i2c.c
 *
 * Created: 11/10/2012 5:10:16 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include "utilities.h"
#include "i2c.h"


void I2C_InitMaster(void) //< Function to initialize master
{
	TWBR=I2C_configTWBR; //< Bit rate
	_BIT_SET(TWSR,I2C_configTWPS_bits); //< Setting prescalar bits
	//< SCL freq= F_CPU/(16+2(TWBR).4^TWPS)
}

void I2C_SendStart(void)
{
	//< Clear TWI interrupt flag, Put start condition on SDA, Enable TWI
	TWCR= (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while(!(TWCR & (1<<TWINT))); //< Wait till start condition is transmitted
	//while((TWSR & 0xF8)!= 0x08); //< Check for the acknowledgment
}

void I2C_SendRepeatedStart(void)
{
	//< Clear TWI interrupt flag, Put start condition on SDA, Enable TWI
	TWCR= (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while(!(TWCR & (1<<TWINT))); //< Wait till start condition is transmitted
	//while((TWSR & 0xF8)!= 0x10); //< Check for the acknowledgment
}

void I2C_SendAddress(unsigned char data)
{
	TWDR=data;    //< Address and read instruction
	TWCR=(1<<TWINT)|(1<<TWEN);    //< Clear TWI interrupt flag,Enable TWI
	while (!(TWCR & (1<<TWINT))); //< Wait till complete TWDR byte received
	//while((TWSR & 0xF8)!= 0x40);  //< Check for the acknowledgment
}

void I2C_SendData(unsigned char data)
{
	TWDR=data;    //< put data in TWDR
	TWCR=(1<<TWINT)|(1<<TWEN);    //< Clear TWI interrupt flag,Enable TWI
	while (!(TWCR & (1<<TWINT))); //< Wait till complete TWDR byte transmitted
	//while((TWSR & 0xF8) != 0x28); //< Check for the acknowledgment
}

void I2C_SendStop(void)
{
	//< Clear TWI interrupt flag, Put stop condition on SDA, Enable TWI
	TWCR= (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	while(!(TWCR & (1<<TWSTO)));  //< Wait till stop condition is transmitted
}

unsigned char I2C_ReadData(unsigned char status)
{
	unsigned recv_data;
	
	if(status) //< true if last data is read
	{
		TWCR=(1<<TWINT)|(1<<TWEN);    //< Clear TWI interrupt flag,Enable TWI,Enable NACK
		while (!(TWCR & (1<<TWINT))); //< Wait till complete TWDR byte transmitted
		//while((TWSR & 0xF8) != 0x58); //< Check for the acknowledgment
		recv_data=TWDR;
	}
	else //< true if we want to read more data
	{
		TWCR=(1<<TWINT)|(1<<TWEN)|(1<<TWEA); //< Clear TWI interrupt flag,Enable TWI,Enable ACK
		while (!(TWCR & (1<<TWINT))); //< Wait till complete TWDR byte transmitted
		//while((TWSR & 0xF8) != 0x50); //< Check for the acknowledgment
		recv_data=TWDR;
	}
	return recv_data;
}