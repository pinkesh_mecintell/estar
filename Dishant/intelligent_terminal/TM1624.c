/*
 * TM1624.c
 *
 * Created: 07/01/14 6:09:52 PM
 *  Author: Pinkesh
 */ 

#include <util/delay.h>
#include "TM1624.h"
#include "utilities.h"
#include "internal_display.h"

/***** Extern Veriables *******/
extern unsigned char SEG_CODE[];
extern unsigned char EXT_SEG_CODE[];
extern unsigned char weight_str[];
/******************************/


/**** Static Function Declaration *****/
static void TM1624_ClockPulse();
/**************************************/

/***** Function Definition *****/
void TM1624_Initialize()
{
	//< data_pin=0
	SET(TM1624_DDR,TM1624_DATA_Pin);
	CLR(TM1624_PORT,TM1624_DATA_Pin);
	
	//< clock_pin=1
	SET(TM1624_DDR,TM1624_CLOCK_Pin);
	SET(TM1624_PORT,TM1624_CLOCK_Pin);
	
	//< strobe_pin=1
	SET(TM1624_DDR,TM1624_STROBE_Pin);
	TM1624_STROBE_DEACTIVE;
}

static void TM1624_ClockPulse()
{
	CLR(TM1624_PORT,TM1624_CLOCK_Pin);
	_delay_us(TM1624_CLOCK_Delay);
	SET(TM1624_PORT,TM1624_CLOCK_Pin);
	_delay_us(TM1624_CLOCK_Delay);
}

void TM1624_WriteByte(unsigned char data)
{
	unsigned char i;
	
	//< sends LSB bit first
	for(i=0;i<8;i++)
	{
		//< Output the data on Data line
		//< according to the Value of LSB
		if(data & 0b00000001)
		{
			//< LSB is 1 so output high
			SET(TM1624_PORT,TM1624_DATA_Pin);
		}
		else
		{
			//< LSB is 0 so output low
			CLR(TM1624_PORT,TM1624_DATA_Pin);
		}
		
		//< Pulse the Clock line
		TM1624_ClockPulse();
		//< bring next bit at LSB position
		data=data>>1;
	}
}

void TM1624_DisplayData()
{
	unsigned char i;
	
	TM1624_STROBE_ACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_WriteByte(0x02); //< Command-1 - display mode setting command
	
	TM1624_STROBE_DEACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_STROBE_ACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_WriteByte(0x40); //< Command-2 - data read-write setting command
	
	TM1624_STROBE_DEACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_STROBE_ACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_WriteByte(0xC0); //< Command-3 - address setting command
	
	//< data to display
	for(i=0;i<14;i++)
	{
		//< data delay
		_delay_us(TM1624_DATA_Delay);
		
		//< send data to display
		TM1624_WriteByte(0xff);
	}
	TM1624_STROBE_DEACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_STROBE_ACTIVE;
	_delay_us(TM1624_STROBE_Delay);
	TM1624_WriteByte(0x88); //< Command-4 - display control command
	
	_delay_us(TM1624_STROBE_Delay);
	TM1624_STROBE_ACTIVE;
}