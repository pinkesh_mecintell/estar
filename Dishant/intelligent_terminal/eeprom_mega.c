/*
 * eeprom_mega.c
 *
 * Created: 6/12/2012 7:17:41 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "eeprom_mega.h"
#include "eeprom_mega_config.h"


/************************************************************************/
/*   FUNCTION IMPLEMENTATION                                            */
/************************************************************************/

#if ( eeprom_mega_includeReadByte==1 )
uint8_t eeprom_mega_ReadByte(uint16_t eeprom_address)
{
	//< Wait for completion of previous write
	while(CHK(EECR,EEWE));

	//< Set up address register
	EEAR = eeprom_address;

	//< Start eeprom read by writing EERE
	SET(EECR,EERE);

	//< Return data from data register
	return EEDR;
}
#endif

#if( eeprom_mega_includeWriteByte==1 )
void eeprom_mega_WriteByte(uint16_t eeprom_address,uint8_t eeprom_data)
{
	//< Wait for completion of previous write
	while(CHK(EECR,EEWE));
	
	//< Set up address and data registers
	EEAR = eeprom_address;
	EEDR = eeprom_data;
	
	//< disable global interrupt
	cli();
	
	//< Write logical one to EEMWE
	SET(EECR,EEMWE);
	
	//< Start eeprom write by setting EEWE
	SET(EECR,EEWE);
	
	//< enable global interrupt
	sei();
}
#endif

#if ( eeprom_mega_includeReadData==1 )
void eeprom_mega_ReadData(uint16_t start_address,uint8_t *data_buffer,uint16_t number_data)
{
	uint16_t num;
	for(num=0;num<number_data;num++)
	{
		data_buffer[num] = eeprom_mega_ReadByte(start_address+num);
	}
}
#endif

#if ( eeprom_mega_includeWriteData==1 )
void eeprom_mega_WriteData(uint16_t start_address,uint8_t *data_buffer,uint16_t number_data)
{
	uint16_t num;
	for(num=0;num<number_data;num++)
	{
		eeprom_mega_WriteByte(start_address+num,data_buffer[num]);
	}
}
#endif

#if ( eeprom_mega_includeReadWriteData==1 )
void eeprom_mega_ReadWriteData(bool read_write,uint16_t start_address,uint8_t *data_buffer,uint16_t number_data)
{
	uint16_t num;
	
	if(read_write)
	{
		for(num=0;num<number_data;num++)
		{
			data_buffer[num] = eeprom_mega_ReadByte(start_address+num);
		}
	}
	else
	{
		for(num=0;num<number_data;num++)
		{
			eeprom_mega_WriteByte(start_address+num,data_buffer[num]);
		}
	}
}
#endif