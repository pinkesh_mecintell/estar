/*
 * usart.h
 *
 * Created: 11/10/13 6:04:41 PM
 *  Author: Pinkesh
 */ 


#ifndef USART_H_
#define USART_H_


void usart0_Initialize();
unsigned char usart0_ReadData();
void usart0_WriteData(unsigned char data);


#endif /* USART_H_ */