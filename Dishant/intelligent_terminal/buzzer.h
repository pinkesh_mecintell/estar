/*
 * buzzer.h
 *
 * Created: 16/09/13 8:55:17 PM
 *  Author: Pinkesh
 */ 


#ifndef BUZZER_H_
#define BUZZER_H_


//< Buzzer Related
#define BUZZER_OFF	CLR(PORTD,5)
#define BUZZER_ON	SET(PORTD,5)
//< Buzzer timer related
#define BUZZER_TIMER_ON		SET(TCCR2,CS22); SET(TCCR2,CS20)
#define BUZZER_TIMER_OFF	CLR(TCCR2,CS22); CLR(TCCR0,CS20)


void buzzer_init();


#endif /* BUZZER_H_ */