/*
 * spi_mega_config.h
 *
 * Created: 6/10/2012 11:12:06 AM
 *  Author: Pinkesh
 */ 


#ifndef SPI_MEGA_CONFIG_H_
#define SPI_MEGA_CONFIG_H_

#include <avr/io.h>

/**
* Pin and PORT Register configuration
*/
#if defined(__AVR_ATmega128__) || defined(__AVR_ATmega64__) \
		|| defined(__AVR_ATmega640__) || defined(__AVR_ATmega1280__) \
			|| defined(__AVR_ATmega2560)
#define spi_mega_SPIPort PORTB
#define spi_mega_SPIDDR DDRB
#define spi_mega_SSPin 0
#define spi_mega_MOSIPin 2
#define spi_mega_MISOPin 3
#define spi_mega_SCKPin	1	
#endif

#if defined(__AVR_ATmega16__) || defined(__AVR_ATmega32__) 
#define spi_mega_SPIPort PORTB
#define spi_mega_SPIDDR DDRB
#define spi_mega_SSPin 4
#define spi_mega_MOSIPin 5
#define spi_mega_MISOPin 6
#define spi_mega_SCKPin	7	
#endif

#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega48__) \
		|| defined(__AVR_ATmega88__) || defined(__AVR_ATmega168__)
#define spi_mega_SPIPort PORTB
#define spi_mega_SPIDDR DDRB
#define spi_mega_SSPin 2
#define spi_mega_MOSIPin 3
#define spi_mega_MISOPin 4
#define spi_mega_SCKPin	5	
#endif

//< custom setting.
//< if your uC is different than defined above then set following $defines as per your uC.
#ifndef spi_mega_SPIPort
#pragma "custom pin configuration for SPI system is used"
#define spi_mega_SPIPort PORTB
#define spi_mega_SPIDDR DDRB
#define spi_mega_SSPin 2
#define spi_mega_MOSIPin 3
#define spi_mega_MISOPin 4
#define spi_mega_SCKPin	5	
#endif

/**
* value = 0 then SPI Interrupt is disabled
* value = 1 then SPI Interrupt is enabled
*/
#define spi_mega_configSPIInterrupt 0

/**
* value = 0 then MSB of Data is transmitted first
* value = 1 then LSB of data is transmitted first
*/
#define spi_mega_configDataOrder 0

/**
* value = 0 then SPI is as Slave device
* value = 1 then SPI is as Master device
*/
#define spi_mega_configSPIMode 1

/**
* sets SPI system's sampling and setup mechanism
*
* value = 0 then SPI DataMode = 0
*    i.e. Sample(Rising) Setup(Falling)
* value = 1 then SPI DataMode = 1
*    i.e. Setup(Rising) Sample(Falling)
* value = 2 then SPI DataMode = 2
*    i.e. Sample(Falling) Setup(Rising)
* value = 3 then SPI DataMode = 3
*	 i.e. Setup(Falling) Sample(Rising)
*/
#define spi_mega_configDataMode 0

/**
* sets the SCK frequency
*
* value = 0 then SCK_Frequency = F_CPU/2
* value = 1 then SCK_Frequency = F_CPU/4
* value = 2 then SCK_Frequency = F_CPU/8
* value = 3 then SCK_Frequency = F_CPU/16
* value = 4 then SCK_Frequency = F_CPU/32
* value = 5 then SCK_Frequency = F_CPU/64
* value = 6 then SCK_Frequency = F_CPU/128
*/
#define spi_mega_configSCKFrequency 2


#endif /* SPI_MEGA_CONFIG_H_ */