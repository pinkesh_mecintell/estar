/*
 * printer.c
 *
 * Created: 22/09/13 12:51:23 AM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "utilities.h"
#include "uart_vnc2.h"
#include "printer.h"
#include "lcd_mega.h"
#include "lcd_display.h"
#include "password_strings.h"
#include "ps2_kb.h"


/*** Extern variables ****/
//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////

extern unsigned char fun_ret;
/*************************/



//< usb printer related
#if (PRINTER_BUS_TYPE==PRINTER_USB)
	extern volatile unsigned char printer_status;
#endif

void printer_init()
{
#if (PRINTER_BUS_TYPE==PRINTER_PARALLEL)
	//< printer data pins
	//< D0-PORTA0....D7-PORTA7
	DDRA=0xFF;
	PORTA=0;
	//< printer buffer initialize
	//< output enable-F7
	SET(DDRF,7);
	CLR(PORTF,7);
	//< buffer clock - F6
	SET(DDRF,6);
	CLR(PORTF,6);
	
	//< printer signal pins
	//< strobe-F5-output
	SET(DDRF,5);
	SET(PORTF,5);
	//< ACK-F4-input
	CLR(DDRF,4);
	SET(PORTF,4);
	//< Busy-F3-input
	CLR(DDRF,3);
	SET(PORTF,3);	
#endif
}

void printer_writeData(unsigned char pdata)
{
#if (PRINTER_BUS_TYPE==PRINTER_PARALLEL)
	while(CHK(PINF,3));
	
	PORTA=pdata;
	//< buffer related
	SET(PORTF,6);
	_delay_us(10);
	CLR(PORTF,6);
	_delay_us(10);
	SET(PORTF,6);
	_delay_us(10);
	
	//< strobe related
	SET(PORTF,5);
	_delay_us(10);
	CLR(PORTF,5);
	_delay_us(10);
	SET(PORTF,5);
	_delay_us(10);
	
	while(!CHK(PINF,4));
	
#elif (PRINTER_BUS_TYPE == PRINTER_USB)
	uart_vnc2_WriteData(pdata);
#endif
}

void printer_writeString(char *str)
{
#if (PRINTER_BUS_TYPE==PRINTER_PARALLEL || PRINTER_BUS_TYPE==PRINTER_USB)
	while(*str)
	{
		printer_writeData(*str++);
	}
#endif
}

void printer_writeStringF(const char *str)
{
#if (PRINTER_BUS_TYPE==PRINTER_PARALLEL || PRINTER_BUS_TYPE==PRINTER_USB)
	while(pgm_read_byte(&(*str)))
	{
		printer_writeData(pgm_read_byte(&(*str++)));
	}
#endif
}

unsigned char printer_get_status()
{
#if (PRINTER_BUS_TYPE==PRINTER_USB)
	unsigned char i=0;
	printer_status=STATUS_PRINTER_NONE;
	uart_vnc2_WriteData(CMD_PRINTER_STATUS);
	
	while(printer_status==STATUS_PRINTER_NONE)
	{
		_delay_ms(1);
		i++;
		if(i>200)
		{
			return STATUS_PRINTER_ERROR;
		}
	}
	return printer_status;
#elif (PRINTER_BUS_TYPE==PRINTER_PARALLEL)
	return STATUS_PRINTER_OK;
#endif
}

unsigned char check_printer()
{
	unsigned char status,ent_ret,try=0;

#if (PRINTER_TYPE==PRINTER_USB)
PRINTER_RETRY:
	status = printer_get_status();
	switch(status)
	{
		case STATUS_PRINTER_OK:
			return STATUS_PRINTER_OK;
			break;

		case STATUS_PRINTER_NO_PAPER:
		case STATUS_PRINTER_ERROR:
			//< display message on LCD
			lcd_mega_ClearDispaly();
			if(status==STATUS_PRINTER_NO_PAPER)
			{
				lcd_mega_StringF(PSTR("Printer NO PAPER"));
			}
			else if(status==STATUS_PRINTER_ERROR)
			{
				lcd_mega_StringF(PSTR("Printer ERROR"));
			}
			//< check again
			for(try=0;try<40;try++)
			{
				if(printer_get_status()==STATUS_PRINTER_OK)
				{
					//< reset lcd display
					lcd_mega_ClearDispaly();
					lcd_mega_String(lcd_Line1_data);
					lcd_mega_String(lcd_Line2_data);
					return STATUS_PRINTER_OK;
				}
				_delay_ms(100);
			}
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ENT=RTRY ESC=EXT"));
			ent_ret = wait_for_enter();
			if(ent_ret==RETURN_CAUSE_ENTER)
			{
				goto PRINTER_RETRY;
			}
			else//< return_cause_esc
			{
				return STATUS_PRINTER_ERROR;
			}
			break;
	}
	return STATUS_PRINTER_OK;
	
#elif (PRINTER_BUS_TYPE==PRINTER_PARALLEL)
	return STATUS_PRINTER_OK;
#endif
}

void printer_set_horizontal_pos(unsigned int mm)
{
	mm = (unsigned int)((((float)mm)/(25.4))*(60.0)); //< convert into inch
	PRINTER_ESC;
	printer_writeData(0x24);
	printer_writeData(mm%256);
	printer_writeData(mm/256);
}

void printer_set_verticle_pos(unsigned int mm)
{
	mm = (unsigned int)((((float)mm)/(25.4))*(216.0)); //< convert to inch
	while(1)
	{
		if(mm<=255)
		{
			PRINTER_ESC;
			printer_writeData(0x4A);
			printer_writeData(mm);
			break;
		}
		else
		{
			PRINTER_ESC;
			printer_writeData(0x4A);
			printer_writeData(255);
			mm = mm - 255;
		}
	}
}