/*
 * ds1307.c
 *
 * Created: 11/08/13 8:28:24 PM
 *  Author: Pinkesh
 */ 
#include "utilities.h"
#include "ds1307.h"
#include "i2c.h"
#include "lcd_mega.h"

/**** Extern variables *****/
extern RTC_DATA RTC_data;
/***************************/


/**
* Function To Read Internal Registers of DS1307
* \param	address		Address of the register
* \param	data		pointer to store register after read
* \return				0= Failure\n
*						1= Success
*/
unsigned char ds1307_ReadRegister(unsigned char address,unsigned char *data)
{
	unsigned char temp_data;
	
	//< send Start condition
	I2C_SendStart();
	
	//< send device address in write mode
	I2C_SendAddress(DS1307_CHIP_ADDRESS);	
	if(I2C_STATUS!=I2C_MT_SLA_ACK)
	{
		I2C_SendStop();
		lcd_mega_gotoxy1(8);
		lcd_mega_SendData('1');
		return 0;//< error return
	}
	
	//< send the register address
	I2C_SendData(address);
	if(I2C_STATUS!=I2C_MT_DATA_ACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	//< send repeated start condition
	I2C_SendRepeatedStart();
	
	//< send device address in read mode
	I2C_SendAddress((DS1307_CHIP_ADDRESS|0x01));
	if(I2C_STATUS!=I2C_MR_SLA_ACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	//Now read the value with NACK
	temp_data = I2C_ReadData(I2C_READ_LAST_DATA);
	if(I2C_STATUS!=I2C_MR_DATA_NACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	*data = temp_data;
	
	//< send stop condition
	I2C_SendStop();
	
	//< successful return
	return 1;
}

/**
* Function To Write Internal Registers of DS1307
* \param	address		Address of the register
* \param	data		value to write.
* return				0= Failure\n
*						1= Success
***************************************************/

unsigned char ds1307_WriteRegister(unsigned char address,unsigned char data)
{
	//< send Start condition
	I2C_SendStart();
	
	//< send device address in write mode
	I2C_SendAddress(DS1307_CHIP_ADDRESS);
	if(I2C_STATUS!=I2C_MT_SLA_ACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	//< send the register address
	I2C_SendData(address);
	if(I2C_STATUS!=I2C_MT_DATA_ACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	//< send data byte to write
	I2C_SendData(data);
	if(I2C_STATUS!=I2C_MT_DATA_ACK)
	{
		I2C_SendStop();
		return 0;//< error return
	}
	
	//< send stop condition
	I2C_SendStop();
	
	//< successful return
	return 1;
}

/**
* Refreshes RTC data(seconds,minutes,etc) and stores into
* RTC_data structure variable
* return	0= Failure\n
*			1= Success
*/
unsigned char ds1307_RefreshRTC_data()
{
	unsigned char temp;
	unsigned char result;
	
	//< second reading
	result=ds1307_ReadRegister(0,&temp);
	if(result)
	{
		RTC_data.second[0] = ((temp&0x70)>>4)+'0';
		RTC_data.second[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}		
	
	//< minute reading
	result=ds1307_ReadRegister(1,&temp);
	if(result)
	{
		RTC_data.minute[0] = ((temp&0x70)>>4)+'0';
		RTC_data.minute[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}
	
	//< hour reading
	result=ds1307_ReadRegister(2,&temp);
	if(result)
	{
		RTC_data.hour[0] = ((temp&0x30)>>4)+'0';
		RTC_data.hour[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}
	
	//< date reading
	result=ds1307_ReadRegister(4,&temp);
	if(result)
	{
		RTC_data.date[0] = ((temp&0x30)>>4)+'0';
		RTC_data.date[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}
	
	//< month reading
	result=ds1307_ReadRegister(5,&temp);
	if(result)
	{
		RTC_data.month[0] = ((temp&0x10)>>4)+'0';
		RTC_data.month[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}
	
	//< year reading
	result=ds1307_ReadRegister(6,&temp);
	if(result)
	{
		RTC_data.year[0] = ((temp&0xF0)>>4)+'0';
		RTC_data.year[1] = (temp&0x0F)+'0';
	}
	else
	{
		return 0;
	}
	
	//< successful return
	return 1;
}

/**
* checks RTC is available or not
* if available it checks whether it is running or not
* if not running it turns on its function
* also checks hour mode is 24 or not
* if not it sets hour mode 24
* return	0= Failure\n
*			1= Success
*/
unsigned char ds1307_CheckRTC()
{
	unsigned char result;
	unsigned char temp;
	unsigned char temp_hour;
	
	result = ds1307_ReadRegister(0,&temp);
	if(result)
	{
		//< if clock not running
		if(CHK(temp,7))
		{
			//< initialize all the values to their default value
			ds1307_WriteRegister(1,0x00);//< minute=0
			ds1307_WriteRegister(2,0x00);//< hour,24 mode=0
			ds1307_WriteRegister(4,0x01);//< date=1
			ds1307_WriteRegister(5,0x01);//< month=1
			ds1307_WriteRegister(6,0x13);//< year=2013
			
			//< enable clock and set second
			CLR(temp,7);
			result = ds1307_WriteRegister(0,temp);
			if(!result)
			{
				return 0;
			}
		}
	}
	else
	{
		return 0;
	}
	
	//< check hour mode is 24
	result = ds1307_ReadRegister(2,&temp);
	if(result)
	{
		//< if hour mode is AM/PM
		if(CHK(temp,6))
		{
			//< convert AM/PM_hour to 24_hour
			temp_hour=(temp&0x01)*10 + (temp&0xF0);
			if(CHK(temp,5))
			{
				temp_hour+=12;
				temp_hour%=24;
			}
			//< set hour mode 24
			CLR(temp_hour,6);
			ds1307_WriteRegister(2,temp_hour);
		}
	}
	
	//< successful return
	return 1;
}