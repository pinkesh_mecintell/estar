/*
 * function_keys.h
 *
 * Created: 05/10/13 12:17:19 PM
 *  Author: Pinkesh
 */


#ifndef FUNCTION_KEYS_H_
#define FUNCTION_KEYS_H_

#define MAX_RST_VALUE  200000

void RST_check();
unsigned long int RST_read();
void RST_entry_print(unsigned char *_sd_block,unsigned int _rst_no);
void Report_entry_print(unsigned char *_sd_block,unsigned int _rst_no);
void RST_write(unsigned long int _rst_no);
void function_F1();
//This Function Calls different parts of F1 whole function as per num. argument
//It does not save Buffer in SD card it only Edits SD_data_buffer
unsigned char function_get_data(unsigned char fun_num);
//Both Functions similar to F1 entry
void function_F2();
//< flag should be - 1 for viewing rst entry using F3
//< flag should be - 0 for viewing rst entry using function_F6()
//< returns	0-successful
//<			1-unsuccessful
unsigned char function_F3(unsigned char flag);
//Show RST entry or print from RST input
void function_F4();
void function_F5();
//Show RST entry or print from Vehicle No. Input
void function_F6();
void function_F7();
void function_F8();
void function_F9();
void function_F10();
void F1_print_sleep_type1(unsigned long int rst);
void F1_print_sleep_type2(unsigned long int rst);
void F2_print_sleep_type1(unsigned long int rst);
void function_operator_login();
void function_operator_logout();
//takes a RST no Entry and matches it with query to see if it matches
//CAUTION : Pass Choice as choice + 1 as First choice is All
unsigned char does_it_match(unsigned long int rst,unsigned char choice,unsigned char* ip_str,unsigned char f_c_p);
//From values of to date and from date returns number of to rst and from rst
unsigned char does_date_match(unsigned long int from_date,unsigned long int to_date,unsigned int from_time,unsigned int to_time,unsigned long int rst);
//print header bytes for general report
void print_header_general_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice);
void print_header_time_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice);
void print_header_summery_report(unsigned char rst_date,unsigned long int frm_rst,unsigned long int to_rst,unsigned char choice);
//print footer for general report
void print_footer_general_report(unsigned long int weight,unsigned long int charges);
void print_footer_summery_report(unsigned long int records,unsigned long int weight,unsigned long int charges);
void engg_login();
void engg_change_pass();

#endif /* FUNCTION_KEYS_H_ */