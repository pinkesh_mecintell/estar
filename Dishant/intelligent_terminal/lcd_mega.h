/*
 * lcd_mega.h
 *
 * Created: 6/15/2012 6:38:53 PM
 *  Author: Pinkesh
 */ 


#ifndef LCD_MEGA_H_
#define LCD_MEGA_H_

#include "lcd_mega_utilities.h"



/************************************************************************//**
*	Function Declaration
************************************************************************/

/**
* Initialize LCD.
*\nCall the this function before calling any other functions
* to initialize the LCD. It initialize lcd in 8-bit mode,2-line display
* cursor on, shift cursor right, clear display.
*/
void lcd_mega_Initialize(void);

/**
* sends command to lcd specified into argument.
*\n \param	cmd		command to lcd
*/
void lcd_mega_SendCmd(uint8_t cmd);

/**
* sends data to lcd specified into argument.
*\n \param	data	data to lcd
*/
void lcd_mega_SendData(uint8_t data);

/**
* prints the whole string on the lcd. 
* string should be NULL terminated
* \param	str		pointer to the string
*/
void lcd_mega_String(char *str);

/**
* prints the whole string from FLASH(ROM) on the lcd.
* string must be NULL terminated
* \param	str		pointer to the string stored in FLASH(ROM)
*/
void lcd_mega_StringF(const char *str);

/**
* prints the decimal three digit value of num on LCD
* \n for ex. - if num = 15 then print 015 on lcd
* \param	num		specify number which should printed on lcd
*/
void lcd_mega_Showvalue(uint8_t num);

#endif /* LCD_MEGA_H_ */