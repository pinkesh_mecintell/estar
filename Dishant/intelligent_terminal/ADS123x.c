/*
 * ADS123x.c
 *
 * Created: 11/10/13 1:03:33 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <math.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "utilities.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "lcd_mega.h"
#include "internal_display.h"
#include "ADS123x.h"


#define CLK_HI	SET(PORTD,7)
#define CLK_LOW	CLR(PORTD,7)


/*** Extern variables ****/

///////////////////////////
//< ADS123x - ADC related
extern signed long int ADC_count;
extern signed long int ADC_buff[];
extern unsigned char ADC_buff_count;
///////////////////////////

/////////////////////////////
//< weight related
extern signed long int weight;
extern WEIGHT_CONSTANTS weight_const;
/////////////////////////////

///////////////////////////
//< internal 7-segment display related
extern unsigned char weight_str[];
/////////////////////////////
/*************************/



void ADS123x_Initialize()
{
	//< DATA pin
	CLR(DDRD,6);
	SET(PORTD,6);
	
	//< CLOCK pin
	SET(DDRD,7);
	CLR(PORTD,7);
}

void ADS123x_Calibrate()
{
	signed char i;

	//< check DATA pin is low or not
	while(CHK(PIND,6));
	_delay_ms(1);
	
#if ((ADS_USED==_ADS1230_) || (ADS_USED==_ADS1232_))
	for(i=26;i>0;i--)
	{
		CLK_HI;
		_delay_us(17);
		CLK_LOW;
		_delay_us(17);
	}
#endif

	_delay_ms(20);	
	//< check DATA pin is low or not
	while(CHK(PIND,6));
}

signed long int ADS123x_getReading()
{
	signed long int ads_data=0;
	signed char i;
	
	//< check DATA pin is low or not
	while(CHK(PIND,6));
	_delay_ms(1);
	
#if (ADS_USED==_ADS1230_)
	for(i=20;i>0;i--)
	{
		CLK_HI;
		_delay_us(17);
		if(CHK(PIND,6))
		{
			ads_data += (1L << (i-1));
		}
		CLK_LOW;
		_delay_us(17);
	}
	CLK_HI;
	_delay_us(17);
	CLK_LOW;
	_delay_us(17);
	
	//< out of range data
	if(ads_data>0xFFFFF)
	{
		return ADS123x_NAN;
	}
	
	//< negative data
	if(ads_data>0x7FFFF && ads_data<=0xFFFFF)
	{
		ads_data=0xFFFFF-ads_data;
		ads_data+=1;
		ads_data*=(-1);
	}
	
#elif (ADS_USED==_ADS1232_)
	for(i=24;i>0;i--)
	{
		CLK_HI;
		_delay_us(17);
		if(CHK(PIND,6))
		{
			ads_data += (1L << (i-1));
		}
		CLK_LOW;
		_delay_us(17);
	}
	CLK_HI;
	_delay_us(17);
	CLK_LOW;
	_delay_us(17);
	
	//< out of range data
	if(ads_data>0xFFFFFF)
	{
		return ADS123x_NAN;
	}
	
	//< negative data
	if(ads_data>0x7FFFFF && ads_data<=0xFFFFFF)
	{
		ads_data=0xFFFFFF-ads_data;
		ads_data+=1;
		ads_data*=(-1);
	}
#endif

	return ads_data;
}

void ADC_getReading()
{
	unsigned char j;
	signed long int ADC_sum;
	signed long int temp_ADC_count;
	static unsigned char i=0;
	
	//< get ADS1230 reading and store into buffer
	temp_ADC_count=ADS123x_getReading();
	if(temp_ADC_count!=ADS123x_NAN)
	{
		if(labs(temp_ADC_count-ADC_count) >= ((weight_const.unit_weight_count/1000) * 10))
		{
			i++;
			if(i==4)
			{
				for(j=0;j<20;j++)
				{
					i=0;
					ADC_buff[ADC_buff_count]=temp_ADC_count;
					ADC_buff_count++;
					if(ADC_buff_count==ADC_BUFF_LENGTH)
					{
						ADC_buff_count=0;
					}
				}
				ADC_sum=0;
				for(j=0;j<ADC_BUFF_LENGTH;j++)
				{
					ADC_sum+=ADC_buff[j];
				}
				ADC_count = ADC_sum/ADC_BUFF_LENGTH;
			}
			else if(i>2)
			{
				ADC_buff[ADC_buff_count]=temp_ADC_count;
				ADC_buff_count++;
				if(ADC_buff_count==ADC_BUFF_LENGTH)
				{
					ADC_buff_count=0;
				}
			}
		}
		else
		{
			i=0;
			ADC_buff[ADC_buff_count]=temp_ADC_count;
			ADC_buff_count++;
			if(ADC_buff_count==ADC_BUFF_LENGTH)
			{
				ADC_buff_count=0;
			}
			
			ADC_sum=0;
			for(j=0;j<ADC_BUFF_LENGTH;j++)
			{
				ADC_sum+=ADC_buff[j];
			}
			ADC_count = ADC_sum/ADC_BUFF_LENGTH;
		}
// 		ADC_buff[ADC_buff_count]=temp_ADC_count;
// 		ADC_buff_count++;
// 		if(ADC_buff_count==ADS_BUFF_LENGTH)
// 		{
// 			ADC_buff_count=0;
// 		}
	}
	
	//< moving average filtering
// 	ADC_sum=0;
// 	for(j=0;j<ADS_BUFF_LENGTH;j++)
// 	{
// 		ADC_sum+=ADC_buff[j];
// 	}
// 	ADC_count = ADC_sum/ADS_BUFF_LENGTH;
	//ADC_count = (ADC_count>>5);
}

void WEIGHT_CONSTANTS_Read()
{
	unsigned char LSB;
	unsigned char LSB_plus1;
	unsigned char LSB_plus2;
	unsigned char LSB_plus3;
	
	//< unit_weight_value reading - unsigned char
	weight_const.unit_weight_value = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_VALUE);
	
	
	//< unit_weight_count reading - unsigned long
	LSB = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT);
	LSB_plus1 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+1);
	LSB_plus2 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+2);
	LSB_plus3 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+3);
	
	weight_const.unit_weight_count = (unsigned long int)((((unsigned long int)LSB_plus3)<<24) + \
	(((unsigned long int)LSB_plus2)<<16) + \
	(((unsigned long int)LSB_plus1)<<8) + \
	(((unsigned long int)LSB)));
	
	
	//< ADC_max_count reading - signed long
	LSB = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT);
	LSB_plus1 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+1);
	LSB_plus2 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+2);
	LSB_plus3 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+3);
	
	weight_const.ADC_max_count = (signed long int)((((unsigned long int)LSB_plus3)<<24) + \
	(((unsigned long int)LSB_plus2)<<16) + \
	(((unsigned long int)LSB_plus1)<<8) + \
	(((unsigned long int)LSB)));
	
	
	//< ADC_min_count reading - signed long
	LSB = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT);
	LSB_plus1 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+1);
	LSB_plus2 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+2);
	LSB_plus3 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+3);
	
	weight_const.ADC_min_count = (signed long int)((((unsigned long int)LSB_plus3)<<24) + \
	(((unsigned long int)LSB_plus2)<<16) + \
	(((unsigned long int)LSB_plus1)<<8) + \
	(((unsigned long int)LSB)));
	
	
	//< max_weight_value reading - unsigned long
	LSB = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE);
	LSB_plus1 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+1);
	LSB_plus2 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+2);
	LSB_plus3 = eeprom_mega_ReadByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+3);
	
	weight_const.max_weight_value = (unsigned long int)((((unsigned long int)LSB_plus3)<<24) + \
	(((unsigned long int)LSB_plus2)<<16) + \
	(((unsigned long int)LSB_plus1)<<8) + \
	(((unsigned long int)LSB)));
}

void WEIGHT_CONSTANTS_Write()
{
	unsigned char LSB;
	unsigned char LSB_plus1;
	unsigned char LSB_plus2;
	unsigned char LSB_plus3;
	
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS,0xAA);//< key setting in copy-1
	
	//< unit_weight_value writing - unsigned char
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_VALUE,weight_const.unit_weight_value);
	
	//< unit_weight_count writing - unsigned long
	LSB = (unsigned char)(weight_const.unit_weight_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.unit_weight_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.unit_weight_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.unit_weight_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT,LSB);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+3,LSB_plus3);
	
	//< ADC_max_count writing - signed long
	LSB = (unsigned char)(weight_const.ADC_max_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.ADC_max_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.ADC_max_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.ADC_max_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT,LSB);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+3,LSB_plus3);
	
	//< ADC_min_count writing - signed long
	LSB = (unsigned char)(weight_const.ADC_min_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.ADC_min_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.ADC_min_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.ADC_min_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT,LSB);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+3,LSB_plus3);
	
	//< max_weight_value writing - unsigned long
	LSB = (unsigned char)(weight_const.max_weight_value % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.max_weight_value >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.max_weight_value >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.max_weight_value >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE,LSB);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+1,LSB_plus1);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+2,LSB_plus2);
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+3,LSB_plus3);
	
	
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS,0xAA);//< key setting in copy-2
	
	//< unit_weight_value writing - unsigned char
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_VALUE,weight_const.unit_weight_value);
	
	//< unit_weight_count writing - unsigned long
	LSB = (unsigned char)(weight_const.unit_weight_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.unit_weight_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.unit_weight_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.unit_weight_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT,LSB);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT+3,LSB_plus3);
	
	//< ADC_max_count writing - signed long
	LSB = (unsigned char)(weight_const.ADC_max_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.ADC_max_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.ADC_max_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.ADC_max_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT,LSB);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MAX_COUNT+3,LSB_plus3);
	
	//< ADC_min_count writing - signed long
	LSB = (unsigned char)(weight_const.ADC_min_count % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.ADC_min_count >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.ADC_min_count >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.ADC_min_count >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT,LSB);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+1,LSB_plus1);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+2,LSB_plus2);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_ADC_MIN_COUNT+3,LSB_plus3);
	
	//< max_weight_value writing - unsigned long
	LSB = (unsigned char)(weight_const.max_weight_value % 0x100);
	LSB_plus1 = (unsigned char)((weight_const.max_weight_value >> 8) % 0x100);
	LSB_plus2 = (unsigned char)((weight_const.max_weight_value >> 16) % 0x100);
	LSB_plus3 = (unsigned char)((weight_const.max_weight_value >> 24) % 0x100);
	//< data saving
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE,LSB);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+1,LSB_plus1);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+2,LSB_plus2);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS+WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE+3,LSB_plus3);
	
	
	//< set copy-1's key to 0
	eeprom_mega_WriteByte(ADD_WEIGHT_CONSTANTS,0);
	//< set copy-2's key to 0
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_WEIGHT_CONSTANTS,0);
}

void ADC_getWeight()
{
	signed long int ADC_count_diff;
	double temp;
	char str[10];
	signed char len;
	signed char i;
	
	ADC_count_diff = ADC_count - weight_const.ADC_min_count;
	temp = (double)(((double)ADC_count_diff) / (double)(weight_const.unit_weight_count));
	weight = (signed long int)(temp * (double)(weight_const.unit_weight_value) * 1000.0);
	if((weight%weight_const.unit_weight_value)>(weight_const.unit_weight_value/2))
	{
		weight = ((weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value))+(weight_const.unit_weight_value);
	}
	else
	{
		weight = ((weight/weight_const.unit_weight_value)*(weight_const.unit_weight_value));
	}
	
	ltoa(weight,str,10);
	len = strlen(str);
	if(len>6)
	{
		len=6;
	}
	for(i=0;i<6-len;i++)
	{
		weight_str[i]=DIGIT_OFF;
	}
	for(i=6-len;i<6;i++)
	{
		if(str[i-(6-len)]=='-')
		{
			weight_str[i] = DIGIT_MINUS;
		}
		else
		{
			weight_str[i] = str[i-(6-len)]-'0';
		}
	}
}
