/*
 * ps2_kb_scancodes.h
 *
 * Created: 11/08/13 1:22:19 PM
 *  Author: Pinkesh
 */ 


#ifndef PS2_KB_SCANCODES_H_
#define PS2_KB_SCANCODES_H_

#include <avr/pgmspace.h>
#include "ps2_kb.h"


#if (KB_TYPE==KB_PS2)
//< ps2 keyboard scancodes
//< printable characters only
const unsigned char ps2_scancodes[][2] PROGMEM =
{
	{0x15,'Q'},
	{0x16,'1'},
	{0x1a,'Z'},
	{0x1b,'S'},
	{0x1c,'A'},
	{0x1d,'W'},
	{0x1e,'2'},
	{0x21,'C'},
	{0x22,'X'},
	{0x23,'D'},
	{0x24,'E'},
	{0x25,'4'},
	{0x26,'3'},
	{0x29,' '},
	{0x2a,'V'},
	{0x2b,'F'},
	{0x2c,'T'},
	{0x2d,'R'},
	{0x2e,'5'},
	{0x31,'N'},
	{0x32,'B'},
	{0x33,'H'},
	{0x34,'G'},
	{0x35,'Y'},
	{0x36,'6'},
	{0x3a,'M'},
	{0x3b,'J'},
	{0x3c,'U'},
	{0x3d,'7'},
	{0x3e,'8'},
//	{0x41,','},
	{0x42,'K'},
	{0x43,'I'},
	{0x44,'O'},
	{0x45,'0'},
	{0x46,'9'},
	{0x49,'.'},
//	{0x4a,'/'},
	{0x4b,'L'},
//	{0x4c,';'},
	{0x4d,'P'},
//	{0x4e,'-'},
//	{0x52,'\''},
//	{0x54,'['},
//	{0x55,'='},
//	{0x5b,']'},
//	{0x5d,'\\'},
 	{0x69,'1'},
 	{0x6b,'4'},
 	{0x6c,'7'},
 	{0x70,'0'},
 	{0x71,'.'},
 	{0x72,'2'},
 	{0x73,'5'},
 	{0x74,'6'},
 	{0x75,'8'},
//	{0x79,'+'},
	{0x7a,'3'},
//	{0x7b,'-'},
//	{0x7c,'*'},
	{0x7d,'9'},
	{0,0}
};


//< special key scancodes
//< which are preceded by 0xE0
const unsigned char ps2_scancodes_special_key_list1[][2] PROGMEM =
{
	{0x5A,SPECIAL_KEY_ENTER},//< numeric key pad ENTER
	{0x11,SPECIAL_KEY_ALT},//< right ENTER
	{0x75,SPECIAL_KEY_ARROW_UP},
	{0x72,SPECIAL_KEY_ARROW_DOWN},
	{0x6B,SPECIAL_KEY_ARROW_LEFT},
	{0x74,SPECIAL_KEY_ARROW_RIGHT},
	{0,0}
};


//< special key scancodes
//< which are not preceded by 0xE0
//< i.e. normal key press
const unsigned char ps2_scancodes_special_key_list2[][2] PROGMEM =
{
	{0x5A,SPECIAL_KEY_ENTER},//< normal ENTER
	{0x66,SPECIAL_KEY_BACKSPACE},
	{0x11,SPECIAL_KEY_ALT},//< left ALT
	{0x76,SPECIAL_KEY_ESC},
	{0x05,SPECIAL_KEY_F1},
	{0x06,SPECIAL_KEY_F2},
	{0x04,SPECIAL_KEY_F3},
	{0x0C,SPECIAL_KEY_F4},
	{0x03,SPECIAL_KEY_F5},
	{0x0B,SPECIAL_KEY_F6},
	{0x83,SPECIAL_KEY_F7},
	{0x0A,SPECIAL_KEY_F8},
	{0x01,SPECIAL_KEY_F9},
	{0x09,SPECIAL_KEY_F10},
	{0x78,SPECIAL_KEY_F11},
	{0x07,SPECIAL_KEY_F12},
	{0,0}	
};

#elif (KB_TYPE==KB_USB)
//< nothing
#endif

#endif /* PS2_KB_SCANCODES_H_ */