/*
 * lcd_display.c
 *
 * Created: 16/09/13 9:08:01 PM
 *  Author: Pinkesh
 */ 
#include <avr/pgmspace.h>
#include "lcd_display.h"
#include "lcd_mega.h"
#include "utilities.h"



/**** Extern variable ****/
//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
//////////////////////////
/*************************/



/***** Function Definition *****/
void clear_lcd_data(unsigned char line)
{
	unsigned char i;

	//< only line-1
	if(line==LINE1)
	{
		for(i=0;i<LCD_LEN;i++)
		{
			lcd_Line1_data[i] = ' ';
		}
		lcd_Line1_data[LCD_LEN]='\0';
	}
	//< only line-2
	else if(line==LINE2)
	{
		for(i=0;i<LCD_LEN;i++)
		{
			lcd_Line2_data[i] = ' ';
		}
		lcd_Line2_data[LCD_LEN]='\0';
	}
	//< both line
	else
	{
		for(i=0;i<LCD_LEN;i++)
		{
			lcd_Line1_data[i] = ' ';
			lcd_Line2_data[i] = ' ';
		}
		lcd_Line1_data[LCD_LEN]='\0';
		lcd_Line2_data[LCD_LEN]='\0';
	}
}

void set_string_in_lcd_data(unsigned char line,char *str,unsigned char from,unsigned char to)
{
	unsigned char i;
	
	if(to==0 || to>=LCD_LEN)
	{
		to=(LCD_LEN-1);
	}
	
	if(line==LINE1)
	{
		for(i=from;i<=to;i++)
		{
			if(str[i-from]!='\0')
			{
				lcd_Line1_data[i] = str[i-from];
			}
			else
			{
				lcd_Line1_data[i] = '\0';
				return;
			}
		}
	}
	else
	{
		for(i=from;i<=to;i++)
		{
			if(str[i-from]!='\0')
			{
				lcd_Line2_data[i] = str[i-from];
			}
			else
			{
				lcd_Line2_data[i] = '\0';
				return;
			}
		}
	}
}

void set_string_in_lcd_dataF(unsigned char line,const char *str,unsigned char from,unsigned char to)
{
	unsigned char i;
	
	if(to==0 || to>=LCD_LEN)
	{
		to=(LCD_LEN-1);
	}
	
	if(line==LINE1)
	{
		for(i=from;i<=to;i++)
		{
			if(pgm_read_byte(&str[i-from])!='\0')
			{
				lcd_Line1_data[i] = pgm_read_byte(&str[i-from]);
			}
			else
			{
				lcd_Line1_data[i] = '\0';
				return;
			}
		}
	}
	else
	{
		for(i=from;i<=to;i++)
		{
			if(pgm_read_byte(&str[i-from])!='\0')
			{
				lcd_Line2_data[i] = pgm_read_byte(&str[i-from]);
			}
			else
			{
				lcd_Line2_data[i] = '\0';
				return;
			}
		}
	}
}

void lcd_normal_display()
{
	lcd_mega_gotoxy1(0);
	lcd_mega_StringF(PSTR("Weight:             "));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("                    "));
}
/*******************************/