/*
 * password_strings.h
 *
 * Created: 18/11/13 8:11:25 PM
 *  Author: Pinkesh
 */ 


#ifndef PASSWORD_STRINGS_H_
#define PASSWORD_STRINGS_H_


#define LEN_STRING_PASSWORD	5

////////////////////////////////////////////
//< Macros used by get_string_from_user()///
#define RETURN_CAUSE_ENTER_WITH_CHANGE		1
#define RETURN_CAUSE_ENTER_WITHOUT_CHANGE	2
#define RETURN_CAUSE_ESC					3
#define RETURN_CAUSE_ENTER					4
#define USE_NUMERIC			(0b00000001)
#define USE_CHARACTERS		(0b00000010)
#define USE_SPACE			(0b00000100)
#define USE_ARROW_KEY		(0b00001000)
#define USE_DILIT			(0b00010000)
#define USE_ENTER_ESC		(0b00000000)
#define USE_ALL_KEY			(USE_NUMERIC|USE_CHARACTERS|USE_SPACE)
#define STORE_ALL_DATA		1
#define STORE_UPTO_POINTER	2
#define NO_MATCH_FOUND      40 
////////////////////////////////////////////


#define __STARTUP_HEADER__				(PSTR("STRHD"))
#define __PRINTER_HEADER_FOOTER__		(PSTR("HFSTP"))
#define __SIGNATURE_LINES__             (PSTR("CHNTC"))
#define __TICKET_HEADER_ENTRY__         (PSTR("STSBT"))
#define __CODE_SYSTEM_PERMS__           (PSTR("PERM2"))
#define __CODE1_INPUT_SYSTEM__          (PSTR("CODE1"))
#define __CODE2_INPUT_SYSTEM__          (PSTR("CODE2"))
#define __CODE3_INPUT_SYSTEM__          (PSTR("CODE3"))
#define __CODE4_INPUT_SYSTEM__          (PSTR("CODE4"))
#define __CODE1_RESET_SYSTEM__			(PSTR("RSTC1"))
#define __CODE2_RESET_SYSTEM__			(PSTR("RSTC2"))
#define __CODE3_RESET_SYSTEM__			(PSTR("RSTC3"))
#define __CODE4_RESET_SYSTEM__			(PSTR("RSTC4"))
#define __CODE1_PRINT_ALL__ 			(PSTR("PRNT1"))
#define __CODE2_PRINT_ALL__ 			(PSTR("PRNT2"))
#define __CODE3_PRINT_ALL__ 			(PSTR("PRNT3"))
#define __CODE4_PRINT_ALL__ 			(PSTR("PRNT4"))
#define __RST_CLEAR_SYSTEM__			(PSTR("RSTMM"))
#define __LOGIN_SYSTEM__                (PSTR("OPTRL"))
#define __CLR_OPERATOR_MEM__            (PSTR("OPTRC"))
#define __CUSTOM_TKT_SETTING__			(PSTR("DPPTS"))

/**
* this function is used to get string 
* from user, string can be password,
* username, etc.
* \param	str		char buffer where to
*					store user inputed string
*					must have space to store
*					string with null char
* \param	from	lcd second line start position
* \param	cur	lcd second line current cursor position
* \param	to		lcd second line stop position
* \param	key_flag	has value USE_ macros
* \param	store_flag	has value STORE_ macros
* \return			1 = return cause is ENTER key press
*					2 = return cause is ESC key press
*/
unsigned char get_string_from_user(char* str,unsigned char from,unsigned char cur,unsigned char to,unsigned char key_flag,unsigned char store_flag);

// Same Function parameters to get password from user
unsigned char get_password_from_user(char* str,unsigned char from,unsigned char cur,unsigned char to,unsigned char key_flag,unsigned char store_flag);

/**
* this function is used to get string
* from user, string can be password,
* username, etc.
* \param	chr		char pointer where to
*					store user inputed char
* \param	chr_str	null terminated string contains
*					valid char input from user
* \param	default_chr	default character shown in lcd
* \param	cur			lcd cursor position
* \param	cur_line	lcd cursor line
* \return			1 = return cause is ENTER key press
*					2 = return cause is ESC key press
*/
unsigned char get_character_from_user_P(char* chr,const char* chr_str_P,char default_chr,unsigned char cur,unsigned char cur_line);

/**
* this function is used to get enter/esc key press
* \param	none
* \return	1 = return cause is ENTER key press
*			2 = return cause is ESC key press
*/
unsigned char wait_for_enter();

#endif /* PASSWORD_STRINGS_H_ */