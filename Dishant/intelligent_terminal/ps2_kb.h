/*
 * ps2_kb.h
 *
 * Created: 11/08/13 1:21:29 PM
 *  Author: Pinkesh
 */ 


#ifndef PS2_KB_H_
#define PS2_KB_H_

#define KB_PS2		0
#define KB_USB		1
#define KB_TYPE				KB_PS2
#define PS2_KB_BUFF_SIZE	8


/////////////////////////
//< ps2 key board related
#define SPECIAL_KEY	0
#define NORMAL_KEY	1
/////////////////////////

typedef struct 
{
	unsigned char val;
	unsigned char type;
}PS2_KEY;

enum special_key
{
	SPECIAL_KEY_IDENTIFIER	= 0x80,
	SPECIAL_KEY_ENTER		= 0x81,
	SPECIAL_KEY_BACKSPACE	= 0x82,
	SPECIAL_KEY_ALT			= 0x83,
	SPECIAL_KEY_ESC			= 0x84,
	SPECIAL_KEY_ARROW_UP	= 0x85,
	SPECIAL_KEY_ARROW_DOWN	= 0x86,
	SPECIAL_KEY_ARROW_LEFT	= 0x87,
	SPECIAL_KEY_ARROW_RIGHT	= 0x88,
	SPECIAL_KEY_F1			= 0x89,
	SPECIAL_KEY_F2			= 0x8A,
	SPECIAL_KEY_F3			= 0x8B,
	SPECIAL_KEY_F4			= 0x8C,
	SPECIAL_KEY_F5			= 0x8D,
	SPECIAL_KEY_F6			= 0x8E,
	SPECIAL_KEY_F7			= 0x8F,
	SPECIAL_KEY_F8			= 0x90,
	SPECIAL_KEY_F9			= 0x91,
	SPECIAL_KEY_F10			= 0x92,
	SPECIAL_KEY_F11			= 0x93,
	SPECIAL_KEY_F12			= 0x94,
	SPECIAL_KEY_DELETE		= 0X95
};


void ps2_kb_Initialize(void);
//< returns 1 if successful else 0
unsigned char ps2_kb_getkey(PS2_KEY *ps2_key);
unsigned char ps2_kb_available_key();
void ps2_kb_flush_keybuffer();
void ps2_kb_start();
void ps2_kb_stop();

#endif /* PS2_KB_H_ */