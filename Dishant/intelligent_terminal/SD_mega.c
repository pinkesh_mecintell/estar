/*
 * SD_mega.c
 *
 * Created: 6/23/2012 11:50:27 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include "utilities.h"
#include "spi_mega.h"
#include "SD_mega_config.h"
#include "SD_mega.h"
#include "lcd_mega.h"


SD_mega_CardInfo SD_mega_Initialize(void)
{
	SD_mega_CardInfo cardinfo;
	uint8_t i, response,pattern,voltage_check,timeout_flag,temp_retry,retry=0;

	//< CS-pin of microcontroller initialize
	SET(SD_mega_CSDDR,SD_mega_CSPin); //< make output
	SET(SD_mega_CSPORT,SD_mega_CSPin); //< make high
	
	
	
	//< ///////////// Required Clock Pulses ////////////////
	SD_mega_CS_HIGH;
	for(i=0;i<10;i++)
	{
		//< 80-clock(74 required) pulses sent before sending the first command
		spi_mega_TransmittData(0xff);
	}	
	//< /////////////////////////////////////////////////////
	
	
	
	//< //////// SETs the SD CARD into SPI mode /////////////
	SD_mega_CS_LOW;
	do
	{
		//< send CMD0 = 40 00 00 00 00 95(hex) - 'reset & go idle' command with CS=low 
		//< responce = R1(8-bit) with value = 0x01
		response = SD_mega_SendCommand(GO_IDLE_STATE, 0);
		retry++;
		if(retry>0x20)
		{
			cardinfo.success_init = 0;
			cardinfo.error_type = SD_mega_NOCARD;
   			return cardinfo;   //time out, card not detected
		}			   
	} while(response != 0x01);
	SD_mega_CS_HIGH;
	//< /////////////////////////////////////////////////////

	

	//< ///// Wait for some times with running clock ////////
	spi_mega_TransmittData(0xff);
	spi_mega_TransmittData(0xff);
	//< /////////////////////////////////////////////////////

	
	
	//< ///// CHECK SD cards Interface Conditions /////////////////////
	//< ///// Using CMD8 = 48 00 00 01 AA 87(hex) - SEND_IF_COND //////
	//< ///// Response = R7(5 Bytes)			  /////////////////////
	retry = 0;
	cardinfo.version = SD_mega_SDVERSION_2; //< default set to SD compliance with ver2.x; 
					//< this may change after checking the next command
	do
	{
		//< Check power supply status, mandatory for SDHC card
				
		SD_mega_CS_LOW;
		//< CMD-8
		spi_mega_TransmittData(0x48);
		spi_mega_TransmittData(0x00);
		spi_mega_TransmittData(0x00);
		spi_mega_TransmittData(0x01);
		spi_mega_TransmittData(0xAA);
		spi_mega_TransmittData(0x87);
		
		temp_retry=0;
		timeout_flag=0;
		while((response = spi_mega_TransmittData(0xFF)) == 0xff) //wait response
		{
			if(temp_retry++ > 0xfe)
			{
				timeout_flag = 1;
				break;
			}				
		}
		
		if(!timeout_flag)
		{
			spi_mega_TransmittData(0xFF); //< unused
			spi_mega_TransmittData(0xFF); //< unused
			voltage_check = spi_mega_TransmittData(0xFF);
			pattern = spi_mega_TransmittData(0xFF);
		
			if(voltage_check != 0x01)
			{
				cardinfo.success_init = 0;
				cardinfo.error_type = SD_mega_VOTAGEMISMATCH;
				return cardinfo;
			}
			
			if(pattern==0xAA && response==0x01)
			{
				cardinfo.version = SD_mega_SDVERSION_2;
				break;
			}
		}					
		retry++;
		if(retry>0xfe) 
		{
			cardinfo.version = SD_mega_SDVERSION_1;
			break;
		} //< time out
	}while(1);
/////////////////////////////////////////////////////////////



	//< ////// Start initializing process of SD Card /////////
	//< ////// By sending ACMD41 - SD_SEND_OP_COND	 /////////
	//< ////// respose R1 (8-bit)					 /////////
	retry = 0;
	do
	{
		SD_mega_SendCommand(APP_CMD,0); //< CMD55, must be sent before sending any ACMD command
		response = SD_mega_SendCommand(SD_SEND_OP_COND,0x40000000); //< ACMD41 with HCS = 1 means host support SDHC/SDXC

		retry++;
		if(retry>0xfe) 
		{
			cardinfo.success_init = 0;  //< time out, card initialization failed
			cardinfo.error_type = SD_mega_INITERROR;
			return cardinfo;
		} 
	}while(response != 0x00);
	//////// End of initialization - card is now out of IDIAL MODE ///////
	//////////////////////////////////////////////////////////////////////



	//< ////// Determine the type of SD card i.e. SD,SDHC/SDXC ////////
	//< ////// using CMD58 - READ_OCR						   ////////
	//< ////// Response R3 (5 Bytes)						   ////////
	cardinfo.type = SD_mega_SDSTANDARD;
	if(cardinfo.version == SD_mega_SDVERSION_2)
	{ 	
		response = SD_mega_SendCommand(READ_OCR,0);
		if(CHK(response,6))
		{
			cardinfo.type = SD_mega_SDHC_SDXC;
		}		
	}
	///////////////////////////////////////////////////////////////////
	
	cardinfo.success_init = 1;
	return cardinfo; //successful return
}





uint8_t SD_mega_SendCommand(uint8_t cmd, unsigned long arg)
{
	unsigned char response, retry=0;	   

	//< all commands are 6-byte long
	//< their contents are determine by cmd and arg parameters
	SD_mega_CS_LOW;
	spi_mega_TransmittData(cmd | 0x40); //send command, first two bits always '01'
	spi_mega_TransmittData(arg>>24);
	spi_mega_TransmittData(arg>>16);
	spi_mega_TransmittData(arg>>8);
	spi_mega_TransmittData(arg);

	//< it is compulsory to send correct CRC for CMD8 (CRC=0x87) & CMD0 (CRC=0x95)
	if(cmd == 8)
	{
		spi_mega_TransmittData(0x87);
	}
	//< for CMD0 CRC = 0x95
	else 
	{
		spi_mega_TransmittData(0x95); //< for remaining commands, CRC is ignored in SPI mode
									  //< sothat this CRC byte is CMD0 and all other commands 
									  //< accepts CMD8
	}
	 

	while((response = spi_mega_TransmittData(0xFF)) == 0xff) //< wait response
	{
		if(retry++ > 0xfe)
		{
			break; //< time out error
		}			
	}
		
	if(response==0 && cmd==READ_OCR)  //checking response-R3 of CMD58
	{
		response = spi_mega_TransmittData(0xFF) & 0x40;//first byte of the OCR register (bit 31:24)
	
		spi_mega_TransmittData(0xFF); //remaining 3 bytes of the OCR register are ignored here
		spi_mega_TransmittData(0xFF); //one can use these bytes to check power supply limits of SD
		spi_mega_TransmittData(0xFF); 
	}

	spi_mega_TransmittData(0xFF); //extra 8 CLK
	SD_mega_CS_HIGH;

	return response; //return state
}





SD_mega_CommError SD_mega_ReadSingleBlock(uint8_t* data_buf,uint32_t block_add)
{
	SD_mega_CommError commError;
	uint8_t response;
	uint16_t i;
	
	//< convert block_add into byte address by multiplying it with 512
//	block_add = (block_add << 9);
	
	
	//< send CMD16-read single Block command
	//< Response R1
	response = SD_mega_SendCommand(READ_SINGLE_BLOCK,block_add);
	
	
	//< check command response, value = 0x00 then OK
	if(response != 0x00)
	{
		commError.error_occure=1;
		commError.commandExecution=1;
		commError.timeout=0;
		return commError; 
	}
	
	
	//< if No error occured then wait for start block token from SD card
	//< value of start block token = 0xfe (0x11111110)
	SD_mega_CS_LOW;
	i=0;
	while(spi_mega_TransmittData(0xFF) != 0xfe)
	{
		//< return if time-out occured
		if(i++ > 0xfffe)
		{
			commError.error_occure=1;
			commError.commandExecution=0;
			commError.timeout=1;
			SD_mega_CS_HIGH;
			return commError;
		} 
	}
	
	
	//< if start block token from SD card is successfully received then
	//< read SD_mega_BLOCK_LEN = 512 bytes int0 data_buf
	for(i=0; i<SD_mega_BLOCK_LEN; i++) //< read 512 bytes
	{
		data_buf[i] = spi_mega_TransmittData(0xFF);
	}	


	//< receive incoming CRC (16-bits), CRC is ignored here
	spi_mega_TransmittData(0xFF);
	spi_mega_TransmittData(0xFF);


	//< send extra 8 clock pulses
	spi_mega_TransmittData(0xFF);
	SD_mega_CS_HIGH;

	commError.error_occure=0;
	return commError;
}






SD_mega_CommError SD_mega_WriteSingleBlock(const uint8_t* data_buf,uint32_t block_add)
{
	SD_mega_CommError commError;
	uint8_t response;
	uint16_t i;

	//< convert block_add into byte address by multiplying it with 512
//	block_add = (block_add << 9);
	
	
	//< send CMD24 - write a Block command
	//< Response R1
	response = SD_mega_SendCommand(WRITE_SINGLE_BLOCK, block_add);
  
	//< check command response, value = 0x00 then OK
	if(response != 0x00)
	{
		commError.error_occure=1;
		commError.commandExecution=1;
		commError.timeout=0;
		commError.dataRejected = 0;
		return commError;
	}


	//< if no error occured in command response then
	//< send SD_mega_BLOCK_LEN=512 dataBytes
	SD_mega_CS_LOW;
	//< Send start block token 0xfe (0x11111110)
	spi_mega_TransmittData(0xfe);
	//send SD_mega_BLOCK_LEN=512 bytes data
	for(i=0; i<SD_mega_BLOCK_LEN; i++)
	{
		spi_mega_TransmittData(data_buf[i]);
	}
	//< transmit dummy CRC (16-bit), CRC is ignored in SPI mode by default
	spi_mega_TransmittData(0xff);
	spi_mega_TransmittData(0xff);


	//< Receive DATA_RESPONSE token
	response = spi_mega_TransmittData(0xff);
	//< data_response format is 0xXXX0AAA1
	//< \n when AAA='010'	-	data accepted
	//< \n when AAA='101'	-	data rejected due to CRC error
	//<	\n when AAA='110'	-	data rejected due to write error
	if( (response & 0x1f) != 0x05) 
	{                              
		SD_mega_CS_HIGH;              
		commError.error_occure = 1;
		commError.commandExecution = 0;
		commError.timeout = 0;
		commError.dataRejected = 1;
		return commError;
	}


	//< if data accepted by the SD card then
	//< wait for SD card to complete writing and get idle
	//< by checking busy flag
	//< \n busy flag = 0x00 when SD card is busy in programming the data into memory
	//< \n if card is not busy and ready to accepts commands the busy flag is nonzero
	i=0;
	while(!spi_mega_TransmittData(0xFF))
	{
		if(i++ > 0xfffe)
		{
			SD_mega_CS_HIGH;
			commError.error_occure = 1;
			commError.commandExecution = 0;
			commError.timeout = 1;
			commError.dataRejected = 0;
			return commError;
		}
	}
	
	
	//< send extra 8 clock pulses
	spi_mega_TransmittData(0xFF);
	SD_mega_CS_HIGH;
	
	commError.error_occure = 0;
	return commError;
}