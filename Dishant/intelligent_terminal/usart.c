/*
 * usart.c
 *
 * Created: 11/10/13 6:04:25 PM
 *  Author: Pinkesh
 */ 
#include <avr/io.h>
#include <util/delay.h>
#include "usart.h"
#include "utilities.h"


void usart0_Initialize()
{
	//< Set Baud rate - 9600 bps
	//< F_CPU = 14.7456 MHz
	UBRR0L=95;

	//< Set Frame Format
	//< No Parity-1 StopBit-char size 8-bit
	UCSR0C=(1<<UCSZ01)|(1<<UCSZ00);
	
	//< Enable The recevier and transmitter
	UCSR0B=(1<<TXEN0)|(1<<RXEN0);
}

unsigned char usart0_ReadData()
{
	unsigned char x;
	
	while (!CHK(UCSR0A,RXC0));
	x=UDR0;
	CLR(UCSR0A,RXC0);
	return x;
}

void usart0_WriteData(unsigned char data)
{
	while(!CHK(UCSR0A,UDRE0));
	UDR0=data;
	while(!CHK(UCSR0A,TXC0));
	CLR(UCSR0A,TXC0);
}
