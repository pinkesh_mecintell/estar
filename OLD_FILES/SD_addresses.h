/*
 * SD_addresses.h
 *
 * Created: 05/10/13 12:28:16 PM
 *  Author: Pinkesh
 */ 

#ifndef SD_ADDRESSES_H_
#define SD_ADDRESSES_H_


#define ADD_RST_NO				7000000 //< address
#define RST_NO_ADD_LSB			0
#define RST_NO_ADD_MSB			1
#define RST_NO_ADD_MAGIC_BYTE	511
#define RST_NO_LSB				0
#define RST_NO_MSB				1
#define RST_NO_WRITE_TIMES_LSB	2
#define RST_NO_WRITE_TIMES_MSB	3
#define ADD_RST_NO_COPY_OFFSET	2000


//< comment has (length(in byte))/(default value)/(all values) format
#define RST_ENTRY_TYPE	0	//< (1)/(non-zero)/(0/nonzero) 0=Occupied, non-zero=not occupied
#define RST_VEHICLE_NO	1	//< (12)/()/(printable characters)
#define RST_SUPPLIER	13	//< (12)/()/(printable characters)
#define RST_MATERIAL	25	//< (12)/()/(printable characters)
#define RST_ADDRESS		37	//< (12)/()/(printable characters)
#define RST_SOURCE		49	//< (12)/()/(printable characters)
#define RST_CHARGE1_IS_PRESENT		61	//< (1)/(nonzero)/(0/nonzero) 0=present,nonzero=not present
#define RST_CHARGE1					62	//< (5)/()/(xxxxx)
#define RST_CHARGE2_IS_PRESENT		67	//< (1)/(nonzero)/(0/nonzero)
#define RST_CHARGE2					68	//< (5)/()/(xxxxx)
#define RST_TOTALCHARGE_IS_PRESENT	73	//< (1)/(nonzero)/(0/nonzero)
#define RST_TOTALCHARGE				74	//< (5)/()/(xxxxx)
#define RST_FIRSTENTRYTYPE			79	//< (1)/()/('G','T')
#define RST_GROSSWEIGHT_IS_PRESENT	80	//< (1)/(nonzero)/(0/nonzero)
#define RST_GROSSWEIGHT				81	//< (6)/()/(xxxxxx) LSB first-MSB last
#define RST_TAREWEIGHT_IS_PRESENT	87	//< (1)/(nonzero)/(0/nonzero)
#define RST_TAREWEIGHT				88	//< (6)/()/(xxxxxx)
#define RST_NETWEIGHT_IS_PRESENT	94	//< (1)/(nonzero)/(0/nonzero)
#define RST_NETWEIGHT				95	//< (6)/()/(xxxxxx)
#define RST_GROSS_DATE				101	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_GROSS_TIME				107	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_TARE_DATE				111	//< (6)/()/(xxxxxx) LSB=Date  MSB=Year
#define RST_TARE_TIME				117	//< (4)/()/(xxxx) LSB=Hour  MSB=Minute
#define RST_BOTHENTRY_COMPLETED		121 //< (1)/(non-zero)/(0/nonaero)


#endif /* SD_ADDRESSES_H_ */