/*
 * EEPROM_addresses.h
 *
 * Created: 18/09/13 6:59:16 PM
 *  Author: Pinkesh
 */ 


#ifndef EEPROM_ADDRESSES_H_
#define EEPROM_ADDRESSES_H_

/**
* all the bellow field has two components
* first #define is 'base address'
* and all subsequent #define is member
* parameters of field
* member define value is offset to base value
* its comment has (length(in byte))/(default value)/(all values) format
*/

//< first copy at 0
//< second copy at 0+copy_offset
#define COPY_OFFSET	2000
//< code 1 address - code 0 address (i.e. address difference between two successive code)
#define CODE_OFFSET	75


////////////////////
//< Field: PASSWORD
#define ADD_PASSWORD	1982//< base address(0x07BE)
#define PASSWORD_LEN	1	//< (1)/(1)/(0-16)
#define PASSWORD_DATA	2	//< (16)/('1')/('xxxxxxxxxxxxxxxx')
////////////////////

////////////////////
//< Field: ALL_L
#define ADD_ALT_L						0//< base address
#define ALT_L_HEADER_POSITION			1//< (1)/('T')/('N','T','B')
#define ALT_L_CHARGE_TYPE				2//< (1)/('F')/('N','F','S','B')
#define ALT_L_LINE_FEED					3//< (1)/(1)/(0-20)
#define ALT_L_FIRST_SLIP_COPIES			4//< (1)/(1)/(0-9)
#define ALT_L_SECOND_SLIP_COPIES		5//< (1)/(1)/(0-9)
#define ALT_L_USE_TIME_DATE_IN_PRINT	6//< (1)/('Y')/('Y','N')
#define ALT_L_ENABLE_CODE_SYSTEM		7//< (1)/('N')/('Y','N')
///////////////////////

//////////////////////
//< Field: HEADER_LINE1
#define ADD_HEADER_LINE1	8//< base address
#define HEADER_LINE1_LEN	1//< (1)/(0)/(0-20)
#define HEADER_LINE1_DATA	2//< (40)/(none)/(any printable character uto 20 char)
///////////////////////

//////////////////////
//< Field: HEADER_LINE2
#define ADD_HEADER_LINE2	50//< base address
#define HEADER_LINE2_LEN	1//< (1)/(0)/(0-20)
#define HEADER_LINE2_DATA	2//< (40)/(none)/(any printable character uto 20 char)
///////////////////////

//////////////////////
//< Field: HEADER_LINE3
#define ADD_HEADER_LINE3	92//< base address
#define HEADER_LINE3_LEN	1//< (1)/(0)/(0-20)
#define HEADER_LINE3_DATA	2//< (40)/(none)/(any printable character uto 20 char)
///////////////////////

//////////////////////
//< Field: NAME_CHANGE_SUPPLIER
#define ADD_NAME_CHANGE_SUPPLIER		134	//< base address
#define NAME_CHANGE_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define NAME_CHANGE_SUPPLIER_DATA		2	//< (8)/(none)/(any printable char,not start with space)
///////////////////////

//////////////////////
//< Field: NAME_CHANGE_SOURCE__
#define ADD_NAME_CHANGE_SOURCE__		144	//< base address
#define NAME_CHANGE_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define NAME_CHANGE_SOURCE___DATA		2	//< (8)/(none)/(any printable char,not start with space)
///////////////////////

//////////////////////
//< Field: NAME_CHANGE_MATERIAL
#define ADD_NAME_CHANGE_MATERIAL		154	//< base address
#define NAME_CHANGE_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define NAME_CHANGE_MATERIAL_DATA		2	//< (8)/(none)/(any printable char,not start with space)
///////////////////////

///////////////////////
//< Field: CODE_0_SUPPLIER
#define ADD_CODE_0_SUPPLIER			164	//< base address
#define CODE_0_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_0_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_0_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_0_SOURCE__
#define ADD_CODE_0_SOURCE__			179	//< base address
#define CODE_0_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_0_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_0_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_0_MATERIAL
#define ADD_CODE_0_MATERIAL			194	//< base address
#define CODE_0_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_0_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_0_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_0_VEHICLE_NO
#define ADD_CODE_0_VEHICLE_NO			209	//< base address
#define CODE_0_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_0_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_0_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_0_ADDRESS
#define ADD_CODE_0_ADDRESS			224	//< base address
#define CODE_0_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_0_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_0_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_1_SUPPLIER
#define ADD_CODE_1_SUPPLIER			239	//< base address
#define CODE_1_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_1_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_1_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_1_SOURCE__
#define ADD_CODE_1_SOURCE__			254	//< base address
#define CODE_1_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_1_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_1_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_1_MATERIAL
#define ADD_CODE_1_MATERIAL			269	//< base address
#define CODE_1_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_1_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_1_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_1_VEHICLE_NO
#define ADD_CODE_1_VEHICLE_NO			284	//< base address
#define CODE_1_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_1_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_1_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_1_ADDRESS
#define ADD_CODE_1_ADDRESS			299	//< base address
#define CODE_1_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_1_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_1_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_2_SUPPLIER
#define ADD_CODE_2_SUPPLIER			314	//< base address
#define CODE_2_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_2_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_2_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_2_SOURCE__
#define ADD_CODE_2_SOURCE__			329	//< base address
#define CODE_2_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_2_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_2_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_2_MATERIAL
#define ADD_CODE_2_MATERIAL			344	//< base address
#define CODE_2_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_2_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_2_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_2_VEHICLE_NO
#define ADD_CODE_2_VEHICLE_NO			359	//< base address
#define CODE_2_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_2_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_2_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_2_ADDRESS
#define ADD_CODE_2_ADDRESS			374	//< base address
#define CODE_2_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_2_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_2_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_3_SUPPLIER
#define ADD_CODE_3_SUPPLIER			389	//< base address
#define CODE_3_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_3_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_3_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_3_SOURCE__
#define ADD_CODE_3_SOURCE__			404	//< base address
#define CODE_3_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_3_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_3_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_3_MATERIAL
#define ADD_CODE_3_MATERIAL			419	//< base address
#define CODE_3_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_3_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_3_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_3_VEHICLE_NO
#define ADD_CODE_3_VEHICLE_NO			434	//< base address
#define CODE_3_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_3_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_3_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_3_ADDRESS
#define ADD_CODE_3_ADDRESS			449	//< base address
#define CODE_3_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_3_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_3_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_4_SUPPLIER
#define ADD_CODE_4_SUPPLIER			464	//< base address
#define CODE_4_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_4_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_4_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_4_SOURCE__
#define ADD_CODE_4_SOURCE__			479	//< base address
#define CODE_4_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_4_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_4_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_4_MATERIAL
#define ADD_CODE_4_MATERIAL			494	//< base address
#define CODE_4_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_4_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_4_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_4_VEHICLE_NO
#define ADD_CODE_4_VEHICLE_NO			509	//< base address
#define CODE_4_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_4_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_4_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_4_ADDRESS
#define ADD_CODE_4_ADDRESS			524	//< base address
#define CODE_4_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_4_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_4_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_5_SUPPLIER
#define ADD_CODE_5_SUPPLIER			539	//< base address
#define CODE_5_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_5_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_5_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_5_SOURCE__
#define ADD_CODE_5_SOURCE__			554	//< base address
#define CODE_5_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_5_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_5_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_5_MATERIAL
#define ADD_CODE5_5_MATERIAL			569	//< base address
#define CODE_5_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_5_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_5_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_5_VEHICLE_NO
#define ADD_CODE_5_VEHICLE_NO			584	//< base address
#define CODE_5_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_5_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_5_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_5_ADDRESS
#define ADD_CODE_5_ADDRESS			599	//< base address
#define CODE_5_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_5_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_5_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_6_SUPPLIER
#define ADD_CODE_6_SUPPLIER			614	//< base address
#define CODE_6_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_6_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_6_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_6_SOURCE__
#define ADD_CODE_6_SOURCE__			629	//< base address
#define CODE_6_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_6_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_6_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_6_MATERIAL
#define ADD_CODE_6_MATERIAL			644	//< base address
#define CODE_6_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_6_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_6_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_6_VEHICLE_NO
#define ADD_CODE_6_VEHICLE_NO			659	//< base address
#define CODE_6_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_6_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_6_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_6_ADDRESS
#define ADD_CODE_6_ADDRESS			674	//< base address
#define CODE_6_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_6_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_6_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_7_SUPPLIER
#define ADD_CODE_7_SUPPLIER			689	//< base address
#define CODE_7_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_7_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_7_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_7_SOURCE__
#define ADD_CODE_7_SOURCE__			704	//< base address
#define CODE_7_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_7_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_7_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_7_MATERIAL
#define ADD_CODE_7_MATERIAL			719	//< base address
#define CODE_7_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_7_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_7_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_7_VEHICLE_NO
#define ADD_CODE_7_VEHICLE_NO			734	//< base address
#define CODE_7_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_7_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_7_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_7_ADDRESS
#define ADD_CODE_7_ADDRESS			749	//< base address
#define CODE_7_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_7_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_7_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_8_SUPPLIER
#define ADD_CODE_8_SUPPLIER			764	//< base address
#define CODE_8_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_8_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_8_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_8_SOURCE__
#define ADD_CODE_8_SOURCE__			779	//< base address
#define CODE_8_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_8_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_8_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_8_MATERIAL
#define ADD_CODE_8_MATERIAL			794	//< base address
#define CODE_8_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_8_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_8_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_8_VEHICLE_NO
#define ADD_CODE_8_VEHICLE_NO			809	//< base address
#define CODE_8_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_8_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_8_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_8_ADDRESS
#define ADD_CODE_8_ADDRESS			824	//< base address
#define CODE_8_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_8_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_8_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_9_SUPPLIER
#define ADD_CODE_9_SUPPLIER			839	//< base address
#define CODE_9_SUPPLIER_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_9_SUPPLIER_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_9_SUPPLIER_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_9_SOURCE__
#define ADD_CODE_9_SOURCE__			854	//< base address
#define CODE_9_SOURCE___IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_9_SOURCE___LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_9_SOURCE___DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_9_MATERIAL
#define ADD_CODE_9_MATERIAL			869	//< base address
#define CODE_9_MATERIAL_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_9_MATERIAL_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_9_MATERIAL_DATA		3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_9_VEHICLE_NO
#define ADD_CODE_9_VEHICLE_NO			884	//< base address
#define CODE_9_VEHICLE_NO_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_9_VEHICLE_NO_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_9_VEHICLE_NO_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

///////////////////////
//< Field: CODE_9_ADDRESS
#define ADD_CODE_9_ADDRESS			899	//< base address
#define CODE_9_ADDRESS_IS_PRESENT	1	//< (1)/(non_zero)/(0/non_zero)
#define CODE_9_ADDRESS_LEN			2	//< (1)/(none,0)/(0-12)
#define CODE_9_ADDRESS_DATA			3	//< (12)/(none)/(any printable char)
///////////////////////

////////////////////////
//< Field: WEIGHT_CONSTANTS
#define ADD_WEIGHT_CONSTANTS				914 //< base address
#define WEIGHT_CONSTANTS_UNIT_WEIGHT_VALUE	1	//< (1)/()/(5 or 10)-unsigned char
#define WEIGHT_CONSTANTS_UNIT_WEIGHT_COUNT	2	//< (4)/()/()-unsigned long
#define WEIGHT_CONSTANTS_ADC_MAX_COUNT		6	//< (4)/()/()-signed long
#define WEIGHT_CONSTANTS_ADC_MIN_COUNT		10	//< (4)/()/()-signed long
#define WEIGHT_CONSTANTS_MAX_WEIGHT_VALUE	14	//< (4)/()/()-unsigned long
/////////////////////////

#endif /* EEPROM_ADDRESSES_H_ */