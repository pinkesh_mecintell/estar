/*
 * function_keys.h
 *
 * Created: 05/10/13 12:17:19 PM
 *  Author: Pinkesh
 */ 


#ifndef FUNCTION_KEYS_H_
#define FUNCTION_KEYS_H_

void RST_check();
unsigned int RST_read();
void RST_entry_print(unsigned char *_sd_block,unsigned int _rst_no);
void Report_entry_print(unsigned char *_sd_block,unsigned int _rst_no);
void RST_write(unsigned int _rst_no);
void function_F1();
void function_F2();
//< flag should be - 1 for viewing rst entry using F3
//< flag should be - 0 for viewing rst entry using function_F6()
//< returns	0-successful
//<			1-unsuccessful
unsigned char function_F3(unsigned char flag);
void function_F4();
void function_F5();
void function_F6();
void function_F7();
void function_F8();
void function_F9();
void function_F10();

#endif /* FUNCTION_KEYS_H_ */