/*
 * function_keys.c
 *
 * Created: 05/10/13 12:16:52 PM
 *  Author: Pinkesh
 */ 
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "lcd_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "ps2_kb.h"
#include "buzzer.h"
#include "utilities.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "printer.h"
#include "SD_addresses.h"
#include "SD_mega.h"
#include "setting_controls.h"
#include "function_keys.h"
#include "usart.h"

/***** Extern Variables *****/
//////////////////
//< sd card buffer
extern uint8_t SD_data_buf[];
//////////////////////////

//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////

/////////////////////////////
//< weight related
extern signed long int weight;
/////////////////////////////

///////////////////////////
//< RTC related
extern RTC_DATA RTC_data;
///////////////////////////

/****************************/


/***** local function ****/
static void remove_space_from_string(unsigned char *str);
/*************************/


void RST_check()
{
	unsigned int cur_rst_add;
	unsigned int cur_rst_no;
	unsigned int write_cycles;
	
	//< check MAGIC_BYTE
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	if(SD_data_buf[RST_NO_ADD_MAGIC_BYTE]!=0xAA)
	{
		SD_data_buf[RST_NO_ADD_LSB] = 1;
		SD_data_buf[RST_NO_ADD_MSB] = 0;
		SD_data_buf[RST_NO_ADD_MAGIC_BYTE]=0xAA;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO);
		
		//< write RST_NO=0 and WRITE_TIMES=0 at current location
		SD_data_buf[RST_NO_LSB]=0;
		SD_data_buf[RST_NO_MSB]=0;
		SD_data_buf[RST_NO_WRITE_TIMES_LSB]=0;
		SD_data_buf[RST_NO_WRITE_TIMES_MSB]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+1);
		//< also write at copy location
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+1);
		
		return;
	}
	
	cur_rst_add = SD_data_buf[RST_NO_ADD_MSB]*0xFF + SD_data_buf[RST_NO_ADD_LSB];
	//< read current RST_NO loaction
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	cur_rst_no = SD_data_buf[RST_NO_MSB]*0xFF + SD_data_buf[RST_NO_LSB];
	write_cycles = SD_data_buf[RST_NO_WRITE_TIMES_MSB]*0xFF + SD_data_buf[RST_NO_WRITE_TIMES_LSB];
	//< read copy location
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
	//< check current location and copy location
	//< has same values or not
	if(cur_rst_no!=(SD_data_buf[RST_NO_MSB]*0xFF + SD_data_buf[RST_NO_LSB]) || \
		write_cycles!=(SD_data_buf[RST_NO_WRITE_TIMES_MSB]*0xFF + SD_data_buf[RST_NO_WRITE_TIMES_LSB]))
	{
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	}
	
	write_cycles = SD_data_buf[RST_NO_WRITE_TIMES_MSB]*0xFF + SD_data_buf[RST_NO_WRITE_TIMES_LSB];
	if(write_cycles>50000)
	{
		//< increment RST_NO location
		cur_rst_add+=1;
		SD_data_buf[RST_NO_ADD_LSB] = cur_rst_add % 0xFF;
		SD_data_buf[RST_NO_ADD_MSB] = cur_rst_add / 0xFF;
		SD_data_buf[RST_NO_ADD_MAGIC_BYTE]=0xAA;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO);
		
		//< write RST_NO=0 and WRITE_TIMES=0 at current location
		SD_data_buf[RST_NO_LSB]=0;
		SD_data_buf[RST_NO_MSB]=0;
		SD_data_buf[RST_NO_WRITE_TIMES_LSB]=0;
		SD_data_buf[RST_NO_WRITE_TIMES_MSB]=0;
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
		//< also write at copy location
		SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
	}
}

unsigned int RST_read()
{
	unsigned int cur_rst_add;
	unsigned int cur_rst_no;
	
	//< read current rst_no address
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	cur_rst_add = SD_data_buf[RST_NO_ADD_MSB]*0xFF + SD_data_buf[RST_NO_ADD_LSB];
	
	//< read current rst_no
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	cur_rst_no = SD_data_buf[RST_NO_MSB]*0xFF + SD_data_buf[RST_NO_LSB];
	
	return cur_rst_no;
}

void RST_write(unsigned int _rst_no)
{
	unsigned int cur_rst_add;
	unsigned int write_cycle;
	
	//< read current rst_no address
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO);
	cur_rst_add = SD_data_buf[RST_NO_ADD_MSB]*0xFF + SD_data_buf[RST_NO_ADD_LSB];
	
	//< read current rst_no and write cycle
	SD_mega_ReadSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	write_cycle = SD_data_buf[RST_NO_WRITE_TIMES_MSB]*0xFF + SD_data_buf[RST_NO_WRITE_TIMES_LSB];
	
	//< write new rst_no and increment write cycle
	write_cycle+=1;
	SD_data_buf[RST_NO_LSB]= _rst_no % 0xFF;
	SD_data_buf[RST_NO_MSB]= _rst_no / 0xFF;
	SD_data_buf[RST_NO_WRITE_TIMES_LSB]= write_cycle % 0xFF;
	SD_data_buf[RST_NO_WRITE_TIMES_MSB]= write_cycle / 0xFF;
	SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+cur_rst_add);
	//< also write at copy location
	SD_mega_WriteSingleBlock(SD_data_buf,ADD_RST_NO+ADD_RST_NO_COPY_OFFSET+cur_rst_add);
}

void RST_entry_print(unsigned char *_sd_block,unsigned int _rst_no)
{
	unsigned char i;
	unsigned char temp;
	
	//< check if this related both entry completed or not
	//< if both entry completed
	if(!_sd_block[RST_BOTHENTRY_COMPLETED])
	{
		//< printing header
		//< check header position is top
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='T')
		{
			setting_print_header(0);
		}
		
		//< set font
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		
		//< RST No. printing
		printer_writeStringF(PSTR("RST NO      : "));
		temp=1;
		if((_rst_no/1000)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no/1000)+'0');
			temp=0;
		}
		if(((_rst_no%1000)/100)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%1000)/100)+'0');
			temp=0;
		}
		if(((_rst_no%100)/10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%100)/10)+'0');
			temp=0;
		}
		printer_writeData((_rst_no%10)+'0');
		
		//< 16-spaces
		for(i=0;i<16;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Vehicle No. Printing
		printer_writeStringF(PSTR("VEHICLE NO. : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
		}
		PRINTER_ENTER;
		
		//< Supplier
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SUPPLIER    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SUPPLIER+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Material
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("MATERIAL    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_MATERIAL+i]);
		}
		PRINTER_ENTER;
		
		//< Address
		printer_writeStringF(PSTR("ADDRESS     : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_ADDRESS+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Source
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SOURCE      : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SOURCE+i]);
		}
		PRINTER_ENTER;
		
		//< Putting line (------)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< gross weight
		printer_writeStringF(PSTR("GROSS   : "));
		
		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;
		
		//< check if gross weight presents
		if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));
		
		if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
		{
			//< Date
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("DATE:"));
			printer_writeData(_sd_block[RST_GROSS_DATE]);//<DD
			printer_writeData(_sd_block[RST_GROSS_DATE+1]);
			printer_writeData('/');
			printer_writeData(_sd_block[RST_GROSS_DATE+2]);//<MM
			printer_writeData(_sd_block[RST_GROSS_DATE+3]);
			printer_writeData('/');
			printer_writeData('2');//<YYYY
			printer_writeData('0');
			printer_writeData(_sd_block[RST_GROSS_DATE+4]);
			printer_writeData(_sd_block[RST_GROSS_DATE+5]);
			
			//< Time
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("TIME:"));
			printer_writeData(_sd_block[RST_GROSS_TIME]);//<HH
			printer_writeData(_sd_block[RST_GROSS_TIME+1]);
			printer_writeData(':');
			printer_writeData(_sd_block[RST_GROSS_TIME+2]);//<MM
			printer_writeData(_sd_block[RST_GROSS_TIME+3]);
		}
		PRINTER_ENTER;
		
		//< TARE weight
		printer_writeStringF(PSTR("TARE    : "));
		
		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;
		
		//< check if tare weight presents
		if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));
		
		if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
		{
			//< Date
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("DATE:"));
			printer_writeData(_sd_block[RST_TARE_DATE]);//<DD
			printer_writeData(_sd_block[RST_TARE_DATE+1]);
			printer_writeData('/');
			printer_writeData(_sd_block[RST_TARE_DATE+2]);//<MM
			printer_writeData(_sd_block[RST_TARE_DATE+3]);
			printer_writeData('/');
			printer_writeData('2');//<YYYY
			printer_writeData('0');
			printer_writeData(_sd_block[RST_TARE_DATE+4]);
			printer_writeData(_sd_block[RST_TARE_DATE+5]);
			
			//< Time
			//< 6-spaces
			for(i=0;i<6;i++)
			{
				printer_writeData(' ');
			}
			printer_writeStringF(PSTR("TIME:"));
			printer_writeData(_sd_block[RST_TARE_TIME]);//<HH
			printer_writeData(_sd_block[RST_TARE_TIME+1]);
			printer_writeData(':');
			printer_writeData(_sd_block[RST_TARE_TIME+2]);//<MM
			printer_writeData(_sd_block[RST_TARE_TIME+3]);
		}
		PRINTER_ENTER;
		
		//< net weight
		printer_writeStringF(PSTR("NET     : "));
		
		//< set weight font
		PRINTER_ESC;
		PRINTER_SET_BOLD;
		
		//< check if net weight presents
		if(!_sd_block[RST_NETWEIGHT_IS_PRESENT])
		{
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && _sd_block[RST_NETWEIGHT+5-i]=='0' && i!=5)
				{
					printer_writeData(' ');
				}
				else
				{
					printer_writeData(_sd_block[RST_NETWEIGHT+5-i]);
					temp=0;
				}
			}
		}
		else
		{
			printer_writeStringF(PSTR("------"));
		}
		PRINTER_ESC;
		PRINTER_CANCEL_BOLD;
		printer_writeStringF(PSTR(" Kg"));
		
		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< Charges
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge1 present or not
			if(!_sd_block[RST_CHARGE1_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE1+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}
		else if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge2 present or not
			if(!_sd_block[RST_CHARGE2_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE2+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}
		else if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
		{
			printer_writeStringF(PSTR("Charges(1): Rs  "));
			//< check if charge1 present or not
			if(!_sd_block[RST_CHARGE1_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE1+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			
			//< Charges2
			printer_writeStringF(PSTR("                "));
			printer_writeStringF(PSTR("Charges(2): Rs  "));
			//< check if charge2 present or not
			if(!_sd_block[RST_CHARGE2_IS_PRESENT])
			{
				for(i=0;i<5;i++)
				{
					printer_writeData(_sd_block[RST_CHARGE2+i]);
				}
			}
			else
			{
				printer_writeData(' ');
				printer_writeData(' ');
				printer_writeData('-');
				printer_writeData('-');
				printer_writeData('-');
			}
			//< Put line (------)
			PRINTER_ENTER;
			for(i=0;i<76;i++)
			{
				printer_writeData('-');
			}
			PRINTER_ENTER;
		}
		
		printer_writeStringF(PSTR("Operator Signature :"));
		PRINTER_ENTER;
		PRINTER_ENTER;
		
		//< put line (-----)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< printing header
		//< check header position is bottom
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='B')
		{
			setting_print_header(0);
		}
		
		//< no.of LINE_FEED after slip print as per ALT_L setting
		for(i=0;i<eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_LINE_FEED);i++)
		{
			PRINTER_ENTER;
		}
	}
	//< if only first entry completed
	else
	{
		//< printing header
		//< check header position is top
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='T')
		{
			setting_print_header(0);
		}
		
		//< set font
		PRINTER_ESC;
		PRINTER_SET_RECEIPT_FONT;
		
		//< RST No. printing
		//< RST No. printing
		printer_writeStringF(PSTR("RST NO      : "));
		temp=1;
		if((_rst_no/1000)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no/1000)+'0');
			temp=0;
		}
		if(((_rst_no%1000)/100)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%1000)/100)+'0');
			temp=0;
		}
		if(((_rst_no%100)/10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData(((_rst_no%100)/10)+'0');
			temp=0;
		}
		if((_rst_no%10)==0 && temp)
		{
			printer_writeData(' ');
		}
		else
		{
			printer_writeData((_rst_no%10)+'0');
			temp=0;
		}
		
		//< 16-spaces
		for(i=0;i<16;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Vehicle No. Printing
		printer_writeStringF(PSTR("VEHICLE NO. : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
		}
		PRINTER_ENTER;
		
		//< Supplier
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SUPPLIER    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SUPPLIER+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Material
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("MATERIAL    : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_MATERIAL+i]);
		}
		PRINTER_ENTER;
		
		//< Address
		printer_writeStringF(PSTR("ADDRESS     : "));
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_ADDRESS+i]);
		}
		//< 8-spaces
		for(i=0;i<8;i++)
		{
			printer_writeData(' ');
		}
		PRINTER_TAB;
		PRINTER_TAB;
		
		//< Source
		//< if alternative name is exist then set that name
		if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
		{
			for(i=0;i<8;i++)
			{
				temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
				printer_writeData(temp);
			}
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData(':');
			printer_writeData(' ');
		}
		else
		{
			printer_writeStringF(PSTR("SOURCE      : "));
		}
		for(i=0;i<12;i++)
		{
			printer_writeData(_sd_block[RST_SOURCE+i]);
		}
		PRINTER_ENTER;
		
		//< Putting line (------)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< Gross_Tare weight
		if(_sd_block[RST_FIRSTENTRYTYPE]=='G')
		{
			printer_writeStringF(PSTR("GROSS   : "));
			
			//< set weight font
			PRINTER_ESC;
			PRINTER_SET_BOLD;
			
			//< check if gross weight presents
			if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
			{
				temp=1;
				for(i=0;i<6;i++)
				{
					//< leading 0's removing
					if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
					{
						printer_writeData(' ');
					}
					else
					{
						printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
						temp=0;
					}
				}
			}
			else
			{
				printer_writeStringF(PSTR("------"));
			}
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			printer_writeStringF(PSTR(" Kg"));
			
			if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
			{
				//< Date
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("DATE:"));
				printer_writeData(_sd_block[RST_GROSS_DATE]);//<DD
				printer_writeData(_sd_block[RST_GROSS_DATE+1]);
				printer_writeData('/');
				printer_writeData(_sd_block[RST_GROSS_DATE+2]);//<MM
				printer_writeData(_sd_block[RST_GROSS_DATE+3]);
				printer_writeData('/');
				printer_writeData('2');//<YYYY
				printer_writeData('0');
				printer_writeData(_sd_block[RST_GROSS_DATE+4]);
				printer_writeData(_sd_block[RST_GROSS_DATE+5]);
							
				//< Time
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("TIME:"));
				printer_writeData(_sd_block[RST_GROSS_TIME]);//<HH
				printer_writeData(_sd_block[RST_GROSS_TIME+1]);
				printer_writeData(':');
				printer_writeData(_sd_block[RST_GROSS_TIME+2]);//<MM
				printer_writeData(_sd_block[RST_GROSS_TIME+3]);
			}
		}
		else
		{
			printer_writeStringF(PSTR("TARE    : "));
			
			//< set weight font
			PRINTER_ESC;
			PRINTER_SET_BOLD;
			
			//< check if tare weight presents
			if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
			{
				temp=1;
				for(i=0;i<6;i++)
				{
					//< leading 0's removing
					if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
					{
						printer_writeData(' ');
					}
					else
					{
						printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
						temp=0;
					}
				}
			}
			else
			{
				printer_writeStringF(PSTR("------"));
			}
			PRINTER_ESC;
			PRINTER_CANCEL_BOLD;
			printer_writeStringF(PSTR(" Kg"));
			
			if((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT))=='Y')
			{
				//< Date
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("DATE:"));
				printer_writeData(_sd_block[RST_TARE_DATE]);//<DD
				printer_writeData(_sd_block[RST_TARE_DATE+1]);
				printer_writeData('/');
				printer_writeData(_sd_block[RST_TARE_DATE+2]);//<MM
				printer_writeData(_sd_block[RST_TARE_DATE+3]);
				printer_writeData('/');
				printer_writeData('2');//<YYYY
				printer_writeData('0');
				printer_writeData(_sd_block[RST_TARE_DATE+4]);
				printer_writeData(_sd_block[RST_TARE_DATE+5]);
			
				//< Time
				//< 6-spaces
				for(i=0;i<6;i++)
				{
					printer_writeData(' ');
				}
				printer_writeStringF(PSTR("TIME:"));
				printer_writeData(_sd_block[RST_TARE_TIME]);//<HH
				printer_writeData(_sd_block[RST_TARE_TIME+1]);
				printer_writeData(':');
				printer_writeData(_sd_block[RST_TARE_TIME+2]);//<MM
				printer_writeData(_sd_block[RST_TARE_TIME+3]);
			}				
		}
		
		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< Charges1
		printer_writeStringF(PSTR("Charges(1): Rs  "));
		//< check if charge1 present or not
		if(!_sd_block[RST_CHARGE1_IS_PRESENT])
		{
			for(i=0;i<5;i++)
			{
				printer_writeData(_sd_block[RST_CHARGE1+i]);
			}
		}
		else
		{
			printer_writeData(' ');
			printer_writeData(' ');
			printer_writeData('-');
			printer_writeData('-');
			printer_writeData('-');
		}
		
		//< Put line (------)
		PRINTER_ENTER;
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		printer_writeStringF(PSTR("Operator Signature :"));
		PRINTER_ENTER;
		PRINTER_ENTER;
		
		//< put line (-----)
		for(i=0;i<76;i++)
		{
			printer_writeData('-');
		}
		PRINTER_ENTER;
		
		//< printing header
		//< check header position is bottom
		if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION)=='B')
		{
			setting_print_header(0);
		}
		
		//< no.of LINE_FEED after slip print as per ALT_L setting
		for(i=0;i<eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_LINE_FEED);i++)
		{
			PRINTER_ENTER;
		}
	}
}

void Report_entry_print(unsigned char *_sd_block,unsigned int _rst_no)
{
	unsigned char temp;
	unsigned char i;
	
	char str[8];
	
	//< set font
	PRINTER_ESC;
	PRINTER_SET_REPORT_FONT;
	
	//< RST NO. printing
	utoa(_rst_no,str,10);
	temp = strlen(str);
	for(i=0;i<4-temp;i++)
	{
		printer_writeData(' ');
	}
	printer_writeString(str);
	printer_writeStringF(PSTR(" |"));
	
	//< Date Printing
	//< if first entry is gross entry then
	if(_sd_block[RST_FIRSTENTRYTYPE]=='G')
	{
		//< date
		printer_writeData(_sd_block[RST_GROSS_DATE]);
		printer_writeData(_sd_block[RST_GROSS_DATE+1]);
		//< month
		printer_writeData('/');
		printer_writeData(_sd_block[RST_GROSS_DATE+2]);
		printer_writeData(_sd_block[RST_GROSS_DATE+3]);
		//< year
		printer_writeStringF(PSTR("/20"));
		printer_writeData(_sd_block[RST_GROSS_DATE+4]);
		printer_writeData(_sd_block[RST_GROSS_DATE+5]);
	}
	//< if first entry is tare entry then
	else
	{
		//< date
		printer_writeData(_sd_block[RST_TARE_DATE]);
		printer_writeData(_sd_block[RST_TARE_DATE+1]);
		//< month
		printer_writeData('/');
		printer_writeData(_sd_block[RST_TARE_DATE+2]);
		printer_writeData(_sd_block[RST_TARE_DATE+3]);
		//< year
		printer_writeStringF(PSTR("/20"));
		printer_writeData(_sd_block[RST_TARE_DATE+4]);
		printer_writeData(_sd_block[RST_TARE_DATE+5]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< vehicle no.
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_VEHICLE_NO+i]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< Supplier
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_SUPPLIER+i]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< Material
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_MATERIAL+i]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< Address
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_ADDRESS+i]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< SOURCE
	for(i=0;i<12;i++)
	{
		printer_writeData(_sd_block[RST_SOURCE+i]);
	}
	printer_writeStringF(PSTR(" |"));
	
	//< GROSS WEIGHT
	if(!_sd_block[RST_GROSSWEIGHT_IS_PRESENT])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_GROSSWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));
	
	//< TARE weight
	if(!_sd_block[RST_TAREWEIGHT_IS_PRESENT])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_TAREWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_TAREWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));
	
	//< NET weight
	if(!_sd_block[RST_BOTHENTRY_COMPLETED])
	{
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && _sd_block[RST_NETWEIGHT+5-i]=='0' && i!=5)
			{
				printer_writeData(' ');
			}
			else
			{
				printer_writeData(_sd_block[RST_NETWEIGHT+5-i]);
				temp=0;
			}
		}
	}
	else
	{
		printer_writeStringF(PSTR("------"));
	}
	printer_writeStringF(PSTR(" |"));
	
	//< Charges
	//< check if total charge present or not
	if(!_sd_block[RST_TOTALCHARGE_IS_PRESENT])
	{
		temp=0;
		while(temp!=5)
		{
			if(SD_data_buf[RST_TOTALCHARGE+temp]==' ')
			{
				break;
			}
			temp++;
		}
		for(i=0;i<5-temp;i++)
		{
			printer_writeData(' ');
		}
		for(i=0;i<temp;i++)
		{
			printer_writeData(_sd_block[RST_TOTALCHARGE+i]);
		}
	}
	else
	{
		printer_writeStringF(PSTR("     "));
	}
	printer_writeStringF(PSTR(" |"));
	
	PRINTER_ENTER;
}

void function_F1()
{
	unsigned int _rst_no;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned int i;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char supplier[13];
	unsigned char material[13];
	unsigned char address[13];
	unsigned char source[13];
	unsigned char is_charge1_present;
	unsigned char charge1[6];
	unsigned char gross_tare;
	unsigned char _weight[7];
	
	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);
	
	//< vehicle no.
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	vehicle_no[12]='\0';
	

	//< Supplier
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}
		
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		supplier[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	supplier[12]='\0';


	//< Material
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		material[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	material[12]='\0';


	//< Address
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ADDRESS :"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		address[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	address[12]='\0';
	
	
	//< Source
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		source[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	source[12]='\0';
	
	
	//< charge1
	//< check whether charge1 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge1_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));
		
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}
		
		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge1_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge1[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge1[5]='\0';
		}
	}
	else
	{
		is_charge1_present=1;//< not present
	}
	
	
	//< calculate weight
	_weight[6] = '\0';
	_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_weight[0] = ((weight % 10)) + '0'; //< LSB
	
	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));
	
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	
	//< GROSS-TARE
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("GROSS/TARE(G/T) :"));
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);	
	//< setting default - GROSS
	lcd_Line2_data[lcd_Line2_pointer] = 'G';
	lcd_mega_SendData('G');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='G' || ps2_key.val=='T'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	gross_tare = lcd_Line2_data[lcd_Line2_pointer];

	
	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	
	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}
	
	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
FUNCTION_F1_CHECK_RST:	
		//< retrive RST no
		_rst_no = RST_read();
		
		if(_rst_no>9999)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Memory is Full"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("ENTER= Overwrite"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ESC  = Back"));
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==SPECIAL_KEY)
					{
						if(ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						else if(ps2_key.val==SPECIAL_KEY_ESC)
						{
							return;
						}
					}
				}
			}
			function_F9();
			goto FUNCTION_F1_CHECK_RST;
		}
		//< create ENTRY
		//< clear sd buffer
		for(i=0;i<512;i++)
		{
			SD_data_buf[i]=0xFF;
		}
		//< entry occupied
		SD_data_buf[RST_ENTRY_TYPE]=0;
		//< vehicle no
		i=0;
		while(vehicle_no[i])
		{
			SD_data_buf[RST_VEHICLE_NO+i]=vehicle_no[i];
			i++;
		}
		//< Supplier
		i=0;
		while(supplier[i])
		{
			SD_data_buf[RST_SUPPLIER+i]=supplier[i];
			i++;
		}
		//< material
		i=0;
		while(material[i])
		{
			SD_data_buf[RST_MATERIAL+i]=material[i];
			i++;
		}
		//< address
		i=0;
		while(address[i])
		{
			SD_data_buf[RST_ADDRESS+i]=address[i];
			i++;
		}
		//< source
		i=0;
		while(source[i])
		{
			SD_data_buf[RST_SOURCE+i]=source[i];
			i++;
		}
		//< charge1
		SD_data_buf[RST_CHARGE1_IS_PRESENT]=is_charge1_present;
		if(!is_charge1_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				SD_data_buf[RST_TOTALCHARGE+i]=charge1[i];
				i++;
			}
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0xFF;
		}
		
		//< GROSS_TARE
		SD_data_buf[RST_FIRSTENTRYTYPE]=gross_tare;
		if(gross_tare=='G')//< GROSS
		{
			SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_GROSSWEIGHT+i]=_weight[i];
				i++;
			}
		}
		else//< TARE
		{
			SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_TAREWEIGHT+i]=_weight[i];
				i++;
			}
		}
		
		//< Date and Time
		if(gross_tare=='G')//< GROSS
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
			SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];
			
			//< Time
			SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		}
		else //<TARE
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
			SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];
			
			//< Time
			SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		}
		
		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0xFF;
		
		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
		
		//< increment rst_no
		RST_write(_rst_no+1);
	}
	
	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< retrive entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);
		
		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("RST No. is"));
	lcd_mega_gotoxy2(12);
	lcd_mega_SendData((_rst_no/1000)+'0');
	lcd_mega_SendData(((_rst_no%1000)/100)+'0');
	lcd_mega_SendData(((_rst_no%100)/10)+'0');
	lcd_mega_SendData((_rst_no%10)+'0');
	_delay_ms(1000);
}

void function_F2()
{
	unsigned int _rst_no;
	unsigned char i;
	unsigned char is_charge2_present;
	unsigned char charge2[6];
	unsigned char _weight[7];
	unsigned int _charge1=0;
	unsigned int _charge2=0;
	unsigned int _tot_charge=0;
	unsigned char temp;
	unsigned char str[7];
	unsigned long int weight_grosstare;
	unsigned long int net_weight;
	
	//< ASK for RST No.
FUNCTION_F2_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("2nd Entry) : "),0,0);
	lcd_Line2_pointer = 13;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F2_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
			case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
			case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
			case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
		}
		
		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);
		
		//< check this RST entry is valid or not
		if(SD_data_buf[RST_ENTRY_TYPE]!=0)
		{
			//< un occupied entry
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("RST No. Not Found!"));
			_delay_ms(1000);
			goto FUNCTION_F2_GET_RST;
		}
		
		if(SD_data_buf[RST_BOTHENTRY_COMPLETED]==0)
		{
			//< both entry is completed
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("3rd Entry Invalid"));
			_delay_ms(1000);
			goto FUNCTION_F2_GET_RST;
		}
	}
	
	//< now we have RST no. and related entry (tested)
	//< display vehicle no. from first entry
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_VEHICLE_NO+i]);
	}
	
	//< wait here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.val==SPECIAL_KEY_ENTER && ps2_key.type==SPECIAL_KEY)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	
	//< charge2
	//< check whether charge2 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge2_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));
		
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}
		
		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge2_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge2[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge2[5]='\0';
		}
	}
	else
	{
		is_charge2_present=1;//< not present
	}
		
	//< calculate weight
	_weight[6] = '\0';
	_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_weight[0] = ((weight % 10)) + '0'; //< LSB
	
	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));
	
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	
	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	
	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}
	
	
	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
		//< charge2
		SD_data_buf[RST_CHARGE2_IS_PRESENT]=is_charge2_present;
		if(!is_charge2_present)
		{
			i=0;
			while(charge2[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge2[i];
				i++;
			}
		}
		//< set total charge
		//< if both charge not exists
		if(SD_data_buf[RST_CHARGE1_IS_PRESENT] && SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge not present
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0xFF;
		}
		//< if only charge1 exists
		else if(!SD_data_buf[RST_CHARGE1_IS_PRESENT] && SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge1
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0;
			for(i=0;i<5;i++)
			{
				SD_data_buf[RST_TOTALCHARGE] = SD_data_buf[RST_CHARGE1];
			}
		}
		//< if only charge2 exists
		else if(SD_data_buf[RST_CHARGE1_IS_PRESENT] && !SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge2
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT] = 0;
			for(i=0;i<5;i++)
			{
				SD_data_buf[RST_TOTALCHARGE] = SD_data_buf[RST_CHARGE2];
			}
		}
		//< if both charge exist
		else if(!SD_data_buf[RST_CHARGE1_IS_PRESENT] && !SD_data_buf[RST_CHARGE2_IS_PRESENT])
		{
			//< set total charge = charge1 + charge2
			//< calculating charge1
			temp=0;
			while(temp!=5)
			{
				if(SD_data_buf[RST_CHARGE1+temp]==' ')
				{
					break;
				}
				temp++;
			}
			switch(temp)
			{
				case 1:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0');
					break;
				case 2:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0');
					break;
				case 3:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
				case 4:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 1000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
				case 5:
					_charge1 = (unsigned int)(SD_data_buf[RST_CHARGE1]-'0') * 10000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+1]-'0') * 1000 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE1+2]-'0');
					break;
			}
			//< calculating charge2
			temp=0;
			while(temp!=5)
			{
				if(SD_data_buf[RST_CHARGE2+temp]==' ')
				{
					break;
				}
				temp++;
			}
			switch(temp)
			{
				case 1:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0');
					break;
				case 2:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0');
					break;
				case 3:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 100 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 10 + \
								(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
				case 4:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 1000 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 100 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 10 + \
							(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
				case 5:
					_charge2 = (unsigned int)(SD_data_buf[RST_CHARGE2]-'0') * 10000 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+1]-'0') * 1000 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 100 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0') * 10 + \
									(unsigned int)(SD_data_buf[RST_CHARGE2+2]-'0');
					break;
			}
			_tot_charge = _charge1 + _charge2;
			utoa(_tot_charge,str,10);
			temp=1;
			for(i=0;i<5;i++)
			{
				if(str[i]=='\0')
				{
					temp=0;
				}
				
				if(temp)
				{
					SD_data_buf[RST_TOTALCHARGE+i] = str[i];
				}
				else
				{
					SD_data_buf[RST_TOTALCHARGE+i] = ' ';
				}
			}
		}
		
		//< gross_tare weight
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< now TARE weight
			SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_TAREWEIGHT+i]=_weight[i];
				i++;
			}
		}
		else//< TARE aleready done
		{
			//< now gross weight
			SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
			i=0;
			while(_weight[i])
			{
				SD_data_buf[RST_GROSSWEIGHT+i]=_weight[i];
				i++;
			}
		}
		//< NET WEIGHT
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< then retrive GROSS weight
			weight_grosstare = (unsigned long int)(SD_data_buf[RST_GROSSWEIGHT]-'0') + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+1]-'0') * 10 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+2]-'0') * 100 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+3]-'0') * 1000 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+4]-'0') * 10000 + \
								(unsigned long int)(SD_data_buf[RST_GROSSWEIGHT+5]-'0') * 100000;
			net_weight = weight_grosstare - weight;
		}
		else//< TARE aleready done
		{
			//< then retrive TARE weight
			weight_grosstare = (unsigned long int)(SD_data_buf[RST_TAREWEIGHT]-'0') + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+1]-'0') * 10 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+2]-'0') * 100 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+3]-'0') * 1000 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+4]-'0') * 10000 + \
								(unsigned long int)(SD_data_buf[RST_TAREWEIGHT+5]-'0') * 100000;
			net_weight = weight - weight_grosstare;
		}
		//< store net weight
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 0;
		SD_data_buf[RST_NETWEIGHT+5] = ((unsigned char)(net_weight / 100000UL)) + '0'; //< MSB
		SD_data_buf[RST_NETWEIGHT+4] = ((unsigned char)((net_weight % 100000UL) / 10000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+3] = ((unsigned char)((net_weight % 10000UL) / 1000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+2] = ((unsigned char)((net_weight % 1000UL) / 100UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+1] = ((unsigned char)((net_weight % 100UL) / 10UL)) + '0';
		SD_data_buf[RST_NETWEIGHT] = ((net_weight % 10)) + '0'; //< LSB
	
		//< Date and Time
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')//< GROSS aleready done
		{
			//< then stor tare date
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
			SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];
			
			//< Time
			SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		}
		else
		{
			ds1307_RefreshRTC_data();
			SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
			SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
			SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
			SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
			SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
			SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];
			
			//< Time
			SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
			SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
			SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
			SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		}			
		
		
		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0;
		
		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
	}
	
	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}
}

unsigned char function_F3(unsigned char flag)
{
	unsigned int _rst_no;
	unsigned char i;
	unsigned char temp;
	unsigned char str[8];
	
	//< ASK for RST No.
FUNCTION_F3_GET_RST:
	if(flag)
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("Enter RST No. (For"));
	
		set_string_in_lcd_dataF(LINE2,PSTR("Entry View) : "),0,0);
		lcd_Line2_pointer = 14;
		lcd_Line2_max = lcd_Line2_pointer+4;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
	
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return 1;//< unsuccessful return
				}
			}
		}
	
		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Invalid RST No."));
			_delay_ms(1000);
			goto FUNCTION_F3_GET_RST;
		}
		//< if user has inputed RST no. then
		else
		{
			//< create RST no from user input
			switch(lcd_Line2_pointer-lcd_Line2_min)
			{
				case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
				case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
				case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
				case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
			}
		
			//< read RST entry
			SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);
		
			//< check this RST entry is valid or not
			if(SD_data_buf[RST_ENTRY_TYPE]!=0)
			{
				//< un occupied entry
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("RST No. Not Found!"));
				_delay_ms(1000);
				goto FUNCTION_F3_GET_RST;
			}
		}
	
		//< now we have RST_no and its related entry
		//< show RST_no
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("At RST No. :"));
		lcd_mega_gotoxy2(13);
		lcd_mega_String(utoa(_rst_no,str,10));
	
		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}
	}
		
	//< show Vehicle No
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_VEHICLE_NO+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	//< show supplier
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_SUPPLIER+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	//< show material
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_MATERIAL+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	//< show address
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("ADDRESS :"));
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_ADDRESS+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	//< show source
	lcd_mega_ClearDispaly();
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}
	lcd_mega_gotoxy2(0);
	for(i=0;i<12;i++)
	{
		lcd_mega_SendData(SD_data_buf[RST_SOURCE+i]);
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	//< weight
	//< if both entry completed
	//< then show GROSS,TARE,NET weight
	if(!SD_data_buf[RST_BOTHENTRY_COMPLETED])
	{
		//< GROSS weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("GROSS WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_GROSSWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}
		
		//< TARE weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("TARE WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_TAREWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_TAREWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
		//< stay here till user not pressed ENTER
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
				{
					return 1;
				}
			}
		}
		
		//< NET weight
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("NET WEIGHT :"));
		lcd_mega_gotoxy2(0);
		temp=1;
		for(i=0;i<6;i++)
		{
			//< leading 0's removing
			if(temp && SD_data_buf[RST_NETWEIGHT+5-i]=='0' && i!=5)
			{
				lcd_mega_SendData(' ');
			}
			else
			{
				lcd_mega_SendData(SD_data_buf[RST_NETWEIGHT+5-i]);
				temp=0;
			}
		}
		lcd_mega_SendData(' ');
		lcd_mega_StringF(PSTR("KG"));
	}
	//< if one entry is completed
	//< then show only GROSS or TERE entry
	else
	{
		//< if GROSS
		if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')
		{
			//< GROSS weight
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("GROSS WEIGHT :"));
			lcd_mega_gotoxy2(0);
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && SD_data_buf[RST_GROSSWEIGHT+5-i]=='0' && i!=5)
				{
					lcd_mega_SendData(' ');
				}
				else
				{
					lcd_mega_SendData(SD_data_buf[RST_GROSSWEIGHT+5-i]);
					temp=0;
				}
			}
			lcd_mega_SendData(' ');
			lcd_mega_StringF(PSTR("KG"));
		}
		//< if TARE
		else
		{
			//< TARE weight
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("TARE WEIGHT :"));
			lcd_mega_gotoxy2(0);
			temp=1;
			for(i=0;i<6;i++)
			{
				//< leading 0's removing
				if(temp && SD_data_buf[RST_TAREWEIGHT+5-i]=='0' && i!=5)
				{
					lcd_mega_SendData(' ');
				}
				else
				{
					lcd_mega_SendData(SD_data_buf[RST_TAREWEIGHT+5-i]);
					temp=0;
				}
			}
			lcd_mega_SendData(' ');
			lcd_mega_StringF(PSTR("KG"));
		}
	}
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return 1;
			}
		}
	}
	
	return 0;
}

void function_F4()
{
	unsigned int _rst_no;
	
	//< ASK for RST No.
FUNCTION_F4_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Printing) : "),0,0);
	lcd_Line2_pointer = 12;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F4_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
				break;
			case 2:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				break;
			case 3:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
				break;
			case 4:
				_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
				break;
		}
		
		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);
		
		//< check this RST entry is valid or not
		if(SD_data_buf[RST_ENTRY_TYPE]!=0)
		{
			//< un occupied entry
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("RST No. Not Found!"));
			_delay_ms(1000);
			goto FUNCTION_F4_GET_RST;
		}
	}
	
	//< now we have RST No. and its related entry
	//< print entry
	RST_entry_print(SD_data_buf,_rst_no);	
}

void function_F5()
{
	unsigned int _rst_no;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned int i;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char supplier[13];
	unsigned char material[13];
	unsigned char address[13];
	unsigned char source[13];
	unsigned char is_charge1_present=0xff;
	unsigned char charge1[6];
	unsigned char is_charge2_present=0xff;
	unsigned char charge2[6];
	unsigned char _gross_weight[7];
	unsigned char _tare_weight[7];
	unsigned long int tare_weight;
	unsigned long int net_weight;
	
	
	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);
	
	//< vehicle no.
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("VEHICLE NO. :"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	vehicle_no[12]='\0';
	

	//< Supplier
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SUPPLIER :"));
	}
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		supplier[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	supplier[12]='\0';


	//< Material
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("MATERIAL :"));
	}
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		material[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	material[12]='\0';


	//< Address
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ADDRESS :"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		address[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	address[12]='\0';
	
	
	//< Source
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	//< if alternative name is exist then set that name
	if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
	{
		for(i=0;i<8;i++)
		{
			temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(temp==' ')
			{
				break;
			}
			lcd_mega_gotoxy1(i);
			lcd_mega_SendData(temp);
		}
		lcd_mega_SendData(' ');
		lcd_mega_SendData(':');
	}
	else
	{
		lcd_mega_StringF(PSTR("SOURCE :"));
	}
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 12;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				return;
			}
		}
	}
	//< store data for temperory
	for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
	{
		source[i-lcd_Line2_min] = lcd_Line2_data[i];
	}
	source[12]='\0';
	
	//< charge1
	//< check whether charge1 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='F' || eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='B')
	{
		is_charge1_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));
		
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}
		
		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge1_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge1[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge1[5]='\0';
		}
	}
	else
	{
		is_charge1_present=1;//< not present
	}
	
	//< charge2
	//< check whether charge2 is enable in ALT_L setting or not
	if(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE)=='S')
	{
		is_charge2_present=0;//< present

		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("CHARGE (in Rs.) :"));
		
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 5;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< returns without seting time
					return;//< unsuccessful return
				}
			}
		}
		
		//< if user not inputed anything then
		if(lcd_Line2_pointer==lcd_Line2_min)
		{
			is_charge2_present = 1; //< not present
		}
		else
		{
			//< store data for temperory
			for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
			{
				charge2[i-lcd_Line2_min] = lcd_Line2_data[i];
			}
			charge2[5]='\0';
		}
	}
	else
	{
		is_charge2_present=1;//< not present
	}
	
	//< calculate gross weight
	_gross_weight[6] = '\0';
	_gross_weight[5] = ((unsigned char)(weight / 100000UL)) + '0'; //< MSB
	_gross_weight[4] = ((unsigned char)((weight % 100000UL) / 10000UL)) + '0';
	_gross_weight[3] = ((unsigned char)((weight % 10000UL) / 1000UL)) + '0';
	_gross_weight[2] = ((unsigned char)((weight % 1000UL) / 100UL)) + '0';
	_gross_weight[1] = ((unsigned char)((weight % 100UL) / 10UL)) + '0';
	_gross_weight[0] = ((weight % 10)) + '0'; //< LSB
	
	//< display on lcd
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("WEIGHT (Press Enter)"));
	lcd_mega_gotoxy2(0);
	temp=1;
	for(i=0;i<6;i++)
	{
		//< leading 0's removing
		if(temp && _gross_weight[5-i]=='0' && i!=5)
		{
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_SendData(_gross_weight[5-i]);
			temp=0;
		}
	}
	lcd_mega_SendData(' ');
	lcd_mega_StringF(PSTR("KG"));
	
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	
	//< MANUAL TARE INPUT
FUNCTION_F5_MANUAL_TARE:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter TARE WEIGHT :"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("(In KG) : "),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+6;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid Entry"));
		_delay_ms(1000);
		goto FUNCTION_F5_MANUAL_TARE;
	}
	else
	{
		for(i=0;i<7;i++)
		{
			_tare_weight[i]='\0';
		}
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_pointer;i++)
		{
			_tare_weight[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		tare_weight=atol(_tare_weight);
		//< calculate gross weight
		_tare_weight[6] = '\0';
		_tare_weight[5] = ((unsigned char)(tare_weight / 100000UL)) + '0'; //< MSB
		_tare_weight[4] = ((unsigned char)((tare_weight % 100000UL) / 10000UL)) + '0';
		_tare_weight[3] = ((unsigned char)((tare_weight % 10000UL) / 1000UL)) + '0';
		_tare_weight[2] = ((unsigned char)((tare_weight % 1000UL) / 100UL)) + '0';
		_tare_weight[1] = ((unsigned char)((tare_weight % 100UL) / 10UL)) + '0';
		_tare_weight[0] = ((tare_weight % 10)) + '0'; //< LSB
	}
	
	//< PRINT-SAVE-CANCEL
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Print/Save/Cancel"));
	set_string_in_lcd_dataF(LINE2,PSTR("(P/S/C) :"),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	//< setting default - PRINT
	lcd_Line2_data[lcd_Line2_pointer] = 'P';
	lcd_mega_SendData('P');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='P' || ps2_key.val=='S' || ps2_key.val=='C'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	
	//< if user selected CANCEL
	if(lcd_Line2_data[lcd_Line2_pointer]=='C')
	{
		return;
	}
	
	//< if user selected PRINT or SAVE then save data first
	if((lcd_Line2_data[lcd_Line2_pointer]=='P')||(lcd_Line2_data[lcd_Line2_pointer]=='S'))
	{
		//< retrive RST no
FUNCTION_F5_CHECK_RST:
		_rst_no = RST_read();
		
		if(_rst_no>9999)
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Memory is Full"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("ENTER= Overwrite"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("ESC  = Back"));
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==SPECIAL_KEY)
					{
						if(ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						else if(ps2_key.val==SPECIAL_KEY_ESC)
						{
							return;
						}
					}
				}
			}
			function_F9();
			goto FUNCTION_F5_CHECK_RST;
		}
		//< create ENTRY
		//< clear sd buffer
		for(i=0;i<512;i++)
		{
			SD_data_buf[i]=0xFF;
		}
		//< entry occupied
		SD_data_buf[RST_ENTRY_TYPE]=0;
		//< vehicle no
		i=0;
		while(vehicle_no[i])
		{
			SD_data_buf[RST_VEHICLE_NO+i]=vehicle_no[i];
			i++;
		}
		//< Supplier
		i=0;
		while(supplier[i])
		{
			SD_data_buf[RST_SUPPLIER+i]=supplier[i];
			i++;
		}
		//< material
		i=0;
		while(material[i])
		{
			SD_data_buf[RST_MATERIAL+i]=material[i];
			i++;
		}
		//< address
		i=0;
		while(address[i])
		{
			SD_data_buf[RST_ADDRESS+i]=address[i];
			i++;
		}
		//< source
		i=0;
		while(source[i])
		{
			SD_data_buf[RST_SOURCE+i]=source[i];
			i++;
		}
		//< charge1
		SD_data_buf[RST_CHARGE1_IS_PRESENT]=is_charge1_present;
		if(!is_charge1_present)
		{
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				i++;
			}
		}
		//< charge2
		SD_data_buf[RST_CHARGE2_IS_PRESENT]=is_charge2_present;
		if(!is_charge2_present)
		{
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_CHARGE1+i]=charge1[i];
				i++;
			}
		}
		//< total charge
		if(!is_charge1_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge1[i])
			{
				SD_data_buf[RST_TOTALCHARGE+i]=charge1[i];
				i++;
			}
		}
		else if(!is_charge2_present)
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0;
			i=0;
			while(charge2[i])
			{
				SD_data_buf[RST_TOTALCHARGE+i]=charge2[i];
				i++;
			}
		}
		else
		{
			SD_data_buf[RST_TOTALCHARGE_IS_PRESENT]=0xFF;
		}		
		//< GROSS_TARE weight
		SD_data_buf[RST_FIRSTENTRYTYPE]='G';
		//< GROSS
		SD_data_buf[RST_GROSSWEIGHT_IS_PRESENT]=0;
		i=0;
		while(_gross_weight[i])
		{
			SD_data_buf[RST_GROSSWEIGHT+i]=_gross_weight[i];
			i++;
		}
		//< TARE
		SD_data_buf[RST_TAREWEIGHT_IS_PRESENT]=0;
		i=0;
		while(_tare_weight[i])
		{
			SD_data_buf[RST_TAREWEIGHT+i]=_tare_weight[i];
			i++;
		}
		//< NET Weight
		net_weight = weight - tare_weight;
		//< store net weight
		SD_data_buf[RST_NETWEIGHT_IS_PRESENT] = 0;
		SD_data_buf[RST_NETWEIGHT+5] = ((unsigned char)(net_weight / 100000UL)) + '0'; //< MSB
		SD_data_buf[RST_NETWEIGHT+4] = ((unsigned char)((net_weight % 100000UL) / 10000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+3] = ((unsigned char)((net_weight % 10000UL) / 1000UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+2] = ((unsigned char)((net_weight % 1000UL) / 100UL)) + '0';
		SD_data_buf[RST_NETWEIGHT+1] = ((unsigned char)((net_weight % 100UL) / 10UL)) + '0';
		SD_data_buf[RST_NETWEIGHT] = ((net_weight % 10)) + '0'; //< LSB
		
		//< Date and Time
		//< GROSS
		//< date
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_GROSS_DATE] = RTC_data.date[0];
		SD_data_buf[RST_GROSS_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_GROSS_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_GROSS_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_GROSS_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_GROSS_DATE+5] = RTC_data.year[1];
		//< Time
		SD_data_buf[RST_GROSS_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_GROSS_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_GROSS_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_GROSS_TIME+3] = RTC_data.minute[1];
		
		//< TARE
		//< Date
		ds1307_RefreshRTC_data();
		SD_data_buf[RST_TARE_DATE] = RTC_data.date[0];
		SD_data_buf[RST_TARE_DATE+1] = RTC_data.date[1];
		SD_data_buf[RST_TARE_DATE+2] = RTC_data.month[0];
		SD_data_buf[RST_TARE_DATE+3] = RTC_data.month[1];
		SD_data_buf[RST_TARE_DATE+4] = RTC_data.year[0];
		SD_data_buf[RST_TARE_DATE+5] = RTC_data.year[1];
			
		//< Time
		SD_data_buf[RST_TARE_TIME] = RTC_data.hour[0];
		SD_data_buf[RST_TARE_TIME+1] = RTC_data.hour[1];
		SD_data_buf[RST_TARE_TIME+2] = RTC_data.minute[0];
		SD_data_buf[RST_TARE_TIME+3] = RTC_data.minute[1];
		
		SD_data_buf[RST_BOTHENTRY_COMPLETED] = 0;
		
		//< write this entry into SD card
		SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
		
		//< increment rst_no
		RST_write(_rst_no+1);
	}
	
	//< if user selected PRINT then print
	if(lcd_Line2_data[lcd_Line2_pointer]=='P')
	{
		//< retrive entry
		SD_mega_ReadSingleBlock(SD_data_buf,_rst_no);
		
		//< print this entry
		//< check number of copies ALT_L setting
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Printing Start"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Wait..."));
		for(i=0;i<(eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES));i++)
		{
			RST_entry_print(SD_data_buf,_rst_no);
		}
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("RST No. is"));
	lcd_mega_gotoxy2(12);
	lcd_mega_SendData((_rst_no/1000)+'0');
	lcd_mega_SendData(((_rst_no%1000)/100)+'0');
	lcd_mega_SendData(((_rst_no%100)/10)+'0');
	lcd_mega_SendData((_rst_no%10)+'0');
	_delay_ms(1000);
	
}

void function_F6()
{
	unsigned char code_enable;
	unsigned char code_len;
	unsigned char temp;
	unsigned char vehicle_no[13];
	unsigned char _vehicle_no[13];
	unsigned int i;
	unsigned char j;
	unsigned int _rst_no;
	unsigned char str[8];
	
	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);
	
	//< ASK for Vehicle no
FUNCTION_F6_GET_VEHICLE_NO:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter VEHICLE NO.(To"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Search):"),0,0);
	lcd_Line2_pointer = 8;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			//< code base system related
			if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
			{
				//< check whether code available or not
				if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
				{
					code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
					//< set code as per key press
					for(i=0;i<code_len;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_mega_SendData(temp);
						lcd_Line2_data[lcd_Line2_pointer++]=temp;
					}
				}
			}
			
			else if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid VEHICLE NO."));
		_delay_ms(1000);
		goto FUNCTION_F6_GET_VEHICLE_NO;
	}
	//< if user has inputed vehicle no. then
	else
	{
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
		{
			vehicle_no[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		vehicle_no[12]='\0';
		//< remove space from vehicle no
		remove_space_from_string(vehicle_no);
	}
	
	//< retrive current RST no.
	_rst_no = RST_read();
	if(_rst_no>9999)
	{
		_rst_no = 9999;
	}
	_vehicle_no[12]='\0';
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Searching..."));
	//< search RST entry using Vehicle no.
	for(i=0;i<=_rst_no;i++)
	{
		//< read RST entry
		SD_mega_ReadSingleBlock(SD_data_buf,i);
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
		
		//< check this entry is occupid or not
		if(!SD_data_buf[RST_ENTRY_TYPE])
		{
			//< its occupied
			//< retrive _vehicle no
			for(j=0;j<12;j++)
			{
				_vehicle_no[j] = SD_data_buf[RST_VEHICLE_NO+j];
			}
			//< remove space from _vehicle no
			remove_space_from_string(_vehicle_no);
			//< compare two vehicle no
			if(!strcmp(vehicle_no,_vehicle_no))
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Entry Found..."));
				lcd_mega_gotoxy2(0);
				lcd_mega_StringF(PSTR("At RST No.: "));
				lcd_mega_String(utoa(i,str,10));
				//< stay here till user not pressed ENTER
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
						{
							break;
						}
						if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
						{
							return;
						}
					}
				}
				//< if both string same then show this RST entry
				if(function_F3(0))
				{
					return;
				}
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Searching..."));
			}
		}
	}
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Entry Not Found"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Search is Over"));
	_delay_ms(1000);
}

static void remove_space_from_string(unsigned char *str)
{
	unsigned char i=0,j=0;
	
	while(str[i])
	{
		//< if space found
		if(str[i]==' ')
		{
			j=0;
			while(str[i+j])
			{
				str[i+j]=str[i+j+1];
				j++;
			}
		}
		else
		{
			i++;
		}
	}
}

void function_F7()
{
	unsigned char report_type;
	unsigned char report_variable;
	unsigned int from_rst;
	unsigned int to_rst;
	unsigned long int from_date;
	unsigned long int to_date;
	unsigned long int temp_date;
	unsigned char _find_str[13];
	unsigned char _temp_str[13];
	unsigned int i;
	unsigned char temp;
	unsigned char code_enable;
	unsigned char code_len;
	unsigned long int _net_weight;
	unsigned long int tot_NET_weight=0;
	unsigned long int _charge;
	unsigned long int tot_charge=0;
	unsigned char match;
	
	_find_str[12] = '\0';
	_temp_str[12] = '\0';
	//< check code based system enable or not
	code_enable = ((eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM)=='Y')?1:0);
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	//< Report Type
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("REPORT(ALL/RST/DATE)"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< default showing
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='3')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	report_type = lcd_Line2_data[lcd_Line2_pointer]-'0';
	
	//< inputing RST NO. or DATE
	switch(report_type)
	{
		//< if ALL
		case 1:
			from_rst = 0;
			to_rst = RST_read();
			break;
			
		//< if RST WISE
		case 2:
FUNCTION_F7_SET_FROM_RST:
			//< from RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("FROM RST NO.:"));
			
			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+4;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			
			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}
			
			//< if user not inputed anything then
			if(lcd_Line2_pointer==lcd_Line2_min)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid RST No."));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_FROM_RST;
			}
			//< if user has inputed RST no. then
			else
			{
				//< create RST no from user input
				switch(lcd_Line2_pointer-lcd_Line2_min)
				{
					case 1:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0');
					break;
					case 2:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
					break;
					case 3:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
					break;
					case 4:
					from_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
					break;
				}
			}
				
FUNCTION_F7_SET_TO_RST:
			//< TO RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("TO RST NO.:"));
			
			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+4;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			
			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}
			
			//< if user not inputed anything then
			if(lcd_Line2_pointer==lcd_Line2_min)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid RST No."));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_TO_RST;
			}
			//< if user has inputed RST no. then
			else
			{
				//< create RST no from user input
				switch(lcd_Line2_pointer-lcd_Line2_min)
				{
					case 1:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0');
					break;
					case 2:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
					break;
					case 3:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
					break;
					case 4:
					to_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
					break;
				}
			}
			break;
		
		//< DATE WISE
		case 3:
			from_rst = 0;
			to_rst = RST_read();
FUNCTION_F7_SET_FROM_DATE:
			//< from RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("FROM DATE:"));
			
			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+8;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			
			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_data[lcd_Line2_pointer]='/';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_Line2_pointer++;
							lcd_mega_gotoxy2(lcd_Line2_pointer);							
						}
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}
			
			//< if user not inputed date properly
			if(lcd_Line2_pointer!=lcd_Line2_max)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid Date"));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_FROM_DATE;
			}
			//< if user has inputed Date then
			else
			{
				from_date = (lcd_Line2_data[lcd_Line2_min+6]-'0')*100000UL + \
							(lcd_Line2_data[lcd_Line2_min+7]-'0')*10000UL + \
							(lcd_Line2_data[lcd_Line2_min+3]-'0')*1000UL + \
							(lcd_Line2_data[lcd_Line2_min+4]-'0')*100UL + \
							(lcd_Line2_data[lcd_Line2_min+0]-'0')*10UL + \
							(lcd_Line2_data[lcd_Line2_min+1]-'0')*1UL;
			}
			
FUNCTION_F7_SET_TO_DATE:
			//< to RST no.
			lcd_mega_ClearDispaly();
			clear_lcd_data(LINE_BOTH);
			lcd_mega_StringF(PSTR("TO DATE:"));
			
			lcd_Line2_pointer = 0;
			lcd_Line2_max = lcd_Line2_pointer+8;
			lcd_Line2_min = lcd_Line2_pointer;
			lcd_CursorLine = LINE2;
			//< set lcd disply
			lcd_mega_gotoxy2(0);
			lcd_mega_String(lcd_Line2_data);
			//< set cursor position
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			
			//< take user input
			while(1)
			{
				//< get key pressed
				while(1)
				{
					if(ps2_kb_available_key())
					{
						ps2_kb_getkey(&ps2_key);
						break;
					}
				}
				//< numeric key processing
				if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
				{
					if(lcd_Line2_pointer<lcd_Line2_max)
					{
						lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_Line2_pointer++;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_data[lcd_Line2_pointer]='/';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_Line2_pointer++;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
				}
				//< special key processing
				else if(ps2_key.type==SPECIAL_KEY)
				{
					//< backspace checking
					if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
					{
						if(lcd_Line2_pointer>lcd_Line2_min)
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
						if(lcd_Line2_pointer==(lcd_Line2_min+2) || lcd_Line2_pointer==(lcd_Line2_min+5))
						{
							lcd_Line2_pointer--;
							lcd_mega_gotoxy2(lcd_Line2_pointer);
							lcd_Line2_data[lcd_Line2_pointer]=' ';
							lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
							lcd_mega_gotoxy2(lcd_Line2_pointer);
						}
					}
					//< enter key processing
					else if(ps2_key.val==SPECIAL_KEY_ENTER)
					{
						break;
					}
					//< ESC key processing
					else if(ps2_key.val==SPECIAL_KEY_ESC)
					{
						//< returns without seting time
						return;//< unsuccessful return
					}
				}
			}
			
			//< if user not inputed date properly
			if(lcd_Line2_pointer!=lcd_Line2_max)
			{
				lcd_mega_ClearDispaly();
				lcd_mega_StringF(PSTR("Invalid Date"));
				_delay_ms(1000);
				goto FUNCTION_F7_SET_TO_DATE;
			}
			//< if user has inputed Date then
			else
			{
				to_date = (lcd_Line2_data[lcd_Line2_min+6]-'0')*100000UL + \
							(lcd_Line2_data[lcd_Line2_min+7]-'0')*10000UL + \
							(lcd_Line2_data[lcd_Line2_min+3]-'0')*1000UL + \
							(lcd_Line2_data[lcd_Line2_min+4]-'0')*100UL + \
							(lcd_Line2_data[lcd_Line2_min+0]-'0')*10UL + \
							(lcd_Line2_data[lcd_Line2_min+1]-'0')*1UL;
			}
			break;
	}
	
	
	//< take report_variable
	//< i.e.ALL			= 1
	//<		Supplier	= 2
	//<		source		= 3
	//<		vehicle no.	= 4
	//<		Material	= 5
	//<		Address		= 6
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("(ALL/SU/SO/VE/MA/AD)"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< default showing
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='6')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	report_variable = lcd_Line2_data[lcd_Line2_pointer]-'0';
	
	
	//< if not ALL
	//< take find_str i.e vehicle_no in the case of report as per vehicle_no
	if(report_variable!=1)
	{
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		switch(report_variable)
		{
			//< SUPPLIER
			case 2:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("SUPPLIER :"));
				}
				break;
		
			//< SOURCE
			case 3:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("SOURCE :"));
				}
				break;
			
			//< Vehicle No
			case 4:
				lcd_mega_StringF(PSTR("VEHICLE NO :"));
				break;
			
			//< MATERIAL
			case 5:
				//< if alternative name is exist then set that name
				if(!eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT))
				{
					for(i=0;i<8;i++)
					{
						temp=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
						if(temp==' ')
						{
							break;
						}
						lcd_mega_gotoxy1(i);
						lcd_mega_SendData(temp);
					}
					lcd_mega_SendData(' ');
					lcd_mega_SendData(':');
				}
				else
				{
					lcd_mega_StringF(PSTR("MATERIAL :"));
				}
				break;
			
			case 6:
				lcd_mega_StringF(PSTR("ADDRESS :"));
				break;
		}
		lcd_Line2_pointer = 0;
		lcd_Line2_max = 12;
		lcd_Line2_min = 0;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
	
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
			}
			//< normal key processing
			if(ps2_key.type==NORMAL_KEY)
			{
				//< code base system related
				if(code_enable && lcd_Line2_pointer==lcd_Line2_min && ps2_key.val>='0' && ps2_key.val<='9')
				{
					//< vehicle no related
					switch(report_variable)
					{
						//< SUPPLIER
						case 2:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
					
						//< SOURCE
						case 3:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
					
						//< VEHICLE NO	
						case 4:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
						
						//< MATERIAL
						case 5:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
						
						//< ADDRESS
						case 6:
							//< check whether code available or not
							if(!eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*(ps2_key.val-'0')))
							{
								code_len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*(ps2_key.val-'0'));
								//< set code as per key press
								for(i=0;i<code_len;i++)
								{
									temp=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*(ps2_key.val-'0')+i);
									lcd_mega_gotoxy2(lcd_Line2_pointer);
									lcd_mega_SendData(temp);
									lcd_Line2_data[lcd_Line2_pointer++]=temp;
								}
							}
							break;
					}
				}
			
				else if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					//< break with saving
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					//< break without saving
					return;
				}
			}
		}
		//< store data for temperory
		for(i=lcd_Line2_min;i<lcd_Line2_max;i++)
		{
			_find_str[i-lcd_Line2_min] = lcd_Line2_data[i];
		}
		_find_str[12]='\0';
		//< remove space from find_str
		remove_space_from_string(_find_str);
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Report Generating..."));
	
	//< print header of report
	PRINTER_ESC;
	PRINTER_SET_REPORT_FONT;
	
	//< header printing
	//< line1
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_DATA+i));
		}
		PRINTER_ENTER;
	}
	//< line2
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_DATA+i));
		}
		PRINTER_ENTER;
	}
	//< line3
	temp = eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_LEN);
	if(temp!=0)
	{
		for(i=0;i<temp;i++)
		{
			printer_writeData(eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_DATA+i));
		}
		PRINTER_ENTER;
	}
	
	switch(report_type)
	{
		//< ALL
		case 1:
			printer_writeStringF(PSTR("For ALL Records"));
			break;
			
		//< RST wise
		case 2:
			printer_writeStringF(PSTR("For RST NO. wise Records"));
			PRINTER_ENTER;
			printer_writeStringF(PSTR("RST:  "));
			printer_writeString(utoa(from_rst,_temp_str,10));
			printer_writeStringF(PSTR(" ... "));
			printer_writeString(utoa(to_rst,_temp_str,10));
			break;
		//< DATE wise
		case 3:
			printer_writeStringF(PSTR("For DATE wise Records"));
			PRINTER_ENTER;
			printer_writeStringF(PSTR("DATE:  "));
			//< from date
			ultoa(from_date,_temp_str,10);
			printer_writeData(_temp_str[4]);
			printer_writeData(_temp_str[5]);
			printer_writeData('/');
			printer_writeData(_temp_str[2]);
			printer_writeData(_temp_str[3]);
			printer_writeStringF(PSTR("/20"));
			printer_writeData(_temp_str[0]);
			printer_writeData(_temp_str[1]);
			printer_writeStringF(PSTR(" ... "));
			//< to date
			ultoa(to_date,_temp_str,10);
			printer_writeData(_temp_str[4]);
			printer_writeData(_temp_str[5]);
			printer_writeData('/');
			printer_writeData(_temp_str[2]);
			printer_writeData(_temp_str[3]);
			printer_writeStringF(PSTR("/20"));
			printer_writeData(_temp_str[0]);
			printer_writeData(_temp_str[1]);
			break;
	}
	PRINTER_ENTER;
	
	if(report_variable!=1)
	{
		switch(report_variable)
		{
			//< supplier
			case 2:
				printer_writeStringF(PSTR("SUPPLIER : "));
				break;
		
			//< source
			case 3:
				printer_writeStringF(PSTR("SOURCE : "));
				break;
		
			//< vehicle no
			case 4:
				printer_writeStringF(PSTR("VEHICLE NO. : "));
				break;
		
			//< material
			case 5:
				printer_writeStringF(PSTR("MATERIAL : "));
				break;
		
			//< ADDRESS
			case 6:
				printer_writeStringF(PSTR("ADDRESS : "));
				break;
		}
		printer_writeString(_find_str);
		PRINTER_ENTER;
	}	
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	   
	printer_writeStringF(PSTR("RST  |"));
	printer_writeStringF(PSTR("   DATE    |"));
	printer_writeStringF(PSTR("VEHICLE NO.  |"));
	printer_writeStringF(PSTR("SUPPLIER     |"));
	printer_writeStringF(PSTR("MATERIAL     |"));
	printer_writeStringF(PSTR("ADDRESS      |"));
	printer_writeStringF(PSTR("SOURCE       |"));
	printer_writeStringF(PSTR(" GROSS |"));
	printer_writeStringF(PSTR(" TARE  |"));
	printer_writeStringF(PSTR(" NETT  |"));
	printer_writeStringF(PSTR("CHARGE|"));
	
	PRINTER_ENTER;
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	
	
	
	//< report generating and printing
	//< if to_rst greater than cur_rst
	//< then set to_rst = cur_rst
	i=RST_read();
	if(to_rst>i)
	{
		to_rst=i;
	}
			
	for(i=from_rst;i<=to_rst;i++)
	{
		//< retrive RST Entry
		SD_mega_ReadSingleBlock(SD_data_buf,i);
		match = 0;
				
		//< check if entry is occupied
		if(!SD_data_buf[RST_ENTRY_TYPE])
		{
			//< date wise report related processing
			if(report_type==3)
			{
				//< calculate entry date
				if(SD_data_buf[RST_FIRSTENTRYTYPE]=='G')
				{
					temp_date = (SD_data_buf[RST_GROSS_DATE+4]-'0')*100000UL + \
								(SD_data_buf[RST_GROSS_DATE+5]-'0')*10000UL + \
								(SD_data_buf[RST_GROSS_DATE+2]-'0')*1000UL + \
								(SD_data_buf[RST_GROSS_DATE+3]-'0')*100UL + \
								(SD_data_buf[RST_GROSS_DATE+0]-'0')*10UL + \
								(SD_data_buf[RST_GROSS_DATE+1]-'0')*1UL;
				}
				else
				{
					temp_date = (SD_data_buf[RST_TARE_DATE+4]-'0')*100000UL + \
								(SD_data_buf[RST_TARE_DATE+5]-'0')*10000UL + \
								(SD_data_buf[RST_TARE_DATE+2]-'0')*1000UL + \
								(SD_data_buf[RST_TARE_DATE+3]-'0')*100UL + \
								(SD_data_buf[RST_TARE_DATE+0]-'0')*10UL + \
								(SD_data_buf[RST_TARE_DATE+1]-'0')*1UL;
				}
				
				//< check entry date is in range or not
				if(temp_date>to_date || temp_date<from_date)
				{
					//< then goto next entry
					continue;
				}
			}
			
			switch(report_variable)
			{
				//< if ALL
				case 1:
					match = 1;
					break;
						
				//< if SUPPLIER	
				case 2:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_SUPPLIER+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
						
				//< if SOURCE	
				case 3:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_SOURCE+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
						
				//< if VEHICLE NO	
				case 4:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_VEHICLE_NO+temp];
					}
					//< remove space from
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
						
				//< if MATERIAL	
				case 5:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_MATERIAL+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
						
				//< if ADDRESS	
				case 6:
					//< retrive
					for(temp=0;temp<12;temp++)
					{
						_temp_str[temp] = SD_data_buf[RST_ADDRESS+temp];
					}
					//< remove space
					remove_space_from_string(_temp_str);
					//< compare two strings
					if(!strcmp(_find_str,_temp_str))
					{
						//< if both same
						match = 1;
					}
					break;
			}
					
			//< if occupied than print RST Entry
			if(match)
			{
				Report_entry_print(SD_data_buf,i);
						
				//< calculate total NET_weight
				if(!SD_data_buf[RST_BOTHENTRY_COMPLETED])
				{
					_net_weight = (unsigned long int)(SD_data_buf[RST_NETWEIGHT]-'0') + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+1]-'0') * 10 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+2]-'0') * 100 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+3]-'0') * 1000 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+4]-'0') * 10000 + \
									(unsigned long int)(SD_data_buf[RST_NETWEIGHT+5]-'0') * 100000;
					tot_NET_weight += _net_weight;
				}
						
				//< calculate total_charge
				//< calculating charge
				temp=0;
				while(temp!=5)
				{
					if(SD_data_buf[RST_TOTALCHARGE+temp]==' ')
					{
						break;
					}
					temp++;
				}
				switch(temp)
				{
					case 0:
						_charge = 0;
					case 1:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0');
						break;
					case 2:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0');
						break;
					case 3:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
					case 4:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 1000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
					case 5:
						_charge = (unsigned int)(SD_data_buf[RST_TOTALCHARGE]-'0') * 10000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+1]-'0') * 1000 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 100 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0') * 10 + \
						(unsigned int)(SD_data_buf[RST_TOTALCHARGE+2]-'0');
						break;
				}
				tot_charge += _charge;
			}
		}
	}
	
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	
	//< total nett weight and total charge printing
	printer_writeStringF(PSTR("Total--------->"));//< 15 char
	for(int i=0;i<85;i++)
	{
		printer_writeData(' ');
	}
	//< total net weight printing
	ultoa(tot_NET_weight,_temp_str,10);
	temp = strlen(_temp_str);
	for(i=0;i<10-temp;i++)//< 10 char
	{
		printer_writeData(' ');
	}
	for(i=0;i<temp;i++)
	{
		printer_writeData(_temp_str[i]);
	}
	printer_writeStringF(PSTR(" | "));//< 3 char
	//< total charge printing
	ultoa(tot_charge,_temp_str,10);
	printer_writeString(_temp_str);//< 7 char
	
	PRINTER_ENTER;
	for(int i=0;i<120;i++)
	{
		printer_writeData('-');
	}
	PRINTER_ENTER;
	PRINTER_ENTER;
}

void function_F8()
{
	unsigned int _rst_no;
	unsigned int i;
	
	//< ASK for RST No.
FUNCTION_F8_GET_RST:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter RST No. (For"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Entry Delete): "),0,0);
	lcd_Line2_pointer = 15;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto FUNCTION_F8_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0');
			break;
			case 2:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
			break;
			case 3:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
			break;
			case 4:
			_rst_no = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
			break;
		}
	}
	
	//< now we have rst no.
	//< erase thst rst no entry
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Erasing..."));
	for(i=0;i<512;i++)
	{
		SD_data_buf[i] = 0xFF;
	}
	SD_mega_WriteSingleBlock(SD_data_buf,_rst_no);
	_delay_ms(500);
}

void function_F9()
{
	unsigned int i;

	//< password checking
	if(!check_password())
	{
		return;
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("To ERASE Memory"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENTER"));
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Erasing..."));
	lcd_mega_gotoxy2(0);
	//< write 0xFF in all RST blocks
	for(i=0;i<512;i++)
	{
		SD_data_buf[i] = 0xFF;
	}
	for(i=0;i<10000;i++)
	{
		SD_mega_WriteSingleBlock(SD_data_buf,i);
		if(!(i%500))
		{
			lcd_mega_SendData('~');
		}
	}
	
	//< set RST no to 0
	RST_write(0);
}

void function_F10()
{
	unsigned char c;
	unsigned char str[20];
	unsigned char str_temp[8];
	unsigned char i;
	unsigned char j;
	unsigned int from_rst;
	unsigned int to_rst;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Computer Data Backup"));
	
	lcd_mega_gotoxy2(0);
	while(1)
	{
		c = usart0_ReadData();
//		lcd_mega_SendData(c);
		
		if(c=='s')
		{
			usart0_WriteData('I');
			usart0_WriteData('T');
			//usart0_WriteData(0);
			
			c = usart0_ReadData();
			if(c=='o')
			{
				break;
			}
		}
  	}
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Connected..."));	
	_delay_ms(1000);
	
	i=0;
	while(1)
	{
		c=usart0_ReadData();
		if(c=='o')
		{
			str[i] = c;
			str[i+1]='\0';
			break;
		}
		else
		{
			str[i]=c;
		}
		i++;
	}	
	usart0_WriteData('o');
	
	//< rst number processing
	if(str[0]=='f')
	{
		//< calculate from rst
		j=0;
		while(str[j++]!='t');
		for(i=1;i<j;i++)
		{
			str_temp[i-1] = str[i];
		}
		str_temp[j] = '\0';
		from_rst = atoi(str_temp);
		
		//< calculate to rst
		c=0;
		while(str[c++]!='t');
		j=0;
		while(str[j++]!='o');
		for(i=(c+1);i<j;i++)
		{
			str_temp[(i-(c+1))] = str[i];
		}
		str_temp[j] = '\0';
		to_rst = atoi(str_temp);
		
		lcd_mega_ClearDispaly();
		lcd_mega_String("FromRST= ");
		lcd_mega_String(itoa(from_rst,str_temp,10));
		lcd_mega_gotoxy2(0);
		lcd_mega_String("ToRST= ");
		lcd_mega_String(itoa(to_rst,str_temp,10));
	}
	//< if all rst no selected
	else
	{
		
	}
	
	SD_mega_ReadSingleBlock(SD_data_buf,8);
	for(i=0;i<=121;i++)
	{
		usart0_WriteData(SD_data_buf[i]);
	}
	usart0_WriteData('#');
	_delay_ms(3000);
}
