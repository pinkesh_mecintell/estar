/*
 * time_date_Setting.h
 *
 * Created: 17/09/13 5:57:59 PM
 *  Author: Pinkesh
 */ 


#ifndef SETTING_CONTROLS_H_
#define SETTING_CONTROLS_H_


void setting_time();
void setting_date();
void setting_password();
unsigned char check_password();
void setting_ALT_L();
void setting_header();
void setting_name_change();
void setting_code_system();
//< flag should be - 1 for printing header using ALT+P
//< flag should be - 0 for printing header for print or report purpose
void setting_print_header(unsigned char flag);
void setting_RST_change();
void setting_weightCallibration();
void setting_AutoZero();
void setting_view_ADC_count();

#endif /* TIME_DATE_SETTING_H_ */