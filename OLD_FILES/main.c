/*
 * main.c
 *
 * Created: 03/09/13 2:42:58 PM
 *  Author: Pinkesh
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "utilities.h"
#include "lcd_mega.h"
#include "ps2_kb.h"
#include "spi_mega.h"
#include "SD_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "ADS1230.h"
#include "buzzer.h"
#include "shift_register.h"
#include "function_keys.h"
#include "SD_addresses.h"
#include "printer.h"
#include "usart.h"


/*** Macro Definition ****/


/*************************/

/**** Global Variable ****/
//< sd card buffer
uint8_t SD_data_buf[SD_mega_BLOCK_LEN];
////////////////////

//////////////////
//< ps2 key board related
PS2_KEY ps2_key;
//////////////////////////

//////////////////////////
//< LCD related
char lcd_Line1_data[LCD_LEN+1];
char lcd_Line2_data[LCD_LEN+1];
unsigned char lcd_Line1_pointer;
unsigned char lcd_Line2_pointer;
unsigned char lcd_Line1_max;
unsigned char lcd_Line1_min;
unsigned char lcd_Line2_max;
unsigned char lcd_Line2_min;
unsigned char lcd_CursorLine;
unsigned char LCD_CODE[] = {'0','1','2','3','4','5','6','7','8','9',' ','-'};
///////////////////////////

///////////////////////////
//< RTC related
RTC_DATA RTC_data;
///////////////////////////

///////////////////////////
//< ADS1230 - ADC related
signed long int ADC_count=100;
signed long int ADC_buff[50]={0};
unsigned char ADC_buff_count=0;
///////////////////////////

///////////////////////////
//< internal 7-segment display related
unsigned char SEG_CODE[] = {0x18,0xDE,0x34,0x94,0xD2,0x91,0x11,0xDC,0x10,0x90,0xFF,0xF7};
/////////////////////////////

/////////////////////////////
//< weight related
signed long int weight=0;
unsigned char weight_str[6];//< 0=MSB,5=LSB
WEIGHT_CONSTANTS weight_const;
/////////////////////////////

//////////////////////
//< RST entry related
unsigned int RST_no;
//////////////////////
/*************************/


/**** Function Declaration *****/
void init_main();
/*******************************/


/***** Main Function start *****/
int main()
{
	unsigned long int try;
	unsigned char str[10];
	
	_delay_ms(500);
	init_main();
	_delay_ms(200);
	
	
	//< weight constant testing
/*
	char str[10];
	weight_const.ADC_min_count = -1111;
	WEIGHT_CONSTANTS_Write();
	weight_const.ADC_min_count = 0;
	WEIGHT_CONSTANTS_Read();
	lcd_mega_ClearDispaly();
	lcd_mega_String(ltoa(weight_const.ADC_min_count,str,10));
	while(1);
*/
	
	//< Data Bakup - Serial POrt - USART0 testing
/*
	lcd_mega_ClearDispaly();
	unsigned char c;
	while(1)
	{
 		c = usart0_ReadData();
		lcd_mega_SendData(c);
// 		usart0_WriteData('A');
// 		_delay_ms(1000);
	}
*/
	
	//< RST no testing
/*
	unsigned int _rst_no;
	unsigned char str[8];
	RST_check();
// 	RST_write(1000);
// 	lcd_mega_ClearDispaly();
// 	lcd_mega_StringF(PSTR("Hello"));
// 	while(1);
	_rst_no = RST_read();
	lcd_mega_ClearDispaly();
	lcd_mega_String(utoa(_rst_no,str,10));
	while(1);
*/	
	
	//< internal 7-segment display testing
/*
	//< following code doesn't multiplex display
	//< it only shows the same character on all
	//< digits
	//< using shift register we can create display
	//< which has all the features of multiplexing
	//< i.e. all digits may have different values
	//< but this solution does not do ON single digit
	//< and do OFF all other display rapidly
	//< so that power consumption is high

	unsigned char i=0,j=0;
	while(1)
	{
		for(i=0;i<=10;i++)
		{
// 			//< Erase Display
// 			for(j=0;j<8;j++)
// 			{
// 				shift_register_WriteByte(0xFF);
// 			}
// 			//< enables new display value
// 			shift_register_LatchPulse();
			
			//< write new character
			for(j=0;j<6;j++)
			{
				shift_register_WriteByte(SEG_CODE[i]);
			}
			//< enables new display value
			shift_register_LatchPulse();
		
			_delay_ms(1000);
		}
	}
*/	
	
	//< buzzer testing
/*
	while(1)
	{
		BUZZER_ON;
		BUZZER_TIMER_ON;
		_delay_ms(1000);
	}
*/	
	
	//< ADS1230 testing
/*
	char str[21];
	unsigned char j;
	signed long int ADC_sum;
	
	ADS1230_Calibrate();
	_delay_ms(100);
	while(1)
	{
		lcd_mega_ClearDispaly();
		
		//< get ADS1230 reading and store into buffer
		ADC_count=ADS1230_getReading();
		if(ADC_count!=ADS1230_NAN)
		{
			ADC_buff[ADC_buff_count]=ADC_count;
			lcd_mega_gotoxy1(0);
			lcd_mega_String(ltoa(ADC_buff[ADC_buff_count],str,10));
			ADC_buff_count++;
			if(ADC_buff_count==10)
			{
				ADC_buff_count=0;
			}
		}
		
		//< moving average filtering
		ADC_sum=0;
		for(j=0;j<10;j++)
		{
			ADC_sum+=ADC_buff[j];
		}
		ADC_count = ADC_sum/10;
		
		lcd_mega_gotoxy2(0);
		lcd_mega_String(ltoa(ADC_count,str,10));

		_delay_ms(100);
	}
*/
	
	//< printer testing
/*
	unsigned char count=0;
	_delay_ms(1000);
	_delay_ms(3000);
	lcd_mega_String("Printer testing");
	printer_writeString("hello");
	while(1);
*/	
	
	//< ps2 keyboard testing
/*	
	ps2_kb_start();
	lcd_mega_gotoxy1(0);
	lcd_mega_String("Ps2 Keyboard:");
	
	unsigned char key_count=0;
	char str[8];
	while(1)
	{
		while(!ps2_kb_available_key());
		ps2_kb_getkey(&ps2_key);
		if(ps2_key.type==SPECIAL_KEY)
		{
 			lcd_mega_gotoxy2(0);
			lcd_mega_String(utoa(ps2_key.val,str,16));
		}
		else
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_String("   ");
 			lcd_mega_gotoxy2(0);
			lcd_mega_SendData(ps2_key.val);
		}
		lcd_mega_gotoxy2(8);
		lcd_mega_Showvalue(key_count++);
		_delay_ms(10);
	}
*/	

	//< SD card testing
/*
	unsigned char i;
	unsigned int pat;
	unsigned long int sector_add=7000000;
	SD_mega_CardInfo CardInfo;
	bool match;	
	unsigned char temp_buff[512];
	
	//< SD card initialization
// 	for (i=0; i<10; i++)
// 	{
// 		CardInfo = SD_mega_Initialize();
// 		if(CardInfo.success_init)
// 		{
// 			break;
// 		}
// 	}
// 		
// 	if(CardInfo.success_init==0)
// 	{
// 		lcd_mega_String("No SD");
// 		while(1);
// 	}
// 	else
// 	{
// 		lcd_mega_String("SD Detect");
// 			
// 		lcd_mega_gotoxy1(11);
// 		//< type
// 		if(CardInfo.type==SD_mega_SDHC_SDXC)
// 		{
// 			lcd_mega_String("SDHC");
// 		}
// 		else
// 		{
// 			lcd_mega_String("SD");
// 		}
// 	}

	for(pat=0;pat<512;pat++)
	{
		SD_data_buf[pat]=0x55;
	}
	SD_mega_WriteSingleBlock(SD_data_buf,sector_add);

	SD_mega_ReadSingleBlock(temp_buff,sector_add);
	match=true;
	for(pat=0;pat<512;pat++)
	{
		if(temp_buff[pat]!=SD_data_buf[pat])
		{
			match=false;
		}
	}
	
	lcd_mega_ClearDispaly();
	if(match)
	{
		lcd_mega_StringF(PSTR("Pattern Matched"));
	}
	else
	{
		lcd_mega_StringF(PSTR("Pattern not Matched"));
	}
	while(1);
*/
	
	//< entry using keyboard
	//< and displaying data on lcd
/*
	ps2_kb_start();
	_delay_ms(10);
	clear_lcd_data(LINE_BOTH);
	lcd_mega_ClearDispaly();
	lcd_Line1_pointer=0;
	lcd_mega_gotoxy1(lcd_Line1_pointer);
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}

		//< printable key displaying on lcd
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line1_pointer<LCD_LEN)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
				lcd_Line1_pointer++;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
			}
		}
		
		//< special key processing
		if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line1_pointer>0)
				{
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
					lcd_Line1_data[lcd_Line1_pointer]=' ';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
		
			//< Enter checking
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				lcd_mega_gotoxy2(0);
				lcd_mega_String(lcd_Line1_data);
			}
			
			//< LEFT key checking
			if(ps2_key.val==SPECIAL_KEY_ARROW_LEFT)
			{
				if(lcd_Line1_pointer>0)
				{
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
			
			//< if RIGHT key checking
			if(ps2_key.val==SPECIAL_KEY_ARROW_RIGHT)
			{
				if(lcd_Line1_pointer<LCD_LEN)
				{
					lcd_Line1_pointer++;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}				
			}
		}
	}
*/

	//< DS1307 - testing
/*
	unsigned char second;
	unsigned char minute;
	//< start rtc function of ds1307
	//< if it is not start(i.e. clear bit-7 of register-0)
	while(!ds1307_WriteRegister(0,0));
	
	while(1)
	{
		if(ds1307_ReadRegister(0,&second) && ds1307_ReadRegister(1,&minute))
		{
			lcd_mega_gotoxy1(0);
			lcd_mega_SendData(((minute&0xF0)>>4)+'0');
			lcd_mega_SendData((minute&0x0F)+'0');
			lcd_mega_SendData(':');
			lcd_mega_SendData(((second&0x70)>>4)+'0');
			lcd_mega_SendData((second&0x0F)+'0');
			_delay_ms(10);
		}
		else
		{
			lcd_mega_gotoxy1(0);
			lcd_mega_String("error");
			while(1);
		}		
	}
*/
	
	//< RTC testing
/*
	ps2_kb_start();
	_delay_ms(10);
	if(!ds1307_CheckRTC())
	{
		lcd_mega_ClearDispaly();
		lcd_mega_String("RTC not found");
		while(1);
	}
	while(1)
	{
		//< displaying time/date
		if(ds1307_RefreshRTC_data())
		{
			lcd_mega_gotoxy1(0);
			//< date display
			lcd_mega_String(RTC_data.date);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.month);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.year);

			//< time display
			lcd_mega_gotoxy2(0);
			lcd_mega_String(RTC_data.hour);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.minute);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.second);
		}
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String("RTC Error");
			while(1);
		}
		
		//< get key pressed
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY)
			{
				if(ps2_key.val==SPECIAL_KEY_ALT)
				{
					while(!ps2_kb_available_key());
					ps2_kb_getkey(&ps2_key);
					if(ps2_key.type==NORMAL_KEY)
					{
						switch(ps2_key.val)
						{
							//< time setting
							case 'T':
								lcd_mega_ClearDispaly();
								setting_time();
								lcd_mega_ClearDispaly();
								break;
							
							//< date setting
							case 'D':
								lcd_mega_ClearDispaly();
								setting_date();
								lcd_mega_ClearDispaly();
								break;
						}
					}
					//< if key=special key identifier
					else
					{
						ps2_kb_flush_keybuffer();
					}
				}				
			}
		}
		_delay_ms(100);
	}
*/
	
/***** MAIN CODE START ******/
	//< ps2 keyboard related
	ps2_kb_start();
	_delay_ms(10);
	//< RTC related
	if(!ds1307_CheckRTC())
	{
		lcd_mega_ClearDispaly();
		lcd_mega_String("RTC not found");
		while(1);
	}
	//< EEPROM related
	EEPROM_check();
	
	//< RST NUMBER related
	RST_check();

	//< LCD related
	lcd_normal_display();
	
	//< weight measurement testing
/*	
	setting_weightCallibration();
	_delay_ms(1000);
	
	lcd_mega_ClearDispaly();
	lcd_mega_String(ultoa(weight_const.unit_weight_value,str,10));
	_delay_ms(2000);
	lcd_mega_ClearDispaly();
	lcd_mega_String(ultoa(weight_const.unit_weight_count,str,10));
	_delay_ms(2000);
	lcd_mega_ClearDispaly();
	lcd_mega_String(ltoa(weight_const.ADC_max_count,str,10));
	_delay_ms(2000);
	lcd_mega_ClearDispaly();
	lcd_mega_String(ltoa(weight_const.ADC_min_count,str,10));
	_delay_ms(2000);
	lcd_mega_ClearDispaly();
	lcd_mega_String(ultoa(weight_const.max_weight_value,str,10));
	_delay_ms(2000);
*/	

	while(1)
	{		
		//< displaying time/date
		if(ds1307_RefreshRTC_data())
		{
			lcd_mega_gotoxy2(0);
			//< date display
			lcd_mega_String(RTC_data.date);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.month);
			lcd_mega_SendData('/');
			lcd_mega_String(RTC_data.year);

			//< time display
			lcd_mega_gotoxy2(10);
			lcd_mega_String(RTC_data.hour);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.minute);
			lcd_mega_SendData(':');
			lcd_mega_String(RTC_data.second);
		}
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_String("RTC Error");
			while(1);
		}		
		
		//< getting ADC_count from ADS1230
		ADC_getReading();
		
		//< get current weight from ADC
		ADC_getWeight();
		
		//< displaying weight on LCD
		lcd_mega_gotoxy1(8);
		lcd_mega_SendData(LCD_CODE[weight_str[0]]);//< MSB
		lcd_mega_SendData(LCD_CODE[weight_str[1]]);
		lcd_mega_SendData(LCD_CODE[weight_str[2]]);
		lcd_mega_SendData(LCD_CODE[weight_str[3]]);
		lcd_mega_SendData(LCD_CODE[weight_str[4]]);
		lcd_mega_SendData(LCD_CODE[weight_str[5]]);//< LSB
		lcd_mega_SendData(' ');
		lcd_mega_SendData('K');
		lcd_mega_SendData('G');
		

		//< displaying weight on internal 7-segment display
		shift_register_WriteByte(SEG_CODE[weight_str[5]]);//< LSB
		shift_register_WriteByte(SEG_CODE[weight_str[4]]);
		shift_register_WriteByte(SEG_CODE[weight_str[3]]);
		shift_register_WriteByte(SEG_CODE[weight_str[2]]);
		shift_register_WriteByte(SEG_CODE[weight_str[1]]);
		shift_register_WriteByte(SEG_CODE[weight_str[0]]);//< MSB
		shift_register_LatchPulse();
		
		//< displaying weight on external display
		
		
		//< get key pressed
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			
			////////////////////////////////
			//< Special Key Processing START
			////////////////////////////////
			if(ps2_key.type==SPECIAL_KEY)
			{
				////////////////////////////////
				//< ALT + X key processing START
				////////////////////////////////
				if(ps2_key.val==SPECIAL_KEY_ALT)
				{
					try=0;
					while(!ps2_kb_available_key())
					{
						if((++try)>1000000)
						{
							break;
						}
					}
					if(ps2_kb_getkey(&ps2_key))
					{
						if(ps2_key.type==NORMAL_KEY)
						{
							switch(ps2_key.val)
							{
								//< ALT+Z: AUTO ZERO-Hardware Callibration
								case 'Z':
									//lcd_mega_ClearDispaly();
									//lcd_mega_StringF(PSTR("-AUTO ZERO-"));
									//ADS1230_Calibrate();
									//_delay_ms(1000);
									setting_AutoZero();
									lcd_normal_display();
									break;
								//< ALT+S: RST No. Change
								case 'S':
									setting_RST_change();
									lcd_normal_display();
									break;
								//< ALT+P: printe header for test purpose
								case 'P':
									setting_print_header(1);
									lcd_normal_display();
									break;
								//< ALT+C: code system setting
								case 'C':
									setting_code_system();
									lcd_normal_display();
									break;
								//< ALT+A: header setting
								case 'A':
									setting_header();
									lcd_normal_display();
									break;
								//< ALT+R: name change setting
								case 'R':
									setting_name_change();
									lcd_normal_display();
									break;
								//< ALT+W: password set
								case 'W':
									setting_password();
									lcd_normal_display();
									break;
								//< ALT+L: setting mode
								case 'L':
									setting_ALT_L();
									lcd_normal_display();
									break;
								//< ALT+T: time setting
								case 'T':
									setting_time();
									lcd_normal_display();
									break;
							
								//< ALT+D: date setting
								case 'D':
									setting_date();
									lcd_normal_display();
									break;
								
								//< ALT+Q: to view ADC count
								case 'Q':
									setting_view_ADC_count();
									lcd_normal_display();
									break;
							}
						}
						else if(ps2_key.type==SPECIAL_KEY)
						{
							switch(ps2_key.val)
							{
								//< ALT+F1 : Weight Measurement Calibration
								case SPECIAL_KEY_F1:
									setting_weightCallibration();
									lcd_normal_display();
									break;
							}
						}
					}
				}
				////////////////////////////////
				//< ALT + X key processing END
				////////////////////////////////
				
				//////////////////////////////////
				//< Function key Processing START
				//////////////////////////////////
				//< F1 - First Entry
				else if(ps2_key.val==SPECIAL_KEY_F1)
				{
					function_F1();
					lcd_normal_display();
				}
				/////////////////////
				//< F2 - Second Entry
				else if(ps2_key.val==SPECIAL_KEY_F2)
				{
					function_F2();
					lcd_normal_display();
				}
				////////////////////////////
				//< F3 - View previous entry
				else if(ps2_key.val==SPECIAL_KEY_F3)
				{
					function_F3(1);
					lcd_normal_display();
				}
				////////////////////////////
				//< F4 - Print RST Entry
				else if(ps2_key.val==SPECIAL_KEY_F4)
				{
					function_F4();
					lcd_normal_display();
				}
				////////////////////////////
				//< F5 - Print Manual Tare Entry
				else if(ps2_key.val==SPECIAL_KEY_F5)
				{
					function_F5();
					lcd_normal_display();
				}
				////////////////////////////
				//< F6 - RST Entry Find usign vehicle no
				else if(ps2_key.val==SPECIAL_KEY_F6)
				{
					function_F6();
					lcd_normal_display();
				}
				//< F7 - Report Generate
				else if(ps2_key.val==SPECIAL_KEY_F7)
				{
					function_F7();
					lcd_normal_display();
				}
				//< F8 - Erase RST No. Entry from SD Memory
				else if(ps2_key.val==SPECIAL_KEY_F8)
				{
					function_F8();
					lcd_normal_display();
				}
				//< F9 - Erase Entire SD Memory
				else if(ps2_key.val==SPECIAL_KEY_F9)
				{
					function_F9();
					lcd_normal_display();
				}
				//< F10 - Data Backup in computer
				else if(ps2_key.val==SPECIAL_KEY_F10)
				{
					function_F10();
					lcd_normal_display();
				}
				//////////////////////////////////
				//< Function key Processing END
				//////////////////////////////////
			}
			////////////////////////////////
			//< Special Key Processing END
			////////////////////////////////
		}

//		_delay_ms(100);
	}
	
	
	while(1);
	return 0;
}
/****** Main Function End *****/


/****** Function Defintion *****/
void init_main()
{
	//< lcd initialization
	SET(DDRC,3);//< lcd select pin
	LCD_ON;
	_delay_ms(10);	
	lcd_mega_Initialize();
	//< cursor off
//	lcd_mega_CursorOff();
	//< lcd buffer related
	lcd_Line1_data[LCD_LEN]='\0';
	lcd_Line2_data[LCD_LEN]='\0';

	//< ps2 keyboard initialize
	ps2_kb_Initialize();
	
	//< spi initialization
	spi_mega_Initialize();
	
	//< SD card initialization
	unsigned char i;
	SD_mega_CardInfo CardInfo;
	for (i=0; i<10; i++)
	{
		CardInfo = SD_mega_Initialize();
		if(CardInfo.success_init)
		{
			break;
		}
	}		
	if(CardInfo.success_init==0)
	{
		lcd_mega_StringF(PSTR("Memory Init Fail"));
		_delay_ms(2000);
		//while(1);
	}
	
	//< I2C bus initialization
	//< for RTC-DS1307
	//< pull up SCL/SDA pins
	SET(PORTD,0);//< SCL
	CLR(DDRD,0);
	SET(PORTD,1);//< SDA
	CLR(DDRD,1);
	//< i2c initialization
	I2C_InitMaster();
	//< RTC_data structure variable initializing
	RTC_data.second[2]='\0';
	RTC_data.minute[2]='\0';
	RTC_data.hour[2]='\0';
	RTC_data.date[2]='\0';
	RTC_data.month[2]='\0';
	RTC_data.year[2]='\0';
	
	//< printer related
	printer_init();
	
	//< ADS1231 initialization
	ADS1230_Initialize();
	//< hardware callibration of ADC
	ADS1230_Calibrate();
	//< retrive WEIGHT_CONSTANTS
	WEIGHT_CONSTANTS_Read();
	
	//< buzzer initialization
	buzzer_init();

	//< shift register initialization for internal 7-segment display
	shift_register_Initialize();
	
	//< computer data backup - serial port - usart0 initialize
	usart0_Initialize();
	
	//< global interrupt enable
	sei();
}
