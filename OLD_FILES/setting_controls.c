/*
 * time_date_setting.c
 *
 * Created: 17/09/13 5:59:10 PM
 *  Author: Pinkesh
 */ 
#include <avr/io.h>
#include <util/delay.h>
#include <math.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <string.h>
#include "lcd_mega.h"
#include "lcd_display.h"
#include "i2c.h"
#include "ds1307.h"
#include "ps2_kb.h"
#include "buzzer.h"
#include "utilities.h"
#include "EEPROM.h"
#include "EEPROM_addresses.h"
#include "eeprom_mega.h"
#include "setting_controls.h"
#include "printer.h"
#include "function_keys.h"
#include "ADS1230.h"
#include "shift_register.h"

/****** Macros Definition ****/
//< Password related
#define LEN_PASSWORD_MAX	16
/*****************************/


/*** Extern variables ****/
//////////////////////////
//< LCD related
extern char lcd_Line1_data[];
extern char lcd_Line2_data[];
extern unsigned char lcd_Line1_pointer;
extern unsigned char lcd_Line2_pointer;
extern unsigned char lcd_Line1_max;
extern unsigned char lcd_Line1_min;
extern unsigned char lcd_Line2_max;
extern unsigned char lcd_Line2_min;
extern unsigned char lcd_CursorLine;
extern unsigned char LCD_CODE[];
extern unsigned char SEG_CODE[];
///////////////////////////

//////////////////
//< ps2 key board related
extern PS2_KEY ps2_key;
//////////////////////////

///////////////////////////
//< ADS1230 - ADC related
extern signed long int ADC_count;
extern signed long int ADC_buff[];
extern unsigned char ADC_buff_count;
///////////////////////////

/////////////////////////////
//< weight related
extern signed long int weight;
extern WEIGHT_CONSTANTS weight_const;
/////////////////////////////
/*************************/

void setting_time()
{
	unsigned char hour=0,minute=0;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("-Time Setting-"));
	
	//< hour setting
SET_TIME_HOUR:
	set_string_in_lcd_dataF(LINE2,PSTR("Hour(00-23): "),0,0);
	lcd_Line2_pointer = 13;
	lcd_Line2_max = lcd_Line2_pointer+2;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					hour=0xFF;
					break;
				}
				//< if user has entered only one digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+1))
				{
					hour=lcd_Line2_data[lcd_Line2_min]-'0';
				}
				//< if user has entered two digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+2))
				{
					hour=(lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				}
				//< validate hour
				if(hour>23)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Hour"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME_HOUR;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}			
	
	//< minute setting
SET_TIME_MINUTE:
	clear_lcd_data(LINE2);
	set_string_in_lcd_dataF(LINE2,PSTR("Minute(00-59): "),0,0);
	lcd_Line2_pointer = 15;
	lcd_Line2_max = lcd_Line2_pointer+2;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					minute=0xFF;
					break;
				}
				//< if user has entered only one digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+1))
				{
					minute=lcd_Line2_data[lcd_Line2_min]-'0';
				}
				//< if user has entered two digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+2))
				{
					minute=(lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				}
				//< validate minute
				if(minute>59)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Minute"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME_MINUTE;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without seting time
				return;//< unsuccessful return
			}
		}
	}
		
	//< storing hour data into ds1307-RTC
	//< user has modified hour
	if(hour!=0xFF)
	{
		ds1307_WriteRegister(2,(((hour/10)<<4)+(hour%10)));
	}
	//< storing minute data into ds1307-RTC
	//< user has modified hour
	if(minute!=0xFF)
	{
		ds1307_WriteRegister(1,(((minute/10)<<4)+(minute%10)));
	}
	
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Time Saved"));
	_delay_ms(1000);
	return;//< successful return
}

void setting_date()
{
	unsigned char date=0,month=0,year=0,hour=0,minute=0;
	unsigned char label_length = 5;
	unsigned char D1=label_length,D2=label_length+1,C1=label_length+2,M1=label_length+3,M2=label_length+4,C2=label_length+5,Y1=label_length+6,Y2=label_length+7,Y3=label_length+8,Y4=label_length+9;
	unsigned char H1=label_length,H2=label_length+1;
SET_DATE_DATE:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Date:"));
	lcd_mega_gotoxy2(label_length);
	lcd_mega_StringF(PSTR("DD/MM/YYYY"));
	
	//< hour setting
	lcd_Line1_pointer = label_length;
	lcd_Line1_max = lcd_Line1_pointer+10;
	lcd_Line1_min = lcd_Line1_pointer;
	lcd_CursorLine = LINE1;
	
	//< set cursor position
	lcd_mega_gotoxy1(lcd_Line1_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line1_pointer<lcd_Line1_max)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
				lcd_Line1_pointer++;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
				if (lcd_Line1_pointer==C1 || lcd_Line1_pointer==C2)
				{
					lcd_Line1_data[lcd_Line1_pointer]='/';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_Line1_pointer++;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line1_pointer>lcd_Line1_min)
				{
					if (lcd_Line1_pointer==(C1+1) || lcd_Line1_pointer==(C2+1))
					{
						lcd_Line1_pointer--;
						lcd_mega_gotoxy1(lcd_Line1_pointer);
						lcd_Line1_data[lcd_Line1_pointer]=' ';
						lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
						lcd_mega_gotoxy1(lcd_Line1_pointer);
					}
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
					lcd_Line1_data[lcd_Line1_pointer]=' ';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temporary
				date=(lcd_Line1_data[D1]-'0')*10 + (lcd_Line1_data[D2]-'0');
				month=(lcd_Line1_data[M1]-'0')*10 + (lcd_Line1_data[M2]-'0');
				year=(lcd_Line1_data[Y3]-'0')*10 + (lcd_Line1_data[Y4]-'0');
				//< validate date
				if (lcd_Line1_data[D1]==' ')
				{
					goto SET_TIME;
				}
				if(date>31 || date==0)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Date"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				if(month>12 || month==0)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Month"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				if(year>99)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Year"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_DATE_DATE;
				}
				//< storing date data into ds1307-RTC
				//< user has modified date
				ds1307_WriteRegister(4,(((date/10)<<4)+(date%10)));
				
				//< storing month data into ds1307-RTC
				//< user has modified month
				ds1307_WriteRegister(5,(((month/10)<<4)+(month%10)));
				
				//< storing year data into ds1307-RTC
				//< user has modified year
				ds1307_WriteRegister(6,(((year/10)<<4)+(year%10)));
				
				
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting date
				return;//< unsuccessful return
			}
		}
	}
SET_TIME:
	M1=label_length+3;
	M2=label_length+4;

	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Time:"));
	lcd_mega_gotoxy2(label_length);
	lcd_mega_StringF(PSTR("HH:MM"));
	
	//< hour setting
	lcd_Line1_pointer = label_length;
	lcd_Line1_max = lcd_Line1_pointer+5;
	lcd_Line1_min = lcd_Line1_pointer;
	lcd_CursorLine = LINE1;
	
	//< set cursor position
	lcd_mega_gotoxy1(lcd_Line1_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line1_pointer<lcd_Line1_max)
			{
				lcd_Line1_data[lcd_Line1_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
				lcd_Line1_pointer++;
				lcd_mega_gotoxy1(lcd_Line1_pointer);
				if (lcd_Line1_pointer==C1)
				{
					lcd_Line1_data[lcd_Line1_pointer]=':';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_Line1_pointer++;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line1_pointer>lcd_Line1_min)
				{
					if (lcd_Line1_pointer==(C1+1))
					{
						lcd_Line1_pointer--;
						lcd_mega_gotoxy1(lcd_Line1_pointer);
						lcd_Line1_data[lcd_Line1_pointer]=' ';
						lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
						lcd_mega_gotoxy1(lcd_Line1_pointer);
					}
					lcd_Line1_pointer--;
					lcd_mega_gotoxy1(lcd_Line1_pointer);
					lcd_Line1_data[lcd_Line1_pointer]=' ';
					lcd_mega_SendData(lcd_Line1_data[lcd_Line1_pointer]);
					lcd_mega_gotoxy1(lcd_Line1_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temporary
				hour=(lcd_Line1_data[H1]-'0')*10 + (lcd_Line1_data[H2]-'0');
				minute=(lcd_Line1_data[M1]-'0')*10 + (lcd_Line1_data[M2]-'0');

				//< validate time
				if (lcd_Line1_data[H1]==' ')
				{
					return;
				}
				if(minute>59)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Minute"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME;
				}
				if(hour>23)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Hour"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_TIME;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting date
				return;//< unsuccessful return
			}
		}
	}
	//< storing hour data into ds1307-RTC
	//< user has modified hour
	ds1307_WriteRegister(2,(((hour/10)<<4)+(hour%10)));
	
	//< storing minute data into ds1307-RTC
	//< user has modified minute
	ds1307_WriteRegister(1,(((minute/10)<<4)+(minute%10)));
	
	return;//< successful return
}

void setting_password()
{
	unsigned char i;
	unsigned char pswd_len;
	unsigned char pswd[LEN_PASSWORD_MAX];
	
	//< old password checking
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("-Password Setting-"));
	_delay_ms(1000);
	
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Type Old Password:"));
	
	clear_lcd_data(LINE2);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = LEN_PASSWORD_MAX;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val!=' ')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without checking password
				return;//< unsuccessful return
			}
		}
	}
	
	//< count length of user seted password on lcd
	pswd_len=lcd_Line2_pointer;
	
	//< compare it with stored password len
	if(eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_LEN)==pswd_len)
	{
		//< compare password data with stored data
		while(1)
		{
			//< if there is no more password char then break
			if(pswd_len==0)
			{
				break;
			}
			//< compare each char
			if(lcd_Line2_data[pswd_len-1]!=eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_DATA+pswd_len-1))
			{
				break;
			}
			pswd_len--;
		}
		//< if all password chars are matched
		if(pswd_len==0)
		{
		}
		//< if there is mismatch found
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Password not Matched"));
			_delay_ms(1000);
			return;//< unsuccessful return
		}
	}
	//< if both are not same
	else
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Password not Matched"));
		_delay_ms(1000);
		return;//< unsuccessful return
	}
	
	//< successful password matched
	//< take new password
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Type New Password:"));
	
	clear_lcd_data(LINE2);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = LEN_PASSWORD_MAX;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val!=' ')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without settinging password
				return;//< unsuccessful return
			}
		}
	}
	
	//< store password and password len temperory
	pswd_len=lcd_Line2_pointer;
	i=0;
	while(i<LEN_PASSWORD_MAX)
	{
		pswd[i]=lcd_Line2_data[i];
		i++;
	}
	
	//< take new password again
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("ReType New Password:"));
		
	clear_lcd_data(LINE2);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = LEN_PASSWORD_MAX;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
		
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val!=' ')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without settinging password
				return;//< unsuccessful return
			}
		}
	}
	
	//< check both new password entered by user
	//< if inavalid len find
	if(lcd_Line2_pointer!=pswd_len)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Type and ReType"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Password Mismatch"));
		_delay_ms(1000);
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Password not Saved"));
		_delay_ms(1000);
		return; //< unsuccessful return
	}
	//< check password data
	for(i=0;i<LEN_PASSWORD_MAX;i++)
	{
		if(pswd[i]!=lcd_Line2_data[i])
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Type and ReType"));
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Password Mismatch"));
			_delay_ms(1000);
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Password not Saved"));
			_delay_ms(1000);
			return; //< unsuccessful return
		}
	}
	
	//< if both password matched then store them EEPROM
	eeprom_mega_WriteByte(ADD_PASSWORD,0xAA);//< key setting in copy-1
	//< password writing in copy-1
	//< password length
	eeprom_mega_WriteByte(ADD_PASSWORD+PASSWORD_LEN,pswd_len);
	//< save password into EEPROM
	for(i=0;i<LEN_PASSWORD_MAX;i++)
	{
		if(pswd[i]==' ')
		{
			eeprom_mega_WriteByte(ADD_PASSWORD+PASSWORD_DATA+i,'\0');
		}
		else
		{
			eeprom_mega_WriteByte(ADD_PASSWORD+PASSWORD_DATA+i,pswd[i]);
		}		
	}

	eeprom_mega_WriteByte(COPY_OFFSET+ADD_PASSWORD,0xAA);//< key setting in copy-2
	//< password writing in copy-2
	//< password length
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_PASSWORD+PASSWORD_LEN,pswd_len);
	//< save password into EEPROM
	for(i=0;i<LEN_PASSWORD_MAX;i++)
	{
		if(pswd[i]==' ')
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_PASSWORD+PASSWORD_DATA+i,'\0');
		}
		else
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_PASSWORD+PASSWORD_DATA+i,pswd[i]);
		}
	}
	
	//< set copy-1's key to 0
	eeprom_mega_WriteByte(ADD_PASSWORD,0);
	//< set copy-2's key to 0
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_PASSWORD,0);

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Password Saved"));
	_delay_ms(1000);
	return;
}

unsigned char check_password()
{
	unsigned char pswd_len;
	
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("-Enter Password-"));
	
	clear_lcd_data(LINE2);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = LEN_PASSWORD_MAX;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val!=' ')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without checking password
				return 0;//< unsuccessful return
			}
		}
	}
		
	//< count length of user seted password on lcd
	pswd_len=lcd_Line2_pointer;
		
	//< compare it with stored password len
	if(eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_LEN)==pswd_len)
	{
		//< compare password data with stored data
		while(1)
		{
			//< if there is no more password char then break
			if(pswd_len==0)
			{
				break;
			}
			//< compare each char
			if(lcd_Line2_data[pswd_len-1]!=eeprom_mega_ReadByte(ADD_PASSWORD+PASSWORD_DATA+pswd_len-1))
			{
				break;
			}
			pswd_len--;
		}
		//< if all password chars are matched
		if(pswd_len==0)
		{
			return 1;
		}
		//< if there is mismatch found
		else
		{
			lcd_mega_ClearDispaly();
			lcd_mega_StringF(PSTR("Password not Matched"));
			_delay_ms(1000);
			return 0;
		}
	}
	//< if both are not same
	else
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Password not Matched"));
		_delay_ms(1000);
		return 0;
	}
	
	//< successful return
	return 1;
}

void setting_ALT_L()
{
	unsigned char header_pos,charge_type,line_feed,first_slip_copy,second_slip_copy;
	unsigned char date_time_use,code_enable;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	//< Header position
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Header Position:"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("(No/Top/Bottom): "),0,0);
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	header_pos = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_HEADER_POSITION);
	lcd_Line2_data[lcd_Line2_pointer] = header_pos;
	lcd_mega_SendData(header_pos);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='N' || ps2_key.val=='T' || ps2_key.val=='B'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);				
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				header_pos = lcd_Line2_data[lcd_Line2_pointer];
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}

	
	//< Charge Type
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Charge Type(No/First"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("/Second/Both): "),0,0);
	lcd_Line2_pointer = 15;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	charge_type = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_CHARGE_TYPE);
	lcd_Line2_data[lcd_Line2_pointer] = charge_type;
	lcd_mega_SendData(charge_type);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val=='N' || ps2_key.val=='F' || ps2_key.val=='S' || ps2_key.val=='B'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				charge_type = lcd_Line2_data[lcd_Line2_pointer];
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}

	
	//< Line Feed after slip print
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Line Feed After Slip"));
	
SET_ALT_L_LINE_FEED:
	clear_lcd_data(LINE2);
	set_string_in_lcd_dataF(LINE2,PSTR("Print(0-20): "),0,0);
	lcd_Line2_pointer = 13;
	lcd_Line2_max = lcd_Line2_pointer+2;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored date from EEPROM
	line_feed = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_LINE_FEED);
	if(line_feed>9)
	{
		lcd_mega_SendData((line_feed/10)+'0');
		lcd_mega_SendData((line_feed%10)+'0');
		lcd_Line2_data[lcd_Line2_pointer++]=(line_feed/10)+'0';
		lcd_Line2_data[lcd_Line2_pointer++]=(line_feed%10)+'0';
	}
	else
	{
		lcd_mega_SendData(line_feed+'0');
		lcd_Line2_data[lcd_Line2_pointer++]=(line_feed+'0');
	}		
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val<='9' && ps2_key.val>='0')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					line_feed=0xFF;
				}
				//< if user has entered only one digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+1))
				{
					line_feed=lcd_Line2_data[lcd_Line2_min]-'0';
				}
				//< if user has entered two digit
				else if(lcd_Line2_pointer==(lcd_Line2_min+2))
				{
					line_feed=(lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
				}
				//< validate line feed
				if(line_feed>20)
				{
					//< if invalid entry found
					//< display appropriate message
					clear_lcd_data(LINE2);
					set_string_in_lcd_dataF(LINE2,PSTR("Invalid Line Feed"),0,0);
					lcd_mega_gotoxy2(0);
					lcd_mega_String(lcd_Line2_data);
					_delay_ms(1000);
					goto SET_ALT_L_LINE_FEED;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}

	
	//< Copy in First Slip
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("No. of Copies in"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("First Slip(0-9): "),0,0);
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	first_slip_copy = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES);
	lcd_Line2_data[lcd_Line2_pointer]=first_slip_copy+'0';
	lcd_mega_SendData(first_slip_copy+'0');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				first_slip_copy = lcd_Line2_data[lcd_Line2_pointer]-'0';
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}


	//< Copy in Second Slip
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("No. of Copies in"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Second Slip(0-9): "),0,0);
	lcd_Line2_pointer = 18;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	second_slip_copy = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES);
	lcd_Line2_data[lcd_Line2_pointer]=second_slip_copy+'0';
	lcd_mega_SendData(second_slip_copy+'0');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				second_slip_copy = lcd_Line2_data[lcd_Line2_pointer]-'0';
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}

	//< Date and Time use in ptint or not
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Date and Time Use"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("in Print(Yes/No): "),0,0);
	lcd_Line2_pointer = 18;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	date_time_use = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT);
	lcd_Line2_data[lcd_Line2_pointer]=date_time_use;
	lcd_mega_SendData(date_time_use);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val>='Y' || ps2_key.val<='N'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				date_time_use = lcd_Line2_data[lcd_Line2_pointer];
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	//< code based system enable or not
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code based System"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Enable(Yes/No): "),0,0);
	lcd_Line2_pointer = 16;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	code_enable = eeprom_mega_ReadByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM);
	lcd_Line2_data[lcd_Line2_pointer]=code_enable;
	lcd_mega_SendData(code_enable);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && (ps2_key.val>='Y' || ps2_key.val<='N'))
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				code_enable = lcd_Line2_data[lcd_Line2_pointer];
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	//< all the data is seted by user
	//< now store that data into EEPROM
	eeprom_mega_WriteByte(ADD_ALT_L,0xAA);//< key setting in copy-1
	//< data writing in copy-1
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_HEADER_POSITION,header_pos);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_CHARGE_TYPE,charge_type);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_LINE_FEED,line_feed);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES,first_slip_copy);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES,second_slip_copy);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT,date_time_use);
	eeprom_mega_WriteByte(ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM,code_enable);

	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L,0xAA);//< key setting in copy-2
	//< data writing in copy-2
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_HEADER_POSITION,header_pos);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_CHARGE_TYPE,charge_type);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_LINE_FEED,line_feed);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_FIRST_SLIP_COPIES,first_slip_copy);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_SECOND_SLIP_COPIES,second_slip_copy);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_USE_TIME_DATE_IN_PRINT,date_time_use);
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L+ALT_L_ENABLE_CODE_SYSTEM,code_enable);
	
	//< set copy-1's key to 0
	eeprom_mega_WriteByte(ADD_ALT_L,0);
	//< set copy-2's key to 0
	eeprom_mega_WriteByte(COPY_OFFSET+ADD_ALT_L,0);

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Settings Saved"));
	_delay_ms(1000);	
}

void setting_header()
{
	unsigned char save_line;
	unsigned char line_len;
	unsigned char i;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	//< header line1 setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Header Set-LINE1:"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 20;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_LEN);
	for(i=0;i<line_len;i++)
	{
		//< save_line used for teperory storage
		save_line=eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_DATA+i);
		lcd_mega_gotoxy2(i);
		lcd_mega_SendData(save_line);
		lcd_Line2_data[lcd_Line2_pointer++]=save_line;
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_line=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_line=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save_line)
	{
		eeprom_mega_WriteByte(ADD_HEADER_LINE1,0xAA);//< key setting in copy-1
		//< header line1 writing in copy-1
		//< header length
		eeprom_mega_WriteByte(ADD_HEADER_LINE1+HEADER_LINE1_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(ADD_HEADER_LINE1+HEADER_LINE1_DATA+i,lcd_Line2_data[i]);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE1,0xAA);//< key setting in copy-2
		//< header line1 writing in copy-2
		//< header length
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE1+HEADER_LINE1_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE1+HEADER_LINE1_DATA+i,lcd_Line2_data[i]);
		}
			
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_HEADER_LINE1,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE1,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}	
	
	//< header line2 setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Header Set-LINE2:"));
		
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 20;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
		
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_LEN);
	for(i=0;i<line_len;i++)
	{
		//< save_line used for teperory storage
		save_line=eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_DATA+i);
		lcd_mega_gotoxy2(i);
		lcd_mega_SendData(save_line);
		lcd_Line2_data[lcd_Line2_pointer++]=save_line;
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_line=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_line=0;
				break;
			}
		}
	}
		
	//< if user pressed ENTER then save
	if(save_line)
	{
		eeprom_mega_WriteByte(ADD_HEADER_LINE2,0xAA);//< key setting in copy-1
		//< header line2 writing in copy-1
		//< header length
		eeprom_mega_WriteByte(ADD_HEADER_LINE2+HEADER_LINE2_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(ADD_HEADER_LINE2+HEADER_LINE2_DATA+i,lcd_Line2_data[i]);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE2,0xAA);//< key setting in copy-2
		//< header line2 writing in copy-2
		//< header length
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE2+HEADER_LINE2_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE2+HEADER_LINE2_DATA+i,lcd_Line2_data[i]);
		}
			
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_HEADER_LINE2,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE2,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
	
	//< header line3 setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Header Set-LINE3:"));
		
	lcd_Line2_pointer = 0;
	lcd_Line2_max = 20;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
		
	//< showing stored data from EEPROM
	line_len=eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_LEN);
	for(i=0;i<line_len;i++)
	{
		//< save_line used for temperory storage
		save_line=eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_DATA+i);
		lcd_mega_gotoxy2(i);
		lcd_mega_SendData(save_line);
		lcd_Line2_data[lcd_Line2_pointer++]=save_line;
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_line=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_line=0;
				break;
			}
		}
	}
		
	//< if user pressed ENTER then save
	if(save_line)
	{
		eeprom_mega_WriteByte(ADD_HEADER_LINE3,0xAA);//< key setting in copy-1
		//< header line3 writing in copy-1
		//< header length
		eeprom_mega_WriteByte(ADD_HEADER_LINE3+HEADER_LINE3_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(ADD_HEADER_LINE3+HEADER_LINE3_DATA+i,lcd_Line2_data[i]);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE3,0xAA);//< key setting in copy-2
		//< header line3 writing in copy-2
		//< header length
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE3+HEADER_LINE3_LEN,lcd_Line2_pointer);
		//< save header string into EEPROM
		for(i=0;i<20;i++)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE3+HEADER_LINE3_DATA+i,lcd_Line2_data[i]);
		}
			
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_HEADER_LINE3,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_HEADER_LINE3,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
}

void setting_name_change()
{
	unsigned char save_name;
	unsigned char i;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	//< supplier name change setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Name Change-In Place"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Supplier: "),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+8;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	//< save_name used here temprory
	save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT);
	if(!save_name)
	{
		for(i=0;i<8;i++)
		{
			save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i);
			if(save_name==' ')
			{
				break;
			}
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save_name);
			lcd_Line2_data[lcd_Line2_pointer++]=save_name;
		}
	}		

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max && ps2_key.val!=' ')
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_name=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_name=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save_name)
	{
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_SUPPLIER,0xAA);//< key setting in copy-1
		//< supplier name change writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SUPPLIER,0xAA);//< key setting in copy-2
		//< supplier name change writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}

		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SUPPLIER+NAME_CHANGE_SUPPLIER_IS_PRESENT,1);
		}
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_SUPPLIER,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SUPPLIER,0);

		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Saved                 "));
		}
		else
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Change to Default     "));
		}
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
	
	//< source name change setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Name Change-In Place"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Source  : "),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+8;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	//< save_name used here temprory
	save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT);
	if(!save_name)
	{
		for(i=0;i<8;i++)
		{
			save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i);
			if(save_name==' ')
			{
				break;
			}
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save_name);
			lcd_Line2_data[lcd_Line2_pointer++]=save_name;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max && ps2_key.val!=' ')
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_name=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_name=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save_name)
	{
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_SOURCE__,0xAA);//< key setting in copy-1
		//< source name change writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SOURCE__,0xAA);//< key setting in copy-2
		//< supplier name change writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}

		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SOURCE__+NAME_CHANGE_SOURCE___IS_PRESENT,1);
		}
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_SOURCE__,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_SOURCE__,0);

		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Saved                 "));
		}
		else
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Change to Default     "));
		}
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
	
	//< material name change setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Name Change-In Place"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("Material: "),0,0);
	lcd_Line2_pointer = 10;
	lcd_Line2_max = lcd_Line2_pointer+8;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	//< save_name used here temprory
	save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT);
	if(!save_name)
	{
		for(i=0;i<8;i++)
		{
			save_name=eeprom_mega_ReadByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i);
			if(save_name==' ')
			{
				break;
			}
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save_name);
			lcd_Line2_data[lcd_Line2_pointer++]=save_name;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max && ps2_key.val!=' ')
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save_name=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save_name=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save_name)
	{
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_MATERIAL,0xAA);//< key setting in copy-1
		//< material name change writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_MATERIAL,0xAA);//< key setting in copy-2
		//< material name change writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT,0);
			//< save supplier name change string into EEPROM
			for(i=0;i<8;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_DATA+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< default name
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_MATERIAL+NAME_CHANGE_MATERIAL_IS_PRESENT,1);
		}
		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_NAME_CHANGE_MATERIAL,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_NAME_CHANGE_MATERIAL,0);

		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Saved                 "));
		}
		else
		{
			lcd_mega_gotoxy2(0);
			lcd_mega_StringF(PSTR("Change to Default     "));
		}
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
}

void setting_code_system()
{
	unsigned char code_no;
	unsigned char is_present;
	unsigned char len;
	unsigned char i;
	unsigned char save;
	
	//< password checking
	if(!check_password())
	{
		return;
	}

CODE_SETTING:	
	//< get code no to be set
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Enter Code No(0-9):"));
	
	//< no string in second line
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val<='9' && ps2_key.val>='0')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				code_no = lcd_Line2_data[lcd_Line2_pointer]-'0';
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	
	//< code-X supplier name setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code X - Supplier:"));
	lcd_mega_gotoxy1(5);
	lcd_mega_SendData(code_no+'0');
	
	//< no string in second line
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	is_present=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*code_no);
	if(!is_present)
	{
		len = eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*code_no);
		for(i=0;i<len;i++)
		{
			//< save used here temperory
			save=eeprom_mega_ReadByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*code_no+i);
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save);
			lcd_Line2_data[lcd_Line2_pointer++]=save;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save)
	{
		eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_OFFSET*code_no,0xAA);//< key setting in copy-1
		//< supplier writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_OFFSET*code_no,0xAA);//< key setting in copy-2
		//< supplier writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_0_SUPPLIER_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_CODE_0_SUPPLIER+CODE_OFFSET*code_no,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SUPPLIER+CODE_OFFSET*code_no,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}

	//< code-X source name setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code X - Source:"));
	lcd_mega_gotoxy1(5);
	lcd_mega_SendData(code_no+'0');
	
	//< no string in second line
	//set_string_in_lcd_dataF(LINE2,PSTR("Supplier: "),0,0);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	is_present=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*code_no);
	if(!is_present)
	{
		len = eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*code_no);
		for(i=0;i<len;i++)
		{
			//< save used here temperory
			save=eeprom_mega_ReadByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*code_no+i);
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save);
			lcd_Line2_data[lcd_Line2_pointer++]=save;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save)
	{
		eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_OFFSET*code_no,0xAA);//< key setting in copy-1
		//< source writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_OFFSET*code_no,0xAA);//< key setting in copy-2
		//< source writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_0_SOURCE___LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_0_SOURCE___DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_0_SOURCE___IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_CODE_0_SOURCE__+CODE_OFFSET*code_no,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_SOURCE__+CODE_OFFSET*code_no,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}
 
	//< code-X Material name setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code X - Material:"));
	lcd_mega_gotoxy1(5);
	lcd_mega_SendData(code_no+'0');
		
	//< no string in second line
	//set_string_in_lcd_dataF(LINE2,PSTR("Supplier: "),0,0);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
		
	//< showing stored data from EEPROM
	is_present=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*code_no);
	if(!is_present)
	{
		len = eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*code_no);
		for(i=0;i<len;i++)
		{
			//< save used here temperory
			save=eeprom_mega_ReadByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*code_no+i);
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save);
			lcd_Line2_data[lcd_Line2_pointer++]=save;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save=0;
				break;
			}
		}
	}
		
	//< if user pressed ENTER then save
	if(save)
	{
		eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_OFFSET*code_no,0xAA);//< key setting in copy-1
		//< material writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_OFFSET*code_no,0xAA);//< key setting in copy-2
		//< source writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save supplier into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for supplier
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_0_MATERIAL_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_CODE_0_MATERIAL+CODE_OFFSET*code_no,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_MATERIAL+CODE_OFFSET*code_no,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}

	//< code-X vehicle setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code X - Vehicle No:"));
	lcd_mega_gotoxy1(5);
	lcd_mega_SendData(code_no+'0');
	
	//< no string in second line
	//set_string_in_lcd_dataF(LINE2,PSTR("Supplier: "),0,0);
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	is_present=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*code_no);
	if(!is_present)
	{
		len = eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*code_no);
		for(i=0;i<len;i++)
		{
			//< save used here temperory
			save=eeprom_mega_ReadByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*code_no+i);
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save);
			lcd_Line2_data[lcd_Line2_pointer++]=save;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save)
	{
		eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_OFFSET*code_no,0xAA);//< key setting in copy-1
		//< vehicle no writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save vehicle no into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for vehicle no
			eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_OFFSET*code_no,0xAA);//< key setting in copy-2
		//< vehicle no writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save vehicle into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for vehicle no
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_0_VEHICLE_NO_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_CODE_0_VEHICLE_NO+CODE_OFFSET*code_no,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_VEHICLE_NO+CODE_OFFSET*code_no,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}


	//< code-X address name setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Code X - Address:"));
	lcd_mega_gotoxy1(5);
	lcd_mega_SendData(code_no+'0');
	
	//< no string in second line
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+12;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< showing stored data from EEPROM
	is_present=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*code_no);
	if(!is_present)
	{
		len = eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*code_no);
		for(i=0;i<len;i++)
		{
			//< save used here temperory
			save=eeprom_mega_ReadByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*code_no+i);
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(save);
			lcd_Line2_data[lcd_Line2_pointer++]=save;
		}
	}

	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< break with saving
				save=1;
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< break without saving
				save=0;
				break;
			}
		}
	}
	
	//< if user pressed ENTER then save
	if(save)
	{
		eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_OFFSET*code_no,0xAA);//< key setting in copy-1
		//< address writing in copy-1
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save address into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for address
			eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_OFFSET*code_no,0xAA);//< key setting in copy-2
		//< address writing in copy-2
		//< is present parameter
		if(lcd_Line2_pointer>lcd_Line2_min)
		{
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*code_no,0);
			//< lenght store
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_LEN+CODE_OFFSET*code_no,lcd_Line2_pointer);
			//< save address into EEPROM
			for(i=0;i<lcd_Line2_pointer;i++)
			{
				eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_DATA+CODE_OFFSET*code_no+i,lcd_Line2_data[lcd_Line2_min+i]);
			}
		}
		else
		{
			//< no code for address
			eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_0_ADDRESS_IS_PRESENT+CODE_OFFSET*code_no,1);
		}

		//< set copy-1's key to 0
		eeprom_mega_WriteByte(ADD_CODE_0_ADDRESS+CODE_OFFSET*code_no,0);
		//< set copy-2's key to 0
		eeprom_mega_WriteByte(COPY_OFFSET+ADD_CODE_0_ADDRESS+CODE_OFFSET*code_no,0);

		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Saved                 "));
		_delay_ms(1000);
	}
	//< if user pressed ESC then not save
// 	else
// 	{
// 		lcd_mega_gotoxy2(0);
// 		lcd_mega_StringF(PSTR("Change not Saved      "));
// 		_delay_ms(1000);
// 	}


	goto CODE_SETTING;
}

void setting_print_header(unsigned char flag)
{
	char header_line1[21];
	char header_line2[21];
	char header_line3[21];
	unsigned char len;
	unsigned char i;
	
	if(flag)
	{
		//< password checking
		if(!check_password())
		{
			return;
		}
		
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Header Printing"));
	}
	
	//< header_line1 reading from EEPROM
	//< line1 string len
	len = eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_LEN);
	for(i=0;i<len;i++)
	{
		header_line1[i] = eeprom_mega_ReadByte(ADD_HEADER_LINE1+HEADER_LINE1_DATA+i);
	}
	header_line1[len]='\0';
	
	//< header_line2 reading from EEPROM
	//< line2 string len
	len = eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_LEN);
	for(i=0;i<len;i++)
	{
		header_line2[i] = eeprom_mega_ReadByte(ADD_HEADER_LINE2+HEADER_LINE2_DATA+i);
	}
	header_line2[len]='\0';
	
	//< header_line3 reading from EEPROM
	//< line3 string len
	len = eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_LEN);
	for(i=0;i<len;i++)
	{
		header_line3[i] = eeprom_mega_ReadByte(ADD_HEADER_LINE3+HEADER_LINE3_DATA+i);
	}
	header_line3[len]='\0';
	
	//< Printing strats
	PRINTER_ENTER;

	PRINTER_ESC;
	PRINTER_SET_BOLD;

	for(i=0;i<20;i++)
	{
		printer_writeData(' ');
	}

	PRINTER_ESC;
	PRINTER_SET_HEADER;
	printer_writeString(header_line1);

	PRINTER_ENTER;

	for(i=0;i<20;i++)
	{
		printer_writeData(' ');
	}

	PRINTER_ESC;
	PRINTER_SET_HEADER;
	printer_writeString(header_line2);

	PRINTER_ENTER;

	for(i=0;i<20;i++)
	{
		printer_writeData(' ');
	}

	PRINTER_ESC;
	PRINTER_SET_HEADER;
	printer_writeString(header_line3);

	PRINTER_ENTER;
	PRINTER_ENTER;

	PRINTER_ESC;
	PRINTER_CANCEL_HEADER;

	PRINTER_ESC;
	PRINTER_CANCEL_BOLD;
}

void setting_RST_change()
{
	unsigned int cur_rst;
	unsigned int new_rst;
	unsigned char temp;
	
	//< password checking
	if(!check_password())
	{
		return;
	}

SETTING_RST_CHANGE_GET_RST:	
	//< RST No. change setting
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("New RST No.(Greater"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("than XXXX) : "),0,0);
	lcd_Line2_pointer = 13;
	lcd_Line2_max = lcd_Line2_pointer+4;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	
	//< showing Current rst no.
	cur_rst = RST_read();
	lcd_mega_gotoxy2(5);
	lcd_mega_SendData((cur_rst/1000)+'0');
	lcd_Line2_data[5]=(cur_rst/1000)+'0';
	
	lcd_mega_SendData(((cur_rst%1000)/100)+'0');
	lcd_Line2_data[6]=((cur_rst%1000)/100)+'0';
	
	lcd_mega_SendData(((cur_rst%100)/10)+'0');
	lcd_Line2_data[7]=((cur_rst%100)/10)+'0';
	
	lcd_mega_SendData((cur_rst%10)+'0');
	lcd_Line2_data[8]=(cur_rst%10)+'0';
			
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY)
		{
			if(lcd_Line2_pointer<lcd_Line2_max && (ps2_key.val>='0' || ps2_key.val<='9'))
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				return;
			}
		}
	}
	
	//< if user not inputed anything then
	if(lcd_Line2_pointer==lcd_Line2_min)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid RST No."));
		_delay_ms(1000);
		goto SETTING_RST_CHANGE_GET_RST;
	}
	//< if user has inputed RST no. then
	else
	{
		//< create RST no from user input
		switch(lcd_Line2_pointer-lcd_Line2_min)
		{
			case 1:
			new_rst = (lcd_Line2_data[lcd_Line2_min]-'0');
			break;
			case 2:
			new_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');
			break;
			case 3:
			new_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+2]-'0');
			break;
			case 4:
			new_rst = (lcd_Line2_data[lcd_Line2_min]-'0')*1000 + (lcd_Line2_data[lcd_Line2_min+1]-'0')*100 + (lcd_Line2_data[lcd_Line2_min+2]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+3]-'0');
			break;
		}
	}
	
	//< now we have new rst no.
	//< check it is greater than current rst no.
	if(new_rst>cur_rst)
	{
		//< then save new rst no.
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("New RST No. Saved"));
		RST_write(new_rst);
	}
	else
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("New RST No. MUST"));
		lcd_mega_gotoxy2(0);
		lcd_mega_StringF(PSTR("Greater than "));
		temp=1;
		if((cur_rst/1000)==0 && temp)
		{
			lcd_mega_gotoxy2(13);
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_gotoxy2(13);
			lcd_mega_SendData((cur_rst/1000)+'0');
			temp=0;
		}
		if(((cur_rst%1000)/100)==0 && temp)
		{
			lcd_mega_gotoxy2(14);
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_gotoxy2(14);
			lcd_mega_SendData(((cur_rst%1000)/100)+'0');
			temp=0;
		}
		if(((cur_rst%100)/10)==0 && temp)
		{
			lcd_mega_gotoxy2(15);
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_gotoxy2(15);
			lcd_mega_SendData(((cur_rst%100)/10)+'0');
			temp=0;
		}
		if((cur_rst%10)==0 && temp)
		{
			lcd_mega_gotoxy2(16);
			lcd_mega_SendData(' ');
		}
		else
		{
			lcd_mega_gotoxy2(16);
			lcd_mega_SendData((cur_rst%10)+'0');
			temp=0;
		}
		_delay_ms(1000);
		goto SETTING_RST_CHANGE_GET_RST;
	}
}

void setting_weightCallibration()
{
	unsigned long int capacity;
	unsigned char max_steps;
	unsigned char division;
	signed long int weight_zero_ADC_count;
	signed long int unit_weight_count1;
	signed long int unit_weight_count2;
	unsigned long int steps_weight[5]={0};
	signed long int steps_ADC_count[5]={0};
	double steps_slope[5]={0};
	double steps_distace[5]={0};
	double slope_nume=0.0;
	double slope_denom=0.0;
	double slope;
	unsigned char i;
	
	//< password checking
	if(!check_password())
	{
		return;
	}
	
	//< Capacity inputing
SET_WEIGHT_CALIB_CAPACITY:	
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("CAPACITY (in KG):"));
	
	lcd_Line2_pointer = 0;
	lcd_Line2_max = lcd_Line2_pointer+6;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					lcd_mega_ClearDispaly();
					lcd_mega_StringF(PSTR("Invalid Entry"));
					goto SET_WEIGHT_CALIB_CAPACITY;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				return;//< unsuccessful return
			}
		}
	}
	lcd_Line2_data[lcd_Line2_pointer] = '\0';
	capacity = (unsigned long int)(atol(&lcd_Line2_data[lcd_Line2_min]));
	
	
	//< MAX_STEP inputing
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("MAX_STEPS (1-5):"));
	
	lcd_Line2_pointer = 17;
	lcd_Line2_max = lcd_Line2_pointer+1;
	lcd_Line2_min = lcd_Line2_pointer;
	lcd_CursorLine = LINE2;
	
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);

	//< showing stored data from EEPROM
	lcd_Line2_data[lcd_Line2_pointer] = '1';
	lcd_mega_SendData('1');
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
		}
		//< normal key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='1' && ps2_key.val<='5')
		{
			////
			lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
			lcd_mega_gotoxy2(lcd_Line2_pointer);
			lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
			////
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< enter key processing
			if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				//< returns without setting
				return;//< unsuccessful return
			}
		}
	}
	max_steps = lcd_Line2_data[lcd_Line2_pointer] - '0';
	
	
	//< Division Inputing
SET_DIVISION:
	lcd_mega_ClearDispaly();
	clear_lcd_data(LINE_BOTH);
	lcd_mega_StringF(PSTR("Division :"));
	
	set_string_in_lcd_dataF(LINE2,PSTR("5"),0,0);
	lcd_Line2_pointer = 1;
	lcd_Line2_max = 2;
	lcd_Line2_min = 0;
	lcd_CursorLine = LINE2;
	//< set lcd disply
	lcd_mega_gotoxy2(0);
	lcd_mega_String(lcd_Line2_data);
	//< set cursor position
	lcd_mega_gotoxy2(lcd_Line2_pointer);
	
	//< take user input
	while(1)
	{
		//< get key pressed
		while(1)
		{
			if(ps2_kb_available_key())
			{
				ps2_kb_getkey(&ps2_key);
				break;
			}
			ADC_getReading();
		}
		//< numeric key processing
		if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
		{
			if(lcd_Line2_pointer<lcd_Line2_max)
			{
				lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
				lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
				lcd_Line2_pointer++;
				lcd_mega_gotoxy2(lcd_Line2_pointer);
			}
		}
		//< special key processing
		else if(ps2_key.type==SPECIAL_KEY)
		{
			//< backspace checking
			if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
			{
				if(lcd_Line2_pointer>lcd_Line2_min)
				{
					lcd_Line2_pointer--;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
					lcd_Line2_data[lcd_Line2_pointer]=' ';
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< enter key processing
			else if(ps2_key.val==SPECIAL_KEY_ENTER)
			{
				//< store user input temperory
				//< if user has entered without typing anything
				if(lcd_Line2_pointer==lcd_Line2_min)
				{
					lcd_mega_ClearDispaly();
					lcd_mega_StringF(PSTR("Invalid Entry"));
					_delay_ms(1000);
					goto SET_DIVISION;
				}
				break;
			}
			//< ESC key processing
			else if(ps2_key.val==SPECIAL_KEY_ESC)
			{
				return;//< unsuccessful return
			}
		}
	}
	if(lcd_Line2_pointer==(lcd_Line2_min+1))
	{
		division = lcd_Line2_data[lcd_Line2_min]-'0';
	}
	else if(lcd_Line2_pointer==(lcd_Line2_min+2))
	{
		division = (lcd_Line2_data[lcd_Line2_min]-'0')*10 + (lcd_Line2_data[lcd_Line2_min+1]-'0');;
	}
	if(division==0)
	{
		lcd_mega_ClearDispaly();
		lcd_mega_StringF(PSTR("Invalid Entry"));
		_delay_ms(1000);
		goto SET_DIVISION;
	}
	
	//< ZERO weight ADC_count calculation
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Clear LoadCell"));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENTER"));
	
	//< stay here till user not pressed ENTER
	//< get key pressed
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				break;
			}
			if(ps2_key.val==SPECIAL_KEY_ESC && ps2_key.type==SPECIAL_KEY)
			{
				return;
			}
		}
		ADC_getReading();
	}
	weight_zero_ADC_count = ADC_count;
	
	
	//< STEPS : USER enters weight
	for(i=0;i<max_steps;i++)
	{
SET_WEIGHT_STEPS:
		lcd_mega_ClearDispaly();
		clear_lcd_data(LINE_BOTH);
		lcd_mega_StringF(PSTR("STEP-X(Enter Weight)"));
		lcd_mega_gotoxy1(5);
		lcd_mega_SendData((i+1)+'0');
		
		set_string_in_lcd_dataF(LINE2,PSTR("(In KG): "),0,0);
		lcd_Line2_pointer = 9;
		lcd_Line2_max = lcd_Line2_pointer+6;
		lcd_Line2_min = lcd_Line2_pointer;
		lcd_CursorLine = LINE2;
		//< set lcd disply
		lcd_mega_gotoxy2(0);
		lcd_mega_String(lcd_Line2_data);
		//< set cursor position
		lcd_mega_gotoxy2(lcd_Line2_pointer);
		
		//< take user input
		while(1)
		{
			//< get key pressed
			while(1)
			{
				if(ps2_kb_available_key())
				{
					ps2_kb_getkey(&ps2_key);
					break;
				}
				ADC_getReading();
			}
			//< numeric key processing
			if(ps2_key.type==NORMAL_KEY && ps2_key.val>='0' && ps2_key.val<='9')
			{
				if(lcd_Line2_pointer<lcd_Line2_max)
				{
					lcd_Line2_data[lcd_Line2_pointer]=ps2_key.val;
					lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
					lcd_Line2_pointer++;
					lcd_mega_gotoxy2(lcd_Line2_pointer);
				}
			}
			//< special key processing
			else if(ps2_key.type==SPECIAL_KEY)
			{
				//< backspace checking
				if(ps2_key.val==SPECIAL_KEY_BACKSPACE)
				{
					if(lcd_Line2_pointer>lcd_Line2_min)
					{
						lcd_Line2_pointer--;
						lcd_mega_gotoxy2(lcd_Line2_pointer);
						lcd_Line2_data[lcd_Line2_pointer]=' ';
						lcd_mega_SendData(lcd_Line2_data[lcd_Line2_pointer]);
						lcd_mega_gotoxy2(lcd_Line2_pointer);
					}
				}
				//< enter key processing
				else if(ps2_key.val==SPECIAL_KEY_ENTER)
				{
					//< store user input temperory
					//< if user has entered without typing anything
					if(lcd_Line2_pointer==lcd_Line2_min)
					{
						lcd_mega_ClearDispaly();
						lcd_mega_StringF(PSTR("Invalid Entry"));
						goto SET_WEIGHT_STEPS;
					}
					break;
				}
				//< ESC key processing
				else if(ps2_key.val==SPECIAL_KEY_ESC)
				{
					return;//< unsuccessful return
				}
			}
		}
		lcd_Line2_data[lcd_Line2_pointer] = '\0';
		steps_weight[i] = (unsigned long int)(atol(&lcd_Line2_data[lcd_Line2_min]));
		steps_ADC_count[i] = ADC_count;
		steps_slope[i] = (double)(((double)steps_weight[i])/((double)(steps_ADC_count[i]-weight_zero_ADC_count)));
		steps_distace[i] = sqrt(square((double)(steps_ADC_count[i]-weight_zero_ADC_count)) + square((double)(steps_weight[i])));
	}
	
	//< set LCD Display
	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Configuring..."));
	
	//< calculate final slope
	for(i=0;i<max_steps;i++)
	{
		slope_denom	+= (steps_distace[i]);					//< denominator of final slope formula
		slope_nume	+= (steps_slope[i] * steps_distace[i]);	//< numerator of final slope formula, slope used here temperory
	}
	slope = (slope_nume / slope_denom);
	
	///////////////////////////////
	//< WEIGHT_CONSTANTS Setting //
	///////////////////////////////
	//< unit_weight_value
// 	if(capacity>50000UL)
// 	{
// 		weight_const.unit_weight_value = 10;
// 	}
// 	else
// 	{
// 		weight_const.unit_weight_value = 5;
// 	}
	weight_const.unit_weight_value = division;
	
	//< unit_weight_count
	unit_weight_count1 = (signed long int)(((((double)(weight_const.unit_weight_value)) * 1.0)/slope) + (double)weight_zero_ADC_count);
	unit_weight_count2 = (signed long int)(((((double)(weight_const.unit_weight_value)) * 2.0)/slope) + (double)weight_zero_ADC_count);
	weight_const.unit_weight_count = (unsigned long int)(unit_weight_count2 - unit_weight_count1);
	
	//< ADC_min_count
	weight_const.ADC_min_count = weight_zero_ADC_count;
	
	//< max_weight_value
	weight_const.max_weight_value = capacity;
	
	//< ADC_max_count
	weight_const.ADC_max_count = (unsigned long int)((((double)capacity)/slope) + (double)weight_zero_ADC_count);
	
	//< store all constant into EEPROM
	WEIGHT_CONSTANTS_Write();
	
	_delay_ms(1000);
}

void setting_AutoZero()
{
	unsigned char i;
	
	lcd_mega_ClearDispaly();
	//lcd_mega_StringF(PSTR("-Auto Zero-"));
	
	ADS1230_Calibrate();
	for(i=0;i<=10;i++)
	{
		//< getting ADC_count from ADS1230
		ADC_getReading();
	}
	
	weight_const.ADC_min_count = ADC_count;
	//WEIGHT_CONSTANTS_Write();
}

void setting_view_ADC_count()
{
	char str[10];
	unsigned char len,i=0;

	lcd_mega_ClearDispaly();
	lcd_mega_StringF(PSTR("Count: "));
	lcd_mega_gotoxy2(0);
	lcd_mega_StringF(PSTR("Press ENT to Exit"));
	
	while(1)
	{
		if(ps2_kb_available_key())
		{
			ps2_kb_getkey(&ps2_key);
			if(ps2_key.type==SPECIAL_KEY && ps2_key.val==SPECIAL_KEY_ENTER)
			{
				return;
			}
		}
		
		//< getting ADC_count from ADS1230
		ADC_getReading();
		ltoa(ADC_count,str,10);
		len = strlen(str);
		lcd_mega_gotoxy1(7);
		lcd_mega_StringF(PSTR("           "));
		lcd_mega_gotoxy1(7);
		lcd_mega_String(str);

/*		
		if(len>6)
		{
			len=6;
		}
		for(i=0;i<6-len;i++)
		{
			str[i]=DIGIT_OFF;
		}
		for(i=6-len;i<6;i++)
		{
			if(str[i-(6-len)]=='-')
			{
				str[i] = DIGIT_MINUS;
			}
			else
			{
				str[i] = str[i-(6-len)]-'0';
			}
		}
		
		//< displaying count on LCD
		lcd_mega_gotoxy1(7);
		lcd_mega_SendData(LCD_CODE[str[0]]);//< MSB
		lcd_mega_SendData(LCD_CODE[str[1]]);
		lcd_mega_SendData(LCD_CODE[str[2]]);
		lcd_mega_SendData(LCD_CODE[str[3]]);
		lcd_mega_SendData(LCD_CODE[str[4]]);
		lcd_mega_SendData(LCD_CODE[str[5]]);//< LSB
			

		//< displaying count on internal 7-segment display
		shift_register_WriteByte(SEG_CODE[str[5]]);//< LSB
		shift_register_WriteByte(SEG_CODE[str[4]]);
		shift_register_WriteByte(SEG_CODE[str[3]]);
		shift_register_WriteByte(SEG_CODE[str[2]]);
		shift_register_WriteByte(SEG_CODE[str[1]]);
		shift_register_WriteByte(SEG_CODE[str[0]]);//< MSB
		shift_register_LatchPulse();
*/
	}		
}